#include <tet_cc.hpp>

#include <boost/program_options.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/device/file.hpp>

#include <boost/thread/thread.hpp>

using namespace std;
using namespace tet;

bool cmp_verts(int v1,int v2, const fn_list_t &fns)
{
  fn_t f1 = fns[v1],f2 = fns[v2];

  if(f1 != f2)
    return f1 < f2;

  return v1 < v2;
}

int get_max_vert(cellid_t c ,const tet_cc_t &tcc,const fn_list_t &fns)
{
  cellid_t verts[DMAX];

  int ct = tcc.get_vrts(c,verts);

  int mvert = verts[0];

  for(int i = 1 ; i < ct; ++i)
    if(cmp_verts(mvert,verts[i],fns))
      mvert = verts[i];

  return mvert;
}

bool cmp_vert_fn(int v1,fn_t f, const fn_list_t &fns)
{
  return fns[v1]<f;
}

void extract_dialated_sublevelset
(const tet_cc_t &tcc,const fn_list_t &fns, const vert_list_t &verts,
 fn_t cutoff,fn_t dia ,ostream & os)
{
  bool_list_t is_sel(tcc.num_dcells(0),0);

  br::transform(tcc.cel_rng(0),is_sel.begin(),
                bind(cmp_vert_fn,_1,cutoff,boost::cref(fns)));

  int_list_t diaverts;

  BOOST_FOREACH(cellid_t c, tcc.cel_rng(0))
  {
    cellid_t link[DMAX];

    if(!is_sel[c])
    {
      int lct = tcc.get_link(c,link);

      for(int i = 0 ; i < lct; ++i)
        if(tcc.get_dim(link[i]) == 0 && is_sel[link[i]] &&
            (verts[c] - verts[link[i]]).norm2() < dia )
          diaverts.push_back(link[i]);
    }
  }

  BOOST_FOREACH(cellid_t c, diaverts)
  {
    is_sel[c] = true;
  }

  BOOST_FOREACH(cellid_t c, tcc.cel_rng(0))
  {
    if(!is_sel[c])
      continue;

    cellid_t star[DMAX];
    cellid_t cverts[DMAX];

    int sct = tcc.get_star(c,star);

    for(int i = 0; i < sct; ++i)
    {
      int vct = tcc.get_vrts(star[i],cverts);

      bool omit = false;

      for(int j = 0; j < vct; ++j)
      {
        if(!is_sel[cverts[j]])
        {
          omit = true;
          break;
        }
      }

      if(omit)
        continue;

      for(int j = 0 ; j < vct; ++j)
      {
        os<<cverts[j]<<" ";
      }
      os<<endl;
    }
  }
}

void extract_dialated_suplevelset
(const tet_cc_t &tcc,fn_list_t &fns, const vert_list_t &verts,
 fn_t cutoff,fn_t dia ,ostream & os)
{
  br::transform(fns,fns.begin(),std::negate<fn_t>());

  extract_dialated_sublevelset(tcc,fns,verts,-cutoff,dia,os);

  br::transform(fns,fns.begin(),std::negate<fn_t>());
}

int main(int ac,char **av)
{
  namespace bpo = boost::program_options;
  namespace io  = boost::iostreams;

  string tsfile;
  int compno;
  vector<double> cutoff_values;
  double dil;
  string opfile;

  bpo::options_description desc("Allowed options");
  desc.add_options()
  ("help,h", "produce help message")
  ("ts-file,f",bpo::value(&tsfile)->required(),"ts file")
  ("vert-comp,c",bpo::value(&compno)->default_value(3),"vert comp to read")
  ("values,v",bpo::value< std::vector<double> >(&cutoff_values)->multitoken(),
   "list of cutoff values for sup/sub level set")
  ("dialation,d",bpo::value(&dil)->default_value(0.0),
   "radius to dialate the sub/sup level set")
  ("output-basename,o",bpo::value(&opfile)->required(),"")
  ;

  int par_style = bpo::command_line_style::unix_style ^
      bpo::command_line_style::allow_short;

  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(ac, av, desc,par_style), vm);

  if (vm.count("help"))
  {
    cout << desc << endl;
    return 0;
  }
  try
  {
    bpo::notify(vm);
  }
  catch(bpo::required_option e)
  {
    cout<<e.what()<<endl;
    cout<<desc<<endl;
    return 1;
  }

  tet_list_t tets;
  fn_list_t  fns;
  vert_list_t verts;

  cout<<"Reading Tets.."<<endl;

  read_ts(tsfile,&tets,compno,&fns,&verts,NULL,NULL);

  cout<<"Setting up triangulation.."<<endl;

  tet_cc_t tcc(tets);

  cout<<"====================================="<<endl;
  cout<<"Extracting dialted Sup/Sub level sets"<<endl;
  cout<<"Dialation = "<<dil<<endl;

  const int MAX_THREADS = 8;

  for(int i = 0 ; i < cutoff_values.size(); i += MAX_THREADS)
  {
    boost::thread_group tg;

    io::filtering_ostream *zfs[MAX_THREADS];

    for( int j = 0,ipj = i ; j < MAX_THREADS; ++j,++ipj)
    {
      fn_t cf = cutoff_values[ipj];
      cout<<ipj+1<<" of "<<cutoff_values.size()<<"\t cut-off = "<<cf<<endl;

      string fname = boost::str
          (boost::format("%s_dil_%.3f_cf_%.3f_subcplx.txt.gz")%opfile%dil%cf);

      zfs[j] = new io::filtering_ostream;

      io::filtering_ostream &gz_fs = *zfs[j];
      gz_fs.push(io::gzip_compressor());

      io::basic_file_sink<char> ofile(fname);
      ensure(ofile.is_open(),"unable to open output file");
      gz_fs.push(ofile);

      tg.create_thread(bind(extract_dialated_suplevelset,boost::cref(tcc),
        boost::ref(fns),boost::cref(verts),cf,dil,boost::ref(gz_fs)));

//      extract_dialated_suplevelset(tcc,fns,verts,cf,dil,gz_fs);
    }

    tg.join_all();

    for( int j = 0; j < MAX_THREADS; ++j)
    {
      io::close(*zfs[j]);
      delete zfs[j];
    }
  }

  cout<<"====================================="<<endl;
}

