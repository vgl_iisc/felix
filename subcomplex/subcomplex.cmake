find_package(Boost 1.48 COMPONENTS program_options iostreams thread REQUIRED)

include_directories(
  ${Boost_INCLUDE_DIR}

  ${CMAKE_CURRENT_SOURCE_DIR}/subcomplex/
)

# Set your files and resources here
SET(SUBCOMPLEX_SRCS
  ${CMAKE_CURRENT_SOURCE_DIR}/subcomplex/main.cpp

  ${CMAKE_CURRENT_SOURCE_DIR}/vec.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/utl.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/utl.hpp

  ${CMAKE_CURRENT_SOURCE_DIR}/tet.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/tet.hpp

  ${CMAKE_CURRENT_SOURCE_DIR}/tet_cc.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/tet_cc.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/tet_cc_inl.hpp
)

add_executable(${PROJECT_NAME}-subcomplex ${SUBCOMPLEX_SRCS})

target_link_libraries(${PROJECT_NAME}-subcomplex  ${Boost_LIBRARIES})

