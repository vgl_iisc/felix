from PythonQt import *


#if not ms_mw.get_mscomplex():
  #ms_mw.load_raw("HD-7.pos_1_subset_g64.raw",
    #64,64,64,
    #0,2.75,0,2.68,0,2.62)
  #ms_mw.set_threshold(0.01)
  #ms_mw.update_pipelines()  

if not ms_mw.get_mscomplex():
  ms_mw.load_raw("Cosmogrid_particles_g128.log.raw",
    128,128,128,
    0,127,0,127,0,127)
  #ms_mw.load_raw("Cosmogrid_particles_g256.log.raw",
    #256,256,256,
    #0,255,0,255,0,255)
    
  ms_mw.set_threshold(0.05)
  ms_mw.set_geom_clipbox(1, 126, 1, 126, 69, 105)
  ms_mw.update_pipelines()    

ms_mw.set_geom_clipbox(1, 126, 1, 126, 69, 105)  
dec = ms_mw.get_decren()
dec.set_background_color(0,0,0)
dec.set_orientation_marker_enabled(False)

#dec.load_camera("cam3.cam")
ms_mw.update_pipelines()  

vol = ms_mw.get_volren()
vol.set_enabled(True)
#vol.set_brightness(8)
vol.enable_colorbar();
ms_mw.update_pipelines()

cpren = ms_mw.get_cpren()
cpren.set_spheres_enabled(True)
cpren.set_spheres_radii(0.03)
cpren.set_enabled(False)



fils = ms_mw.get_fil_ren()
fils.set_enabled(True)
fils.set_colorbar_enabled(False)
fils.set_tf_enabled(False)
fils.set_tubes_enabled(True)
fils.set_tubes_geomprops(0.4)
fils.set_color(1,0.25,0)
fils.set_tubes_enabled(True)


#fils.set_color(1,0.25,0,
#1,0.25,0,
#1,0.25,0,
#1,0.1,0.6,2)

deselect_all_cps()
#dec.load_camera("cam9.cam")


#vol.load_tf("vol11.tf")
ms_mw.update_pipelines()
#dec.save_screenshot("cosmogrid_sgrid_clust_fils_vren.png")

ms_mw.rangeSelect2Saddles(0,9.6,2.3,9.6)
ms_mw.update_pipelines()
#dec.save_screenshot("cosmogrid_sgrid_clust_fils.png")


#deselect_all_cps()
#vol.load_tf("vol10.tf")
#ms_mw.update_pipelines()
#dec.save_screenshot("cosmogrid_sgrid_void_fils_vren.png")

#ms_mw.rangeSelect2Saddles(-2.25,0.75,0.75,3.75)
#ms_mw.update_pipelines()
#dec.save_screenshot("cosmogrid_sgrid_void_fils.png")




#dec.save_screenshot("imgs/cosmo_grid_cluster_fils.png")

## Filaments in void like regions
#deselect_all_cps()
#vol.load_tf("vol4.tf")
#fils.set_tubes_geomprops(0.02,6)
#ranged_select_two_saddles(-4,1,-4,1)
#ms_mw.update_pipelines()
#dec.save_screenshot("imgs/cosmo_grid_void_fils.png")

## Filaments in starting from clusters down to void regions
#deselect_all_cps()
#ranged_select_two_saddles(3,11,10,11)
#cpren.set_enabled(True)
#fils.set_smoother_params(0.000001,5000)
#ms_mw.update_pipelines()
#dec.save_screenshot("imgs/cosmo_grid_cluster_to_void.png")

# A volume rendering with particles
#deselect_all_cps()
#vol.load_tf("vol4.tf")
#fils.set_particles_enabled(True)
#fils.set_particles_viewprops(1,1,1,1)
#ms_mw.update_pipelines()
#dec.save_screenshot("imgs/cosmo_grid_particles.png")




