#include <pymstet.hpp>


BOOST_PYTHON_MODULE(pymsc)
{
  pymsc::init();
}

//int main(int argc, char **argv)
//{
//    // This line makes our module available to the embedded Python intepreter.
//# if PY_VERSION_HEX >= 0x03000000
//    PyImport_AppendInittab("pymsc", &PyInit_pymsc);
//# else
//    PyImport_AppendInittab("pymsc", &initpymsc);
//# endif
//    // Initialize the Python runtime.
//    Py_Initialize();

//    ENSURES(argc == 2) << "Usage : " << argv[0] << " <filename.py>" << endl;

//    std::ifstream t(argv[1]);

//    ENSURES(t.is_open()) << "Unable to open File="<<argv[1] << endl;

//    std::string s((std::istreambuf_iterator<char>(t)),
//                  std::istreambuf_iterator<char>());

//    PyRun_SimpleString( s.c_str()   );
//    Py_Finalize();

//    return 0;
//}
