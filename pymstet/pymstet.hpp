/*=========================================================================

  Program:   pymsc

  Copyright (c) Nithin Shivashankar, Vijay Natarajan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=========================================================================*/

#include <tet.hpp>
#include <boost/python.hpp>

namespace pymsc {

/// \brief mscomplex python type object
extern boost::python::object g_mscomplex_py_pytype;

/// \brief tet_cc python type object
extern boost::python::object g_tcc_py_pytype;

/// \brief Convert a given python object to a c++ object
template <typename T> boost::shared_ptr<T> to_cpp(boost::python::object);

/// \brief initializes the python types etc
void init();
}
