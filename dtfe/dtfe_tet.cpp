#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Periodic_3_triangulation_filtered_traits_3.h>
#include <CGAL/Periodic_3_triangulation_traits_3.h>
#include <CGAL/Periodic_3_Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>

#include <fstream>
#include <algorithm>
#include <string>
#include <cmath>

using namespace std;

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Periodic_3_triangulation_traits_3<K> GT;

typedef CGAL::Periodic_3_triangulation_ds_vertex_base_3<> VbDS;
typedef CGAL::Triangulation_vertex_base_3<GT,VbDS> Vb;

typedef CGAL::Periodic_3_triangulation_ds_cell_base_3<> CbDS;
typedef CGAL::Triangulation_cell_base_3<GT,CbDS> Cb;

struct Vertex_info {
  double value;
  long index;
};

typedef CGAL::Triangulation_vertex_base_with_info_3<Vertex_info, GT, Vb> VbInfo;
typedef CGAL::Triangulation_data_structure_3<VbInfo, Cb>    TDS;
typedef CGAL::Periodic_3_Delaunay_triangulation_3<GT, TDS>  PDT;
typedef PDT::Iso_cuboid Iso_cuboid;

typedef PDT::Point   Point;


template <typename T1>
inline void lineread_val(std::istream &istr,T1 & t1)
{
  std::string s;
  std::getline(istr,s);  
  std::stringstream ss(s);    
  ss >> t1;
}

template <typename T1,typename T2,typename T3,typename T4,typename T5,typename T6>
inline void lineread_vals(std::istream &istr,T1 & t1,T2 & t2,T3 & t3,T4 & t4,T5 & t5,T6 & t6)
{
  std::string s;
  std::getline(istr,s);  
  std::stringstream ss(s);    
  ss >> t1 >> t2 >> t3 >> t4 >> t5 >> t6;
}

template <typename T1,typename T2,typename T3>
inline void lineread_vals(std::istream &istr,T1 & t1,T2 & t2,T3 & t3)
{
  std::string s;
  std::getline(istr,s);  
  std::stringstream ss(s);    
  ss >> t1 >> t2 >> t3 ;
}

template<typename OutputIterator>
OutputIterator read_points(char* filename, OutputIterator out, Iso_cuboid& iso_cuboid) 
{
  std::ifstream ifstr(filename);
  
  if ( ! ifstr.is_open())
    throw std::runtime_error("Unable to open file!!!");
  
  long no_points;
  
  lineread_val(ifstr,no_points);  
  
  int trash;
  trash = 0;
  double x1,x2,y1,y2,z1,z2;
  double a,b,c;
  
  lineread_vals(ifstr,x1,y1,z1,x2,y2,z2);

  iso_cuboid = Iso_cuboid(x1,y1,z1,x2,y2,z2);
  
  for(long i=0;i<no_points;i++) 
  {
    lineread_vals(ifstr,a,b,c);
    if(a>=x1 && a<=x2 & b>=y1 && b<=y2 && c>=z1 && c<=z2)
	*out++ = Point(a,b,c);
    else
	trash++;
  }
  std::cerr<<"# points: "<<no_points<<"\n";
  std::cerr<<"# points discarded: "<<trash;
  
  if(trash > 0 )
    throw std::runtime_error("Some points were discarded");
  
  return out;
}


void volume_of_Delaunay_stars(PDT& dt) {

  int nv = 0;

  for(PDT::Vertex_iterator it = dt.vertices_begin();
      it!=dt.vertices_end();
      it++) {
    //std::cout << it->point() << " " << it->offset() << std::endl;
    std::vector<PDT::Cell_handle> incident_cells;
    dt.incident_cells(it,std::back_inserter(incident_cells));
    double volume = 0.;
    for(std::vector<PDT::Cell_handle>::const_iterator cit=incident_cells.begin();
	cit!=incident_cells.end();cit++) {
      PDT::Periodic_tetrahedron tet = dt.periodic_tetrahedron(*cit);
      /*
      std::cout << "Periodic tetra: " << std::endl;
      std::cout << "1st: " << dt.point(tet[0]) << " >>> " << tet[0].first << " --- " << tet[0].second << std::endl;
      std::cout << "2nd: " << dt.point(tet[1]) << " >>> "<< tet[1].first << " --- " << tet[1].second << std::endl;
      std::cout << "3rd: " << dt.point(tet[2]) << " >>> "<< tet[2].first << " --- " << tet[2].second << std::endl;
      std::cout << "4th: " << dt.point(tet[3]) << " >>> "<< tet[3].first << " --- " << tet[3].second << std::endl;
      */
      double curr_volume = CGAL::to_double(CGAL::volume(dt.point(tet[0]),
							dt.point(tet[1]),
							dt.point(tet[2]),
							dt.point(tet[3])));
      //std::cout << "Volume: " << curr_volume << std::endl;
      volume+=curr_volume;
    }

    nv++;
    it->info().value=1/volume;
    //std::cout << "Total volume: " << volume << std::endl;
  }

//  std::cout << "Num vertices: " << nv << std::endl;



}

template<typename PDT>
struct Sort_vertices_by_value {

  bool operator() (typename PDT::Vertex_handle v1,
		   typename PDT::Vertex_handle v2) {
    return v1->info().value < v2->info().value;
  }

};

template<typename PDT>
struct Sort_vertices_by_index {

  bool operator() (typename PDT::Vertex_handle v1,
		   typename PDT::Vertex_handle v2) {
    return v1->info().index < v2->info().index;
  }

};

template<typename OutputIterator>
OutputIterator assign_ids(PDT& dt, OutputIterator oit) {

  std::vector<PDT::Vertex_handle> vhandles;
  for(PDT::Vertex_iterator it  = dt.vertices_begin();
      it!=dt.vertices_end();it++) {
    vhandles.push_back(it);
  }
  std::sort(vhandles.begin(),vhandles.end(),Sort_vertices_by_value<PDT>());
  for(long i=0;i<vhandles.size();i++) {
    vhandles[i]->info().index=i;
  }
  std::copy(vhandles.begin(),vhandles.end(),oit);
  return oit;

}

using namespace std;

//void save_to_ts(DT &dt,string f)
//{
//  fstream fs(f.c_str(),ios::out);

//  int nv = dt.number_of_vertices();
//  int nt = dt.number_of_finite_cells();

//  fs<<nv<<" "<<nt<<" 2"<<"\n";

//  DT::Vertex_iterator vit_b = dt.finite_vertices_begin();
//  DT::Vertex_iterator vit_e = dt.finite_vertices_end();

//  int vidx = 0;

//  dt.vertices_begin()->info().index = -1;

//  for(DT::Vertex_iterator vit = vit_b;vit !=  vit_e;++vit)
//  {
//    Point pt = vit->point();

//    fs<<pt[0]<<" "
//      <<pt[1]<<" "
//      <<pt[2]<<" "
//      <<vit->info().value<<" "
//      <<log(vit->info().value)<<"\n";

//    vit->info().index = vidx++;
//  }

//  DT::Cell_iterator cit_b = dt.finite_cells_begin();
//  DT::Cell_iterator cit_e = dt.finite_cells_end();

//  for(DT::Cell_iterator cit = cit_b; cit != cit_e;++cit)
//  {
//    // iterators become buggy if we mix DT and PDT code .. not sure why..
//    // Ideally We shouldn't have to check for infinite cells.

//    if (cit->vertex(0)->info().index == -1 ||
//        cit->vertex(1)->info().index == -1 ||
//        cit->vertex(2)->info().index == -1 ||
//        cit->vertex(3)->info().index == -1)
//      continue;

//    for(int j = 0; j < 4; j++)
//      fs<<int(cit->vertex(j)->info().index)<<" ";

//    fs<<"\n";
//  }

//}

void save_to_ts
(PDT &pdt, 
 const std::vector<Point> & pts,
 string f)
{

  fstream fs(f.c_str(),ios::out);

  int nv = pdt.number_of_finite_vertices();
  int nt = pdt.number_of_finite_cells();

  fs<<nv<<" "<<nt<<" 2"<<"\n";


  PDT::Vertex_iterator vit_b = pdt.finite_vertices_begin();
  PDT::Vertex_iterator vit_e = pdt.finite_vertices_end();
  
  std::map<Point,size_t> pts_oidx;
  
  for(size_t i = 0 ; i < pts.size(); ++i)
    pts_oidx[pts[i]] = i;
  
  std::vector<double> vvalue(nv);  
  
  for(PDT::Vertex_iterator vit = vit_b ;vit !=  vit_e;++vit)
  {
    if(pts_oidx.count(vit->point()) ==0)
      throw std::runtime_error("Pdt pt not found it orig list");
      
    size_t oidx = pts_oidx[vit->point()];
    
    vit->info().index = oidx;      
    vvalue[oidx]      = vit->info().value;
  }
  
  for(int i = 0 ; i < pts.size(); ++i)
  {
    Point pt = pts[i];

    fs<<pt[0]<<" "
      <<pt[1]<<" "
      <<pt[2]<<" "
      <<vvalue[i]<<" "
      <<log10(vvalue[i])<<"\n";
  }  

  PDT::Cell_iterator cit_b = pdt.finite_cells_begin();
  PDT::Cell_iterator cit_e = pdt.finite_cells_end();

  for(PDT::Cell_iterator cit = cit_b; cit != cit_e;++cit)
  {
    PDT::Periodic_tetrahedron tet = pdt.periodic_tetrahedron(cit);

    for(int j = 0; j < 4; j++)
      fs<<int(cit->vertex(j)->info().index)<<" ";

    fs<<"\n";
  }

  fs<<"PERIODIC TETRAHEDRA INFO BEGIN"<<endl;

  fs<<"DOMAIN: "
    <<pdt.domain().xmin()<<" "
    <<pdt.domain().xmax()<<" "
    <<pdt.domain().ymin()<<" "
    <<pdt.domain().ymax()<<" "
    <<pdt.domain().zmin()<<" "
    <<pdt.domain().zmax()<<" "<<endl;

  fs<<"OFFSETS BEGIN"<<endl;

  for(PDT::Cell_iterator cit = cit_b; cit != cit_e;++cit)
  {
    PDT::Periodic_tetrahedron tet = pdt.periodic_tetrahedron(cit);

    for(int j = 0; j < 4; j++)
    {
      PDT::Offset off = tet[j].second;
      assert(off[0] == 0 || off[0] == 1);
      assert(off[1] == 0 || off[1] == 1);
      assert(off[2] == 0 || off[2] == 1);

      fs<<int(off[0] + 2*off[1] + 4* off[2])<<" ";
    }

    fs<<"\n";
  }
  fs<<"OFFSETS END"<<endl;

  fs<<"PERIODIC TETRAHEDRA INFO END"<<endl;


//  fstream fs(f.c_str(),ios::out);

//  DT dt;

//  for(PDT::Vertex_iterator vit = pdt.finite_vertices_begin();
//      vit != pdt.finite_vertices_end();++vit)
//  {
//    dt.insert(vit->point())->info().value = vit->info().value;
//  }
}

int main(int argc, char** argv)
{
  if(argc<2) {
    std::cerr << "Arguments!" << std::endl;
  }
  std::vector<Point> points;

  Iso_cuboid iso_cuboid;

  std::cerr << "Reading the input.." << std::flush;

  read_points(argv[1],std::back_inserter(points),iso_cuboid);

  std::cerr << "done\nComputing periodic triangulation.." << std::flush;

  PDT dt(iso_cuboid);

  dt.insert(points.begin(),points.end(), true);

  //PDT dt(points.begin(),points.end(),iso_cuboid);

  std::cerr << "done\nConverting to one sheeted representation.." << std::flush;

  if(!dt.is_triangulation_in_1_sheet()) {
    std::cerr << "WARNING, conversion to one sheet destroys triangulation property" << std::endl;
  }

  dt.convert_to_1_sheeted_covering();

  std::cerr << "done\nCompute the volume of stars.." << std::flush;

  volume_of_Delaunay_stars(dt);

  if(argc >=3)
  {
    std::cerr << "done\nSave to ts.." << std::flush;

//    dt.convert_to_27_sheeted_covering();

    save_to_ts(dt,points,argv[2]);

    std::cerr << "!\n" << std::flush;

    return 0;
  }

  std::cerr << "done\nSort the vertices.." << std::flush;

  std::vector<PDT::Vertex_handle> vhandles_sorted;

  assign_ids(dt,std::back_inserter(vhandles_sorted));

  std::cerr << "done\nCreate the output" << std::endl;

  for(std::vector<PDT::Vertex_handle>::iterator it = vhandles_sorted.begin();
      it!=vhandles_sorted.end();
      it++) {
    PDT::Vertex_handle v = *it;
    std::cout << "v " << v->info().value << std::endl;
    std::cout << "s " << v->info().index << std::endl;
    std::vector<PDT::Edge> edges;
    dt.incident_edges(v,std::back_inserter(edges));
    for(std::vector<PDT::Edge>::iterator eit=edges.begin();
	eit!=edges.end();eit++) {
      PDT::Edge& e = *eit;
      PDT::Cell_handle c = e.first;
      PDT::Vertex_handle v1 = c->vertex(e.second);
      PDT::Vertex_handle v2 = c->vertex(e.third);
      /*
      std::cout << v1->info().index
		<< v2->info().index
		<< std::endl;
      */
      if(v1->info().index > v2->info().index) {
	std::swap(v1,v2);
      }
      assert(v1->info().index <= v2->info().index);
      if(v2->info().index==v->info().index) {
	std::cout << "s " << v1->info().index << " " << v2->info().index
		  << std::endl;
      }
    }
    std::vector<PDT::Facet> facets;
    dt.incident_facets(v,std::back_inserter(facets));
    for(std::vector<PDT::Facet>::iterator fit=facets.begin();
	fit!=facets.end();fit++) {
      PDT::Facet f = *fit;
      PDT::Cell_handle& c = f.first;
      int f_id = f.second;
      std::vector<PDT::Vertex_handle> verts;
      verts.push_back(c->vertex((f_id+1)%4));
      verts.push_back(c->vertex((f_id+2)%4));
      verts.push_back(c->vertex((f_id+3)%4));
      /*
      std::cout << c->vertex((f_id+1)%4)->info().index
		<< c->vertex((f_id+2)%4)->info().index
		<< c->vertex((f_id+3)%4)->info().index << std::endl;
      */
      std::sort(verts.begin(),verts.end(),Sort_vertices_by_index<PDT>());

      if(verts[2]->info().index==v->info().index) {
	std::cout << "s "
		  << verts[0]->info().index << " "
		  << verts[1]->info().index << " "
		  << verts[2]->info().index << std::endl;
      }
    }

    std::vector<PDT::Cell_handle> cells;
    dt.incident_cells(v,std::back_inserter(cells));
    for(std::vector<PDT::Cell_handle>::iterator cit=cells.begin();
	cit!=cells.end();cit++) {
      PDT::Cell_handle c = *cit;
      std::vector<PDT::Vertex_handle> verts;
      verts.push_back(c->vertex(0));
      verts.push_back(c->vertex(1));
      verts.push_back(c->vertex(2));
      verts.push_back(c->vertex(3));
      /*
      std::cout << c->vertex((f_id+1)%4)->info().index
		<< c->vertex((f_id+2)%4)->info().index
		<< c->vertex((f_id+3)%4)->info().index << std::endl;
      */
      std::sort(verts.begin(),verts.end(),Sort_vertices_by_index<PDT>());

      if(verts[3]->info().index==v->info().index) {
	std::cout << "s "
		  << verts[0]->info().index << " "
		  << verts[1]->info().index << " "
		  << verts[2]->info().index << " "
		  << verts[3]->info().index << std::endl;
      }
    }
  }
  std::cout << "!" << std::endl;

  return 0;
}
