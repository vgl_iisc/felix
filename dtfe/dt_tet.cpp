#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <boost/iterator/transform_iterator.hpp>
#include <vector>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_3<unsigned, K> Vb;
typedef CGAL::Triangulation_data_structure_3<Vb> Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds> DT;
typedef DT::Point Point;


#include <fstream>
#include <algorithm>
#include <string>
#include <cmath>

using namespace std;


template <typename T1>
inline void lineread_val(std::istream &istr,T1 & t1)
{
  std::string s;
  std::getline(istr,s);  
  std::stringstream ss(s);    
  ss >> t1;
}

template <typename T1,typename T2,typename T3,typename T4,typename T5,typename T6>
inline void lineread_vals(std::istream &istr,T1 & t1,T2 & t2,T3 & t3,T4 & t4,T5 & t5,T6 & t6)
{
  std::string s;
  std::getline(istr,s);  
  std::stringstream ss(s);    
  ss >> t1 >> t2 >> t3 >> t4 >> t5 >> t6;
}

template <typename T1,typename T2,typename T3>
inline void lineread_vals(std::istream &istr,T1 & t1,T2 & t2,T3 & t3)
{
  std::string s;
  std::getline(istr,s);  
  std::stringstream ss(s);    
  ss >> t1 >> t2 >> t3 ;
}

template<typename OutputIterator>
OutputIterator read_points(char* filename, OutputIterator out/*, Iso_cuboid& iso_cuboid*/)
{
  std::ifstream ifstr(filename);
  
  if ( ! ifstr.is_open())
    throw std::runtime_error("Unable to open file!!!");
  
  long no_points;
  
  lineread_val(ifstr,no_points);  
  
  int trash;
  trash = 0;
  double x1,x2,y1,y2,z1,z2;
  double a,b,c;
  
  lineread_vals(ifstr,x1,y1,z1,x2,y2,z2);

//  iso_cuboid = Iso_cuboid(x1,y1,z1,x2,y2,z2);
  
  for(long i=0;i<no_points;i++) 
  {
    lineread_vals(ifstr,a,b,c);
    if(a>=x1 && a<=x2 & b>=y1 && b<=y2 && c>=z1 && c<=z2)
	*out++ = Point(a,b,c);
    else
	trash++;
  }
  std::cerr<<"# points: "<<no_points<<"\n";
  std::cerr<<"# points discarded: "<<trash;
  
  if(trash > 0 )
    throw std::runtime_error("Some points were discarded");
  
  return out;
}

using namespace std;

void save_to_ts
(DT &pdt,
 const std::vector<Point> & pts,
 string f)
{
  fstream fs(f.c_str(),ios::out);

  int nv = pdt.number_of_vertices();
  int nt = pdt.number_of_finite_cells();

  fs<<nv<<" "<<nt<<" 0"<<"\n";
  
  for(int i = 0 ; i < pts.size(); ++i)
  {
    Point pt = pts[i];

    fs<<pt[0]<<" "
      <<pt[1]<<" "
      <<pt[2]<<"\n";
  }  

  DT::Finite_cells_iterator cit_b = pdt.finite_cells_begin();
  DT::Finite_cells_iterator cit_e = pdt.finite_cells_end();

  for(DT::Finite_cells_iterator cit = cit_b; cit != cit_e;++cit)
  {
    for(int j = 0; j < 4; j++)
      fs<<int(cit->vertex(j)->info())<<" ";

    fs<<"\n";
  }

}


//a functor that returns a std::pair<Point,unsigned>.
//the unsigned integer is incremented at each call to
//operator()
struct Auto_count : public std::unary_function<const Point&,std::pair<Point,unsigned> >{
mutable unsigned i;
Auto_count() : i(0){}
std::pair<Point,unsigned> operator()(const Point& p) const {
return std::make_pair(p,i++);
}
};

int main(int argc, char** argv)
{
  if(argc != 3)
  {
    std::cerr << "Usage ::  " <<argv[0] << "<input pts file> <output ts file>" << std::endl;
    return -1;
  }
  std::vector<Point> points;

  std::cerr << "Reading the input.." << std::flush;

  read_points(argv[1],std::back_inserter(points));

  std::cerr << "done\nComputing Delaunay triangulation.." << std::flush;

  DT dt( boost::make_transform_iterator(points.begin(),Auto_count()),
         boost::make_transform_iterator(points.end(), Auto_count() ) );

  std::cerr << "done\nSave to ts.." << std::flush;

  save_to_ts(dt,points,argv[2]);

  std::cerr << "!\n" << std::flush;

  return 0;
}
