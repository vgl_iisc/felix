#include <config.h>
#ifdef PYTHONQT_ENABLED
// python.h insists on being included first
// else you get distracting compiler warnings
#include <PythonQt.h>
#include <PythonQt_QtAll.h>
#endif

#include <vtkRenderView.h>
#include <vtkRenderer.h>
#include <vtkBox.h>
#include <vtkRenderWindow.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataAlgorithm.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkUnstructuredGrid.h>

#include <QFileDialog>
#include <QDir>
#include <QTime>

#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <tet_mscomplex.hpp>

#include <ui_mainwindow.h>
#include <mainwindow.h>
#include <mainwindow_charts.hpp>
#include <MsComplexDataManager.hpp>
#include <rens/VolumeRenderer.hpp>
#include <rens/SeparatrixLineRenderer.hpp>
#include <rens/SeparatrixSheetRenderer.hpp>
#include <rens/CriticalPointRenderer.hpp>
#include <rens/DecorationsRenderer.hpp>


#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

extern const char *viewer_pyfuncs;

using namespace tet;


/*********************************************************************/

MainWindow::MainWindow()
{
  ui = new Ui_MainWindow;
  ui->setupUi(this);

  statusBar()->setFixedHeight(statusBar()->height());

#ifdef PYTHONQT_ENABLED
  // Set up the script interface
  PythonQt::init(PythonQt::RedirectStdOut);
  PythonQt_QtAll::init();

  // Load pygments from resources
  PythonQt::self()->installDefaultImporter();
  PyObject *path = PySys_GetObject("path");
  PyList_Append(path, PyUnicode_FromString(":/lib"));

  // Setup the context
  m_pqt = PythonQt::self()->getMainModule();
  m_pqt.addObject("ms_mw", this);

  // setup the console widget
  m_pqt_cons = new NicePyConsole(this,m_pqt);
  m_pqt_cons->setToolTip(tr("A scripting interface using PythonQt.\n"\
                            "Key in ms_mw. to see options"));


  ui->dw_python_console->setWidget(m_pqt_cons);
  ui->dw_python_console->setEnabled(true);

//  PythonQt::self()->registerClass(get_mscomplex_qtwrapper_QMetaObject());
//  PythonQt::self()->registerClass(get_tet_cc_qtwrapper_QMetaObject());
  PythonQt::self()->registerClass(&PersistenceCharts::staticMetaObject);
  PythonQt::self()->registerClass(&VolumeRenderer::staticMetaObject);
  PythonQt::self()->registerClass(&CriticalPointRenderer::staticMetaObject);
  PythonQt::self()->registerClass(&SeparatrixLineRenderer::staticMetaObject);
  PythonQt::self()->registerClass(&DecorationsRenderer::staticMetaObject);
  PythonQt::self()->registerClass(&SeparatrixSheetRenderer::staticMetaObject);

#ifdef VTK_VIEWER_PYTHONQT_PYTHONPATH

  QStringList sl = QString(VTK_VIEWER_PYTHONQT_PYTHONPATH).
                                        split(QRegExp("\\:|\\;"));

  BOOST_FOREACH(QString s,sl)  PythonQt::self()->addSysPath(s);
#endif
  m_pqt.evalScript(viewer_pyfuncs);
#endif

  m_msc_dmgr.reset(new MsComplexDataManager);
  m_msc_dmgr->create();

  connect(m_msc_dmgr.get(),SIGNAL(statusMessageEvent(QString)),
          this,SLOT(log_status_message(QString)));

  ui->geom_qvtkWidget->GetRenderWindow()->AddRenderer(m_msc_dmgr->ren());

//  m_pers_hist_charts = new PersistenceHistogramCharts(this);
//  ui->charts_tabWidget->addTab(m_pers_hist_charts,"Pers Hist");

//  m_pers_betti_plots = new PersistentBettiNumbersPlot(this);
//  ui->charts_tabWidget->addTab(m_pers_betti_plots,"Betti No.s");

//  m_cp_hist_charts   = new CriticalPointsHistograms(this);
//  ui->charts_tabWidget->addTab(m_cp_hist_charts,"Cp. Hist.");

  m_pers_charts  = new PersistenceCharts(this);
  m_2sad_asc_ren = new SeparatrixLineRenderer();
  m_1sad_des_ren = new SeparatrixLineRenderer();
  m_2sad_des_ren = new SeparatrixSheetRenderer();
  m_1sad_asc_ren = new SeparatrixSheetRenderer();
  m_cp_ren       = new CriticalPointRenderer();
  m_dec_ren      = new DecorationsRenderer();
  m_volren       = new VolumeRenderer(this);
  m_2sad_asc_sel = new SeparatrixLineSelector(this);

  ui->dw_volren->setWidget(m_volren);
  ui->dw_2sad_sel->setWidget(m_2sad_asc_sel);
  ui->dw_pers_charts->setWidget(m_pers_charts);

  ui->dw_pers_charts->setVisible(false);

  connect(m_2sad_asc_ren,SIGNAL(setEnabledEvent(bool)),this,
          SLOT(on_show_2saddle_asc_checkBox_clicked(bool)));
  connect(m_1sad_asc_ren,SIGNAL(setEnabledEvent(bool)),this,
          SLOT(on_show_1saddle_asc_checkBox_clicked(bool)));
  connect(m_2sad_des_ren,SIGNAL(setEnabledEvent(bool)),this,
          SLOT(on_show_2saddle_des_checkBox_clicked(bool)));
  connect(m_1sad_des_ren,SIGNAL(setEnabledEvent(bool)),this,
          SLOT(on_show_1saddle_des_checkBox_clicked(bool)));

  connect(m_2sad_asc_ren,SIGNAL(statusMessageEvent(QString)),
          this,SLOT(log_status_message(QString)));
  connect(m_1sad_asc_ren,SIGNAL(statusMessageEvent(QString)),
          this,SLOT(log_status_message(QString)));
  connect(m_2sad_des_ren,SIGNAL(statusMessageEvent(QString)),
          this,SLOT(log_status_message(QString)));
  connect(m_1sad_des_ren,SIGNAL(statusMessageEvent(QString)),
          this,SLOT(log_status_message(QString)));
  connect(m_volren,SIGNAL(showStatusMessage(QString)),
          this,SLOT(log_status_message(QString)));

  connect(m_2sad_asc_sel,SIGNAL(rangeSelected2Saddles(double,double,double,double)),
          this,SLOT(rangeSelect2Saddles(double,double,double,double)));
  connect(m_2sad_asc_sel,SIGNAL(clearedCpSelection()),
          this,SLOT(deselectAllCps()));

  m_cp_ren->create(m_msc_dmgr);
  m_dec_ren->create(m_msc_dmgr);
  m_2sad_asc_ren->create(m_msc_dmgr);
  m_1sad_asc_ren->create(m_msc_dmgr);
  m_2sad_des_ren->create(m_msc_dmgr);
  m_1sad_des_ren->create(m_msc_dmgr);

  m_2sad_asc_sel->create(m_msc_dmgr);

  m_dec_ren->update();
  m_msc_dmgr->ren()->ResetCamera();
  ui->geom_qvtkWidget->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

PyObject * MainWindow::get_mscomplex(){return m_msc_dmgr->msc_py();}

/*---------------------------------------------------------------------------*/

//PyObject * MainWindow::get_cellcomplex() {return m_msc_dmgr->tcc_py();}

/*---------------------------------------------------------------------------*/

void MainWindow::load_ts(QString ts_file)
{
  setWindowTitle(ts_file);
  m_msc_dmgr->load_ts(ts_file.toStdString(),4,tet::ASC);
  finish_load();
}

/*---------------------------------------------------------------------------*/

void MainWindow::load_raw
(QString rf, int X, int Y, int Z, double xmin, double xmax,
 double ymin, double ymax, double zmin, double zmax)
{
  setWindowTitle(rf);
  m_msc_dmgr->load_raw(rf.toStdString(),X,Y,Z,
                       utl::mk_vec(xmin,ymin,zmin),
                       utl::mk_vec(xmax,ymax,zmax),
                       tet::ASC);
  finish_load();
}

/*---------------------------------------------------------------------------*/

void MainWindow::finish_load()
{
  using namespace std;

  set_threshold(ui->threshold_doubleSpinBox->value());

  m_pers_charts->create(m_msc_dmgr->msc());
//  m_pers_hist_charts->create(m_msc);
//  m_pers_betti_plots->create(m_msc);
//  m_cp_hist_charts->create(m_msc);

  m_volren->create(m_msc_dmgr);
  m_volren->set_enhancement_points(m_2sad_asc_ren->m_polydata);


  double bnds[6];
  m_msc_dmgr->ccg()->get_bounds(bnds);

  m_msc_dmgr->clip_box()->SetBounds(bnds);
  m_dec_ren->set_clipbox(bnds);
  ui->dslider_roi_x->setRange(bnds[0],bnds[1]);
  ui->dslider_roi_y->setRange(bnds[2],bnds[3]);
  ui->dslider_roi_z->setRange(bnds[4],bnds[5]);
  ui->dslider_roi_x->setPositions(bnds[0],bnds[1]);
  ui->dslider_roi_y->setPositions(bnds[2],bnds[3]);
  ui->dslider_roi_z->setPositions(bnds[4],bnds[5]);


  // update
  update_pipelines();

  //one time update after initial update
  m_msc_dmgr->ren()->ResetCamera();
  m_msc_dmgr->ren()->Render();

  log_status_message(tr("Loading Done"));
}

/*-------------------------------------------------------------------*/

void MainWindow::update_pipelines()
{
  log_status_message(tr("Updating"));

  // if clipbox UI was changed
  if(m_msc_dmgr->clip_box()->GetMTime() < m_dec_ren->get_clipbox_mtime())
  {
    double bounds[6];
    m_dec_ren->get_clipbox(bounds);
    m_msc_dmgr->clip_box()->SetBounds(bounds);
  }

  // Charts must update first since it handles cp selection
  update_charts();

  // push selection updates to various geoms
  update_sep_sheet_geom(1);
  update_sep_sheet_geom(2);
  update_sep_line_geom(1);
  update_sep_line_geom(2);
  update_cp_geom();

  // update everyone else after selection updates
  m_cp_ren->update();    
  m_2sad_asc_ren->update();
  m_1sad_asc_ren->update();
  m_2sad_des_ren->update();
  m_1sad_des_ren->update();
  m_2sad_asc_sel->update();

  // update volume enhancemet after 2sad_asc_ren updates
  m_volren->update();

  log_status_message(tr("Rendering using updated data"));
  ui->geom_qvtkWidget->GetRenderWindow()->Render();

  log_status_message(tr("Update Pipelines finished."));
}

/*---------------------------------------------------------------------------*/

void MainWindow::update_charts()
{
  if(m_charts_MTime <= m_msc_dmgr->get_res_MTime())
  {
    log_status_message(tr("Updating charts"));

    m_pers_charts->set_msc_res(m_msc_dmgr->get_res());

    m_pers_charts->update(); // for now its still a push methodology .. fix l8r
//    m_pers_hist_charts->update_cutoff(m_cancno_cutoff);
//    m_pers_betti_plots->update_cutoff(m_cancno_cutoff);
//    m_cp_hist_charts->update_cutoff(m_cancno_cutoff);

    m_charts_MTime.Modified();
  }
}


/*---------------------------------------------------------------------------*/

bool cp_filter(int dim,int_pair_t pr,mscomplex_cptr_t msc)
{return (msc->index(pr.first) == dim && msc->is_not_canceled(pr.first));}

bool MainWindow::update_sep_sheet_geom(int dim)
{
  eGDIR dir = (dim == 1)?(ASC):(DES);

  SeparatrixSheetRenderer * ren = (dim == 1)?(m_1sad_asc_ren):(m_2sad_des_ren);

  if(!ren->get_enabled())
    return false;

  vtkTimeStamp pctime = ren->get_geom_piece_cps_MTime();

  if( pctime > m_pers_charts->get_selected_cps_MTime() &&
      pctime > m_msc_dmgr->get_res_MTime())
    return false;

//  BOOST_AUTO(rng,m_pers_charts->get_selected_cps()
//             |badpt::filtered(bind(cp_filter,dim,_1,m_msc_dmgr->msc())));

  std::map<int,int> pc_cpid;

  BOOST_FOREACH(int_pair_t pr,m_pers_charts->get_selected_cps()
                |badpt::filtered(bind(cp_filter,dim,_1,m_msc_dmgr->msc())))
  {
    int cp = pr.first;
    int cp_res = pr.second;

    int_list_t cp_pcs;

    m_msc_dmgr->msc()->get_contrib_cps(cp_pcs,dir,cp,cp_res);

    BOOST_FOREACH(int pc,cp_pcs)
        pc_cpid.insert(std::make_pair(pc,cp));
  }

  int_pair_list_t pcs(pc_cpid.size());

  br::copy(pc_cpid,pcs.begin());

  ren->set_geom_piece_cps(pcs);

  return true;
}

/*---------------------------------------------------------------------------*/

bool MainWindow::update_sep_line_geom(int dim)
{
  eGDIR dir = (dim == 2)?(ASC):(DES);

  SeparatrixLineRenderer * ren = (dim == 2)?(m_2sad_asc_ren):(m_1sad_des_ren);

  if(!ren->get_enabled())
    return false;

  vtkTimeStamp pctime = ren->get_geom_piece_cps_MTime();

  if( pctime > m_pers_charts->get_selected_cps_MTime() &&
      pctime > m_msc_dmgr->get_res_MTime())
    return false;

  BOOST_AUTO(rng,m_pers_charts->get_selected_cps()|badpt::filtered
             (bind(cp_filter,dim,_1,m_msc_dmgr->msc())));

  int_list_t pcs;

  int_pair_list_t cps;

  br::copy(rng,std::back_inserter(cps));

  if(!cps.empty()) m_msc_dmgr->msc()->get_contrib_cps(pcs,dir,cps);

  ren->set_geom_piece_cps(pcs);

  return true;
}

/*---------------------------------------------------------------------------*/

void MainWindow::update_cp_geom()
{
  if( m_cp_ren->get_cps_MTime() < m_pers_charts->get_selected_cps_MTime() )
  {
    int_pair_list_t cps;
    br::copy(m_pers_charts->get_selected_cps(),std::back_inserter(cps));
    m_cp_ren->set_cps(cps);
  }
}

/*---------------------------------------------------------------------------*/

void MainWindow::set_show_geom_of_cp_type(int dir,int dim,bool val)
{
  ENSURE_OR_RETURN(is_in_range(dir,0,2))<<"Invalid Dir " << SVAR(dir);
  ENSURE_OR_RETURN(is_in_range(dim,0,4))<<"Invalid Dim " << SVAR(dim);

  QCheckBox * cb = NULL;

  if( dir == ASC && dim == 1) cb = ui->show_1saddle_asc_checkBox;
  if( dir == ASC && dim == 2) cb = ui->show_2saddle_asc_checkBox;
  if( dir == DES && dim == 1) cb = ui->show_1saddle_des_checkBox;
  if( dir == DES && dim == 2) cb = ui->show_2saddle_des_checkBox;

  if( cb != NULL && cb->isChecked() != val)
    cb->setChecked(val);

  if( dir == ASC && dim == 1) m_1sad_asc_ren->set_enabled(val);
  if( dir == ASC && dim == 2) m_2sad_asc_ren->set_enabled(val);
  if( dir == DES && dim == 1) m_1sad_des_ren->set_enabled(val);
  if( dir == DES && dim == 2) m_2sad_des_ren->set_enabled(val);

}
/*-------------------------------------------------------------------*/

bool MainWindow::get_show_geom_of_cp_type(int dir,int dim)
{
  ENSURE_OR_RETURNV(is_in_range(dir,0,2),false)<<"Invalid Dir " << SVAR(dir);
  ENSURE_OR_RETURNV(is_in_range(dim,0,4),false)<<"Invalid Dim " << SVAR(dim);

  if( dir == ASC && dim == 1) return m_1sad_asc_ren->get_enabled();
  if( dir == ASC && dim == 2) return m_2sad_asc_ren->get_enabled();
  if( dir == DES && dim == 1) return m_1sad_des_ren->get_enabled();
  if( dir == DES && dim == 2) return m_2sad_des_ren->get_enabled();

  return false;
}

/*---------------------------------------------------------------------------*/

void MainWindow::set_geom_color(int dir,int dim,double r,double g,double b)
{
  if( dir == ASC && dim == 1) m_1sad_asc_ren->set_color(r,g,b);
  if( dir == ASC && dim == 2) m_2sad_asc_ren->set_color(r,g,b);
  if( dir == DES && dim == 1) m_1sad_des_ren->set_color(r,g,b);
  if( dir == DES && dim == 2) m_2sad_des_ren->set_color(r,g,b);
}

/*---------------------------------------------------------------------------*/

void MainWindow::read_spin_dirs(QString f)
{
  // Remove old data if it exists
  m_msc_dmgr->domain_ds()->GetPointData()->RemoveArray("spinDirs");

  // Nothing to be done for null file
  if (f.size() == 0)
    return;

  // Read the spin directions from file f
  int nv    = m_msc_dmgr->tcc()->num_dcells(0);
  int nv_pd = m_msc_dmgr->domain_ds()->GetNumberOfPoints();

  VTK_CREATE(vtkDoubleArray,spinDirs);

  spinDirs->SetName("spinDirs");
  spinDirs->SetNumberOfComponents(3);
  spinDirs->SetNumberOfTuples(nv_pd);

  QFile inputFile(f);
  if (!inputFile.open(QIODevice::ReadOnly))
  {
    qWarning()<<"File "<<f<<" not found !!!";
    return;
  }

  QTextStream ss(&inputFile);

  int i = 0;

  for(; i < nv && !ss.atEnd(); ++i)
  {
    double x,y,z,m;

    utl::dvec3 s;

    ss >> x >> y >>z >>m >> s[0] >>s[1] >>s[2];

    s /= s.norm2();

    spinDirs->SetTuple(i,s.m_elems);
  }

  if(i != nv)
  {
    qWarning()<<"incorrect number of points in file\n"<<SVAR(i);
    return;
  }

  typedef MsComplexDataManager::pvrt_to_int_t::value_type pvert_to_int_vt;

  BOOST_FOREACH(pvert_to_int_vt v,m_msc_dmgr->pvert_idx_map())
    spinDirs->SetTuple(v.second,spinDirs->GetTuple(v.first.cid));


  m_msc_dmgr->domain_ds()->GetPointData()->AddArray(spinDirs);
}

/*---------------------------------------------------------------------------*/

void MainWindow::replace_volren_scalar_with_spin_dot_fil()
{
  vtkPolyData * pd = vtkPolyData::SafeDownCast
      (m_2sad_asc_ren->m_tangents->GetOutputDataObject(0));

  ENSURE_OR_RETURN(pd && pd->GetNumberOfPoints() != 0) <<"No Filaments !!!";

  vtkFloatArray * tang = vtkFloatArray::SafeDownCast
      (pd->GetPointData()->GetArray("Tangents"));

  ENSURE_OR_RETURN(tang)<<"No Filament Tangents !!!";

  vtkDoubleArray *spinDirs = vtkDoubleArray::SafeDownCast
      (m_msc_dmgr->domain_ds()->GetPointData()->GetArray("spinDirs"));

  ENSURE_OR_RETURN(spinDirs)<< "No Spin Data !!!";

  log_status_message(tr("Computing Spin dir and fil dir dot product"));

  int nv    = m_msc_dmgr->tcc()->num_dcells(0);

  vtkDataArray * scalars = m_msc_dmgr->domain_ds()->GetPointData()->GetScalars();

  for(int i = 0 ; i < nv ; ++i)
  {
    utl::dvec3 t,pt = m_msc_dmgr->tcc()->get_vert(i),sdir;

    spinDirs->GetTuple(i,sdir.m_elems);
    tang->GetTuple(pd->FindPoint(pt.m_elems),t.m_elems);

    double val = std::abs(utl::dot(t,sdir));

    scalars->SetComponent(i,0,val);
  }

  typedef MsComplexDataManager::pvrt_to_int_t::value_type pvert_to_int_vt;

  BOOST_FOREACH(pvert_to_int_vt v,m_msc_dmgr->pvert_idx_map())
    scalars->SetTuple(v.second,scalars->GetTuple(v.first.cid));

 scalars->Modified();
}

/*---------------------------------------------------------------------------*/

void MainWindow::set_threshold(double t)
{
  ENSURE_OR_RETURN(is_in_range(t,0,1.0)) <<"t not in range" << SVAR(t);

  if(ui->threshold_doubleSpinBox->value() != t)
    ui->threshold_doubleSpinBox->setValue(t);

  m_msc_dmgr->set_thresh(t);
}

/*---------------------------------------------------------------------------*/

double MainWindow::get_threshold()
{
  return ui->threshold_doubleSpinBox->value();
}

/*---------------------------------------------------------------------------*/

void MainWindow::set_mscomplex_multires_version(int cutoff)
{
  m_msc_dmgr->set_res(cutoff);
}

/*---------------------------------------------------------------------------*/

int MainWindow::get_mscomplex_multires_version()
{
  return m_msc_dmgr->get_res();
}

/*---------------------------------------------------------------------------*/

void MainWindow::set_geom_clipbox (double xmn,double xmx,double ymn,
                       double ymx,double zmn,double zmx)
{
  double bounds[] = {xmn,xmx,ymn,ymx,zmn,zmx};

  m_dec_ren->set_clipbox(bounds);

  m_msc_dmgr->clip_box()->SetBounds(bounds);
  m_msc_dmgr->clip_box()->Modified();

  ui->dslider_roi_x->blockSignals(true);
  ui->dslider_roi_y->blockSignals(true);
  ui->dslider_roi_z->blockSignals(true);

  ui->dslider_roi_x->setPositions(xmn,xmx);
  ui->dslider_roi_y->setPositions(ymn,ymx);
  ui->dslider_roi_z->setPositions(zmn,zmx);

  ui->dslider_roi_x->blockSignals(false);
  ui->dslider_roi_y->blockSignals(false);
  ui->dslider_roi_z->blockSignals(false);
}

/*---------------------------------------------------------------------------*/

QList<double> MainWindow::get_geom_clipbox()
{
  double bnd[6];
  m_msc_dmgr->clip_box()->GetBounds(bnd);
  QList<double> l;
  std::copy(bnd,bnd+6,std::back_inserter(l));
  return l;
}

/*---------------------------------------------------------------------------*/

#ifdef PYTHONQT_ENABLED
void MainWindow::eval_script(QString str)
{
  m_pqt.evalFile(str);
}
#else
void MainWindow::eval_script(QString str)
{
  cerr<<"PythonQt scripts were not enabled in complie time "<<endl;
}
#endif

/*---------------------------------------------------------------------------*/

void MainWindow::on_actionOpen_MsComplex_triggered(bool )
{
  QString ts_file = QFileDialog::getOpenFileName
      (this,tr("Select ts file"),QDir::currentPath(),
       "ts (*.ts)");

  if(ts_file == "")
    return;

  load_ts(ts_file);
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_actionEval_Script_triggered(bool )
{
  QString pyfile = QFileDialog::getOpenFileName
      (this,tr("Select script file"),QDir::currentPath(),
       "Python script (*.py)");

  if(pyfile == "")
    return;

  eval_script(pyfile);
}

/*---------------------------------------------------------------------------*/

void MainWindow::log_status_message(const QString &mes) const
{
  if (mes.size() == 0)
    return;

  std::cout<<QTime::currentTime().toString("[HH:mm:ss] ").toStdString()
           <<mes.toStdString()
           <<std::endl;

  statusBar()->showMessage(mes,2000);
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_show_minima_checkBox_clicked(bool v)
{
  m_cp_ren->set_index_enabled(0,v);
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_show_1saddle_checkBox_clicked(bool v)
{
  m_cp_ren->set_index_enabled(1,v);
}

/*---------------------------------------------------------------------------*/
void MainWindow::on_show_2saddle_checkBox_clicked(bool v)
{
  m_cp_ren->set_index_enabled(2,v);
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_show_maxima_checkBox_clicked(bool v)
{
  m_cp_ren->set_index_enabled(3,v);
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_dslider_roi_x_positionsChanged(double min, double max)
{
  double bounds[6];
  m_dec_ren->get_clipbox(bounds);
  bounds[0] = min; bounds[1] = max;
  m_dec_ren->set_clipbox(bounds);
  ui->geom_qvtkWidget->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_dslider_roi_y_positionsChanged(double min, double max)
{
  double bounds[6];
  m_dec_ren->get_clipbox(bounds);
  bounds[2] = min; bounds[3] = max;
  m_dec_ren->set_clipbox(bounds);
  ui->geom_qvtkWidget->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

void MainWindow::on_dslider_roi_z_positionsChanged(double min, double max)
{
  double bounds[6];
  m_dec_ren->get_clipbox(bounds);
  bounds[4] = min; bounds[5] = max;
  m_dec_ren->set_clipbox(bounds);
  ui->geom_qvtkWidget->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

void MainWindow::rangeSelect2Saddles
(double smin, double smax, double mmin, double mmax)
{
  int_pair_list_t sadList;
  m_msc_dmgr->rangeSelect2Saddles(smin,smax,mmin,mmax,sadList);

  BOOST_FOREACH(int_pair_t pr,sadList)
  {
    m_pers_charts->select_cp(pr.first,pr.second);
  }

  m_2sad_asc_sel->set_selection_ranges(smin,smax,mmin,mmax);

  update_pipelines();
}

/*---------------------------------------------------------------------------*/

void MainWindow::deselectAllCps()
{
  m_pers_charts->deselect_all_cps();
  update_pipelines();
}


/*****************************************************************************/
