#ifndef __MSCOMPLEXDATAMANAGER_INCLUDED__
#define __MSCOMPLEXDATAMANAGER_INCLUDED__

#include <vtkSmartPointer.h>
#include <vtkTimeStamp.h>
#include <boost/python.hpp>

#include <tet_cc.hpp>

#include <QObject>

class vtkRenderer;
class vtkBox;
class vtkDataSet;

class merge_dag_t;

/*****************************************************************************/
/// \brief This class Manages all the global information/state of the MScomplex
///
/// Specifically this is:
/// a) Loading data mscomplex/geom data from files
/// b) Changing the resolution of the MS complex
/// c) Managing Geometry merge tree/queries
/// d) Managing Global Rendering infomation.
// /// e) Managing the selected list of critical points.
class MsComplexDataManager:public QObject
{
  Q_OBJECT
public:
  typedef std::map<tet::ptet_cc_t::pcellid_t,int> pvrt_to_int_t;

  MsComplexDataManager();

private:

  // a) mscomplex/geom data
  tet::mscomplex_ptr_t                 m_msc;    
  tet::fn_list_t                       m_func;
  boost::python::object                m_msc_py;
  tet::base_cc_geom_ptr_t              m_cc_geom;
  tet::ptet_geom_cc_ptr_t              m_tcc;

  // b) Changing the resolution of the MS complex
  vtkTimeStamp                         m_msc_res_MTime;

  // c) Managing Geometry merge tree/queries
//  std::map<tet::int_pair_t,tet::int_pair_list_t,utl::uo_pair_cmp<int> >
//                                       m_arc_merge_dag;

  // d) Managing Global Rendering infomation.
  vtkSmartPointer<vtkRenderer>         m_ren;
  vtkSmartPointer<vtkBox>              m_clip_box;


  // e) Domain information
  vtkSmartPointer<vtkDataSet>          m_domain_ds;
  pvrt_to_int_t                        m_pvert_idx_map; // non-orig-domain periodic vertex to its index in m_usgrid



  //  // f) Managing the selected list of critical points.
  //  std::map<int,int>                    m_selected_cps;
  //  vtkTimeStamp                         m_selected_cps_MTime;
  // highest hversion in which a 2sad connects unique maxima.
  tet::int_list_t                      m_2sad_unique_max_hver;


public:

  // a) Loading data mscomplex/geom data from files
  void create();
  void load_ts(std::string ts_file,int fcomp,int gconv);
  void load_raw(std::string raw_file,int X, int Y, int Z,
                tet::vert_t lc,tet::vert_t uc,int gconv)
;

  inline tet::ptet_geom_cc_cptr_t tcc() const {return m_tcc;}
  inline tet::base_cc_geom_ptr_t  ccg() const {return m_cc_geom;}
  inline tet::mscomplex_cptr_t    msc() const {return m_msc;}
  inline PyObject * msc_py() const {return m_msc_py.ptr();}

  // b) Changing the resolution of the MS complex
  void set_res(int cutoff);
  void set_thresh(double t);  
  int  get_res_for_thresh(double t) const; // get first res with pers > t
  int  get_res() const;
  inline vtkTimeStamp get_res_MTime() const {return m_msc_res_MTime;}


  // d) Managing Global Rendering infomation.
  inline vtkSmartPointer<vtkRenderer>  ren()  const{return m_ren;}
  inline vtkSmartPointer<vtkBox>    clip_box() const{return m_clip_box;}

  // e) Domain Info/Data
  inline vtkSmartPointer<vtkDataSet> domain_ds() const{return m_domain_ds;}
  inline const pvrt_to_int_t & pvert_idx_map() const {return m_pvert_idx_map;}
  void getScalarRange(double rng[2]) const;


  // f) Manage Selections
  void rangeSelect2Saddles
  (double smin,double smax,double mmin,double mmax,
   tet::int_pair_list_t &sad_hver_list);

signals:
    void statusMessageEvent(const QString &);

private:
  void read_ts(std::string ts_file,int fcomp);
  void read_raw(std::string raw_file,int X,int Y, int Z,tet::vert_t lc,tet::vert_t uc);
  void compute_msc(std::string msc_cache_file, int gconv);
  void finish_load_ts();
  void finish_load_raw(int X,int Y,int Z);
  void clear();
  void compute_arc_merge_graph();
  void compute_2sad_unique_max_hver();

};

/*****************************************************************************/

#endif
