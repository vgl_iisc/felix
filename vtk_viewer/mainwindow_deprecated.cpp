#include <config.h>
#ifdef PYTHONQT_ENABLED
// python.h insists on being included first
// else you get distracting compiler warnings
#include <PythonQt.h>
#include <extn/PythonQtScriptingConsole.h>
#endif

#include <mainwindow.h>
#include <ui_mainwindow.h>
#include <mainwindow_charts.hpp>
#include <MsComplexDataManager.hpp>
#include <rens/VolumeRenderer.hpp>
#include <rens/SeparatrixLineRenderer.hpp>
#include <rens/SeparatrixSheetRenderer.hpp>
#include <rens/CriticalPointRenderer.hpp>
#include <rens/DecorationsRenderer.hpp>


/*****************************************************************************/
// DEPRECATED STUFF .. Just around to support older scripts and reroute calls

#define DEPRECATION_WARNING(depreason,depaction,deprec) \
  qWarning()<<QString::fromStdString(FILEFUNCLINE)<<" has been DEPRECATED\n"\
  <<"Reason         ::"<<(depreason)<<"\n"\
  <<"Current Action ::"<<(depaction)<<"\n"\
  <<"Recommendation ::"<<(deprec)<<"\n"\

#define VOLREN_DEPRECATION_WARNING(nf) (\
  qWarning()<<QString::fromStdString(FILEFUNCLINE)<<" has been DEPRECATED\n"\
  <<"Reason         ::Volume data and settings moved to dedicated VolRen object\n"\
  <<"Current Action ::Call will be redirected to new  object\n"\
  <<"Recommendation ::Replace ms_mw." << __func__ <<"(...)to \n"\
  <<"                 following function(s) of ms_mw.get_volren() \n"\
  <<"                 "<< #nf<<"\n")

#define CPREN_DEPRECATION_WARNING(nf) (\
  qWarning()<<QString::fromStdString(FILEFUNCLINE)<<" has been DEPRECATED\n"\
  <<"Reason         ::Critical point rendering settings moved to dedicated object\n"\
  <<"Current Action ::Call will be redirected to new  object\n"\
  <<"Recommendation ::Replace ms_mw." << __func__ <<"(...)to \n"\
  <<"                 following function(s) of ms_mw.get_cpren() \n"\
  <<"                 "<< #nf<<"\n")

#define DECREN_DEPRECATION_WARNING(nf) (\
  qWarning()<<QString::fromStdString(FILEFUNCLINE)<<" has been DEPRECATED\n"\
  <<"Reason         ::Decorations (clipbox,orientation marker) moved to dedicated object\n"\
  <<"Current Action ::Call will be redirected to new  object\n"\
  <<"Recommendation ::Replace ms_mw." << __func__ <<"(...)to \n"\
  <<"                 following function(s) of ms_mw.get_decren() \n"\
  <<"                 "<< #nf<<"\n")


#define TWOSADREN_DEPRECATION_WARNING(nf) (\
  qWarning()<<QString::fromStdString(FILEFUNCLINE)<<" has been DEPRECATED\n"\
  <<"Reason         ::Two saddle ascending ren moved to dedicated object\n"\
  <<"Current Action ::Call will be redirected to new  object\n"\
  <<"Recommendation ::Replace ms_mw." << __func__ <<"(...)to \n"\
  <<"                 following function(s) of ms_mw.get_filren() \n"\
  <<"                 "<< nf<<"\n")

void MainWindow::add_volume_tfpt(double f,double r,double g,double b,double a)
{
  VOLREN_DEPRECATION_WARNING(add_tfpt);
  m_volren->add_tfpt(f,r,g,b,a);
}

void MainWindow::del_volume_tfpt(int i)
{
  VOLREN_DEPRECATION_WARNING(del_tfpt);
  m_volren->del_tfpt(i);
}

int  MainWindow::num_volume_tfpt() const
{
  VOLREN_DEPRECATION_WARNING(num_tfpt);
  return m_volren->num_tfpt();
}

void MainWindow::clear_volume_tf()
{
  VOLREN_DEPRECATION_WARNING(clear_tf);
  m_volren->clear_tf();
}

bool MainWindow::save_volume_tf(const QString &fn) const
{
  VOLREN_DEPRECATION_WARNING(save_tf);
  return m_volren->save_tf(fn);
}

bool MainWindow::load_volume_tf(const QString &fn)
{
  VOLREN_DEPRECATION_WARNING(load_tf);
  return m_volren->load_tf(fn);
}

void MainWindow::set_volren_enabled(bool val)
{
  VOLREN_DEPRECATION_WARNING(set_enabled);
  m_volren->set_enabled(val);
}

bool MainWindow::get_volren_enabled()
{
  VOLREN_DEPRECATION_WARNING(get_enabled);
  return m_volren->get_enabled();
}

void MainWindow::set_orientation_marker_enabled(bool v)
{
  DEPRECATION_WARNING("replaced by enable/disable_orientaiton_marker",
                      "call will be re-routed","replace call");
  if(v)
    enable_orientation_marker();
  else
    disable_orientation_marker();

}

void MainWindow::enable_2asc_tubes(double conv,int niter,double r,int ns)
{
  DEPRECATION_WARNING
      ("split into 2 calls a) enable_2asc_tubes b) set_2asc_smoother_params",
        "call will be re-routed to both","replace call");
  set_2asc_smoother_params(conv,niter);
  enable_2asc_tubes(r,ns);
}

void MainWindow::enable_vertex_spin_dot_fil_dir_scalar(QString str)
{
  DEPRECATION_WARNING
      ("split into 2 calls a) read_spin_dirs b) replace_volren_scalar_with_spin_dot_fil",
       "call will be re-routed to both","replace call");
  read_spin_dirs(str);
  replace_volren_scalar_with_spin_dot_fil();
}

void MainWindow::set_merge_dag_upto_canc(bool val)
{
  DEPRECATION_WARNING("Feature removed","Call Ignored","None");
}

bool MainWindow::get_merge_dag_upto_canc()
{
  DEPRECATION_WARNING("Feature removed","Call Ignored","None");
  return false;
}

void MainWindow::set_cp_type_show(int i, bool v)
{
  CPREN_DEPRECATION_WARNING(set_index_enabled);
  m_cp_ren->set_index_enabled(i,v);
}

bool MainWindow::get_cp_type_show(int i) const
{
  CPREN_DEPRECATION_WARNING(get_index_enabled);
  return m_cp_ren->get_index_enabled(i);
}

void MainWindow::enable_cp_spheres(double r1, double r2, double r3, double r4)
{
  CPREN_DEPRECATION_WARNING(set_spheres_enabled(true));
  m_cp_ren->set_spheres_enabled(true);
  m_cp_ren->set_spheres_radii(r1,r2,r3,r4);
}

void MainWindow::disable_cp_spheres()
{
  CPREN_DEPRECATION_WARNING(set_spheres_enabled(false));
  m_cp_ren->set_spheres_enabled(false);
}

void MainWindow::enable_cp_selection()
{
  CPREN_DEPRECATION_WARNING(set_selection_enabled(true));
  m_cp_ren->set_selection_enabled(true);
}

void MainWindow::disable_cp_selection()
{
  CPREN_DEPRECATION_WARNING(set_selection_enabled(false));
  m_cp_ren->set_selection_enabled(false);
}

void MainWindow::set_geom_show_clipbox(bool val)
{
  DECREN_DEPRECATION_WARNING(set_clipbox_enabled(val));
  m_dec_ren->set_clipbox_enabled(val);
}

bool MainWindow::get_geom_show_clipbox()
{
  DECREN_DEPRECATION_WARNING(get_clipbox_enabled);
  return false;
}

void MainWindow::enable_orientation_marker()
{
  DECREN_DEPRECATION_WARNING(set_orientation_marker_enabled(true));
  m_dec_ren->set_orientation_marker_enabled(true);
}

void MainWindow::disable_orientation_marker()
{
  DECREN_DEPRECATION_WARNING(set_orientation_marker_enabled(false));
  m_dec_ren->set_orientation_marker_enabled(false);
}

void MainWindow::set_geom_window_background(double r,double g,double b)
{
  DECREN_DEPRECATION_WARNING(set_background_color);
  m_dec_ren->set_background_color(r,g,b);
}

void MainWindow::save_geom_camera(QString  fn) const
{
  DECREN_DEPRECATION_WARNING(save_camera);
  m_dec_ren->save_camera(fn);
}

void MainWindow::load_geom_camera(QString  fn)
{
  DECREN_DEPRECATION_WARNING(load_camera);
  m_dec_ren->load_camera(fn);
}

void MainWindow::save_geom_screenshot(QString  fn,int mag)
{
  DECREN_DEPRECATION_WARNING(save_screenshot);
  m_dec_ren->save_screenshot(fn,mag);
}

void MainWindow::save_geom_screenshot()
{
  DECREN_DEPRECATION_WARNING(save_screenshot);
  m_dec_ren->save_screenshot();
}

void MainWindow::enable_2asc_tubes(double r,int ns)
{
  TWOSADREN_DEPRECATION_WARNING("set_tubes_enabled(True),set_tubes_geomprops");
  m_2sad_asc_ren->set_tubes_enabled(true);
  m_2sad_asc_ren->set_tubes_geomprops(r,ns);
}

void MainWindow::set_2asc_smoother_params(double conv,int niter)
{
  TWOSADREN_DEPRECATION_WARNING("set_smoother_params");
  m_2sad_asc_ren->set_smoother_params(conv,niter);
}

void MainWindow::disable_2asc_tubes()
{
  TWOSADREN_DEPRECATION_WARNING("set_tubes_enabled(False)");
  m_2sad_asc_ren->set_tubes_enabled(false);
}

void MainWindow::save_two_saddle_asc(QString fn)
{
  TWOSADREN_DEPRECATION_WARNING("save_data");
  m_2sad_asc_ren->save_data(fn);
}

/*****************************************************************************/
