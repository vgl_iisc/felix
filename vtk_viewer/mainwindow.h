#ifndef MainWindow_H
#define MainWindow_H

#include <config.h>

#ifdef PYTHONQT_ENABLED
#include <PythonQt.h>
#include <boost/python.hpp>
#endif

#include <vtkTimeStamp.h>

#include <QMainWindow>

#include <tet_cc.hpp>
#include <NicePyConsole.h>
// Forward class declarations
class Ui_MainWindow;

class PersistenceCharts;
class PersistenceHistogramCharts;
class PersistentBettiNumbersPlot;
class CriticalPointsHistograms;
class VolumeRenderer;
class SeparatrixLineRenderer;
class SeparatrixSheetRenderer;
class MsComplexDataManager;
class CriticalPointRenderer;
class DecorationsRenderer;
class SeparatrixLineSelector;
typedef boost::shared_ptr<MsComplexDataManager>       mscdmgr_ptr_t;
typedef boost::shared_ptr<const MsComplexDataManager> mscdmgr_cptr_t;


/// \brief Mainwindow GUI of the application
///
/// \note Python scripts can be executed by the evaluate option in the file
///       menu or by pressing ctrl-e and selecting the script file. An interpreter
///       Console is also present in the bottom of the application, which is similar
///       to the python interpreter.
///
/// \note This object is made available to python scripts by a global variable "ms_mw".
///
/// \note All the public slot methods of this object can be invoked
///       in the script.

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:

  // Constructor/Destructor
  MainWindow();
  ~MainWindow() {}

public slots:
  void load_ts(QString );
  void load_raw(QString, int X,int Y,int Z,
                double xmin, double xmax,
                double ymin, double ymax,
                double zmin, double zmax);
  void update_pipelines();

  double get_threshold();
  void   set_threshold(double t);

/// \brief Get the MS complex resolution
///
/// The morse smale complex has many versions depending on how it
/// was simplified.
/// Version 0 is the full resolution MS complex.
/// Version 1 is with the first cancellation effected .. and so on.
/// This is method gets the current version number.
 int  get_mscomplex_multires_version();

/// \brief Set the MS complex resolution
///
/// \note
/// Setting the multires version is normally done through the set_threshold
/// Doing it through this way will render the threshold value shown in the ui
/// inconsistent. Nothing else should be affected.
  void        set_mscomplex_multires_version(int cancno);

  QList<double> get_geom_clipbox();
  void   set_geom_clipbox(double,double,double,
                          double,double,double);


  void set_show_geom_of_cp_type(int dir,int dim,bool val);
  bool get_show_geom_of_cp_type(int dir,int dim);
  void set_geom_color(int dir,int dim,double r,double g,double b);

  void eval_script(QString str);

  /// \brief Get read only interface to the cell complex
  PyObject * get_mscomplex();
//  /// \brief Get read only interface to the cell complex
//  PyObject * get_cellcomplex();
  /// \brief Get the persistence charts Manager
  inline PersistenceCharts  * get_pers_charts(){return m_pers_charts;}
  /// \brief Get the Volume renderer
  inline VolumeRenderer     * get_volren(){return m_volren;}
  /// \brief Get the critical point renderer
  inline CriticalPointRenderer * get_cpren(){return m_cp_ren;}
  /// \brief Get the decorations renderer
  inline DecorationsRenderer * get_decren(){return m_dec_ren;}



  /// \brief Get two saddle asc renderer
  inline SeparatrixLineRenderer * get_two_sad_asc_ren(){return m_2sad_asc_ren;}
  /// \brief Get one saddle des renderer
  inline SeparatrixLineRenderer * get_one_sad_des_ren(){return m_1sad_des_ren;}
  /// \brief Get two saddle des renderer
  inline SeparatrixSheetRenderer* get_two_sad_des_ren(){return m_2sad_des_ren;}
  /// \brief Get one saddle asc renderer
  inline SeparatrixSheetRenderer* get_one_sad_asc_ren(){return m_1sad_asc_ren;}
  /// \brief Alias for two saddle asc renderer
  inline SeparatrixLineRenderer  * get_fil_ren(){return m_2sad_asc_ren;}
  /// \brief Alias for one saddle asc renderer
  inline SeparatrixSheetRenderer * get_wall_ren(){return m_1sad_asc_ren;}


  /// \brief 2-saddle selection algorithm
  void rangeSelect2Saddles(double smin, double smax, double mmin, double mmax);

  /// \brief clear current selection
  void deselectAllCps();



  /// \brief Read spin Data into memory.
  /// \param[in] fn filename
  /// \note Existing data is deleted. If fn == "" existing data is deleted and
  ///       nothing is read
  void read_spin_dirs(QString fn);
  /// \brief Computes a scalar as the dot product of vertex spin dir
  ///        and the direction of the closest filament point. This replaces
  ///        the volume rendering scalar.
  void replace_volren_scalar_with_spin_dot_fil();


private:
  // the persistence render window related stuff
  PersistenceCharts *              m_pers_charts;

  // histograms of persistence density
  PersistenceHistogramCharts *     m_pers_hist_charts;

  // persistent betti numbers
  PersistentBettiNumbersPlot*      m_pers_betti_plots;

  // statistics on cps above threshold
  CriticalPointsHistograms *       m_cp_hist_charts;

  // The Last Modification time of all charts
  vtkTimeStamp                     m_charts_MTime;

private:

  // Essential actions after a dataset is loaded
  void finish_load();

  // Geometry render window related stuff

  bool update_sep_sheet_geom(int dim);
  bool update_sep_line_geom(int dim);
  void update_cp_geom();
  void update_charts();



private:
// volume renderer related stuff
  VolumeRenderer*                         m_volren;

// Saddle rendering related stuff
  SeparatrixLineRenderer*                 m_2sad_asc_ren;
  SeparatrixLineRenderer*                 m_1sad_des_ren;

  SeparatrixSheetRenderer*                m_1sad_asc_ren;
  SeparatrixSheetRenderer*                m_2sad_des_ren;

  SeparatrixLineSelector*                 m_2sad_asc_sel;

// Critical point rendering related stuff
  CriticalPointRenderer *                 m_cp_ren;

// Misc decorations
  DecorationsRenderer *                   m_dec_ren;

// Manager for the Mscomplex data
  mscdmgr_ptr_t                           m_msc_dmgr;

private:

// Designer form
  Ui_MainWindow *ui;


// For the scripting interface
#ifdef PYTHONQT_ENABLED
private:
  PythonQtObjectPtr           m_pqt;
  NicePyConsole              *m_pqt_cons;
#endif


private slots:
         void on_actionEval_Script_triggered(bool );
        void  on_actionOpen_MsComplex_triggered(bool );

  inline void on_update_pushButton_clicked(bool )
  {update_pipelines();}
  inline void on_threshold_doubleSpinBox_valueChanged(double t)
  {set_threshold(t);}

  void on_show_minima_checkBox_clicked(bool v);
  void on_show_1saddle_checkBox_clicked(bool v);
  void on_show_2saddle_checkBox_clicked(bool v);
  void on_show_maxima_checkBox_clicked(bool v);

  inline void on_show_1saddle_asc_checkBox_clicked(bool v)
  {set_show_geom_of_cp_type(tet::ASC,1,v);}
  inline void on_show_1saddle_des_checkBox_clicked(bool v)
  {set_show_geom_of_cp_type(tet::DES,1,v);}
  inline void on_show_2saddle_asc_checkBox_clicked(bool v)
  {set_show_geom_of_cp_type(tet::ASC,2,v);}
  inline void on_show_2saddle_des_checkBox_clicked(bool v)
  {set_show_geom_of_cp_type(tet::DES,2,v);}

         void log_status_message(const QString &mes) const;

   void on_dslider_roi_x_positionsChanged(double min,double max);
   void on_dslider_roi_y_positionsChanged(double min,double max);
   void on_dslider_roi_z_positionsChanged(double min,double max);



// Deprecated Slots:
public slots:
/// \privatesection
  void add_volume_tfpt(double f, double r, double g,double b, double a);
  void del_volume_tfpt(int i);
  int  num_volume_tfpt() const;
  void clear_volume_tf();
  bool save_volume_tf(const QString &) const;
  bool load_volume_tf(const QString &);
  void set_volren_enabled(bool val);
  bool get_volren_enabled();
  void set_orientation_marker_enabled(bool v);  
  void enable_2asc_tubes(double conv,int niter,double r,int ns);
  void enable_vertex_spin_dot_fil_dir_scalar(QString str);
  void save_two_saddle_asc(QString str);
  void enable_2asc_tubes(double r,int ns);
  void set_2asc_smoother_params(double conv,int niter);
  inline void enable_2asc_tubes(){enable_2asc_tubes(1,6);}
  void disable_2asc_tubes();
  void set_merge_dag_upto_canc(bool val);
  bool get_merge_dag_upto_canc();
  void set_cp_type_show(int i, bool v);
  bool get_cp_type_show(int i) const;
  void enable_cp_spheres(double r1=1,double r2=1,
                         double r3=1,double r4=1);
  void disable_cp_spheres();
  void enable_cp_selection();
  void disable_cp_selection();
  void set_geom_show_clipbox(bool val);
  bool get_geom_show_clipbox();
  void enable_orientation_marker();
  void disable_orientation_marker();
  void set_geom_window_background(double r,double g,double b);
  void save_geom_camera(QString  fn) const;
  void load_geom_camera(QString  fn);
  void save_geom_screenshot(QString  fn,int mag=1);
  void save_geom_screenshot();
};

#endif // MainWindow_H
