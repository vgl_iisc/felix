#include <stack>

#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellType.h>
#include <vtkRenderer.h>
#include <vtkBox.h>
#include <vtkImageData.h>

#include <QThread>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>

#include <boost/bind.hpp>
#include <boost/range/counting_range.hpp>

#include <tet_mscomplex.hpp>
#include <cube_cc.hpp>
#include <tet_cc.hpp>
#include <tet_dataset.hpp>
#include <MsComplexDataManager.hpp>

#include <pymstet.hpp>

#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

using namespace std;
using namespace tet;

namespace bp = boost::python;

#include <vtkLightKit.h>

/*****************************************************************************/

MsComplexDataManager::MsComplexDataManager(){}

/*---------------------------------------------------------------------------*/
void MsComplexDataManager::create()
{
  m_ren       = vtkSmartPointer<vtkRenderer>::New();
  m_clip_box  = vtkSmartPointer<vtkBox>::New();  

  VTK_CREATE(vtkLightKit,lightkit);
  lightkit->AddLightsToRenderer(m_ren);
  m_ren->SetBackground(1,1,1);
  m_clip_box->SetBounds(0,100,0,100,0,100);

  //TODO: Find appropriate place for this class
  pymsc::init();
}
/*---------------------------------------------------------------------------*/
void MsComplexDataManager::clear()
{
  m_msc.reset();
  m_func.clear();
  m_msc_py = boost::python::object();
  m_cc_geom.reset();
  m_tcc.reset();

  m_domain_ds= NULL;
  m_pvert_idx_map.clear();
}
/*---------------------------------------------------------------------------*/
void MsComplexDataManager::load_ts(std::string ts_file, int fcomp, int gconv)
{
  clear();
  read_ts(ts_file,fcomp);
  QString msc_cfile((utl::strip_extn(ts_file,".ts")+".graph.bin").c_str());
  compute_msc(msc_cfile.toStdString(),gconv);
  finish_load_ts();
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::load_raw(std::string raw_file, int X, int Y, int Z,
                                    vert_t lc, vert_t uc, int gconv)
{
  clear();
  read_raw(raw_file,X,Y,Z,lc,uc);
  QString msc_cfile((utl::strip_extn(raw_file,".raw")+".graph.bin").c_str());
  compute_msc(msc_cfile.toStdString(),gconv);
  finish_load_raw(X,Y,Z);
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::finish_load_ts()
{

  emit statusMessageEvent(tr("Creating vtkUnstructuredGrid reprsentation"));

  // Create an unstructured grid to hold the triangulation  

  int nv = tcc()->num_dcells(0);

  for(int i = 0,c = tcc()->num_upto_dcells(2); i<tcc()->num_dcells(3);++i,++c)
  {
    ptet_cc_t::ptet_t t = tcc()->get_pcell<4>(c);

    for(int j = 0 ; j <4; ++j)
    {
      if(!t[j].is_org_dom())
        m_pvert_idx_map.insert(std::make_pair(t[j],nv+m_pvert_idx_map.size()));
    }
  }

  // create the set of points
  VTK_CREATE(vtkPoints,points);
  points->SetNumberOfPoints(tcc()->num_dcells(0)+m_pvert_idx_map.size());

  // and function value at the points
  VTK_CREATE(vtkFloatArray,scalars);
  scalars->SetNumberOfComponents(2);
  scalars->SetName("FnD");
  scalars->SetNumberOfTuples(tcc()->num_dcells(0)+m_pvert_idx_map.size());

  for(int i = 0 ; i < tcc()->num_dcells(0); ++i)
  {
    vert_t v = tcc()->get_centroid<1>(i);
    points->SetPoint(i,v[0],v[1],v[2]);
    scalars->SetTuple2(i,m_func.at(i),0.0);
  }

  BOOST_FOREACH(pvrt_to_int_t::value_type pr,m_pvert_idx_map)
  {
    vert_t v = tcc()->get_vert(pr.first);
    points->InsertPoint(pr.second,v[0],v[1],v[2]);
    scalars->SetTuple2(pr.second,m_func.at(pr.first.cid),0);
  }

  //an unstructured tet mesh to hold the tetras
  VTK_CREATE(vtkUnstructuredGrid,usgrid);
  usgrid->SetPoints(points);
  usgrid->GetPointData()->SetScalars(scalars);

  usgrid->Allocate(tcc()->num_dcells(3));

  for(int i = 0,c = tcc()->num_upto_dcells(2); i<tcc()->num_dcells(3);++i,++c)
  {
    ptet_cc_t::ptet_t ptet = tcc()->get_pcell<4>(c);

    vtkIdType tetra[] = {-1,-1,-1,-1};

    for(int j = 0 ; j <4; ++j)
    {
      ASSERT(ptet[j].is_org_dom()|| m_pvert_idx_map.count(ptet[j]) == 1);
      tetra[j] = (ptet[j].is_org_dom())?(ptet[j].cid):(m_pvert_idx_map[ptet[j]]);
    }
    usgrid->InsertNextCell(VTK_TETRA,4,tetra);
  }

  m_domain_ds = usgrid;
  m_func.clear();
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::finish_load_raw(int X, int Y, int Z)
{
  VTK_CREATE(vtkFloatArray,scalars);

  scalars->SetNumberOfTuples(m_func.size());
  scalars->SetNumberOfComponents(1);
  scalars->SetName("F");

  for(int i = 0 ; i < m_func.size() ; ++i)
    scalars->SetComponent(i,0,m_func[i]);

  VTK_CREATE(vtkImageData,img);

  utl::dvec3 lc  = m_cc_geom->get_lc();
  utl::dvec3 spc = (m_cc_geom->get_uc() - lc)/utl::mk_vec<double>(X,Y,Z);
  img = vtkSmartPointer<vtkImageData>::New();
  img->SetDimensions(X,Y,Z);
  img->SetOrigin(lc.m_elems);
  img->SetSpacing(spc.m_elems);
  img->GetPointData()->SetScalars(scalars);

  m_domain_ds = img;
  m_func.clear();
}


/*---------------------------------------------------------------------------*/

//void MsComplexDataManager::compute_arc_merge_graph()
//{
//  for(int i = 0 ; i < m_msc->m_canc_list.size(); ++i)
//  {
//    int p = m_msc->m_canc_list[i].first;
//    int q = m_msc->m_canc_list[i].second;

//    if(m_msc->index(p) < m_msc->index(q))
//      std::swap(p,q);

//    BOOST_FOREACH(int pd, m_msc->m_des_conn[p]|badpt::map_keys)
//    {
//      if(pd  == q)
//        continue;

//      BOOST_FOREACH(int qa, m_msc->m_asc_conn[q]|badpt::map_keys)
//      {
//        if(qa  == p)
//          continue;

//        m_arc_merge_dag[int_pair_t(qa,pd)].push_back(int_pair_t(qa,q));
//        m_arc_merge_dag[int_pair_t(qa,pd)].push_back(int_pair_t(q,p));
//        m_arc_merge_dag[int_pair_t(qa,pd)].push_back(int_pair_t(p,pd));
//      }
//    }
//  }
//}

///*---------------------------------------------------------------------------*/

void MsComplexDataManager::read_ts(std::string ts_file,int fcomp)
{
  QString tscache_file = (ts_file +".bincache").c_str();

  ifstream ifs(tscache_file.toStdString().c_str(),ios::in|ios::binary);

  QFileInfo cinfo(tscache_file);
  QFileInfo tsinfo(ts_file.c_str());

  if(cinfo.exists() && cinfo.lastModified() > tsinfo.lastModified())
  {
    emit statusMessageEvent(tr("Reading triangulation from cached data"));
    utl::bin_read_vec(ifs,m_func);
    m_tcc.reset(new ptet_geom_cc_t(ifs));
  }
  else
  {
    tet_cc_t::tet_list_t tets;
    ptet_cc_t::tet_pdomain_list_t ptets;
    vert_list_t verts;
    vert_t      dom_sz;

    emit statusMessageEvent(tr("Reading Triangulation."));

    tet::read_ts(ts_file,&tets,fcomp,&m_func,&verts,&ptets,&dom_sz);

    emit statusMessageEvent(tr("Setting up Triangulation"));

    if(ptets.size() == 0)
      ptets.resize(tets.size(),utl::mk_uvec(0,0,0,0));

    m_tcc.reset(new ptet_geom_cc_t(tets,ptets,verts,dom_sz));

    ofstream ofs(tscache_file.toStdString().c_str(),ios::out|ios::binary);

    if(ofs)
    {
      emit statusMessageEvent(tr("Caching Triangulation for future loads"));
      utl::bin_write_vec(ofs,m_func);
      m_tcc->save(ofs);
    }
  }

  m_cc_geom = m_tcc;
}

///*---------------------------------------------------------------------------*/

void MsComplexDataManager::read_raw(std::string raw_file, int X, int Y, int Z,
                                    vert_t lc,vert_t uc)
{
  cout<<"Reading Raw file data"<<endl;

  ifstream ifs(raw_file.c_str(),ios::in|ios::binary);
  ENSURE(ifs.is_open(),"Unable to open file");
  m_func.resize(X*Y*Z);
  ifs.read((char*)(void*)m_func.data(),sizeof(fn_t)*X*Y*Z);

  tet::cube_cc_ptr_t ccc(new cube_cc_t());
  ccc->init(X,Y,Z,lc,uc);
  m_cc_geom = ccc;
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::compute_msc(std::string msc_cache_file, int gconv)
{
  if(!m_cc_geom)
    throw std::runtime_error("Tcc Not loaded");

  if(m_func.size() != m_cc_geom->num_dcells(0))
    throw std::runtime_error("Func Not Loaded");

  // Create a python type instance
  m_msc_py  = pymsc::g_mscomplex_py_pytype();
  m_msc     = pymsc::to_cpp<mscomplex_t>(m_msc_py);

  // Load from cache file if it exists
  QFileInfo cfinfo(msc_cache_file.c_str());
  if(cfinfo.exists())
  {
    emit statusMessageEvent(tr("Loading MsComplex"));
    m_msc->load(msc_cache_file);
    compute_2sad_unique_max_hver();
    return;
  }

  emit statusMessageEvent(tr("Computing Morse Smale complex."));

  dataset_ptr_t ds(new dataset_t(m_func,m_cc_geom));

  if(gconv == ASC)
    for(int i = 0 ; i < m_func.size(); ++i)
      m_func[i] *= -1.0;

  ds->work(m_msc);

  m_msc->simplify_pers(0);
  m_msc->collect_mfolds(ds);
  m_msc->simplify_pers(1);

  if(gconv == ASC)
  {
    m_msc->flip_gradient_convention();
    for(int i = 0 ; i < m_func.size(); ++i)
      m_func[i] *= -1.0;
  }

  QFileInfo d_fi(cfinfo.dir().path());

  if(d_fi.isWritable())
  {
    emit statusMessageEvent(tr("Caching Computed MS complex."));
    m_msc->save(msc_cache_file);
  }

  compute_2sad_unique_max_hver();
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::compute_2sad_unique_max_hver()
{
  int cur_hver = m_msc->get_hversion();

  m_2sad_unique_max_hver.clear();
  m_2sad_unique_max_hver.resize(m_msc->get_num_critpts(),-2);

  m_msc->set_hversion(0);

  BOOST_FOREACH(int cp, m_msc->cpno_range()
                |badpt::filtered(bind(&mscomplex_t::is_index_i_cp,m_msc,2,_1)))
  {
    if(m_msc->m_asc_conn[cp].size() == 1)
      m_2sad_unique_max_hver[cp] = -1;
  }


  for(int i = 1 ; i <= m_msc->m_canc_list.size(); ++i)
  {
    int_pair_t pr =  m_msc->m_canc_list[i-1];

    int _2sad = (m_msc->index(pr.first) == 2)?(pr.first):(pr.second);

    if(m_msc->index(_2sad) != 2 )
      continue;

    int _oth  = (m_msc->index(pr.first) == 2)?(pr.second):(pr.first);
    eGDIR dir = (m_msc->index(_oth) == 3)?(DES):(ASC);

    if(m_2sad_unique_max_hver[_2sad] == -2)
    {
      ENSURES(m_msc->m_asc_conn[_2sad].size() == 2);
      m_2sad_unique_max_hver[_2sad] = i-1;
    }

    m_msc->set_hversion(i);

    BOOST_FOREACH(int _o2sad, m_msc->m_conn[dir][_oth]|badpt::map_keys)
    {
      if(m_msc->m_asc_conn[_o2sad].size() == 1 &&
          m_2sad_unique_max_hver[_o2sad] == -2)
        m_2sad_unique_max_hver[_o2sad] = i-1;
    }
  }

  BOOST_FOREACH(int cp, m_msc->cpno_range()
                |badpt::filtered(bind(&mscomplex_t::is_not_canceled,m_msc,_1))
                |badpt::filtered(bind(&mscomplex_t::is_index_i_cp,m_msc,2,_1)))
  {
    if(m_2sad_unique_max_hver[cp] == -2)
    {
      ENSURES(m_msc->m_asc_conn[cp].size() == 2);
      m_2sad_unique_max_hver[cp] = m_msc->m_canc_list.size();
    }
  }


//  int_pair_list_t hver_2sad_list;

//  BOOST_FOREACH(int cp, m_msc->cpno_range()
//                |badpt::filtered(bind(&mscomplex_t::is_index_i_cp,m_msc,2,_1)))
//  {
//    ASSERTS(m_2sad_unique_max_hver[cp] != -2) << SVAR(cp);
//    if(m_2sad_unique_max_hver[cp] != -1)
//    {
//      hver_2sad_list.push_back(std::make_pair(m_2sad_unique_max_hver[cp],cp));
//    }
//  }

//  br::sort(hver_2sad_list);

//  BOOST_FOREACH(int_pair_t pr, hver_2sad_list)
//  {
//    int hver = pr.first,cp = pr.second;

//    m_msc->set_hversion(hver);
//    ASSERTS(m_msc->m_asc_conn[cp].size() == 2)
//        << SVAR(cp)
//        << SVAR(m_2sad_unique_max_hver[cp])
//        <<SVAR(m_msc->m_asc_conn[cp].size())
//        << SVAR(m_msc->hversion(cp))
//        <<SVAR(m_msc->m_canc_list.size());
//  }


  m_msc->set_hversion(cur_hver);
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::set_res(int res)
{
  ENSURE_OR_RETURN(is_in_range(res,0,m_msc->m_canc_list.size()+1))<<"invalid res";

  if(m_msc->get_hversion() != res)
  {
    m_msc->set_hversion(res);
    m_msc_res_MTime.Modified();
  }
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::set_thresh(double t)
{
  set_res(get_res_for_thresh(t));
}

/*---------------------------------------------------------------------------*/

bool is_tresh_lt_pers(const mscomplex_t &msc,fn_t t, int_pair_t pr)
{return t<std::abs<fn_t>(msc.fn(pr.first)-msc.fn(pr.second));}

int MsComplexDataManager::get_res_for_thresh(double thresh) const
{
  BOOST_AUTO(cmp,bind(is_tresh_lt_pers,boost::cref(*m_msc),_1,_2));

  fn_t tresh = thresh*(m_msc->m_fmax - m_msc->m_fmin);

  return int(br::upper_bound(m_msc->m_canc_list,tresh,cmp)
             - m_msc->m_canc_list.begin());
}
/*---------------------------------------------------------------------------*/

int MsComplexDataManager::get_res() const
{
  return m_msc->get_hversion();
}

/*---------------------------------------------------------------------------*/

void MsComplexDataManager::getScalarRange(double rng[2]) const
{
  domain_ds()->GetPointData()->GetScalars()->GetRange(rng,0);
}

/*---------------------------------------------------------------------------*/

#define dCLAMP(v,b,e) (std::min<double>(std::max<double>(v,b),e))

void MsComplexDataManager::rangeSelect2Saddles
(double smin, double smax, double mmin, double mmax,
 tet::int_pair_list_t &sad_hver_list)
{
  int cur_hver = m_msc->get_hversion();

  smin = dCLAMP(smin,m_msc->m_fmin,m_msc->m_fmax);
  smax = dCLAMP(smax,m_msc->m_fmin,m_msc->m_fmax);
  mmin = dCLAMP(mmin,m_msc->m_fmin,m_msc->m_fmax);
  mmax = dCLAMP(mmax,m_msc->m_fmin,m_msc->m_fmax);

  int_pair_list_t hver_sad_list;

  BOOST_FOREACH(int cp, m_msc->cpno_range()
                |badpt::filtered(bind(&mscomplex_t::is_not_canceled,m_msc,_1))
                |badpt::filtered(bind(&mscomplex_t::is_index_i_cp,m_msc,2,_1)))
  {

    fn_t fv = m_msc->fn(cp);

    if(!is_in_range(fv,smin,smax))
      continue;

    int hver_a = m_2sad_unique_max_hver[cp];

    ENSURES(hver_a != -2) <<"Something stinks";

    if(hver_a < cur_hver)
      continue;

    int hver_b = m_msc->hversion(cp);
    int hver_c = m_msc->get_hversion_pers(mmax - fv ,false);
    int hver   = std::min(std::min(hver_a,hver_b),hver_c);

    hver_sad_list.push_back(std::make_pair(hver,cp));
  }

  br::sort(hver_sad_list);

  BOOST_FOREACH(int_pair_t pr,hver_sad_list)
  {
    int hver = pr.first;
    int cp   = pr.second;

    m_msc->set_hversion(hver);

    ENSURES(m_msc->m_asc_conn[cp].size() == 2) << "Something stinks !!";

    int m1 = m_msc->m_asc_conn[cp].begin()->first;
    int m2 = (++m_msc->m_asc_conn[cp].begin())->first;

    fn_t fv_m0 = m_msc->fn(m1);
    fn_t fv_m1 = m_msc->fn(m2);

    if(!(is_in_range(fv_m0,mmin,mmax)))
      continue;

    if(!(is_in_range(fv_m1,mmin,mmax)))
      continue;

    sad_hver_list.push_back(std::make_pair(cp,hver));
  }

  m_msc->set_hversion(cur_hver);

  br::reverse(sad_hver_list);
}


/*****************************************************************************/
