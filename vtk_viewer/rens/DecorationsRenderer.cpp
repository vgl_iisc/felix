#include <vtkActor.h>
#include <vtkCubeSource.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkAxesActor.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkCaptionActor2D.h>
#include <vtkBox.h>
#include <vtkRenderWindow.h>
#include <vtkCamera.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>

#include <tet_cc.hpp>

#include <MsComplexDataManager.hpp>
#include <rens/DecorationsRenderer.hpp>


#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

#define DEF_COLOR(__name,__R,__G,__B) \
  const unsigned char ub_ ## __name [] = {(__R),(__G),(__B),255};\
  double d_ ## __name[] = {double(__R)/255.0,double(__G)/255.0,\
                           double(__B)/255.0,1.0};
DEF_COLOR(red,        255,  0,  0)
DEF_COLOR(green,        0,255,  0)
DEF_COLOR(blue,         0,  0,255)
DEF_COLOR(yellow,     255,255,  0)
DEF_COLOR(cyan,         0,255,255)
DEF_COLOR(magenta,    255,  0,255)
DEF_COLOR(gray,        32, 32, 32)
DEF_COLOR(brown,      150, 75,  0)
DEF_COLOR(teal,         0,128,128)
DEF_COLOR(maroon,     128,0  ,0)
DEF_COLOR(purple,     128,0  ,128)
DEF_COLOR(olive,      128,128,  0)
DEF_COLOR(navy_blue,     0, 0,128)


double * d_color_list [] ={
  d_magenta,
  d_brown,
  d_gray,
  d_maroon,
  d_purple,
  d_olive,
  d_navy_blue,
  d_teal,
  d_cyan,
};

extern const int d_color_list_size = 9;

/*****************************************************************************/

DecorationsRenderer::DecorationsRenderer(){}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::create(mscdmgr_cptr_t mscdmgr)
{
  m_dmgr = mscdmgr;

  VTK_CREATE(vtkCubeSource,cubesource);

  VTK_CREATE(vtkPolyDataMapper,cubemapper);
  cubemapper->SetInputConnection(cubesource->GetOutputPort());

  VTK_CREATE(vtkActor,cubeactor);
  cubeactor->SetMapper(cubemapper);

  m_geom_clipbox_actor      = cubeactor;
  m_geom_clipbox_cubesource = cubesource;

  m_geom_clipbox_actor->GetProperty()->SetColor(0,0,0);
  m_geom_clipbox_actor->GetProperty()->SetRepresentationToWireframe();
  m_geom_clipbox_actor->GetProperty()->SetLineWidth(3);
  m_geom_clipbox_actor->GetProperty()->SetLighting(false);

  m_dmgr->ren()->AddActor(m_geom_clipbox_actor);

  // Axes actor

  VTK_CREATE(vtkAxesActor,axes);
  VTK_CREATE(vtkOrientationMarkerWidget,om_widget );

  om_widget->SetOutlineColor( 0.9300, 0.5700, 0.1300 );
  om_widget->SetOrientationMarker( axes );
  om_widget->SetInteractor(m_dmgr->ren()->GetRenderWindow()->GetInteractor());
  om_widget->SetViewport( 0.0, 0.0, 0.2, 0.2 );
  om_widget->SetEnabled(true);
  axes->GetXAxisCaptionActor2D()->SetCaption("");
  axes->GetYAxisCaptionActor2D()->SetCaption("");
  axes->GetZAxisCaptionActor2D()->SetCaption("");
//  om_widget->InteractiveOn();

//  vtkTextProperty* tx_prop = vtkTextProperty::New();
//  tx_prop->SetFontFamilyToArial();
//  tx_prop->SetFontSize( 12 );

//  tx_prop->SetColor( 1.0, 0.0, 0.0 );
//  axes->GetXAxisCaptionActor2D()->SetCaptionTextProperty(tx_prop);
//  tx_prop->SetColor( 0.0, 1.0, 0.0 );
//  axes->GetYAxisCaptionActor2D()->SetCaptionTextProperty(tx_prop);
//  tx_prop->SetColor( 0.0, 0.0, 1.0 );
//  axes->GetZAxisCaptionActor2D()->SetCaptionTextProperty(tx_prop);
//  tx_prop->Delete();

  m_orientation_marker = om_widget;
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::update()
{
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::set_clipbox_enabled(bool val)
{
  if(val)
    m_dmgr->ren()->AddViewProp(m_geom_clipbox_actor);
  else
    m_dmgr->ren()->RemoveViewProp(m_geom_clipbox_actor);
}

/*---------------------------------------------------------------------------*/
void DecorationsRenderer::set_clipbox(double bounds[])
{
  m_geom_clipbox_cubesource->SetBounds(bounds);
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::get_clipbox(double bounds[]) const
{
  double center[3];
  m_geom_clipbox_cubesource->GetCenter(center);
  double x = m_geom_clipbox_cubesource->GetXLength()/2;
  double y = m_geom_clipbox_cubesource->GetYLength()/2;
  double z = m_geom_clipbox_cubesource->GetZLength()/2;

  bounds[0] = center[0] - x; bounds[1] = center[0] + x;
  bounds[2] = center[1] - y; bounds[3] = center[1] + y;
  bounds[4] = center[2] - z; bounds[5] = center[2] + z;
}

/*---------------------------------------------------------------------------*/

vtkTimeStamp DecorationsRenderer::get_clipbox_mtime() const
{
  m_geom_clipbox_cubesource->GetMTime();
}

/*---------------------------------------------------------------------------*/
void DecorationsRenderer::set_orientation_marker_enabled(bool val)
{
  m_orientation_marker->SetEnabled(val);
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::set_background_color(double r,double g,double b)
{
  m_dmgr->ren()->SetBackground(r,g,b);
  m_geom_clipbox_actor->GetProperty()->SetColor(1-r,1-g,1-b);
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::save_camera(QString  fn) const
{
  QFile f(fn);
  QTextStream ts(&f);

  if(!f.open(QIODevice::WriteOnly))
  {
    qWarning()<<"Unable to open file!!"<<fn;
    return;
  }

  vtkCamera * cam = m_dmgr->ren()->GetActiveCamera();

  double a,b,c;

  cam->GetPosition(a,b,c);
  ts<<a<<"\t"<<b<<"\t"<<c<<"\n";
  cam->GetFocalPoint(a,b,c);
  ts<<a<<"\t"<<b<<"\t"<<c<<"\n";
  cam->GetViewUp(a,b,c);
  ts<<a<<"\t"<<b<<"\t"<<c<<"\n";
  a = cam->GetViewAngle();
  ts<<a<<"\n";
  cam->GetClippingRange(a,b);
  ts<<a<<"\t"<<b<<"\n";
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::load_camera(QString  fn)
{
  QFile f(fn);
  QTextStream ts(&f);

  if(!f.open(QIODevice::ReadOnly))
  {
    qWarning()<<"Unable to open file!!"<<fn;
    return;
  }

  vtkCamera * cam = m_dmgr->ren()->GetActiveCamera();

  double a,b,c;

  ts>>a>>b>>c;
  cam->SetPosition(a,b,c);
  ts>>a>>b>>c;
  cam->SetFocalPoint(a,b,c);
  ts>>a>>b>>c;
  cam->SetViewUp(a,b,c);
  ts>>a;
  cam->SetViewAngle(a);
  ts>>a>>b;
  cam->SetClippingRange(a,b);

  m_dmgr->ren()->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::save_screenshot(QString  fn,int mag)
{
  VTK_CREATE(vtkWindowToImageFilter,windowToImageFilter);

  if(!fn.endsWith(".png",Qt::CaseInsensitive))
    fn += ".png";

  windowToImageFilter->SetInput(m_dmgr->ren()->GetRenderWindow());
  windowToImageFilter->SetMagnification(mag);
  windowToImageFilter->SetInputBufferTypeToRGBA();
  windowToImageFilter->Update();

  VTK_CREATE(vtkPNGWriter,writer);
  writer->SetFileName(fn.toStdString().c_str());
  writer->SetInputConnection(windowToImageFilter->GetOutputPort());
  writer->Write();

  // The rendered view gets messed up if you use magnifications
  m_dmgr->ren()->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

void DecorationsRenderer::save_screenshot()
{
  QString f = QFileDialog::getSaveFileName(NULL,"ScreenShot","Pngs (*.png )");

  if(f.size())
    save_screenshot(f);
}


/*****************************************************************************/
