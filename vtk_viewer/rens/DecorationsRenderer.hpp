#ifndef __DECORATIONSRENDERER_INCLUDED__
#define __DECORATIONSRENDERER_INCLUDED__
#include <vtkSmartPointer.h>
#include <vtkTimeStamp.h>

#include <QObject>

#include <tet.hpp>


class vtkActor;
class vtkCubeSource;
class vtkOrientationMarkerWidget;

class MsComplexDataManager;
typedef boost::shared_ptr<const MsComplexDataManager> mscdmgr_cptr_t;


/*****************************************************************************/


/// \brief Class to manage the rendering Critical points.
///
/// All public slots are callable.
class DecorationsRenderer:public QObject
{
  Q_OBJECT
public:
  /// Constructor
  DecorationsRenderer();

//  /// Destructor
//  ~DecorationsRenderer();

public:
  /// \brief Create the renderer
  void create( mscdmgr_cptr_t mscdmgr);

  /// \brief Update the rendering
  void update();

public slots:

  /// \brief Enable/Disable clipbox rendering (default on)
  void set_clipbox_enabled(bool val);

  /// \brief Enable/Disable orientation marker rendering (default off)
  void set_orientation_marker_enabled(bool val);
  /// \brief set geom-view background color
  void set_background_color(double r,double g,double b);

  /// \brief Save the current camera view to a file
  /// \param[in] fn fname
  void save_camera(QString  fn) const;
  /// \brief load the camera view from a file
  /// \param[in] fn fname
  void load_camera(QString  fn);

  /// \brief Save a Screenshot (only png format)
  /// \param[in] fn fname
  /// \param[in] mag magnification
  void save_screenshot(QString  fn,int mag=1);
  /// \brief Save a Screenshot (only png format) .. pops a dialog for filename
  void save_screenshot();

public:
  /// \brief Set clipbox
  void set_clipbox(double bounds[6]);

  /// \brief Get clipbox
  void get_clipbox(double bounds[6]) const;

  /// \brief clipbox Mtime
  vtkTimeStamp get_clipbox_mtime() const;


private:
  /// \privatesection
  mscdmgr_cptr_t                        m_dmgr;

  vtkSmartPointer<vtkActor>       m_geom_clipbox_actor;
  vtkSmartPointer<vtkCubeSource>  m_geom_clipbox_cubesource;

  vtkSmartPointer<vtkOrientationMarkerWidget> m_orientation_marker;

};

/*****************************************************************************/
#endif
