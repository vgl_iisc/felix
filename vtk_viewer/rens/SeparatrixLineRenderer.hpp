#ifndef __SEPERATRIXLINERENDERER_INCLUDED__
#define __SEPERATRIXLINERENDERER_INCLUDED__
#include <QObject>

#include <vtkSmartPointer.h>
#include <vtkTimeStamp.h>

#include <tet.hpp>

class QThread;

class vtkRenderer;
class vtkPolyData;
class vtkPolyDataAlgorithm;
class vtkActor;
class vtkBox;
class vtkUnstructuredGrid;
class vtkPolyDataMapper;
class vtkScalarBarWidget;
class vtkColorTransferFunction;

struct TransferFunctionPoint;
typedef QList<TransferFunctionPoint> TransferFunction;
class MsComplexDataManager;
typedef boost::shared_ptr<const MsComplexDataManager> mscdmgr_cptr_t;


/*===========================================================================*/

/// \brief Class to manage the rendering Separatrix lines.
///
/// All public slots are callable.
class SeparatrixLineRenderer:public QObject
{
  Q_OBJECT
public:
  /// Constructor
  SeparatrixLineRenderer();

//  /// Destructor
//  ~SeparatrixLineRenderer();

public:
  /// \brief Create the renderer
  void create( mscdmgr_cptr_t mscdmgr);

  /// \brief Set list of pieces of two saddles
  inline void set_geom_piece_cps(std::vector<int> & pcs )
  {
    m_pcs.resize(pcs.size());
    br::copy(pcs,m_pcs.begin());
    m_pcs_MTime.Modified();
  }

  /// \brief Get Time when pcs were last updated
  inline vtkTimeStamp get_geom_piece_cps_MTime(){return m_pcs_MTime;}

  /// \brief Update the rendering
  void update();

public slots:

  /// \brief Enable/Disable rendering
  void set_enabled(bool val);

  /// \brief Disable rendering
  inline bool get_enabled(){ return m_show;}

  /// \brief Save the extracted two saddle geometry to a vtk XML file
  void save_data(QString str);

  /// \brief Load geometry externally from a vtk XML file
  ///
  /// \note  Data will only exist till no new data is pushed
  ///        using set_geom_piece_cps and updated.
  void load_data(QString str);





  /// \brief Enable/Disable rendering of two-saddle asc mfolds as tubes
  void set_tubes_enabled(bool val);

  /// \brief Set the tubes geometry properties
  ///
  /// \param[in] r      Radius of tube
  /// \param[in] ns     Number of sides in tube polygon
  void set_tubes_geomprops(double r=1,int ns=6);

  /// \brief Set the parameters for smoothing 2asc lines/tubes
  ///
  /// \param[in] c      Convergence value for smoothing ..in [0,1] .. smaller the better
  /// \param[in] ni     Number of iterations to smooth the geometry
  void set_smoother_params(double conv,int niter);





  /// \brief Set RGB color of geometry .. Disables tf
  void set_color(double r,double g,double b);

  /// \brief Set line width
  void set_line_width(double w);

  /// \brief Enabling coloring using a tf on density
  void set_tf_enabled(bool val);

  /// \brief Save tranferfunction to file
  bool save_tf(const QString &) const;

  /// \brief Load Transfer function from file .. Enables tf if not enabled
  bool load_tf(const QString &);

  /// \brief Edit transfer function .. Enables tf if tf edited and not enabled
  void edit_tf();

  /// \brief Set RGB color of geometry .. Disables tf
  void set_color(double dr,double dg,double db,
                 double ar,double ag,double ab,
                 double sr,double sg,double sb,
                 double d, double a, double s,
                 double sp);





  /// \brief Show the particles near the selected geometry
  ///
  /// \param[in] dist       particles within distance (-1 clears the data)
  inline void set_particles_dist(double dist)
  {if(m_particles_dist != dist)
    {m_particles_dist = dist;m_particles_dist_MTime.Modified();}}

  /// \brief Enable particle rendering .. (make sure a correct dist is set first)
  void set_particles_enabled(bool val);

  /// \brief Get Particle rendering states
  inline bool get_particles_enabled(){ return m_particles_show;}

  /// \brief Save distances of all particles to currently selected geometry
  /// \param[in] fn   File Name
  void save_particles_distances(QString fn);

  /// \brief Set the viewing properties
  /// \param[in] r,g,b    color of rendered particles
  /// \param[in] pt_size  size of rendered particles
  void set_particles_viewprops(double r,double g,double b,double pt_size);



  /// \brief Enable/Disable to color bar .. its shown only if goem is being shown
  void set_colorbar_enabled(bool);

public:
  /// \privatesection

  mscdmgr_cptr_t                              m_dmgr;


  vtkSmartPointer<vtkPolyData>                m_polydata;
  vtkSmartPointer<vtkActor>                   m_actor;  
  vtkSmartPointer<vtkPolyDataMapper>          m_mapper;
  vtkTimeStamp                                m_MTime;
  bool                                        m_show;
  boost::shared_ptr<TransferFunction>         m_tf;
  vtkSmartPointer<vtkScalarBarWidget>         m_colorbar;
  bool                                        m_colorbar_show;
  vtkSmartPointer<vtkColorTransferFunction>   m_vtk_tf;

  std::vector<int>   m_pcs;
  vtkTimeStamp       m_pcs_MTime;

  double                                      m_particles_dist;
  vtkTimeStamp                                m_particles_dist_MTime;
  vtkSmartPointer<vtkUnstructuredGrid>        m_particles_usgrid;
  vtkSmartPointer<vtkActor>                   m_particles_actor;
  bool                                        m_particles_show;

  vtkSmartPointer<vtkPolyDataAlgorithm>       m_striper;
  vtkSmartPointer<vtkPolyDataAlgorithm>       m_smoother;
  vtkSmartPointer<vtkPolyDataAlgorithm>       m_tangents;
  vtkSmartPointer<vtkPolyDataAlgorithm>       m_tuber;

signals:
  void setEnabledEvent(bool );
  void statusMessageEvent(const QString &);

private:
  // update subparts
  void update_lines();
  void update_particles();
  void update_tf();

};

/*===========================================================================*/




/*===========================================================================*/

class Ui_SeparatrixLineSelector;

#include <QFrame>


class SeparatrixLineSelector: public QFrame
{
  Q_OBJECT

public:
  /// \brief ctor
  SeparatrixLineSelector(QWidget *par=0);

  /// \brief update
  void create(mscdmgr_cptr_t dmgr);

  /// \brief update
  void update();

public slots:
  /// \brief on ranges changed
  void set_selection_ranges(double smin, double smax, double mmin, double mmax);

private slots:
  /// \brief pushbutton clicked .. perform selection
  void on_pb_select_clicked(bool clicked);

  /// \brief clear selection clicked
  void on_pb_clear_clicked(bool clicked);

signals:
  /// \brief User range selected cps
  void rangeSelected2Saddles(double smin, double smax, double mmin, double mmax);

  /// \brief User clicked clear
  void clearedCpSelection();


private:
  Ui_SeparatrixLineSelector     *m_ui;
  mscdmgr_cptr_t                 m_dmgr;


};

/*===========================================================================*/
#endif
