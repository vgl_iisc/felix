#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPiecewiseFunction.h>
#include <vtkColorTransferFunction.h>
#include <vtkVolumeProperty.h>
#include <vtkVolume.h>
#include <vtkExtractPolyDataGeometry.h>
#include <vtkExtractGeometry.h>
#include <vtkRenderWindow.h>
#include <vtkPointData.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCommand.h>
#include <vtkBox.h>
#include <vtkRenderer.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkScalarBarWidget.h>
#include <vtkScalarBarActor.h>
#include <vtkScalarBarRepresentation.h>
#include <vtkCellData.h>
#include <vtkTextProperty.h>
#include <extn/PT/OpenGLPTVolumeMapper.hpp>
#include <vtkImageData.h>
#include <vtkSmartVolumeMapper.h>
#include <vtkClipVolume.h>

#include <QGroupBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QDebug>
#include <QFile>
#include <extn/QTransferFunctionEditorWidget.hpp>
#include <extn/QPiecewiseFunctionEditorWidget.hpp>
#include <MsComplexDataManager.hpp>

#include <tet_cc.hpp>

#include <rens/VolumeRenderer.hpp>
#include <ui_VolumeRenderer.h>

using namespace tet;

#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

#define CHECKABLE_UI_SYNCED_SETTER(VAR,VALUE,UIVAR,UIVALUE) \
  if ((UIVAR)->isChecked() != (UIVALUE) ) (UIVAR)->setChecked(UIVALUE);\
  if ((VAR) != (VALUE) ) (VAR) = (VALUE);

/*****************************************************************************/

VolumeRenderer::VolumeRenderer(QWidget *par)
  :QFrame(par),m_tf(new TransferFunction),
    m_enhancement_tf(new PiecewiseFunction),
    m_enabled(false),
    m_continuous_rendering(false),
    m_enhancement_enabled(false),
    m_enhancement_tf_type(0)
{
  m_ui = new Ui_VolumeRenderer;
  m_ui->setupUi(this);

  m_enhancement_tf_dialog = new QPiecewiseFunctionEditorDialog(this);

  connect(m_ui->volume_rendering_groupBox,SIGNAL(clicked(bool)),
          this,SLOT(set_enabled(bool)));
  connect(m_ui->continuous_rendering_CheckBox,SIGNAL(clicked(bool)),
          this,SLOT(set_continuous_rendering(bool)));
  connect(m_ui->volume_enhancement_groupBox,SIGNAL(clicked(bool)),
          this,SLOT(set_volume_enhancement(bool)));
  connect(m_ui->edit_enhancement_tf_pushButton,SIGNAL(clicked()),
          this,SLOT(edit_enhancement_tf()));
  connect(m_enhancement_tf_dialog,SIGNAL(applied()),
          this,SLOT(enhancement_tf_dialog_applied()));

//  connect(m_ui->add_enhancement_tf_radioButton,SIGNAL(clicked()),
//          this,SLOT(set_enhancement_tf_type_to_opacity_add()));
//  connect(m_ui->mul_enhancement_tf_radioButton,SIGNAL(clicked()),
//          this,SLOT(set_enhancement_tf_type_to_opacity_mul()));

}

/*---------------------------------------------------------------------------*/

bool VolumeRenderer::save_tf(const QString &fn) const
{
  return SaveTransferFunction(*m_tf,fn);
}

/*---------------------------------------------------------------------------*/

bool VolumeRenderer::load_tf(const QString &fn)
{
  TransferFunction tf;

  bool ret = LoadTransferFunction(tf,fn);

  if(ret)
  {
    qSort(tf);

    qreal m = tf.front().f;
    qreal M = tf.back().f;

    for(int i = 0 ; i < tf.size() ; ++i)
      tf[i].f = (tf[i].f - m) / (M-m);

    *m_tf = tf;

    m_ui->tf_widget->setNormalizedTransferFunction(*m_tf,m,M);
  }

  return ret;
}

/*---------------------------------------------------------------------------*/

bool VolumeRenderer::save_enhancement_tf(const QString &fn) const
{
  return SavePiecewiseFunction(*m_enhancement_tf,fn,0,
                               m_dmgr->tcc()->get_diagonal_length(),0,2);
}

/*---------------------------------------------------------------------------*/

bool VolumeRenderer::load_enhancement_tf(const QString &fn)
{
  return LoadPiecewiseFunction(*m_enhancement_tf,fn,0,
                               m_dmgr->tcc()->get_diagonal_length(),0,2);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::edit_enhancement_tf()
{
  if(!m_enhancement_tf_dialog->isVisible())
  {
    m_enhancement_tf_dialog->setPiecewiseFunction
        (*m_enhancement_tf,0,m_dmgr->tcc()->get_diagonal_length(),0,2);

    m_enhancement_tf_dialog->show();
  }
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::set_enabled(bool val)
{
  CHECKABLE_UI_SYNCED_SETTER(m_enabled,val,
                             m_ui->volume_rendering_groupBox,val);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::set_continuous_rendering(bool val)
{
  CHECKABLE_UI_SYNCED_SETTER(m_continuous_rendering,val,
                             m_ui->continuous_rendering_CheckBox,val);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::set_volume_enhancement(bool val)
{
  CHECKABLE_UI_SYNCED_SETTER(m_enhancement_enabled,val,
                             m_ui->volume_enhancement_groupBox,val);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::save_volume(const QString &fn)
{
  VTK_CREATE(vtkXMLUnstructuredGridWriter,writer);

#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(m_dmgr->domain_ds());
#else
  writer->SetInputData(m_dmgr->domain_ds());
#endif
  writer->SetFileName(fn.toStdString().c_str());
  writer->SetDataModeToAscii();
  writer->Update();
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::save_volume_with_spin(QString of,QString sf)
{
//  VTK_CREATE(vtkFloatArray,spindirs);
//  spindirs->SetNumberOfComponents(3);
//  spindirs->SetName("SpinDirs");
//  spindirs->SetNumberOfTuples(m_dmgr->tcc()->num_dcells(0) +
//                              m_dmgr->pvert_idx_map().size());

//  int nv = m_dmgr->tcc()->num_dcells(0);

//  int ns = 0;

//  QFile inputFile(sf);
//  if (!inputFile.open(QIODevice::ReadOnly))
//  {
//    qWarning()<<"File "<<sf<<" not found !!!";
//    return;
//  }

//  QTextStream ss(&inputFile);

//  double x,y,z,m;
//  utl::dvec3 s;

//  double ml = m_dmgr->tcc()->get_diagonal_length();

//  while(true)
//  {
//    ss >> x >> y >>z >>m >> s[0] >>s[1] >>s[2];

//    if (ns == nv  || ss.atEnd())
//      break;

//    if(s.norm2_sq() != 0)
//      s /= s.norm2();

//    s *= (ml - m_scalars->GetComponent(ns,1))/ml;

//    spindirs->SetTuple(ns++,s.m_elems);
//  }

//  if(ns != nv)
//  {

//    qWarning()<<"incorrect number of points in file\n"
//              <<" i  = "<<ns
//              <<" nv = "<<nv
//              <<"Nothing will be saved!!!";
//    return;
//  }

//  for(int i = 0 ; i < m_dup_pvert_map.size(); ++i)
//    spindirs->SetTuple(nv+i,spindirs->GetTuple(m_dup_pvert_map[i]));

//  m_usgrid->GetPointData()->AddArray(spindirs);

//  save_volume_grid(of);

//  m_usgrid->GetPointData()->RemoveArray(spindirs->GetName());
}


/*---------------------------------------------------------------------------*/

void   VolumeRenderer::enable_enhancement_exp_decay(double var)
{
  OpenGLPTVolumeMapper::SafeDownCast(m_volume_mapper)
      ->SetUseScalar2OpacityExpDecay(true);

  OpenGLPTVolumeMapper::SafeDownCast(m_volume_mapper)
      ->SetScalar2OpacityExpDecayVariance(var);
}

/*---------------------------------------------------------------------------*/

void   VolumeRenderer::disable_enhancement_exp()
{
  OpenGLPTVolumeMapper::SafeDownCast(m_volume_mapper)
      ->SetUseScalar2OpacityExpDecay(false);
}

/*---------------------------------------------------------------------------*/

void   VolumeRenderer::set_brightness(double v)
{
  OpenGLPTVolumeMapper::SafeDownCast(m_volume_mapper)
      ->SetBrightness(v);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::enable_colorbar()
{
  m_colorbar_widget->EnabledOn();
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::disable_colorbar()
{
  m_colorbar_widget->EnabledOff();
}

/*---------------------------------------------------------------------------*/
void VolumeRenderer::set_tf(QVariantList vl)
{
  if (vl.size() < 10)
  {
    qWarning()<<"Too few values Tf not accepted";
    return;
  }

  TransferFunction tf;

  for(int i = 0 ; i < vl.size()-4; i+=5)
  {
    bool ok[5];

    qreal  f = vl.at(i).toDouble(&ok[0]);
    qreal  r = vl.at(i+1).toDouble(&ok[1]);
    qreal  g = vl.at(i+2).toDouble(&ok[2]);
    qreal  b = vl.at(i+3).toDouble(&ok[3]);
    qreal  a = vl.at(i+4).toDouble(&ok[4]);

    if(ok[0]&&ok[1]&&ok[2]&&ok[3]&&ok[4])
      tf.push_back(TransferFunctionPoint(f,QColor::fromRgbF(r,g,b,a)));
    else
    {
      qWarning()<<"Non-numeric values passed: Tf not accepted";
      return;
    }
  }

  qSort(tf);

  qreal m = tf.front().f;
  qreal M = tf.back().f;

  if(m == M)
  {
    qWarning()<<"Too has no range:Tf not accepted";
    return;
  }

  if(m != 0 || M != 1)
  {
    for(int i = 0 ; i < tf.size(); ++i)
      tf[i].f = (tf.at(i).f - m)/ (M-m);
  }

  *m_tf = tf;

}

/*---------------------------------------------------------------------------*/

QList<double>  VolumeRenderer::get_tf()
{
  QList<double> vl;

  for(int i = 0 ; i < m_tf->size(); ++i)
  {
    vl.push_back(m_tf->at(i).f);
    vl.push_back(m_tf->at(i).col.redF());
    vl.push_back(m_tf->at(i).col.greenF());
    vl.push_back(m_tf->at(i).col.blueF());
    vl.push_back(m_tf->at(i).col.alphaF());
  }

  return vl;
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::update_enhancement_distance()
{
  vtkWeakPointer<vtkPointSet> pts = m_enhancement_points;

  vtkDataArray * scalars = m_dmgr->domain_ds()->GetPointData()->GetScalars();

  // a bit crude because scalars reprsents both the field and the enhancement
  // distance.
  if(pts->GetMTime() < scalars->GetMTime())
    return;

  emit showStatusMessage(tr("Updating enhancement distance"));

  int nv = m_dmgr->tcc()->num_dcells(0);

  for(int i = 0 ; i < nv; ++i)
  {
    utl::dvec3 pt = m_dmgr->tcc()->get_vert(i),clpt;
    vtkIdType clpt_id = pts->FindPoint(pt.m_elems);
    pts->GetPoint(clpt_id,clpt.m_elems);
    scalars->SetComponent(i,1,(pt-clpt).norm2());
  }

  typedef MsComplexDataManager::pvrt_to_int_t::value_type pvert_to_int_vt;

  BOOST_FOREACH(pvert_to_int_vt v,m_dmgr->pvert_idx_map())
    scalars->SetComponent(v.second,1,scalars->GetComponent(v.first.cid,1));

  scalars->Modified();
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::create(mscdmgr_cptr_t dmgr)
{
  m_dmgr = dmgr;

  if(m_volume)
    m_dmgr->ren()->RemoveVolume(m_volume);

  // Create transfer mapping scalar value to opacity.
  VTK_CREATE(vtkPiecewiseFunction,opacityTransferFunction);
  VTK_CREATE(vtkColorTransferFunction,colorTransferFunction);

  // The property describes how the data will look.
  VTK_CREATE(vtkVolumeProperty,volumeProperty);
  volumeProperty->SetColor(colorTransferFunction);
  volumeProperty->SetScalarOpacity(opacityTransferFunction);
  volumeProperty->ShadeOff();
  volumeProperty->SetInterpolationTypeToLinear();

  if(m_dmgr->tcc())
  {
    // A dataset clipper that clips our unstructured grid
    VTK_CREATE(vtkExtractGeometry,clipper);
#if VTK_MAJOR_VERSION <= 5
    clipper->SetInputConnection(m_dmgr->domain_ds()->GetProducerPort());
#else
    clipper->SetInputData(m_dmgr->domain_ds());
#endif
    clipper->SetImplicitFunction(m_dmgr->clip_box());

    // The mapper that renders the volume data.
    VTK_CREATE(OpenGLPTVolumeMapper,volumeMapper);
    volumeMapper->SetInputConnection(clipper->GetOutputPort());
    volumeMapper->UseGPUBuffersOn();
    m_volume_mapper = volumeMapper;
  }
  else if(m_dmgr->ccg())
  {
    // The mapper that renders the volume data.
    VTK_CREATE(vtkSmartVolumeMapper,volumeMapper);
#if VTK_MAJOR_VERSION <= 5
    volumeMapper->SetInputConnection(m_dmgr->domain_ds()->GetProducerPort());
#else
    volumeMapper->SetInputData(m_dmgr->domain_ds());
#endif
    m_volume_mapper = volumeMapper;
  }
  else
  {
    ENSURE(false,"Unknown domain type");
  }

  // The volume holds the mapper and the property and can be used to
  // position/orient the volume.
  VTK_CREATE(vtkVolume,volume);
  volume->SetMapper(m_volume_mapper);
  volume->SetProperty(volumeProperty);

  VTK_CREATE(vtkTextProperty,labelprop);
  labelprop->SetColor(0.5,0.5,0.5);
  labelprop->ShadowOff();
  labelprop->SetFontFamilyToArial();
  labelprop->SetBold(true);

  // A colorbar widget.
  VTK_CREATE(vtkScalarBarWidget,scalarWidget);
  scalarWidget->SetInteractor(m_dmgr->ren()->GetRenderWindow()->GetInteractor());
  scalarWidget->GetScalarBarActor()->SetTitle("");
  scalarWidget->GetScalarBarActor()->SetLookupTable(colorTransferFunction);
  scalarWidget->GetScalarBarActor()->SetLabelFormat("%.1f");
  scalarWidget->GetScalarBarActor()->SetLabelTextProperty(labelprop);
  scalarWidget->GetScalarBarActor()->SetNumberOfLabels(3);
  scalarWidget->EnabledOff();

  vtkScalarBarRepresentation *rep = vtkScalarBarRepresentation::SafeDownCast
      (scalarWidget->GetRepresentation());

  rep->SetPosition(.9, .25);
  rep->SetPosition2(.08, .45);

  m_volume  = volume;  
  m_colorbar_widget = scalarWidget;

  m_dmgr->ren()->GetRenderWindow()->GetInteractor()->AddObserver
      (vtkCommand::StartInteractionEvent,this,
       &VolumeRenderer::mouse_event);

  m_dmgr->ren()->GetRenderWindow()->GetInteractor()->AddObserver
      (vtkCommand::EndInteractionEvent,this,
       &VolumeRenderer::mouse_event);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::set_enhancement_points(vtkSmartPointer<vtkPointSet> pts)
{m_enhancement_points = pts;}

/*---------------------------------------------------------------------------*/

void default_tf(TransferFunction & tf)
{
  tf.clear();
  tf.push_back(TransferFunctionPoint(0,QColor::fromRgbF(0,0,0,0)));
  tf.push_back(TransferFunctionPoint(0.25,QColor::fromRgbF(0,0,0,0)));
  tf.push_back(TransferFunctionPoint(0.5,QColor::fromRgbF(1,0,0,0.33)));
  tf.push_back(TransferFunctionPoint(0.75,QColor::fromRgbF(1,1,0,0.66)));
  tf.push_back(TransferFunctionPoint(1,QColor::fromRgbF(1,1,1,1)));
}

void VolumeRenderer::update()
{
  // First things first .. Ensure that the settings are consistent.

  if(!m_enabled)
  {
    if(m_volume)
      m_dmgr->ren()->RemoveVolume(m_volume);

    return;
  }

  emit showStatusMessage(tr("Updating volume rendering info"));

  m_dmgr->ren()->AddVolume(m_volume);

  double rng[2];

  m_dmgr->getScalarRange(rng);
  m_volume_mapper->SetScalarModeToUsePointData();

  // Second update the tf.

  if(m_tf->size() == 0)
  {
    default_tf(*m_tf);
    m_ui->tf_widget->setNormalizedTransferFunction(*m_tf,rng[0],rng[1]);
  }

  vtkPiecewiseFunction * otf =
      m_volume->GetProperty()->GetScalarOpacity();

  vtkColorTransferFunction* ctf =
      m_volume->GetProperty()->GetRGBTransferFunction();

  if(!m_dmgr->tcc())
  {
    double bounds[6];
    m_dmgr->clip_box()->GetBounds(bounds);

    vtkSmartVolumeMapper::SafeDownCast(m_volume_mapper)->
        SetCroppingRegionPlanes(bounds);

    vtkSmartVolumeMapper::SafeDownCast(m_volume_mapper)->
        SetCropping(1);
  }

  ctf->RemoveAllPoints();
  otf->RemoveAllPoints();

  for( int i = 0 ; i < m_tf->size(); ++i)
  {
    double  f = m_tf->at(i).f;
    QColor  c = m_tf->at(i).col;

    f = rng[0] + f*(rng[1]- rng[0]);

    ctf->AddRGBPoint(f,c.redF(),c.greenF(),c.blueF());
    otf->AddPoint(f,c.alphaF());
  }

  otf->Modified();
  ctf->Modified();


  // Third update the enhancement related parameters.
  if(m_enhancement_enabled)
  {
    bool enhancement_points_valid =
        m_enhancement_points != NULL &&
        m_enhancement_points->GetNumberOfPoints() != 0;

    if(enhancement_points_valid)
    {
      update_enhancement_distance();

      if( m_enhancement_tf->size() == 0)
      {
        double diag = m_dmgr->tcc()->get_diagonal_length();

        m_enhancement_tf->push_back(PiecewiseFunctionPoint(0,1.0));
        m_enhancement_tf->push_back(PiecewiseFunctionPoint(diag/100.0,0));
        m_enhancement_tf->push_back(PiecewiseFunctionPoint(diag,0));
      }

      vtkPiecewiseFunction * aux_otf =
          m_volume->GetProperty()->GetScalarOpacity(1);

      aux_otf->RemoveAllPoints();

      for( int i = 0 ; i < m_enhancement_tf->size(); ++i)
      {
        aux_otf->AddPoint(m_enhancement_tf->at(i).x,
                          m_enhancement_tf->at(i).y);
      }

      aux_otf->Modified();

      m_volume->GetProperty()->IndependentComponentsOff();
    }
    else
    {
      WLOG << "Enhancement enabled without proximity ptdata!!";
      m_volume->GetProperty()->IndependentComponentsOn();
    }
  }
  else
  {
    m_volume->GetProperty()->IndependentComponentsOn();
  }


//  m_volume_mapper->SetUseAuxScalars(m_volume_enhancement);
//  m_volume_mapper->SetAuxTransferFunctionType(m_enhancement_tf_type);
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::mouse_event(vtkObject *, unsigned long e,void *)
{
  if(m_enabled && !m_continuous_rendering)
  {
    switch(e)
    {
    case vtkCommand::StartInteractionEvent:
      m_dmgr->ren()->RemoveVolume(m_volume);
      break;

    case vtkCommand::EndInteractionEvent:
      m_dmgr->ren()->AddVolume(m_volume);
      break;
    }
  }
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::on_tf_widget_applied()
{
  m_ui->tf_widget->getNormalizedTransferFunction(*m_tf);
  update();
  m_dmgr->ren()->GetRenderWindow()->Render();
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::enhancement_tf_dialog_applied()
{
  m_enhancement_tf_dialog->getPiecewiseFunction(*m_enhancement_tf);
  update();
  m_dmgr->ren()->GetRenderWindow()->Render();
}

/*****************************************************************************/


/*****************************************************************************/
// DEPRECATED STUFF .. Just around to support older scripts

#define DEPRECATION_WARNING(depreason,depaction,deprec) \
  qWarning()<<QString::fromStdString(FILEFUNCLINE)<<" has been DEPRECATED\n"\
  <<"Reason         ::"<<(depreason)<<"\n"\
  <<"Current Action ::"<<(depaction)<<"\n"\
  <<"Recommendation ::"<<(deprec)<<"\n"\


/*---------------------------------------------------------------------------*/

void VolumeRenderer::add_tfpt(double f,double r,double g,double b,double a)
{
  DEPRECATION_WARNING("Call used to call insistencis","Call Ignored",
                      "Use the newer save_tf/load_tf routines");
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::del_tfpt(int i)
{
  DEPRECATION_WARNING("Call used to call insistencis","Call Ignored",
                      "Use the newer save_tf/load_tf routines");
}

/*---------------------------------------------------------------------------*/

int  VolumeRenderer::num_tfpt() const
{
  DEPRECATION_WARNING("Call used to call insistencis","Call Ignored",
                      "Use the newer save_tf/load_tf routines");
  return -1;
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::clear_tf()
{
  DEPRECATION_WARNING("Call used to call insistencis","Call Ignored",
                      "Use the newer save_tf/load_tf routines");
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::set_enhancement_tf_type_to_opacity_add()
{
  DEPRECATION_WARNING("Implementaion problems","Call Ignored","Nones");
}

/*---------------------------------------------------------------------------*/

void VolumeRenderer::set_enhancement_tf_type_to_opacity_mul()
{
  DEPRECATION_WARNING("Implementaion problems","Call Ignored","Nones");
}

/*---------------------------------------------------------------------------*/

QString VolumeRenderer::get_enhancement_tf_type()
{
  DEPRECATION_WARNING("Implementaion problems","Call Ignored","Nones");
  return "Invalid State";
}

/*****************************************************************************/
