#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkClipPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkStripper.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkPointData.h>
#include <vtkTubeFilter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkCleanPolyData.h>
#include <vtkBox.h>
#include <vtkCellArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkGlyphSource2D.h>
#include <vtkGlyph3D.h>
#include <vtkRenderer.h>
#include <vtkExtractGeometry.h>
#include <vtkFloatArray.h>
#include <vtkColorTransferFunction.h>
#include <extn/vtkPolyLineTangents.h>
#include <vtkScalarBarWidget.h>
#include <vtkScalarBarRepresentation.h>
#include <vtkTextProperty.h>
#include <vtkRenderWindow.h>
#include <vtkScalarBarActor.h>
#include <vtkImageData.h>

#include <extn/QTransferFunctionEditorWidget.hpp>

#include <tet_cc.hpp>
#include <tet_mscomplex.hpp>

#include <rens/SeparatrixLineRenderer.hpp>
#include <MsComplexDataManager.hpp>

#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

#define VTK_NEW(variable,class)\
  variable = vtkSmartPointer<class>::New();

using namespace tet;
using namespace std;

typedef std::map<cellid_t,int>  cellid_to_int_t;
typedef std::map<ptet_cc_t::pcellid_t,int> pvrt_to_int_t;
typedef std::map<ptet_cc_t::pege_t,size_t> pege_to_int_t;
typedef std::map<ptet_cc_t::ptri_t,size_t> ptri_to_int_t;
typedef std::map<ptet_cc_t::ptet_t,size_t> ptet_to_int_t;
typedef std::map<ptet_cc_t::ptet_t,int_pair_t> ptet_to_int_pr_t;

extern const int d_color_list_size;
extern double d_cyan[];
extern double d_gray[];
extern double d_brown[];
extern double *d_color_list[];

/*****************************************************************************/
void SetupPrimalSepLines_ptet
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars);

/*---------------------------------------------------------------------------*/

void SetupDualSepLines_ptet
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars);

/*---------------------------------------------------------------------------*/

void SetupPrimalSepLines_ccg
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::base_cc_geom_ptr_t  ccg,
 vtkPolyData *polydata,
 vtkDataArray *input_scalars);

/*---------------------------------------------------------------------------*/

void SetupDualSepLines_ccg
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::base_cc_geom_ptr_t  ccg,
 vtkPolyData *polydata,
 vtkDataArray *input_scalars);


/*****************************************************************************/




/*****************************************************************************/

SeparatrixLineRenderer::SeparatrixLineRenderer():
  m_tf(new TransferFunction),
  m_particles_dist(-1),
  m_show(false),
  m_particles_show(false),
  m_colorbar_show(true) // colorbar will be available only if theres geometry
{}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::create(mscdmgr_cptr_t mscdmgr)
{
  m_dmgr = mscdmgr;


  // pipeline for line geometry
  {
    VTK_CREATE(vtkPolyData,polydata);

    VTK_CREATE(vtkClipPolyData,clipper)
#if VTK_MAJOR_VERSION <= 5
    clipper->SetInputConnection(polydata->GetProducerPort());
#else
    clipper->SetInputData(polydata);
#endif

    clipper->SetClipFunction(m_dmgr->clip_box());
    clipper->SetInsideOut(true);

    VTK_CREATE(vtkStripper,striper);
    striper->SetInputConnection(clipper->GetOutputPort());

    VTK_CREATE(vtkSmoothPolyDataFilter,smoother);
    smoother->SetInputConnection(striper->GetOutputPort());
    smoother->SetNumberOfIterations(500);
    smoother->SetConvergence(0.0001);
    smoother->BoundarySmoothingOn();
    smoother->FeatureEdgeSmoothingOn();

    VTK_CREATE(vtkPolyLineTangents,tangents);
    tangents->SetInputConnection(smoother->GetOutputPort());

    VTK_CREATE(vtkTubeFilter,tuber);
    tuber->SetInputConnection(tangents->GetOutputPort());
//    tuber->SetRadius(m_dmgr->tcc()->get_diagonal_length() * 0.001);
    tuber->SetNumberOfSides(6);
    tuber->SetCapping(true);

    VTK_CREATE(vtkPolyDataMapper,mapper);
    mapper->SetInputConnection(tangents->GetOutputPort());

    VTK_CREATE(vtkActor,actor);
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(d_cyan);
    actor->GetProperty()->SetLineWidth(2.0);

    m_dmgr->ren()->AddActor(actor);

    m_striper  = striper;
    m_smoother = smoother;
    m_tangents = tangents;
    m_polydata = polydata;
    m_show     = false;
    m_mapper   = mapper;
    m_actor    = actor;
    m_tuber    = tuber;
  }

  {
    // the tranferfunction
    VTK_CREATE(vtkColorTransferFunction,ctf);


    // A colorbar widget.
    VTK_CREATE(vtkTextProperty,labelprop);
    labelprop->SetColor(0.5,0.5,0.5);
    labelprop->ShadowOff();
    labelprop->SetFontFamilyToArial();
    labelprop->SetBold(true);


    VTK_CREATE(vtkScalarBarWidget,scalarWidget);
    scalarWidget->SetInteractor
        (m_dmgr->ren()->GetRenderWindow()->GetInteractor());
    scalarWidget->GetScalarBarActor()->SetTitle("");
    scalarWidget->GetScalarBarActor()->SetLookupTable(ctf);
    scalarWidget->GetScalarBarActor()->SetLabelFormat("%.1f");
    scalarWidget->GetScalarBarActor()->SetLabelTextProperty(labelprop);
    scalarWidget->GetScalarBarActor()->SetNumberOfLabels(3);
    scalarWidget->EnabledOff();

    vtkScalarBarRepresentation *rep = vtkScalarBarRepresentation::SafeDownCast
        (scalarWidget->GetRepresentation());

    rep->SetPosition(.1, .25);
    rep->SetPosition2(.08, .45);

    m_colorbar = scalarWidget;
    m_vtk_tf   = ctf;

  }


  // pipeline for particle geometry
  {

    VTK_CREATE(vtkUnstructuredGrid,usgrid);

    VTK_CREATE(vtkExtractGeometry,clipper);
#if VTK_MAJOR_VERSION <= 5
    clipper->SetInputConnection(usgrid->GetProducerPort());
#else
    clipper->SetInputData(usgrid);
#endif

    clipper->SetImplicitFunction(m_dmgr->clip_box());

    VTK_CREATE(vtkGlyphSource2D,pt_glyph);
    pt_glyph->SetGlyphTypeToVertex();

    VTK_CREATE(vtkGlyph3D,pt_glypher);
#if VTK_MAJOR_VERSION <= 5
    clipper->SetInputConnection(usgrid->GetProducerPort());
    pt_glypher->SetSource(pt_glyph->GetOutput());
#else
    clipper->SetInputData(usgrid);
    pt_glypher->SetSourceData(pt_glyph->GetOutput());
#endif

    VTK_CREATE(vtkPolyDataMapper,mapper);
    mapper->SetInputConnection(pt_glypher->GetOutputPort());

    VTK_CREATE(vtkActor,actor);
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(0,0,0);

    m_dmgr->ren()->AddActor(actor);

    m_particles_actor  = actor;
    m_particles_usgrid = usgrid;
    m_particles_show   = false;
  }
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::update()
{
  update_lines();
  update_tf();
  update_particles();

}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_enabled(bool val)
{
  if (val == m_show) return;

  m_show = val;  

  if(m_show)
  {
    m_dmgr->ren()->AddActor(m_actor);

    if(m_particles_show)
      m_dmgr->ren()->AddActor(m_particles_actor);
    else
      m_dmgr->ren()->RemoveActor(m_particles_actor);
  }
  else
  {
    m_dmgr->ren()->RemoveActor(m_actor);
    m_dmgr->ren()->RemoveActor(m_particles_actor);
  }

  emit setEnabledEvent(val);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::update_lines()
{
  if(m_polydata->GetMTime() > m_pcs_MTime)
    return;

  // release old data
  m_polydata->Initialize();

  // Nothing to do if nothing is shown
  if(!m_show)
    return;

  VTK_CREATE(vtkPoints,points);
  m_polydata->SetPoints(points);

  VTK_CREATE(vtkFloatArray,pd_scalars);
  pd_scalars->SetName("Scalars");
  m_polydata->GetPointData()->SetScalars(pd_scalars);

  if(m_pcs.size() == 0)
    return;

  int idx = m_dmgr->msc()->index(m_pcs[0]);

  ASSERT(idx == 1 || idx == 2);

  for(int i = 0 ; i < m_pcs.size(); ++i)
    ASSERT(m_dmgr->msc()->index(m_pcs[i]) == idx);

  if (idx == 2)
    emit statusMessageEvent(tr("Updating 2-saddle Ascending Lines"));
  else if(idx == 1)
    emit statusMessageEvent(tr("Updating 1-saddle Descending Lines"));

  bool is_prim = (m_dmgr->msc()->m_grad_conv == DES && idx == 1) ||
      (m_dmgr->msc()->m_grad_conv == ASC && idx == 2) ;

  bool is_dual = (m_dmgr->msc()->m_grad_conv == DES && idx == 2) ||
      (m_dmgr->msc()->m_grad_conv == ASC && idx == 1);

  bool is_tcc  = m_dmgr->tcc() != 0;
  bool is_ccg  = m_dmgr->ccg() != 0;

  vtkDataArray * scalars = m_dmgr->domain_ds()->GetPointData()->GetScalars();

  if(is_prim && is_tcc )
    SetupPrimalSepLines_ptet(m_pcs,m_dmgr->msc(),m_dmgr->tcc(),m_polydata,scalars);
  else if(is_prim && is_ccg)
    SetupPrimalSepLines_ccg(m_pcs,m_dmgr->msc(),m_dmgr->ccg(),m_polydata,scalars);
  else if(is_dual && is_tcc )
    SetupDualSepLines_ptet(m_pcs,m_dmgr->msc(),m_dmgr->tcc(),m_polydata,scalars);
  else if(is_dual && is_ccg)
    SetupDualSepLines_ccg(m_pcs,m_dmgr->msc(),m_dmgr->ccg(),m_polydata,scalars);


  m_pcs.clear();
}


/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_tubes_enabled(bool val)
{
  if(val)
  {
    m_actor->GetMapper()->SetInputConnection
        (m_tuber->GetOutputPort());
  }
  else
  {
    m_actor->GetMapper()->SetInputConnection
        (m_tangents->GetOutputPort());

  }
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_tubes_geomprops(double r, int ns)
{
  vtkTubeFilter * tuber = vtkTubeFilter::SafeDownCast(m_tuber);

  tuber->SetRadius(r);
  tuber->SetNumberOfSides(ns);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_smoother_params(double conv,int niter)
{
  vtkSmoothPolyDataFilter * s = vtkSmoothPolyDataFilter::SafeDownCast
      (m_smoother);

  s->SetConvergence(conv);
  s->SetNumberOfIterations(niter);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_color(double r,double g,double b)
{
  m_mapper->SetScalarVisibility(0);
  m_actor->GetProperty()->SetColor(r,g,b);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_color
(double dr,double dg,double db,
 double ar,double ag,double ab,
 double sr,double sg,double sb,
 double d, double a, double s,
 double sp)
{
  m_actor->GetProperty()->SetDiffuseColor(dr,dg,db);
  m_actor->GetProperty()->SetAmbientColor(ar,ag,ab);
  m_actor->GetProperty()->SetSpecularColor(sr,sg,sb);
  m_actor->GetProperty()->SetDiffuse(d);
  m_actor->GetProperty()->SetAmbient(a);
  m_actor->GetProperty()->SetSpecular(s);
  m_actor->GetProperty()->SetSpecularPower(sp);
  m_mapper->SetScalarVisibility(0);
}



/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_line_width(double w)
{
  m_actor->GetProperty()->SetLineWidth(w);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_tf_enabled(bool val)
{
  if(val == m_mapper->GetScalarVisibility())
    return;

  m_mapper->SetScalarVisibility(val);

  if(val)
    update_tf();
}

/*---------------------------------------------------------------------------*/

void default_tf(TransferFunction &);

void SeparatrixLineRenderer::update_tf()
{
  set_colorbar_enabled(m_colorbar_show);

  if(!m_mapper->GetScalarVisibility() || m_polydata->GetNumberOfPoints() ==0)
    return;

  if(m_tf->size() == 0)
    default_tf(*m_tf);

  double rng[2] = {m_dmgr->msc()->m_fmin,m_dmgr->msc()->m_fmax};

  vtkColorTransferFunction * ctf = m_vtk_tf;

  ctf->RemoveAllPoints();

  for( int i = 0 ; i < m_tf->size(); ++i)
  {
    double  f = m_tf->at(i).f;
    QColor  c = m_tf->at(i).col;

    f = rng[0] + f*(rng[1]- rng[0]);

    ctf->AddRGBPoint(f,c.redF(),c.greenF(),c.blueF());
  }

  ctf->Modified();

  m_mapper->SetLookupTable(ctf);
  m_mapper->SetScalarRange(rng);
  m_mapper->Modified();
}

/*---------------------------------------------------------------------------*/

bool SeparatrixLineRenderer::save_tf(const QString &fn) const
{
  return SaveTransferFunction(*m_tf,fn);
}

/*---------------------------------------------------------------------------*/

bool SeparatrixLineRenderer::load_tf(const QString &fn)
{
  TransferFunction tf;

  bool ret = LoadTransferFunction(tf,fn);

  if(ret)
  {
    qSort(tf);

    qreal m = tf.front().f;
    qreal M = tf.back().f;

    for(int i = 0 ; i < tf.size() ; ++i)
      tf[i].f = (tf[i].f - m) / (M-m);

    *m_tf = tf;
  }

  return ret;
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::edit_tf()
{
  if(QTransferFunctionEditorDialog::editTransferFunction
      (*m_tf,m_dmgr->msc()->m_fmin,m_dmgr->msc()->m_fmax,true))
    update_tf();
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::save_data(QString fn)
{
  VTK_CREATE(vtkXMLPolyDataWriter,writer);
  writer->SetInputConnection(m_tangents->GetOutputPort());
  writer->SetFileName(fn.toStdString().c_str());
  writer->SetDataModeToAscii();
  writer->Update();
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::load_data(QString fn)
{
  VTK_CREATE(vtkXMLPolyDataReader,reader);
  reader->SetFileName(fn.toStdString().c_str());
  reader->Update();
  vtkPolyData * opd = reader->GetOutput();

  VTK_CREATE(vtkPolyData,pd);
  pd->SetPoints(opd->GetPoints());
  pd->SetLines(opd->GetLines());

  VTK_CREATE(vtkCleanPolyData,cleaner);
#if VTK_MAJOR_VERSION <= 5
    cleaner->SetInputConnection(pd->GetProducerPort());
#else
    cleaner->SetInputData(pd);
#endif

  m_polydata->DeepCopy(cleaner->GetOutput());

#if VTK_MAJOR_VERSION <= 5
  m_polydata->Update();
#endif
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_particles_enabled(bool val)
{
  if (val == m_particles_show) return;

  m_particles_show = val;

  if(m_particles_show)
    m_dmgr->ren()->AddActor(m_particles_actor);
  else
    m_dmgr->ren()->RemoveActor(m_particles_actor);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_particles_viewprops
(double r, double g, double b, double pt_size)
{
  m_particles_actor->GetProperty()->SetPointSize(pt_size);
  m_particles_actor->GetProperty()->SetColor(r,g,b);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::set_colorbar_enabled(bool val)
{
  m_colorbar_show = val;

  m_colorbar->SetEnabled
      (m_show
       && m_colorbar_show
       && m_polydata->GetNumberOfPoints() != 0
       && m_mapper->GetScalarVisibility());
}


/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::update_particles()
{
  vtkPolyData * pd =  m_polydata;

  if(m_particles_usgrid->GetMTime() > pd->GetMTime() &&
      m_particles_usgrid->GetMTime() > m_particles_dist_MTime)
    return;

  m_particles_usgrid->Initialize();

  if(!m_particles_show)
    return;

  if(m_particles_dist > 0)
  {
    if(pd->GetNumberOfPoints() == 0)
      return;

    VTK_CREATE(vtkPoints,pts);
    m_particles_usgrid->SetPoints(pts);

    m_particles_usgrid->Allocate();

    int nv = m_dmgr->tcc()->num_dcells(0);

    for(vtkIdType i=0,j=0; i < nv; ++i)
    {
      utl::dvec3 pt = m_dmgr->tcc()->get_vert(i),clpt;
      pd->GetPoint(pd->FindPoint(pt.m_elems),clpt.m_elems);

      if((pt-clpt).norm2() < m_particles_dist)
      {
        pts->InsertNextPoint(pt.m_elems);
        m_particles_usgrid->InsertNextCell(VTK_VERTEX,1,&j);++j;
      }
    }

    pts->Modified();
  }
  else
  {
    vtkUnstructuredGrid * usgrid =
        vtkUnstructuredGrid::SafeDownCast(m_dmgr->domain_ds());

    m_particles_usgrid->SetPoints(usgrid->GetPoints());
    m_particles_usgrid->Allocate();

    int nv = m_dmgr->tcc()->num_dcells(0);
    for(vtkIdType i=0; i < nv; ++i)
      m_particles_usgrid->InsertNextCell(VTK_VERTEX,1,&i);
  }

  m_particles_usgrid->Modified();
#if VTK_MAJOR_VERSION <= 5
  m_particles_usgrid->Update();
#endif
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineRenderer::save_particles_distances(QString fn)
{

  ENSURE_OR_RETURN(m_polydata->GetNumberOfPoints() != 0) << "Filament data unavailable!!!" ;

  ofstream fs(fn.toStdString().c_str());

  ENSURE_OR_RETURN(fs.is_open())
      << "Unable to open file !!!" << SVAR(fn.toStdString());

  int nv = m_dmgr->tcc()->num_dcells(0);

  fs << "# Particles List " <<endl;
  fs << "# Xcoord Ycoord Zcoord DistToSelFils" <<endl;

  for(vtkIdType i=0; i < nv; ++i)
  {
    utl::dvec3 pt = m_dmgr->tcc()->get_vert(i),clpt;
    m_polydata->GetPoint(m_polydata->FindPoint(pt.m_elems),clpt.m_elems);

    fs << pt[0] << " "
       << pt[1] << " "
       << pt[2] << " "
       << (pt-clpt).norm2()
       << endl;
  }

  fs.close();
}

/*****************************************************************************/



/*****************************************************************************/

void SetupPrimalSepLines_ptet
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars)
{
  vtkPoints * points            = polydata->GetPoints();
  vtkDataArray *pd_scalars      = polydata->GetPointData()->GetScalars();

  pvrt_to_int_t pvrtmap;

  std::vector<utl::ivec2> lns;

  // Primal separatrix lines are the Descending manifolds of 1 saddles with
  // descending gradient convention. pcs must be all 1 saddles.
  // Primal separatrix lines are the Ascending manifolds of 2 saddles with
  // ascending gradient convention. pcs must be all 2 saddles.
  tet::eGDIR dir = msc->m_grad_conv;

  BOOST_FOREACH(int i,pcs)
  {
    BOOST_FOREACH(cellid_t egec, msc->m_mfolds[dir][i])
    {
      ptet_cc_t::pege_t pege = tcc->get_pcell<2>(egec);

      pvrt_to_int_t::iterator pv1 =
          pvrtmap.insert(std::make_pair(pege[0],pvrtmap.size())).first;

      pvrt_to_int_t::iterator pv2 =
          pvrtmap.insert(std::make_pair(pege[1],pvrtmap.size())).first;

      lns.push_back(utl::mk_vec<int>(pv1->second,pv2->second));
    }
  }

  points->SetNumberOfPoints(pvrtmap.size());
  pd_scalars->SetNumberOfTuples(pvrtmap.size());

  BOOST_FOREACH(pvrt_to_int_t::value_type pr, pvrtmap)
  {
    vert_t  v = tcc->get_vert(pr.first);
    points->SetPoint(pr.second,v[0],v[1],v[2]);

    double f = usgrid_scalars->GetComponent(pr.first.cid,0);
    pd_scalars->SetComponent(pr.second,0,f);
  }

  polydata->Allocate(lns.size());

  for(int i = 0 ; i < lns.size(); i++)
  {
    vtkIdType l[] = {(vtkIdType)(lns[i][0]),(vtkIdType)(lns[i][1])};

    polydata->InsertNextCell(VTK_LINE,2,l);
  }

  pd_scalars->Modified();
}

/*---------------------------------------------------------------------------*/

void SetupDualSepLines_ptet
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars)
{
  vtkPoints * points            = polydata->GetPoints();
  vtkDataArray *pd_scalars      = polydata->GetPointData()->GetScalars();

  ptet_to_int_pr_t ptetset;
  cellid_to_int_t  triset;

  // Dual separatrix lines are the Ascending manifolds of 2 saddles with
  // descending gradient convention. pcs must be all 2 saddles.
  // Dual separatrix lines are the Descending manifolds of 1 saddles with
  // ascending gradient convention. pcs must be all 1 saddles.
  tet::eGDIR dir = (msc->m_grad_conv == tet::DES)?(tet::ASC):(tet::DES);

  std::set<tet_cc_t::ege_t> lns;

  BOOST_FOREACH(int i,pcs)
  {
    BOOST_FOREACH(int tric, msc->m_mfolds[dir][i])
    {
      int tets[DMAX];
      int ct = tcc->get_cofs(tric,tets);

      cellid_to_int_t::iterator tri_it = triset.insert
          (std::make_pair(tric,triset.size())).first;

      ptet_cc_t::ptet_t ptet=tcc->align_cwrtf<4>(tets[0],tric);

      ptet_to_int_pr_t::iterator tet_it = ptetset.insert
          (std::make_pair(ptet,int_pair_t(tets[0],ptetset.size()))).first;

      lns.insert(utl::mk_uvec(tet_it->second.second,tri_it->second));

      if(ct == 2)
      {
        ptet=tcc->align_cwrtf<4>(tets[1],tric);
        tet_it = ptetset.insert
            (std::make_pair(ptet,int_pair_t(tets[1],ptetset.size()))).first;
        lns.insert(utl::mk_uvec(tet_it->second.second,tri_it->second));
      }
    }
  }

  pd_scalars->SetNumberOfTuples(triset.size() + ptetset.size());
  points->SetNumberOfPoints(triset.size() + ptetset.size());

  BOOST_FOREACH(cellid_to_int_t::value_type pr,triset)
  {
    vert_t v = tcc->get_centroid<3>(pr.first);
    points->SetPoint(pr.second,v[0],v[1],v[2]);

    tet::ptet_cc_t::ptri_t tri= tcc->get_pcell<3>(pr.first);
    double f0 = usgrid_scalars->GetComponent(tri[0].cid,0);
    double f1 = usgrid_scalars->GetComponent(tri[1].cid,0);
    double f2 = usgrid_scalars->GetComponent(tri[2].cid,0);

    double   f= max(max(f0,f1),f2);

    pd_scalars->SetComponent(pr.second,0,f);

//    cout << tri[0].cid <<"|"<< f0 << ",";
//    cout << tri[1].cid <<"|"<< f1 << ",";
//    cout << tri[2].cid <<"|"<< f2 << endl;
  }

  int tet_off = triset.size();

  BOOST_FOREACH(ptet_to_int_pr_t::value_type pr, ptetset)
  {
    vert_t v = tcc->get_centroid<4>(pr.first);
    points->SetPoint(pr.second.second + tet_off,v[0],v[1],v[2]);

    double f0 = usgrid_scalars->GetComponent(pr.first[0].cid,0);
    double f1 = usgrid_scalars->GetComponent(pr.first[1].cid,0);
    double f2 = usgrid_scalars->GetComponent(pr.first[2].cid,0);
    double f3 = usgrid_scalars->GetComponent(pr.first[3].cid,0);

    double   f= max(max(max(f0,f1),f2),f3);

    pd_scalars->SetComponent(pr.second.second + tet_off,0,f);

//    cout << pr.first[0].cid <<"|"<< f0 << ",";
//    cout << pr.first[1].cid <<"|"<< f1 << ",";
//    cout << pr.first[2].cid <<"|"<< f2 << ",";
//    cout << pr.first[3].cid <<"|"<< f3 << endl;
  }

  polydata->Allocate(lns.size());

  BOOST_FOREACH(tet_cc_t::ege_t ln,lns)
  {
    vtkIdType ln_vtk[] = {tet_off+ln[0],ln[1]};
    polydata->InsertNextCell(VTK_LINE,2,ln_vtk);
  }

  pd_scalars->Modified();
}

/*---------------------------------------------------------------------------*/
void SetupPrimalSepLines_ccg
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t msc,
 tet::base_cc_geom_ptr_t ccg,
 vtkPolyData *polydata,
 vtkDataArray *scalars
 )
{
  cellid_to_int_t vrtmap;

  std::vector<utl::ivec2> lns;

  // Primal separatrix lines are the Descending manifolds of 1 saddles with
  // descending gradient convention. pcs must be all 1 saddles.
  // Primal separatrix lines are the Ascending manifolds of 2 saddles with
  // ascending gradient convention. pcs must be all 2 saddles.
  tet::eGDIR dir = msc->m_grad_conv;

  BOOST_FOREACH(int i,pcs)
  {
    BOOST_FOREACH(cellid_t egec, msc->m_mfolds[dir][i])
    {
      cellid_t vrts[DMAX];

      int n = ccg->get_vrts(egec,vrts);

      ENSURES(n==2) <<SVAR(egec) << "does not appear to be an Edge" << SVAR(n);

      cellid_to_int_t::iterator v1 =
          vrtmap.insert(std::make_pair(vrts[0],vrtmap.size())).first;

      cellid_to_int_t::iterator v2 =
          vrtmap.insert(std::make_pair(vrts[1],vrtmap.size())).first;

      lns.push_back(utl::mk_vec<int>(v1->second,v2->second));
    }
  }

  vtkDataArray *pd_scalars      = polydata->GetPointData()->GetScalars();
  vtkPoints   * points          = polydata->GetPoints();

  points->SetNumberOfPoints(vrtmap.size());
  pd_scalars->SetNumberOfTuples(vrtmap.size());

  BOOST_FOREACH(cellid_to_int_t::value_type pr, vrtmap)
  {
    vert_t  v = ccg->get_vertex(pr.first);
    points->SetPoint(pr.second,v[0],v[1],v[2]);
    pd_scalars->SetComponent(pr.second,0,scalars->GetComponent(pr.first,0));
  }

  polydata->Allocate(lns.size());

  for(int i = 0 ; i < lns.size(); i++)
  {
    vtkIdType l[] = {(vtkIdType)(lns[i][0]),(vtkIdType)(lns[i][1])};

    polydata->InsertNextCell(VTK_LINE,2,l);
  }
  pd_scalars->Modified();
}

/*---------------------------------------------------------------------------*/

void SetupDualSepLines_ccg
(const std::vector<int> &pcs,
 tet::mscomplex_cptr_t msc,
 tet::base_cc_geom_ptr_t ccg,
 vtkPolyData *polydata,
 vtkDataArray *scalars)
{
  vtkPoints * points            = polydata->GetPoints();

  cellid_to_int_t  cellid_set;

  // Dual separatrix lines are the Ascending manifolds of 2 saddles with
  // descending gradient convention. pcs must be all 2 saddles.
  // Dual separatrix lines are the Descending manifolds of 1 saddles with
  // ascending gradient convention. pcs must be all 1 saddles.
  tet::eGDIR dir = (msc->m_grad_conv == tet::DES)?(tet::ASC):(tet::DES);

  std::set<tet_cc_t::ege_t> lns;

  BOOST_FOREACH(int i,pcs)
  {
    BOOST_FOREACH(int fc, msc->m_mfolds[dir][i])
    {
      int cofs[DMAX];
      int ct = ccg->get_cofs(fc,cofs);

      ENSURES(ct<=2) <<SVAR(fc) << "does not appear to be a face" << SVAR(ct);

      cellid_to_int_t::iterator fc_it = cellid_set.insert
          (std::make_pair(fc,cellid_set.size())).first;

      cellid_to_int_t::iterator cof_it = cellid_set.insert
          (std::make_pair(cofs[0],cellid_set.size())).first;

      lns.insert(utl::mk_uvec(cof_it->second,fc_it->second));

      if(ct == 2)
      {
        cellid_to_int_t::iterator cof_it = cellid_set.insert
            (std::make_pair(cofs[1],cellid_set.size())).first;

        lns.insert(utl::mk_uvec(cof_it->second,fc_it->second));
      }
    }
  }

  points->SetNumberOfPoints(cellid_set.size());

  BOOST_FOREACH(cellid_to_int_t::value_type pr, cellid_set)
  {
    vert_t v = ccg->get_centroid(pr.first);
    points->SetPoint(pr.second,v[0],v[1],v[2]);
  }

  polydata->Allocate(lns.size());

  BOOST_FOREACH(tet_cc_t::ege_t ln,lns)
  {
    vtkIdType ln_vtk[] = {ln[0],ln[1]};
    polydata->InsertNextCell(VTK_LINE,2,ln_vtk);
  }
}

/*===========================================================================*/




/*===========================================================================*/

#include <ui_SeparatrixLineSelector.h>

/*---------------------------------------------------------------------------*/

SeparatrixLineSelector::SeparatrixLineSelector(QWidget *par):QFrame(par)
{

  m_ui = new Ui_SeparatrixLineSelector;
  m_ui->setupUi(this);

  m_ui->dslider_2saddle->setHandleToolTip(tr("2Sad Rng=[%1,%2]"));
  m_ui->dslider_maxima->setHandleToolTip(tr("Max Rng=[%1,%2]"));
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineSelector::create(mscdmgr_cptr_t dmgr)
{
  m_dmgr = dmgr;
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineSelector::update()
{
  double rng[2];
  m_dmgr->getScalarRange(rng);

  m_ui->dslider_2saddle->setRange(rng[0],rng[1]);
  m_ui->dslider_maxima->setRange(rng[0],rng[1]);

  m_ui->label_min->setText(QString("%1").arg(rng[0]));
  m_ui->label_max->setText(QString("%1").arg(rng[1]));
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineSelector::on_pb_select_clicked(bool clicked)
{
  double smin = m_ui->dslider_2saddle->minimumPosition();
  double smax = m_ui->dslider_2saddle->maximumPosition();
  double mmin = m_ui->dslider_maxima->minimumPosition();
  double mmax = m_ui->dslider_maxima->maximumPosition();

  emit rangeSelected2Saddles(smin,smax,mmin,mmax);
}

/*---------------------------------------------------------------------------*/

void SeparatrixLineSelector::on_pb_clear_clicked(bool clicked)
{
  emit clearedCpSelection();
}


/*---------------------------------------------------------------------------*/

void SeparatrixLineSelector::set_selection_ranges
(double smin, double smax, double mmin, double mmax)
{
  m_ui->dslider_2saddle->setPositions(smin,smax);
  m_ui->dslider_maxima->setPositions(mmin,mmax);
}



/*===========================================================================*/

