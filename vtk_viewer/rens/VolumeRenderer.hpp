#ifndef MAINWINDOW_VOLREN_INCLUDED
#define MAINWINDOW_VOLREN_INCLUDED

#include <QObject>
#include <QFrame>
#include <QVariantList>

#include <vtkSmartPointer.h>
#include <vtkWeakPointer.h>

#include <tet.hpp>


struct TransferFunctionPoint;
typedef QList<TransferFunctionPoint> TransferFunction;

struct PiecewiseFunctionPoint;
typedef QList<PiecewiseFunctionPoint> PiecewiseFunction;

class vtkBox;
class vtkRenderer;
class vtkVolume;
class vtkDataArray;
class vtkObject;

class QGroupBox;
class QCheckBox;
class QTransferFunctionEditorDialog;
class QPiecewiseFunctionEditorDialog;

class vtkAbstractVolumeMapper;
class vtkScalarBarWidget;
class vtkPointSet;

class MsComplexDataManager;
typedef boost::shared_ptr<MsComplexDataManager>       mscdmgr_ptr_t;
typedef boost::shared_ptr<const MsComplexDataManager> mscdmgr_cptr_t;

class Ui_VolumeRenderer;

class VolumeRenderer:public QFrame
{
  Q_OBJECT

public:
  VolumeRenderer(QWidget *par=0);

signals:
  void showStatusMessage(const QString &);

public slots:

  void set_enabled(bool val);
  inline bool get_enabled() {return m_enabled;}

  void set_continuous_rendering(bool val);
  inline bool get_continuous_rendering(){return m_continuous_rendering;}

  bool save_tf(const QString &) const;
  bool load_tf(const QString &);

  void set_volume_enhancement(bool val);
  inline bool get_volume_enhancement(){return m_enhancement_enabled;}

  bool save_enhancement_tf(const QString &)const;
  bool load_enhancement_tf(const QString &);
  void edit_enhancement_tf();

  /// \brief Save the the current volume along with the scalars and enhancement
  /// values
  ///
  /// \param[in] fn Output XML file name
  void   save_volume(const QString &fn);


  /// \brief Save the the current volume along with the scalars and enhancement
  /// values
  ///
  /// \param[in] of Output XML file name
  /// \param[in] sf spin info file
  void save_volume_with_spin(QString of,QString sf);


  /// \brief The enhancement values will use an exponential decay with var
  ///
  /// \param[in] var Variance
  void   enable_enhancement_exp_decay(double var);

  /// \brief No such thing
  void   disable_enhancement_exp();

  /// \brief Set Brightness
  /// \param[in] v Brightness
  void   set_brightness(double v);

  /// \brief Show Color bar
  void   enable_colorbar();

  /// \brief Hide Color bar
  void   disable_colorbar();

  /// \brief set the Transfer function
  ///
  /// \note Every 5 values specify f,r,g,b,a
  ///       Its accepted only if there are more than 2 entries (10 values)
  ///       and f1-f2 != 0.
  /// \note tf is always normalized to [0,1] .. so input will be renormed
  ///
  void  set_tf(QVariantList);

  /// \brief get the Transfer function
  ///
  /// \note tf is always normalized to [0,1]
  QList<double> get_tf();


  void   update();

public:

  /// \brief Create the Volume renderer
  void create(mscdmgr_cptr_t dmgr);

  /// \brief Computes the enhancement distance of all points from the given
  ///        points. Is used for enhance the volume rendering.
  ///
  /// \param[in] pts selected point set
  void set_enhancement_points(vtkSmartPointer<vtkPointSet> pts);

public:
  /// \privatesection

  void mouse_event(vtkObject *, unsigned long e,void *);
  void update_enhancement_distance();

  mscdmgr_cptr_t                                   m_dmgr;

  vtkSmartPointer<vtkVolume>                       m_volume;
  vtkSmartPointer<vtkAbstractVolumeMapper>         m_volume_mapper;
  vtkSmartPointer<vtkScalarBarWidget>              m_colorbar_widget;
  boost::shared_ptr<TransferFunction>              m_tf;
  bool                                             m_enabled;
  bool                                             m_continuous_rendering;

  // Enhancement Related stuff
  boost::shared_ptr<PiecewiseFunction>   m_enhancement_tf;
  bool                                   m_enhancement_enabled;
  int                                    m_enhancement_tf_type;
  vtkWeakPointer<vtkPointSet>            m_enhancement_points;

  // UI related stuff
  Ui_VolumeRenderer               *m_ui;
  QPiecewiseFunctionEditorDialog  *m_enhancement_tf_dialog;
private slots:
  void on_tf_widget_applied();
  void enhancement_tf_dialog_applied();

public slots:
  /// \privatesection Deprecated slots
  void add_tfpt(double f,double r,double g,double b,double a);
  void del_tfpt(int i);
  int  num_tfpt() const;
  void clear_tf();
  void    set_enhancement_tf_type_to_opacity_add();
  void    set_enhancement_tf_type_to_opacity_mul();
  QString get_enhancement_tf_type();


};

#endif
