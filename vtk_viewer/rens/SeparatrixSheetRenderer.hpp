#ifndef __SEPERATRIXSHEETRENDERER_INCLUDED__
#define __SEPERATRIXSHEETRENDERER_INCLUDED__

#include <QObject>

#include <vtkSmartPointer.h>
#include <vtkTimeStamp.h>

#include <tet.hpp>

class QThread;

class vtkRenderer;
class vtkPolyData;
class vtkPolyDataAlgorithm;
class vtkActor;
class vtkBox;
class vtkUnstructuredGrid;
class vtkPolyDataMapper;

struct TransferFunctionPoint;
typedef QList<TransferFunctionPoint> TransferFunction;

class MsComplexDataManager;
typedef boost::shared_ptr<const MsComplexDataManager> mscdmgr_cptr_t;


/*****************************************************************************/

/// \brief Class to manage the rendering Separatrix Sheets.
///
/// All public slots are callable.
class SeparatrixSheetRenderer:public QObject
{
  Q_OBJECT
public:
  /// Constructor
  SeparatrixSheetRenderer();

//  /// Destructor
//  ~SeparatrixSheetRenderer();

public:
  /// \brief Create the renderer
  void create(mscdmgr_cptr_t mscdmgr);

  /// \brief Set list of pieces of two saddles
  inline void set_geom_piece_cps(tet::int_pair_list_t& pcs)
  {
    m_pcs.resize(pcs.size());
    br::copy(pcs,m_pcs.begin());
    m_pcs_MTime.Modified();
  }

  /// \brief Get Time when pcs were last updated
  inline vtkTimeStamp get_geom_piece_cps_MTime(){return m_pcs_MTime;}

  /// \brief Update the rendering
  void update();

public slots:

  /// \brief Enable rendering
  void set_enabled(bool val);

  /// \brief Get Rendering state
  inline bool get_enabled(){ return m_show;}

  /// \brief Save the extracted data to a vtk XML file
  void save_data(QString str);





  /// \brief Set RGB color of geometry
  void set_color(double r,double g,double b);

  /// \brief Enabling coloring using a tf on density
  void set_tf_enabled(bool val);

  /// \brief Save density tranferfunction to file
  bool save_tf(const QString &) const;

  /// \brief Load Transfer function from file .. Enables tf if not enabled
  bool load_tf(const QString &);

  /// \brief Edit transfer function .. Enables tf if tf edited and not enabled
  void edit_tf();






  /// \brief Show the particles near the selected sheets
  /// \param[in] dist       particles within distance (-1 clears the data)
  inline void set_particle_dist(double dist)
  {m_particles_dist = dist;m_particles_dist_MTime.Modified();}

  /// \brief Enable particle rendering .. (make sure a correct dist is set first)
  void set_particles_enabled(bool val);

  /// \brief Get Particle rendering states
  inline bool get_particles_enabled(){ return m_particles_show;}

  /// \brief Save the coordinates of the currently selected particles
  /// \param[in] fn   File Name
  void save_particles(QString fn);

  /// \brief Set the viewing properties
  /// \param[in] r,g,b    color of rendered particles
  /// \param[in] pt_size  size of rendered particles
  void set_particle_viewprops(double r,double g,double b,double pt_size);


public:
  /// \privatesection
  // References to objects from other parts
  mscdmgr_cptr_t                              m_dmgr;

  vtkSmartPointer<vtkPolyData>                m_polydata;
  vtkSmartPointer<vtkActor>                   m_actor;
  vtkSmartPointer<vtkPolyDataMapper>          m_mapper;
  bool                                        m_show;
  boost::shared_ptr<TransferFunction>         m_tf;

  tet::int_pair_list_t   m_pcs;
  vtkTimeStamp           m_pcs_MTime;


  double                                      m_particles_dist;
  vtkTimeStamp                                m_particles_dist_MTime;
  vtkSmartPointer<vtkUnstructuredGrid>        m_particles_usgrid;
  vtkSmartPointer<vtkActor>                   m_particles_actor;
  bool                                        m_particles_show;

signals:
  void setEnabledEvent(bool );
  void statusMessageEvent(const QString &);

private:
  // update subparts
  void update_sheets();
  void update_particles();
  void update_tf();

};

/*****************************************************************************/
#endif
