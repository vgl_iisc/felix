#ifndef __CRITICALPOINTRENDERER_INCLUDED__
#define __CRITICALPOINTRENDERER_INCLUDED__
#include <QObject>

#include <vtkSmartPointer.h>
#include <vtkTimeStamp.h>

#include <tet.hpp>

class vtkPoints;
class vtkPolyData;
class vtkActor;
class vtkSphereSource;
class vtkGlyph3D;
class vtkExtractPolyDataGeometry;
class vtkIntArray;
class vtkObject;

class MsComplexDataManager;
typedef boost::shared_ptr<const MsComplexDataManager> mscdmgr_cptr_t;


/*****************************************************************************/


/// \brief Class to manage the rendering Critical points.
///
/// All public slots are callable.
class CriticalPointRenderer:public QObject
{
  Q_OBJECT
public:
  /// Constructor
  CriticalPointRenderer();

//  /// Destructor
//  ~CriticalPointRenderer();

public:
  /// \brief Create the renderer
  void create( mscdmgr_cptr_t mscdmgr);

  /// \brief Update the rendering
  void update();

  /// \brief Set list of cps
  inline void set_cps(tet::int_pair_list_t& cps)
  {
    m_cps.resize(cps.size());
    br::copy(cps,m_cps.begin());
    m_cps_MTime.Modified();
  }

  /// \brief Get Time when ccs were last updated
  inline vtkTimeStamp get_cps_MTime(){return m_cps_MTime;}
public slots:


  /// \brief Enable/Disable rendering cps
  void set_enabled(bool var);
  /// \brief Get rendering state of cps
  bool get_enabled();
  /// \brief Enable/Disable rendering cps of a particular index
  void set_index_enabled(int idx, bool var);
  /// \brief Get rendering state of cps of a particular index
  bool get_index_enabled(int i);

  /// \brief Enable/Disable rendering cps as sphere
  void set_spheres_enabled(bool var);
  /// \brief Set Cp sphere radii of Min,1Sad,2Sad,Max
  void set_spheres_radii(double r1, double r2, double r3, double r4);
  /// \brief Set Cp sphere radii of cps
  inline void set_spheres_radii(double r){set_spheres_radii(r,r,r,r);};

  /// \brief Enable/Disable selecting cps
  void set_selection_enabled(bool var);


private:
  /// \privatesection
  mscdmgr_cptr_t                        m_dmgr;

  void mouse_event(vtkObject *, unsigned long e,void *);

  tet::int_pair_list_t  m_cps;
  vtkTimeStamp          m_cps_MTime;

  bool                                  m_show;
  bool                                  m_cp_show[4];
  vtkSmartPointer<vtkPoints>            m_cp_points[4];
  vtkSmartPointer<vtkPolyData>          m_cp_polydata[4];
  vtkSmartPointer<vtkActor>             m_cp_actors[4];
  vtkTimeStamp                          m_cp_MTime;

  vtkSmartPointer<vtkSphereSource>                 m_cp_spheresource[4];
  vtkSmartPointer<vtkGlyph3D>                      m_cp_glyph[4];
  vtkSmartPointer<vtkExtractPolyDataGeometry>      m_cp_clipper[4];
  vtkSmartPointer<vtkIntArray>                     m_cp_nos[4];

  unsigned long                                    m_cp_click_observer_tag;


};

/*****************************************************************************/
#endif
