#include <vtkIntArray.h>
#include <vtkPolyData.h>
#include <vtkSphereSource.h>
#include <vtkGlyph3D.h>
#include <vtkPointData.h>
#include <vtkExtractPolyDataGeometry.h>
#include <vtkActor.h>
#include <vtkBox.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkPointPicker.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkAlgorithmOutput.h>
#include <vtkCommand.h>


#include <tet_cc.hpp>
#include <tet_mscomplex.hpp>

#include <MsComplexDataManager.hpp>
#include <rens/CriticalPointRenderer.hpp>

extern double d_blue[];
extern double d_green[];
extern double d_yellow[];
extern double d_red[];

#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();


/*****************************************************************************/

CriticalPointRenderer::CriticalPointRenderer():m_cp_click_observer_tag(0)
{}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::create(mscdmgr_cptr_t mscdmgr)
{
  m_dmgr = mscdmgr;
  m_show = true;

  // pipeline for cps
  //
  // points --> polydata ---> pextractor--> mapper --> actor --> renderer
  //                      /
  //            clipbox--/
  //

  double *cp_colors[] = {d_blue,d_green,d_yellow,d_red};

  for(int i = 0 ; i < 4; ++i)
  {
    VTK_CREATE(vtkPoints,points);
    VTK_CREATE(vtkIntArray,cpno);
    cpno->SetName("CpNo");

    VTK_CREATE(vtkPolyData,polydata);
    polydata->Allocate();
    polydata->SetPoints(points);
    polydata->GetPointData()->AddArray(cpno);

    VTK_CREATE(vtkSphereSource, sphereSource);
    sphereSource->SetRadius(1);

    VTK_CREATE(vtkGlyph3D, glyph);
    glyph->SetSourceConnection(sphereSource->GetOutputPort());
#if VTK_MAJOR_VERSION <= 5
    glyph->SetInputConnection(polydata->GetProducerPort());
#else
    glyph->SetInputData(polydata);
#endif
    glyph->Update();
    glyph->SetScaleModeToDataScalingOff();

    VTK_CREATE(vtkExtractPolyDataGeometry,pextractor);
#if VTK_MAJOR_VERSION <= 5
    pextractor->SetInputConnection(polydata->GetProducerPort());
#else
    pextractor->SetInputData(polydata);
#endif
    pextractor->SetImplicitFunction(m_dmgr->clip_box());

    VTK_CREATE(vtkPolyDataMapper,cp_geom_mapper);
    cp_geom_mapper->SetInputConnection(pextractor->GetOutputPort());

    VTK_CREATE(vtkActor,actor);
    actor->SetMapper(cp_geom_mapper);
    actor->GetProperty()->SetPointSize(6);
    actor->GetProperty()->SetColor(cp_colors[i]);

    m_cp_glyph[i]        = glyph;
    m_cp_spheresource[i] = sphereSource;
    m_cp_clipper[i]      = pextractor;
    m_cp_points[i]       = points;
    m_cp_polydata[i]     = polydata;
    m_cp_actors[i]       = actor;
    m_cp_nos[i]          = cpno;
    m_cp_show[i]         = true;
  }
}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::mouse_event(vtkObject *, unsigned long e, void *)
{
  int* pos = m_dmgr->ren()->GetRenderWindow()->GetInteractor()
      ->GetEventPosition();

  vtkRenderWindow * renwin = m_dmgr->ren()->GetRenderWindow();

  VTK_CREATE(vtkPointPicker,picker);

  picker->SetTolerance(0.01);

  VTK_CREATE(vtkRenderer,ren);

  ren->AddActor(m_cp_actors[0]);
  ren->AddActor(m_cp_actors[1]);
  ren->AddActor(m_cp_actors[2]);
  ren->AddActor(m_cp_actors[3]);

  ren->SetActiveCamera(m_dmgr->ren()->GetActiveCamera());

  renwin->AddRenderer(ren);

  renwin->RemoveRenderer(m_dmgr->ren());

  picker->Pick(pos[0], pos[1], 0, ren);

  if(picker->GetPointId() != -1)
  {
    vtkExtractPolyDataGeometry *extractor =
        vtkExtractPolyDataGeometry::SafeDownCast
        (picker->GetActor()->GetMapper()->GetInputConnection(0,0)->GetProducer());

    int cpno = extractor->GetOutput()->GetPointData()->GetArray("CpNo")->
        GetTuple1(picker->GetPointId());

    std::cout<<"Picked CpNo = "<<cpno<<std::endl;
  }

  renwin->AddRenderer(m_dmgr->ren());
}

/*---------------------------------------------------------------------------*/


void CriticalPointRenderer::update()
{
  for(int i = 0 ; i < 4; ++i)
    if(m_cp_show[i] && m_show)
      m_dmgr->ren()->AddActor(m_cp_actors[i]);
    else
      m_dmgr->ren()->RemoveActor(m_cp_actors[i]);

  if( m_cp_MTime > m_dmgr->get_res_MTime() &&
      m_cp_MTime > m_cps_MTime)
    return;

//  statusBar()->showMessage(tr("Updating critical point geometry"));

  m_cp_MTime.Modified();

  for(int i = 0 ; i < 4; ++i)
  {
    m_cp_points[i]->Reset();
    m_cp_points[i]->Modified();

    m_cp_polydata[i]->Reset();
    m_cp_polydata[i]->Modified();

    m_cp_nos[i]->Reset();
    m_cp_nos[i]->Modified();
    m_cp_nos[i]->Allocate(1000);

    m_cp_points[i]->Allocate(1000);
  }

  BOOST_FOREACH(vtkIdType i,m_cps|badpt::map_keys)
  {
    if( m_dmgr->msc()->is_not_canceled(i))
    {
      int indx = m_dmgr->msc()->index(i);

      tet::vert_t v = m_dmgr->ccg()->get_centroid(m_dmgr->msc()->cellid(i));

      vtkIdType pidx = m_cp_points[indx]->InsertNextPoint
          (v[0],v[1],v[2]);

      m_cp_polydata[indx]->InsertNextCell(VTK_VERTEX,1,&pidx);

      m_cp_nos[indx]->InsertNextTuple1(i);
    }
  }

  m_cps.clear();
}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::set_spheres_enabled(bool var)
{
  if(var)
    for(int i = 0 ; i < 4; ++i)
      m_cp_clipper[i]->SetInputConnection
          (m_cp_glyph[i]->GetOutputPort());
  else
    for(int i = 0 ; i < 4; ++i)
#if VTK_MAJOR_VERSION <= 5
      m_cp_clipper[i]->SetInputConnection(m_cp_polydata[i]->GetProducerPort());
#else
      m_cp_clipper[i]->SetInputData(m_cp_polydata[i]);
#endif



}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::set_selection_enabled(bool var)
{
  if (var && !m_cp_click_observer_tag)
  {
    m_cp_click_observer_tag =
        m_dmgr->ren()->GetRenderWindow()->GetInteractor()->AddObserver
        (vtkCommand::LeftButtonPressEvent,this,&CriticalPointRenderer::mouse_event);
  }

  if(!var && m_cp_click_observer_tag)
  {
    m_dmgr->ren()->GetRenderWindow()->GetInteractor()->RemoveObserver
        (m_cp_click_observer_tag);

    m_cp_click_observer_tag = 0;
  }
}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::set_index_enabled(int idx, bool var)
{
  m_cp_show[idx] = var;
}

/*---------------------------------------------------------------------------*/

bool CriticalPointRenderer::get_index_enabled(int i)
{
  if(0 <= i && i < 4)return m_cp_show[i];else return false;
}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::set_enabled(bool var)
{
  m_show = var;
}

/*---------------------------------------------------------------------------*/

bool CriticalPointRenderer::get_enabled()
{
  return m_show;
}

/*---------------------------------------------------------------------------*/

void CriticalPointRenderer::set_spheres_radii
(double r1, double r2, double r3, double r4)
{
  m_cp_spheresource[0]->SetRadius(r1);
  m_cp_spheresource[1]->SetRadius(r2);
  m_cp_spheresource[2]->SetRadius(r3);
  m_cp_spheresource[3]->SetRadius(r4);
}


/*****************************************************************************/
