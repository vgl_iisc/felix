#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkExtractPolyDataGeometry.h>
#include <vtkPolyDataMapper.h>
#include <vtkStripper.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkPointData.h>
#include <vtkTubeFilter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkBox.h>
#include <vtkCellArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkGlyphSource2D.h>
#include <vtkGlyph3D.h>
#include <vtkRenderer.h>
#include <vtkExtractGeometry.h>
#include <vtkFloatArray.h>
#include <vtkColorTransferFunction.h>
#include <extn/vtkPolyLineTangents.h>
#include <vtkCellData.h>

#include <extn/QTransferFunctionEditorWidget.hpp>

#include <tet_cc.hpp>
#include <tet_mscomplex.hpp>

#include <MsComplexDataManager.hpp>
#include <rens/SeparatrixSheetRenderer.hpp>

#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

#define VTK_NEW(variable,class)\
  variable = vtkSmartPointer<class>::New();

using namespace tet;
using namespace std;

typedef std::map<cellid_t,int>  cellid_to_int_t;
typedef std::map<ptet_cc_t::pcellid_t,int> pvrt_to_int_t;
typedef std::map<ptet_cc_t::pege_t,size_t> pege_to_int_t;
typedef std::map<ptet_cc_t::ptri_t,size_t> ptri_to_int_t;
typedef std::map<ptet_cc_t::ptet_t,size_t> ptet_to_int_t;
typedef std::map<ptet_cc_t::ptet_t,int_pair_t> ptet_to_int_pr_t;

extern const int d_color_list_size;
extern double d_cyan[];
extern double d_gray[];
extern double d_brown[];
extern double *d_color_list[];

/*****************************************************************************/
void SetupPrimalSepSheetsPolydata
(const int_pair_list_t &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars);

/*---------------------------------------------------------------------------*/

void SetupDualSepSheetsPolydata
(const int_pair_list_t &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars);

/*****************************************************************************/




/*****************************************************************************/

SeparatrixSheetRenderer::SeparatrixSheetRenderer():
  m_tf(new TransferFunction),
  m_particles_dist(-1),
  m_show(false),
  m_particles_show(false)
{}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::create(mscdmgr_cptr_t mscdmgr)
{
  m_dmgr = mscdmgr;

  // pipeline for sheet geometry
  {
    VTK_CREATE(vtkPolyData,polydata);

    VTK_CREATE(vtkExtractPolyDataGeometry,clipper);
#if VTK_MAJOR_VERSION <= 5
    clipper->SetInputConnection(polydata->GetProducerPort());
#else
    clipper->SetInputData(polydata);
#endif
    clipper->SetImplicitFunction(m_dmgr->clip_box());

    VTK_CREATE(vtkPolyDataMapper,mapper);
    mapper->SetInputConnection(clipper->GetOutputPort());

    VTK_CREATE(vtkActor,actor);
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(d_brown);

    m_dmgr->ren()->AddActor(actor);

    m_polydata = polydata;
    m_show     = false;
    m_mapper   = mapper;
    m_actor    = actor;
  }

  // pipeline for particle geometry
  {

    VTK_CREATE(vtkUnstructuredGrid,usgrid);

    VTK_CREATE(vtkExtractGeometry,clipper);
#if VTK_MAJOR_VERSION <= 5
    clipper->SetInputConnection(usgrid->GetProducerPort());
#else
    clipper->SetInputData(usgrid);
#endif
    clipper->SetImplicitFunction(m_dmgr->clip_box());

    VTK_CREATE(vtkGlyphSource2D,pt_glyph);
    pt_glyph->SetGlyphTypeToVertex();

    VTK_CREATE(vtkGlyph3D,pt_glypher);
#if VTK_MAJOR_VERSION <= 5
    pt_glypher->SetInputConnection(clipper->GetProducerPort());
    pt_glypher->SetSource(pt_glyph->GetOutput());
#else
    pt_glypher->SetInputConnection(clipper->GetOutputPort());
    pt_glypher->SetSourceData(pt_glyph->GetOutput());
#endif


    VTK_CREATE(vtkPolyDataMapper,mapper);
    mapper->SetInputConnection(pt_glypher->GetOutputPort());

    VTK_CREATE(vtkActor,actor);
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(0,0,0);

    m_dmgr->ren()->AddActor(actor);

    m_particles_actor  = actor;
    m_particles_usgrid = usgrid;
    m_particles_show   = false;
  }
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::update()
{
  update_sheets();
  update_particles();
  update_tf();
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::set_enabled(bool val)
{
  if (val == m_show) return;

  m_show = val;  

  if(m_show)
  {
    m_dmgr->ren()->AddActor(m_actor);
    if(m_particles_show)
      m_dmgr->ren()->AddActor(m_particles_actor);
    else
      m_dmgr->ren()->RemoveActor(m_particles_actor);
  }
  else
  {
    m_dmgr->ren()->RemoveActor(m_actor);
    m_dmgr->ren()->RemoveActor(m_particles_actor);
  }

  emit setEnabledEvent(val);
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::update_sheets()
{
  if(m_polydata->GetMTime() > m_pcs_MTime)
    return;

  // release old data
  m_polydata->Initialize();

  VTK_CREATE(vtkPoints,points);
  m_polydata->SetPoints(points);

  VTK_CREATE(vtkFloatArray,scalars);
  scalars->SetName("Scalars");
  m_polydata->GetPointData()->SetScalars(scalars);

  VTK_CREATE(vtkIntArray,cpids);
  cpids->SetName("cpids");
  m_polydata->GetCellData()->SetScalars(cpids);

  if(m_pcs.size() == 0)
    return;

  int idx = m_dmgr->msc()->index(m_pcs[0].first);

  ASSERT(idx == 1 || idx == 2);

  for(int i = 0 ; i < m_pcs.size(); ++i)
    ASSERT(m_dmgr->msc()->index(m_pcs[i].first) == idx);

  if (idx == 1)
    emit statusMessageEvent(tr("Updating 1-saddle Ascending Sheets"));
  else if(idx == 2)
    emit statusMessageEvent(tr("Updating 2-saddle Descending Sheets"));

  // with des gradient convention 2 sad des is primal
  // with asc gradient convention 1 sad asc is primal
  if( (m_dmgr->msc()->m_grad_conv == DES && idx == 2) ||
      (m_dmgr->msc()->m_grad_conv == ASC && idx == 1) )
    SetupPrimalSepSheetsPolydata
        (m_pcs,m_dmgr->msc(),m_dmgr->tcc(),m_polydata,
         m_dmgr->domain_ds()->GetPointData()->GetScalars());

  // with des gradient convention 1 sad asc is dual
  // with asc gradient convention 2 sad des is dual
  else if((m_dmgr->msc()->m_grad_conv == DES && idx == 1) ||
          (m_dmgr->msc()->m_grad_conv == ASC && idx == 2) )
    SetupDualSepSheetsPolydata
        (m_pcs,m_dmgr->msc(),m_dmgr->tcc(),m_polydata,
         m_dmgr->domain_ds()->GetPointData()->GetScalars());

  m_pcs.clear();
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::set_color(double r,double g,double b)
{
  m_mapper->SetScalarVisibility(0);
  m_actor->GetProperty()->SetColor(r,g,b);
}


/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::set_tf_enabled(bool val)
{
  if(val == m_mapper->GetScalarVisibility())
    return;

  m_mapper->SetScalarVisibility(val);

  if(val)
    update_tf();
}

/*---------------------------------------------------------------------------*/

void default_tf(TransferFunction &);

void SeparatrixSheetRenderer::update_tf()
{
  if(!m_mapper->GetScalarVisibility() || m_polydata->GetNumberOfPoints() ==0)
    return;

  if(m_tf->size() == 0)
    default_tf(*m_tf);

  double rng[2];

  m_polydata->GetPointData()->GetScalars()->GetRange(rng,0);

  VTK_CREATE(vtkColorTransferFunction,ctf);

  for( int i = 0 ; i < m_tf->size(); ++i)
  {
    double  f = m_tf->at(i).f;
    QColor  c = m_tf->at(i).col;

    f = rng[0] + f*(rng[1]- rng[0]);

    ctf->AddRGBPoint(f,c.redF(),c.greenF(),c.blueF());
  }

  ctf->Modified();

  m_mapper->SetLookupTable(ctf);
  m_mapper->SetScalarRange(rng);
  m_mapper->Modified();
}

/*---------------------------------------------------------------------------*/

bool SeparatrixSheetRenderer::save_tf(const QString &fn) const
{
  return SaveTransferFunction(*m_tf,fn);
}

/*---------------------------------------------------------------------------*/

bool SeparatrixSheetRenderer::load_tf(const QString &fn)
{
  TransferFunction tf;

  bool ret = LoadTransferFunction(tf,fn);

  if(ret)
  {
    qSort(tf);

    qreal m = tf.front().f;
    qreal M = tf.back().f;

    for(int i = 0 ; i < tf.size() ; ++i)
      tf[i].f = (tf[i].f - m) / (M-m);

    *m_tf = tf;
  }

  return ret;
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::edit_tf()
{
  double rng[2];
  m_polydata->GetPointData()->GetScalars()->GetRange(rng,0);

  if(QTransferFunctionEditorDialog::editTransferFunction
      (*m_tf,rng[0],rng[1],true))
    update_tf();
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::save_data(QString fn)
{
  VTK_CREATE(vtkXMLPolyDataWriter,writer);
#if VTK_MAJOR_VERSION <= 5
  writer->SetInputConnection(m_polydata->GetProducerPort());
#else
  writer->SetInputData(m_polydata);
#endif
  writer->SetFileName(fn.toStdString().c_str());
  writer->SetDataModeToAscii();
  writer->Update();
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::set_particles_enabled(bool val)
{
  if (val == m_particles_show) return;

  m_particles_show = val;

  if(m_particles_show)
    m_dmgr->ren()->AddActor(m_particles_actor);
  else
    m_dmgr->ren()->RemoveActor(m_particles_actor);
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::set_particle_viewprops
(double r, double g, double b, double pt_size)
{
  m_particles_actor->GetProperty()->SetPointSize(pt_size);
  m_particles_actor->GetProperty()->SetColor(r,g,b);
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::update_particles()
{
  vtkPolyData * pd =  m_polydata;

  if(m_particles_usgrid->GetMTime() > pd->GetMTime() &&
      m_particles_usgrid->GetMTime() > m_particles_dist_MTime)
    return;

  m_particles_usgrid->Initialize();

  if(pd->GetNumberOfPoints() == 0 || m_particles_dist < 0)
    return;

  VTK_CREATE(vtkPoints,pts);
  m_particles_usgrid->SetPoints(pts);

  m_particles_usgrid->Allocate();

  int nv = m_dmgr->tcc()->num_dcells(0);

  for(vtkIdType i=0,j=0; i < nv; ++i)
  {
    utl::dvec3 pt = m_dmgr->tcc()->get_vert(i),clpt;
    pd->GetPoint(pd->FindPoint(pt.m_elems),clpt.m_elems);

    if((pt-clpt).norm2() < m_particles_dist)
    {
      pts->InsertNextPoint(pt.m_elems);
      m_particles_usgrid->InsertNextCell(VTK_VERTEX,1,&j);++j;
    }
  }

  pts->Modified();
  m_particles_usgrid->Modified();
#if VTK_MAJOR_VERSION <= 5
  m_particles_usgrid->Update();
#endif
}

/*---------------------------------------------------------------------------*/

void SeparatrixSheetRenderer::save_particles(QString fn)
{
  ofstream fs(fn.toStdString().c_str());

  ENSURE_OR_RETURN(fs.is_open())
      << "Unable to open file !!!" << SVAR(fn.toStdString());


  int nv = m_dmgr->tcc()->num_dcells(0);

  fs << "# Particles List " <<endl;
  fs << "# ListIndex Xcoord Ycoord Zcoord" <<endl;

  for(vtkIdType i=0; i < nv; ++i)
  {
    utl::dvec3 pt = m_dmgr->tcc()->get_vert(i),clpt;
    m_polydata->GetPoint(m_polydata->FindPoint(pt.m_elems),clpt.m_elems);

    if((pt-clpt).norm2() < m_particles_dist)
    {
      fs << i     << " "
         << pt[0] << " "
         << pt[1] << " "
         << pt[2] << " "
         << endl;
    }
  }

  fs.close();
}

/*****************************************************************************/



/*****************************************************************************/

void SetupPrimalSepSheetsPolydata
(const int_pair_list_t &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars)
{
  vtkPoints * points          = polydata->GetPoints();
  vtkDataArray *pd_scalars    = polydata->GetPointData()->GetScalars();
  vtkDataArray *pd_cpids      = polydata->GetCellData()->GetScalars();

  pvrt_to_int_t pvrtmap;

  std::vector<utl::ivec4> tris;

  // Primal separatrix sheets are the Descending manifolds of 2 saddles with
  // descending gradient convention. pcs must be all 2 saddles.
  // Primal separatrix sheets are the Ascending manifolds of 1 saddles with
  // ascending gradient convention. pcs must be all 1 saddles.
  tet::eGDIR dir = msc->m_grad_conv;

  BOOST_FOREACH(int_pair_t pc_pr,pcs)
  {

    int i    = pc_pr.first;
    int i_cp = pc_pr.second;


    BOOST_FOREACH(cellid_t c, msc->m_mfolds[dir][i])
    {
      ptet_cc_t::ptri_t ptri = tcc->get_pcell<3>(c);

      pvrt_to_int_t::iterator pv1 =
          pvrtmap.insert(std::make_pair(ptri[0],pvrtmap.size())).first;

      pvrt_to_int_t::iterator pv2 =
          pvrtmap.insert(std::make_pair(ptri[1],pvrtmap.size())).first;

      pvrt_to_int_t::iterator pv3 =
          pvrtmap.insert(std::make_pair(ptri[2],pvrtmap.size())).first;

      tris.push_back(utl::mk_vec<int>(pv1->second,
                                      pv2->second,
                                      pv3->second,i_cp));
    }
  }

  points->SetNumberOfPoints(pvrtmap.size());
  pd_scalars->SetNumberOfTuples(pvrtmap.size());

  BOOST_FOREACH(pvrt_to_int_t::value_type pr, pvrtmap)
  {
    vert_t  v = tcc->get_vert(pr.first);
    points->SetPoint(pr.second,v[0],v[1],v[2]);

    double f = usgrid_scalars->GetComponent(pr.first.cid,0);
    pd_scalars->SetComponent(pr.second,0,f);
  }

  polydata->Allocate(tris.size());
  pd_cpids->SetNumberOfTuples(tris.size());

  for(int i = 0 ; i < tris.size(); i++)
  {
    vtkIdType t[] = {(vtkIdType)(tris[i][0]),
                     (vtkIdType)(tris[i][1]),
                     (vtkIdType)(tris[i][2])};

    polydata->InsertNextCell(VTK_TRIANGLE,3,t);
    pd_cpids->SetComponent(i,0,tris[i][3]);
  }

  pd_scalars->Modified();
  pd_cpids->Modified();
  polydata->Modified();
#if VTK_MAJOR_VERSION <= 5
  polydata->Update();
#endif
}

/*---------------------------------------------------------------------------*/

void SetupDualSepSheetsPolydata
(const int_pair_list_t &pcs,
 tet::mscomplex_cptr_t    msc,
 tet::ptet_geom_cc_cptr_t tcc,
 vtkPolyData *polydata,
 vtkDataArray *usgrid_scalars)
{
  using std::make_pair;

  vtkPoints * points          = polydata->GetPoints();
  vtkDataArray *pd_scalars    = polydata->GetPointData()->GetScalars();
  vtkDataArray *pd_cpids      = polydata->GetCellData()->GetScalars();

  cellid_to_int_t egeset;
  ptri_to_int_t ptriset;
  ptet_to_int_t ptetset;

  cellid_to_int_t::iterator ege_it;
  ptri_to_int_t::iterator tri_it;
  ptet_to_int_t::iterator tet_it;

  std::vector<utl::ivec4> tris;
  std::map<int,int>       cps;

  ptet_cc_t::ptet_t ptet;
  ptet_cc_t::ptri_t ptri;

  // Dual separatrix Sheets are the Ascending manifolds of 1 saddles with
  // descending gradient convention. pcs must be all 1 saddles.
  // Dual separatrix Sheets are the Descending manifolds of 2 saddles with
  // ascending gradient convention. pcs must be all 2 saddles.
  tet::eGDIR dir = (msc->m_grad_conv == tet::DES)?(tet::ASC):(tet::DES);

  BOOST_FOREACH(int_pair_t pc_pr,pcs)
  {
    int pc = pc_pr.first;
    int cp = pc_pr.second;

    cps.insert(make_pair(cp,cps.size()%d_color_list_size));

    BOOST_FOREACH(cellid_t egec, msc->m_mfolds[dir][pc])
    {
      cellid_t star[DMAX];
      int ct = tcc->get_edge_star(egec,star);

      ege_it = egeset.insert(make_pair(egec,egeset.size())).first;

      ptri   = tcc->align_cwrtf<3>(star[0],egec);
      tri_it = ptriset.insert(make_pair(ptri,ptriset.size())).first;

      for( int k = 0 ; k < ct-2; k += 2)
      {
        ptet = tcc->align_cwrtf<4>(star[k+1],egec);
        tet_it = ptetset.insert(make_pair(ptet,ptetset.size())).first;

        tris.push_back(utl::mk_vec<int>(ege_it->second,
                                        tri_it->second,
                                        tet_it->second,
                                        cp));

        ptri   = tcc->align_cwrtf<3>(star[k+2],egec);
        tri_it = ptriset.insert(make_pair(ptri,ptriset.size())).first;

        tris.push_back(utl::mk_vec<int>(ege_it->second,
                                        tet_it->second,
                                        tri_it->second,
                                        cp));
      }
    }
  }

  points->SetNumberOfPoints(egeset.size()+ptriset.size()+ptetset.size());
  pd_scalars->SetNumberOfTuples(egeset.size()+ptriset.size()+ptetset.size());

  BOOST_FOREACH(cellid_to_int_t::value_type pr,egeset)
  {
    vert_t v = tcc->get_centroid<2>(pr.first);
    points->SetPoint(pr.second,v[0],v[1],v[2]);

    tet::ptet_cc_t::pege_t simplex= tcc->get_pcell<2>(pr.first);

    double f0 = usgrid_scalars->GetComponent(simplex[0].cid,0);
    double f1 = usgrid_scalars->GetComponent(simplex[1].cid,0);
    double   f= max(f0,f1);

    pd_scalars->SetComponent(pr.second,0,f);
  }

  int tri_off = egeset.size();

  BOOST_FOREACH(ptri_to_int_t::value_type pr, ptriset)
  {
    vert_t  v = tcc->get_centroid<3>(pr.first);
    points->SetPoint(pr.second + tri_off,v[0],v[1],v[2]);

    double f0 = usgrid_scalars->GetComponent(pr.first[0].cid,0);
    double f1 = usgrid_scalars->GetComponent(pr.first[1].cid,0);
    double f2 = usgrid_scalars->GetComponent(pr.first[2].cid,0);

    double   f= max(max(f0,f1),f2);

    pd_scalars->SetComponent(pr.second+tri_off,0,f);
  }

  int tet_off = tri_off + ptriset.size();

  BOOST_FOREACH(ptet_to_int_t::value_type pr, ptetset)
  {
    vert_t  v = tcc->get_centroid<4>(pr.first);
    points->SetPoint(pr.second + tet_off,v[0],v[1],v[2]);

    double f0 = usgrid_scalars->GetComponent(pr.first[0].cid,0);
    double f1 = usgrid_scalars->GetComponent(pr.first[1].cid,0);
    double f2 = usgrid_scalars->GetComponent(pr.first[2].cid,0);
    double f3 = usgrid_scalars->GetComponent(pr.first[3].cid,0);

    double   f= max(max(max(f0,f1),f2),f3);

    pd_scalars->SetComponent(pr.second+tet_off,0,f);
  }

  polydata->Allocate(tris.size());
  pd_cpids->SetNumberOfTuples(tris.size());

  for(int i = 0 ; i < tris.size(); i += 2)
  {
    vtkIdType tri1[] = {(vtkIdType)(tris[i][0]),
                        (vtkIdType)(tris[i][1] + tri_off),
                        (vtkIdType)(tris[i][2] + tet_off)};

    vtkIdType tri2[] = {(vtkIdType)(tris[i+1][0]),
                        (vtkIdType)(tris[i+1][1] + tet_off),
                        (vtkIdType)(tris[i+1][2] + tri_off)};


    polydata->InsertNextCell(VTK_TRIANGLE,3,tri1);
    polydata->InsertNextCell(VTK_TRIANGLE,3,tri2);

    pd_cpids->SetComponent(i,  0,cps[tris[i][3]]);
    pd_cpids->SetComponent(i+1,0,cps[tris[i+1][3]]);
  }

  pd_scalars->Modified();
  pd_cpids->Modified();
  polydata->Modified();
#if VTK_MAJOR_VERSION <= 5
  polydata->Update();
#endif
}

/*****************************************************************************/
