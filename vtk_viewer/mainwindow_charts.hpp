#ifndef MAINWINDOW_CHARTS_HPP
#define MAINWINDOW_CHARTS_HPP

#include <QWidget>

#include <vtkSmartPointer.h>
#include <vtkTimeStamp.h>

#include <tet.hpp>

#include <set>

/*********************************************************************/

class QVTKWidget;
class QGridLayout;

class vtkChart;
class vtkObject;

class ChartMatrix:public QWidget
{
  Q_OBJECT

protected:

  struct chart_data_t
  {
    vtkSmartPointer<vtkChart>       chart;
    QVTKWidget                     *widget;
  };

  QGridLayout              * gridl;
  std::vector<chart_data_t>  chart_data;
  std::vector<int>           synced_charts;

public:

  ChartMatrix(QWidget *p=0,Qt::WindowFlags f=0);

  void addChart(vtkSmartPointer<vtkChart>,int r,int c,bool s=true);

  void ranges_changed(vtkObject *,unsigned long ,void *);

  void vtkRender();
};

/*********************************************************************/



/*********************************************************************/
class vtkTable;
class vtkStringArray;
class vtkPlotPoints;
class vtkPlotLine;

class PersistenceCharts:public ChartMatrix
{
  Q_OBJECT

public:
  enum {SELECTION_NONE,SELECTION_ADDITION,
        SELECTION_SUBTRACTION,SELECTION_TOGGLE};

  PersistenceCharts(QWidget *p=0,Qt::WindowFlags f=0);

  void create(tet::mscomplex_cptr_t msc);
  void update();


  /// \brief get the list of selected cps
  ///
  /// The key of the map is the cpno. The value is the requested resolution
  /// During selection. (-1 means nothing was set. )
  inline const std::map<int,int> &get_selected_cps() const{return m_selected_cps;}

  inline vtkTimeStamp get_selected_cps_MTime() const{return m_selected_cps_MTime;}

  inline void set_msc_res(int res) {m_cutoff = res;}



public slots:

  inline int  get_selection_mode() const
  {return m_selection_mode;}

  inline void set_selection_mode(int i)
  {m_selection_mode =i;}

  void select_pers_pts_in_rect(double l,double b, double w, double h);
  void select_pers_pts_above_line(double a,double b, double c);
  void select_pers_pts_range(double l,double u);
  void select_pers_pt(int i);
  bool select_cp(int i,int res=-1);
  bool deselect_cp(int i);
  void deselect_all_cps();

  /// \brief Save a Screenshot (only png format)
  ///
  /// \param[in] fn  fname
  /// \param[in] mag magnification
  ///
  /// \note mag does not behave well with charts so it is ignored now
  void save_screenshot(QString  fn,int mag=1) const;

  /// \brief get the list of cps of dim d selected
  QList<int> get_selected_dcps(int d) const;

  /// \brief enable/disable showing unpaired cps [default = True]
  void set_show_unpaired_cps(bool val);

  /// \brief enable/disable showing axis label [default = True]
  void set_show_axis_labels(bool val);

  /// \brief set font size [default = .]
  void set_font_size(int s);

  /// \brief set point size [default = 1]
  void set_point_size(double s);

  /// \brief set line size [default = 1]
  void set_line_size(double s);




protected:

  void pers_end_selection(vtkObject *,unsigned long ,void *);
  bool _select_pers_pt(int i, int pres=-1,int qres=-1);

  tet::mscomplex_cptr_t            m_msc;
  int                              m_cutoff;
  bool                             m_show_unpaired_cps;

  int                              m_selection_mode;
  std::map<int,int>                m_selected_cps;

  std::vector<tet::int_pair_t>     m_pers_pts;
  std::vector<int>                 m_cp_pers_pt;
  vtkSmartPointer<vtkTable>        m_pers_pts_tables[6];
  vtkSmartPointer<vtkStringArray>  m_pers_pts_labels[6];
  vtkSmartPointer<vtkPlotPoints>   m_pers_pts_plotpts[6];
  vtkSmartPointer<vtkPlotLine>     m_pers_diagonal;

  // Time stamps
  vtkTimeStamp                     m_selected_cps_MTime;

private slots:
  inline void none_clicked()
  {set_selection_mode(SELECTION_NONE);}
  inline void addition_clicked()
  {set_selection_mode(SELECTION_ADDITION);}
  inline void subtraction_clicked()
  {set_selection_mode(SELECTION_SUBTRACTION);}
  inline void toggle_clicked()
  {set_selection_mode(SELECTION_TOGGLE);}

private:
  void create_diagonal();

  void update_display();
  void update_selection_display();

};

/*********************************************************************/



/*********************************************************************/

class vtkColorTransferFunction;
class vtkImageData;

class PersistenceHistogramCharts:public ChartMatrix
{
  Q_OBJECT

public:
  PersistenceHistogramCharts(QWidget *p=0,Qt::WindowFlags f=0);

  void create(tet::mscomplex_ptr_t msc);
  void update_cutoff(int cutoff);

protected:

  struct pers_hist_data_t
  {
    vtkSmartPointer<vtkColorTransferFunction>  tf;
    vtkSmartPointer<vtkImageData>              hist;
  };

  tet::mscomplex_ptr_t             m_msc;
  int                              m_cutoff;
  pers_hist_data_t                 m_pers_hist_data[3];
};

/*********************************************************************/



/*********************************************************************/

#include <QVTKWidget.h>
class PersistentBettiNumbersPlot:public QVTKWidget
{
  Q_OBJECT

public:
  PersistentBettiNumbersPlot(QWidget *p=0,Qt::WindowFlags f=0);

  void create(tet::mscomplex_ptr_t msc);
  void update_cutoff(int cutoff);

protected:
  tet::mscomplex_ptr_t             m_msc;
  int                              m_cutoff;
  vtkSmartPointer<vtkTable>        m_pers_betti_tables[3];
};

/*********************************************************************/



/*********************************************************************/
class CriticalPointsHistograms:public ChartMatrix
{
  Q_OBJECT

public:
  CriticalPointsHistograms(QWidget *p=0,Qt::WindowFlags f=0);

  void create(tet::mscomplex_ptr_t msc);
  void update_cutoff(int cutoff);

protected:
  tet::mscomplex_ptr_t             m_msc;
  int                              m_cutoff;

  vtkSmartPointer<vtkTable>        m_cp_hist_tables[5];
};

/*********************************************************************/



#endif
