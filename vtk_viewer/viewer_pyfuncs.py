from operator import itemgetter  

def select_two_saddles(sthresh,mthresh):  
  """
  go through the all 2-saddles and select them if
  a) they have function value above sthresh  
  b) When they cancel, they are connected to cps with function value > mthresh
  """

  last_msc_version = ms_mw.get_mscomplex_multires_version() 
  
  msc = ms_mw.get_mscomplex()
  ncps = msc.num_cps()

  sad_list = [(x,msc.cancno(x)) for x in range(0,ncps) 
	      if msc.cp_index(x) == 2 and msc.cp_func(x) >= sthresh and msc.cp_is_paired(x) == False ]
	      
  inf_sad_list    = [(s,resno) for s,resno in sad_list if resno == ncps]  
  inf_sad_contrib = [(s,msc.cancno(cs)) for s,sres in inf_sad_list for cs in msc.get_contrib_cps_max(s,1) if s != cs]  
  inf_sad_contrib = sorted(inf_sad_contrib,key=itemgetter(1))  
  inf_max_valres  = dict()
  
  for s,resno in inf_sad_contrib:
    ms_mw.set_mscomplex_multires_version(resno)    
    if len(msc.asc(s)) == 2:
      inf_max_valres[s] = resno
      
  sad_list  = [(s,resno) for s,resno in sad_list if resno != ncps] 
  sad_list += [(s,resno) for s,resno in inf_sad_contrib]


  sad_list = sorted(sad_list,key=itemgetter(1))
  
  sadset,maxset = set(),set()

  for s,cancno in sad_list:
    ms_mw.set_mscomplex_multires_version(cancno)  
    
    should_add = True
    
    asc = msc.asc(s)
    
    if msc.cp_func(asc[0]) < mthresh:    
      should_add = False
    
    if len(asc) == 1 or msc.cp_func(asc[1]) < mthresh:
      should_add = False

    if should_add:
      sadset.add(s)
      for m in msc.asc(s):
	maxset.add(m)
  
  contribsads = set()
  contribmaxs = set()
  
  #ms_mw.set_mscomplex_multires_version(ncps)  
  
  for s in sadset:
    scontribs = None
    if inf_max_valres.has_key(s):
      scontribs = msc.get_contrib_cps(s,1,inf_max_valres[s])
    else:
      scontribs = msc.get_contrib_cps_max(s,1)
      
    for os in scontribs:
      if os != s:
	contribsads.add(os)
	contribmaxs.add(msc.pair_idx(os))
  
  sadset = sadset-contribsads
  maxset = maxset-contribmaxs
  
  
  pers = ms_mw.get_pers_charts()
  
  for s in sadset:
    if inf_max_valres.has_key(s):      
      pers.select_cp(s,inf_max_valres[s])
    else:
      pers.select_cp(s)
  
  for m in maxset:
    pers.select_cp(m)

  ms_mw.set_mscomplex_multires_version(last_msc_version)
  
def ranged_select_two_saddles(Sb,Se,Mb,Me):  

  last_msc_version = ms_mw.get_mscomplex_multires_version() 
  
  msc  = ms_mw.get_mscomplex()
  ncps = msc.num_cps()
  
  # Make sure the input parameters are in range
  Sb,Me = max(Sb,msc.fmin()),min(Me,msc.fmax())
  Se,Mb = max(Sb,Se),min(Mb,Me)
 
  # pick the list of surviving 2saddles that have function in given saddle range
  # the resolution is the minimum of the res at which the saddle gets cancelled and 
  # the res at which the saddle gets connected to a maximum beyond Me
  sad_list = [(x,min(msc.cp_hversion(x),msc.get_hversion_pers(abs(Me - msc.cp_func(x))))) 
              for x in msc.cps(2) if Sb <= msc.cp_func(x)  and msc.cp_func(x) <= Se ]

  # sort the 2saddles list in increasng order of resolution
  sad_list = sorted(sad_list,key=itemgetter(1))
  
  print len(sad_list) 
  
  # Check that the connected Maxima are within the maxima range
  def check_conn_max_range(s,res):
    ms_mw.set_mscomplex_multires_version(res)
    
    asc = msc.asc(s)
    
    if msc.cp_func(asc[0][0]) < Mb or msc.cp_func(asc[0][0]) > Me:
      return False    
    
    if len(asc) == 1:
      return False
    
    if msc.cp_func(asc[1][0]) < Mb or msc.cp_func(asc[1][0]) > Me:
      return False
    
    return True
    
  sad_list = [(s,r) for s,r in sad_list if check_conn_max_range(s,r)] 
  
  sad_list = list(reversed(sad_list))
  
  print len(sad_list)
 
  # update the pers charts object with the selection
  pers = ms_mw.get_pers_charts()
  
  for s,r in sad_list:
    pers.select_cp(int(s),int(r))
    
    ms_mw.set_mscomplex_multires_version(r)
    
    for m in msc.asc(s):
      pers.select_cp(int(m[0]),int(r))

  ms_mw.set_mscomplex_multires_version(last_msc_version)
  
  
def select_max(mthresh):  
  """
  go through the all maxima and select them if
  they have function value above mthresh  
  """  
  msc = ms_mw.get_mscomplex()

  max_list = [x for x in range(0,msc.num_cps()) 
	      if msc.cp_index(x) == 3 and msc.cp_func(x) >= mthresh and msc.cp_is_paired(x) == False ]
	      
  [ms_mw.get_pers_charts().select_cp(m) for m in max_list]
  
def max_des_rand_tf():
  pers = ms_mw.get_pers_charts()
  maxs = sorted(pers.get_selected_dcps(3))
  
  vol = ms_mw.get_volren()
  
   
  if len(maxs) == 0:
    return None 
    
  tf = [-1,0,0,0,0]
  tf = [-0.5,0,0,0,0]
  
  from colorsys import hsv_to_rgb
  import random
  
  random.seed() 
  
    
  for i in range(0,len(maxs)):    
    r,g,b = hsv_to_rgb(random.random(),0.95,0.5)    
    tf += [i - 0.25,r,g,b,0.85]
    tf += [i + 0.25,r,g,b,0.85]
    tf += [i + 0.5,r,g,b,0]
    
  tf = tf[:-5]
    
  tf[-5] = int(tf[-5])    
  
   
  vol.set_tf(tf)
  
#def selected_2sad_1sad_asc():
  #pers  = ms_mw.get_pers_charts()
  #msc   = ms_mw.get_mscomplex()
  
  #last_msc_version = ms_mw.get_mscomplex_multires_version()
  
  #ncps  = msc.num_cps()
  
  #sad2s = [x for x in pers.get_selected_dcps(2)]
  
  #from operator import itemgetter   
 
  #inf_2sad_list    = [(s,resno) for s,msc.cancno(s) in sad2s if resno == ncps]  
  #inf_2sad_contrib = [(s,msc.cancno(cs)) for s,sres in inf_2sad_list for cs in msc.get_contrib_cps_max(s,1) if s != cs]  
  #inf_2sad_contrib = sorted(inf_sad_2contrib,key=itemgetter(1))  
  #inf_2max_valres  = dict()
  
  #for s,resno in inf_2sad_contrib:
    #ms_mw.set_mscomplex_multires_version(resno)    
    #if len(msc.asc(s)) == 2:
      #inf_max_2valres[s] = resno
  
  #sad1s = [ (x,msc.cancno(x)) for x in range(0,msc.num_cps()) 
                     #if msc.cp_is_paired(x) == False and msc.cp_index(x) == 1]
                     
  #contribsads = set()  
  
  #for s in sad2s:
    #scontribs = None
    #if inf_max_valres.has_key(s):
      #scontribs = msc.get_contrib_cps(s,1,inf_max_valres[s])
    #else:
      #scontribs = msc.get_contrib_cps_max(s,1)
      
    #for os in scontribs:
      #if os != s:
	#contribsads.add(os)
	
	
  #sel1sads = []

  
  #for s,cancno in sad1s:
    
    #ms_mw.set_mscomplex_multires_version(cancno)
    
    #asc2 = set(msc.asc(s)) - contribsads
    
    #if len(asc2.intersection(sad2s) != 0):
      
      #sel1sads.append(s)
 
  #for s in sel1sads:
    #pers.select_cp(s)
  
  #ms_mw.set_mscomplex_multires_version(last_msc_version)
  
 
  
def des_conn_at_res(s,resno=None):
  msc   = ms_mw.get_mscomplex()  
  last_msc_version = ms_mw.get_mscomplex_multires_version()  
  if not resno:
    resno = msc.cancno(s)  
  ms_mw.set_mscomplex_multires_version(resno)
  ret = msc.des_conn_cps(s)    
  ms_mw.set_mscomplex_multires_version(last_msc_version)  
  return ret
  
def asc_conn_at_res(s,resno=None):
  msc   = ms_mw.get_mscomplex()  
  last_msc_version = ms_mw.get_mscomplex_multires_version()  
  if not resno:
    resno = msc.cancno(s)  
  ms_mw.set_mscomplex_multires_version(resno)
  ret = msc.asc(s)    
  ms_mw.set_mscomplex_multires_version(last_msc_version)  
  return ret  
  
def select_all_cps():
  msc  = ms_mw.get_mscomplex()
  pers = ms_mw.get_pers_charts()
  
  for cp in msc.cps():
    pers.select_cp(cp)
    
def deselect_all_cps():
  msc  = ms_mw.get_mscomplex()
  pers = ms_mw.get_pers_charts()
 
  for cp in msc.cps():
    pers.deselect_cp(int(cp))
    
  