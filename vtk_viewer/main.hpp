#ifndef MAIN_HPP
#define MAIN_HPP
#include <QApplication>

class MainWindow;

class MainApplication: public QApplication
{
  Q_OBJECT

  MainWindow *m_mw;
  int         m_retcode;
  std::string m_py_script;

public:
  MainApplication( int & argc, char ** argv);
  ~MainApplication();

  inline int get_retcode() const {return m_retcode;}

public slots:
  void eventLoopStarted();
  void cleanUp();
};

#endif
