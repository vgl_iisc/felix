#include <mainwindow.h>
#include <main.hpp>

#include <QTimer>

#include <boost/program_options.hpp>

using namespace std;
namespace bpo = boost::program_options;

/*********************************************************************/

MainApplication::MainApplication(int &ac, char **av):
  QApplication(ac,av),m_retcode(-1),m_mw(NULL)
{
  bool bStartExec = true;

#ifdef PYTHONQT_ENABLED
  bpo::options_description desc("Allowed options");

  desc.add_options()
   ("help,h", "produce help message")
   ("eval-script-file,e",bpo::value(&m_py_script)->default_value(""),
    "a script to evaluate");

  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(ac, av, desc), vm);

  if (vm.count("help"))
  {
    cout << desc << endl;
    bStartExec = false;
  }

  try
  {
    bpo::notify(vm);
  }
  catch(bpo::required_option e)
  {
    cout<<e.what()<<endl;
    cout<<desc<<endl;
    bStartExec = false;
  }
#endif

  if(!bStartExec)
    return;

  m_mw = new MainWindow;
  m_mw->show();

  QTimer::singleShot(0,this,SLOT(eventLoopStarted()));

  connect(this,SIGNAL(aboutToQuit()),this,SLOT(cleanUp()));

  m_retcode = exec();
}

/*-------------------------------------------------------------------*/

MainApplication::~MainApplication()
{
  cleanUp();
}

/*-------------------------------------------------------------------*/

void MainApplication::cleanUp()
{
  if(m_mw)
  {
    delete m_mw;
    m_mw = NULL;
  }
}

/*-------------------------------------------------------------------*/

void MainApplication::eventLoopStarted()
{
  if(!m_py_script.empty())
    m_mw->eval_script(m_py_script.c_str());
}

/*********************************************************************/

int main( int ac, char** av )
{
  MainApplication app( ac, av );
  return app.get_retcode();
}

/*********************************************************************/

