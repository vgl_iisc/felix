project(mscomplex-tet-vtk-viewer)

cmake_minimum_required(VERSION 2.8.3)

find_package(Qt5 COMPONENTS Core Quick OpenGL Gui)

find_package(PythonInterp REQUIRED)
find_package(PythonLibs REQUIRED)
 
set(PYTHONQT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../external/PythonQt/)
set(PYTHONQT_CONSOLE_DIR ${PYTHONQT_DIR}/examples/NicePyConsole)

add_subdirectory(${PYTHONQT_DIR}  ${CMAKE_CURRENT_BINARY_DIR}/PythonQt)


find_package(Boost 1.48 COMPONENTS program_options system date_time python REQUIRED)

find_package(OpenMP REQUIRED)

#this has to come only after qt
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})



if(OPENMP_FOUND)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}  
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/extn/
  ${CMAKE_CURRENT_SOURCE_DIR}/../core
  ${CMAKE_CURRENT_SOURCE_DIR}/../pymstet/
  
  ${Boost_INCLUDE_DIR}
  
  ${PYTHONQT_DIR}/src
  ${PYTHONQT_DIR}/extensions/PythonQt_QtAll
  ${PYTHONQT_CONSOLE_DIR}

  ${PYTHON_INCLUDE_DIRS}
  )

# Set your files and resources here
SET(VTK_VIEWER_SRCS
  main.hpp
  main.cpp

  mainwindow.cpp
  mainwindow_deprecated.cpp
  mainwindow.h

  mainwindow_charts.cpp
  mainwindow_charts.hpp

  MsComplexDataManager.hpp
  MsComplexDataManager.cpp

  extn/QTransferFunctionEditorWidget.hpp
  extn/QTransferFunctionEditorWidget.cpp
  extn/QPiecewiseFunctionEditorWidget.hpp
  extn/QPiecewiseFunctionEditorWidget.cpp
  extn/vtkSmoothPolyDataFilter2.h
  extn/vtkSmoothPolyDataFilter2.cxx
  extn/vtkPolyLineTangents.h
  extn/vtkPolyLineTangents.cxx
  extn/ctkDoubleRangeSlider.h
  extn/ctkDoubleRangeSlider.cpp
  extn/ctkRangeSlider.h
  extn/ctkRangeSlider.cpp

  
  rens/VolumeRenderer.cpp
  rens/VolumeRenderer.hpp

  rens/SeparatrixLineRenderer.cpp
  rens/SeparatrixLineRenderer.hpp

  rens/SeparatrixSheetRenderer.cpp
  rens/SeparatrixSheetRenderer.hpp

  rens/CriticalPointRenderer.cpp
  rens/CriticalPointRenderer.hpp

  rens/DecorationsRenderer.cpp
  rens/DecorationsRenderer.hpp
  
  extn/PythonQtScriptingConsole.h
  extn/PythonQtScriptingConsole.cpp
  
  ${PYTHONQT_CONSOLE_DIR}/NicePyConsole.cpp
  ${PYTHONQT_CONSOLE_DIR}/NicePyConsole.h

  ${PYTHONQT_CONSOLE_DIR}/PygmentsHighlighter.cpp
  ${PYTHONQT_CONSOLE_DIR}/PygmentsHighlighter.h

  ${PYTHONQT_CONSOLE_DIR}/PythonCompleter.cpp
  ${PYTHONQT_CONSOLE_DIR}/PythonCompleter.h

  ${PYTHONQT_CONSOLE_DIR}/PythonCompleterPopup.cpp
  ${PYTHONQT_CONSOLE_DIR}/PythonCompleterPopup.h

  ${PYTHONQT_CONSOLE_DIR}/SimpleConsole.cpp
  ${PYTHONQT_CONSOLE_DIR}/SimpleConsole.h

)

SET(VTK_VIEWER_UI mainwindow.ui rens/VolumeRenderer.ui rens/SeparatrixLineSelector.ui)


SET(VTK_VIEWER_MOC_HEADERS 
  main.hpp
  mainwindow.h
  mainwindow_charts.hpp
  MsComplexDataManager.hpp

  rens/VolumeRenderer.hpp
  rens/SeparatrixLineRenderer.hpp
  rens/SeparatrixSheetRenderer.hpp
  rens/CriticalPointRenderer.hpp
  rens/DecorationsRenderer.hpp

  extn/QTransferFunctionEditorWidget.hpp
  extn/QPiecewiseFunctionEditorWidget.hpp
  extn/ctkDoubleRangeSlider.h
  extn/ctkRangeSlider.h
  
  ${PYTHONQT_CONSOLE_DIR}/NicePyConsole.h
  ${PYTHONQT_CONSOLE_DIR}/PygmentsHighlighter.h
  ${PYTHONQT_CONSOLE_DIR}/PythonCompleter.h
  ${PYTHONQT_CONSOLE_DIR}/PythonCompleterPopup.h
  ${PYTHONQT_CONSOLE_DIR}/SimpleConsole.h

)
SET(VTK_VIEWER_RESOURCES ${VTK_VIEWER_RESOURCES}
  icons/icons.qrc
  ${PYTHONQT_CONSOLE_DIR}/NPC.qrc
)


add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/viewer_pyfuncs.cpp
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/viewer_pyfuncs.py
  COMMAND vtkEncodeString
  ARGS ${CMAKE_CURRENT_BINARY_DIR}/viewer_pyfuncs.cpp ${CMAKE_CURRENT_SOURCE_DIR}/viewer_pyfuncs.py viewer_pyfuncs
  )


set(VTK_VIEWER_PYTHONQT_PYTHONPATH "" CACHE STRING
	"Additional paths to be searched by python for importing modules")


QT5_WRAP_UI(VTK_VIEWER_UISrcs ${VTK_VIEWER_UI})

QT5_WRAP_CPP(VTK_VIEWER_MOCSrcs ${VTK_VIEWER_MOC_HEADERS}
  OPTIONS -DBOOST_TT_HAS_OPERATOR_HPP_INCLUDED)

QT5_ADD_RESOURCES(VTK_VIEWER_RCSrcs ${VTK_VIEWER_RESOURCES})

source_group("Resources" FILES
  ${VTK_VIEWER_UI}
)

source_group("Generated" FILES
  ${VTK_VIEWER_UISrcs}
  ${VTK_VIEWER_MOCSrcs}
  ${VTK_VIEWER_RCSrcs}
)

#include(HAVS/HAVS.cmake)

#include(HAPT/HAPT.cmake)

include(extn/PT/PT.cmake)

configure_file( config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/config.h )

add_executable(mscomplex-tet-vtk-viewer
  ${VTK_VIEWER_SRCS}
  ${VTK_VIEWER_UISrcs}
  ${VTK_VIEWER_MOCSrcs}
  ${VTK_VIEWER_RCSrcs}
  ${PT_SOURCES}
  ${CMAKE_CURRENT_BINARY_DIR}/viewer_pyfuncs.cpp
  $<TARGET_OBJECTS:mscomplex-tet-core>
  $<TARGET_OBJECTS:pymsc-core>
)

qt5_use_modules(mscomplex-tet-vtk-viewer Core Network OpenGL Gui)


target_link_libraries(mscomplex-tet-vtk-viewer
  ${PythonQt}
  ${PythonQt_All}
  ${Boost_LIBRARIES}
  ${PYTHON_LIBRARIES}
  ${VTK_LIBRARIES}
  GL
)

install(TARGETS mscomplex-tet-vtk-viewer DESTINATION ${INSTALL_DIR_BIN})
