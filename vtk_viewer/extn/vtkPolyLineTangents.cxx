/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPolyDataNormals.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPolyLineTangents.h"

#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkFloatArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolygon.h"

vtkStandardNewMacro(vtkPolyLineTangents);

// Construct with feature angle=30, splitting and consistency turned on,
// flipNormals turned off, and non-manifold traversal turned on.
vtkPolyLineTangents::vtkPolyLineTangents(){}

// Generate normals for polygon meshes
int vtkPolyLineTangents::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and output
  vtkPolyData *input = vtkPolyData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkDebugMacro(<<"Generating Line Tangents");

  vtkIdType numLines= input->GetNumberOfLines();
  vtkIdType numPts  = input->GetNumberOfPoints();

  if (numPts < 1 )
  {
    vtkDebugMacro(<<"No data to generate normals for!");
    return 1;
  }

  output->CopyStructure(input);
  output->GetPointData()->PassData(input->GetPointData());
  output->GetCellData()->PassData(input->GetCellData());
  output->SetFieldData(input->GetFieldData());

  // If there is nothing to do, pass the data through
  if ( numLines < 1)
  { //don't do anything! pass data through
    return 1;
  }

  vtkCellArray *inLines;
  vtkIdType npts,i,j;
  vtkIdType *pts=0;
  vtkFloatArray *LineTangents;
  double n1[3],n2[3],p1[3],p2[3];

  LineTangents = vtkFloatArray::New();
  LineTangents->SetNumberOfComponents(3);
  LineTangents->SetNumberOfTuples(numPts);
  LineTangents->SetName("Tangents");

  n1[0] = n1[1] = n1[2] = 0.0;
  for (i=0; i < numPts; i++)
  {
    LineTangents->SetTuple(i,n1);
  }


  for (inLines=input->GetLines(), inLines->InitTraversal();
       inLines->GetNextCell(npts,pts); )
  {
    for (j=1; j<npts; j++)
    {
      input->GetPoint(pts[j-1],p1);
      input->GetPoint(pts[j],p2);

      LineTangents->GetTuple(pts[j-1],n1);
      LineTangents->GetTuple(pts[j],n2);

      n1[0] += p2[0] - p1[0];
      n1[1] += p2[1] - p1[1];
      n1[2] += p2[2] - p1[2];

      n2[0] += p2[0] - p1[0];
      n2[1] += p2[1] - p1[1];
      n2[2] += p2[2] - p1[2];

      LineTangents->SetTuple(pts[j-1],n1);
      LineTangents->SetTuple(pts[j],n2);
    }
  }

  for (i=0; i < numPts; i++)
  {
    LineTangents->GetTuple(i,n1);

    if(n1[0] != 0 || n1[1] != 0 || n1[2] !=0)
    {
      double l = sqrt(n1[0]*n1[0] + n1[1]*n1[1] + n1[2]*n1[2]);

      n1[0] /=l; n1[1] /=l; n1[2] /=l;

      LineTangents->SetTuple(i,n1);
    }
  }

  output->GetPointData()->AddArray(LineTangents);

  return 1;
}

void vtkPolyLineTangents::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

//  os << indent << "Feature Angle: " << this->FeatureAngle << "\n";
//  os << indent << "Splitting: " << (this->Splitting ? "On\n" : "Off\n");
//  os << indent << "Consistency: " << (this->Consistency ? "On\n" : "Off\n");
//  os << indent << "Flip Normals: " << (this->FlipNormals ? "On\n" : "Off\n");
//  os << indent << "Auto Orient Normals: " << (this->AutoOrientNormals ? "On\n" : "Off\n");
//  os << indent << "Num Flips: " << this->NumFlips << endl;
//  os << indent << "Compute Point Normals: "
//     << (this->ComputePointNormals ? "On\n" : "Off\n");
//  os << indent << "Compute Cell Normals: "
//     << (this->ComputeCellNormals ? "On\n" : "Off\n");
//  os << indent << "Non-manifold Traversal: "
//     << (this->NonManifoldTraversal ? "On\n" : "Off\n");
}

