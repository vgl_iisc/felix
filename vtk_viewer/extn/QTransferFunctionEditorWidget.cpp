/******************************************************************************

Copyright (C) 2013 Nithin Shivashankar (nithin19484@gmail.com).
All rights reserved.

This was developed as a standalone Qt widget to edit 1D transfer functions
Code adapted from the Qtfe (Qt Transfer Function Editor) library by
Eric Heitz (er.heitz@gmail.com).

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License.
If not, see <http://www.gnu.org/licenses/>.

******************************************************************************/

#include "QTransferFunctionEditorWidget.hpp"

#include <QPainter>
#include <QColorDialog>
#include <QDebug>
#include <QFile>

/*****************************************************************************/

template <typename T> T lerp(T t,T m, T M) {return (1-t)*m + t*M;}
template <typename T> T normalize(T t,T m, T M){return (t-m)/(M-m);}
template <typename T> T clamp(T t,T m, T M){return(t<=m)?(m):((t<=M)?(t):(M));}

inline int log2(int i){int j = 0;while (i >>= 1) ++j;  return j;}
inline int pow2(int i){return 1 << i;}

/*---------------------------------------------------------------------------*/

inline QString opt_to_string(qreal r)
{
  if (r >= 100)
    return QString().sprintf("%d.",int(r));
  else if (r >= 10)
    return QString().sprintf("%.1f",(r));
  else if (r >= 1)
    return QString().sprintf("%.2f",(r));
  else
    return QString().sprintf("%.2f",(r));
}

/*****************************************************************************/



/*****************************************************************************/

bool SaveTransferFunction(const TransferFunction&tf,const QString& fn)
{
  if(tf.size() < 2)
  {
    qWarning()<<"Tranfer function has too few values!!"
              <<"Not saving!!!";
    return false;
  }

  QFile f(fn);
  if(!f.open(QIODevice::WriteOnly))
  {
    qWarning()<<"Unable to open file!!"<<fn;
    return false;
  }
  QTextStream ts(&f);

  for(int i = 0 ; i < int(tf.size()); ++i)
  {
    ts<<tf[i].f<<"\t"
      <<tf[i].col.redF()<<"\t"
      <<tf[i].col.greenF()<<"\t"
      <<tf[i].col.blueF()<<"\t"
      <<tf[i].col.alphaF()<<"\n";
  }

  f.close();

  return true;
}

/*---------------------------------------------------------------------------*/

bool LoadTransferFunction(TransferFunction &tf, const QString &fn,
                          qreal minf, qreal maxf)
{
  QFile f(fn);
  if(!f.open(QIODevice::ReadOnly))
  {
    qWarning()<<"Unable to open file!!"<<fn;
    return false;
  }

  QTextStream ts(&f);

  TransferFunction _tf;

  while(true)
  {
    qreal f,r,g,b,a;

    ts>>f>>r>>g>>b>>a;

    if(ts.status() != QTextStream::Ok)
      break;

    _tf.push_back(TransferFunctionPoint(f,QColor::fromRgbF(r,g,b,a)));
  }
  f.close();

  if(_tf.size() < 2)
  {
    qWarning()<<"Transfer function read with less that two values\n"
              <<"Not updating\n";
    return false;
  }

  ValidateTransferFunction(_tf,minf,maxf);

  tf.clear();
  tf += _tf;

  return true;
}

/*---------------------------------------------------------------------------*/

void ValidateTransferFunction(TransferFunction &tf,qreal minf,qreal maxf)
{
  bool defaultRangeAssumed = false;

  if(!(minf < maxf))
  {
    // default state i.e user gave no args is
    // assumed when minf = maxf = 0
    // suppress warning only in that case

    if(!(minf == 0 && maxf == 0))
    {
      qWarning()
          <<"Given Range is inconsistent\n"
          <<"minf = "<<minf<<"\n"
          <<"maxf = "<<maxf<<"\n"
          <<"Assuming [0,1]\n";
    }

    minf = 0;
    maxf = 1;

    defaultRangeAssumed = true;
  }

  if(tf.size() == 1)
  {
    qWarning()
        <<"Given a tranfer function with only 1 point\n"
        <<"Clearing it out and entering default values\n";
    tf.clear();
  }

  if(tf.size() == 0)
  {
    tf.push_back(TransferFunctionPoint(minf,QColor::fromRgbF(0,0,0,0)));
    tf.push_back(TransferFunctionPoint(maxf,QColor::fromRgbF(0,0,0,0)));
  }

  qSort(tf);

  qreal _minf = tf.front().f;
  qreal _maxf = tf.back().f;

  if(defaultRangeAssumed)
  {
    minf = _minf;
    maxf = _maxf;
  }

  if(!(qAbs(minf - _minf) < 0.001*qAbs(maxf-minf)) ||
     !(qAbs(maxf - _maxf) < 0.001*qAbs(maxf-minf)))
  {
    qWarning()
        <<"Warning: Given tf's range does not match with given range\n"
        <<"Tf's range\n"
        <<"minf = "<<_minf<<"\n"
        <<"maxf = "<<_maxf<<"\n"
        <<"Given Range\n"
        <<"minf = "<<minf<<"\n"
        <<"maxf = "<<maxf<<"\n"
        <<"Tf will be normalized and rescaled linearly to given Range\n";

    for( int i = 0; i < int(tf.size()) ;++i)
      tf[i].f = lerp<qreal>(normalize<qreal>(tf[i].f,_minf,_maxf),minf,maxf);
  }
}

/*****************************************************************************/



/*****************************************************************************/

const int QTransferFunctionEditorWidget::pointSizePixel = 5;
const int QTransferFunctionEditorWidget::circleSizePixel = 9;
const int QTransferFunctionEditorWidget::lineWidth = 2;
const int QTransferFunctionEditorWidget::tickLength = 5;

QTransferFunctionEditorWidget::QTransferFunctionEditorWidget
(QWidget *par,Qt::WindowFlags f):QWidget(par,f),m_fmin(0),m_fmax(1),
  pressed(false),selected(-1)

{
  this->setMouseTracking(true);

  list.push_back(TransferFunctionPoint(0,QColor::fromRgbF(1,0,0,0)));
  list.push_back(TransferFunctionPoint(1,QColor::fromRgbF(0,0,1,1)));

  background = new QImage(this->size(), QImage::Format_Mono);
  background->fill(1);

  setMinimumSize(fontMetrics().width("0")*10,fontMetrics().height()*5);
}

/*---------------------------------------------------------------------------*/

QTransferFunctionEditorWidget::~QTransferFunctionEditorWidget()
{
  delete background;
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::setTransferFunction
(const TransferFunction &tf,qreal fmin,qreal fmax)
{
  list.clear();
  list += tf;

  ValidateTransferFunction(list,fmin,fmax);

  m_fmin = list.front().f;
  m_fmax = list.back().f;

  for( int i = 0; i < int(list.size()) ;++i)
    list[i].f = normalize<qreal>(list[i].f,m_fmin,m_fmax);
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::getTransferFunction
(TransferFunction &tf) const
{
  tf.clear();
  tf += list;

  for(int i = 0 ; i < int(tf.size()); ++i)
    tf[i].f = lerp<qreal>(tf[i].f,m_fmin,m_fmax);
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::resizeEvent ( QResizeEvent * event )
{
  delete background;

  QSize sz   = event->size();
  QSize tksz = fontMetrics().size(0,QString("-00.0"));

  bg_sz = sz - (tksz*2);
  bg_bp.setX(tksz.width());
  bg_bp.setY(tksz.height());

  background = new QImage(bg_sz, QImage::Format_ARGB32);
}

/*---------------------------------------------------------------------------*/

QPoint QTransferFunctionEditorWidget::listPos2WidgetPos(int pos)
{
  qreal x = list[pos].f;
  qreal y = list[pos].col.alphaF();

  return QPoint(bg_bp.x() + bg_sz.width()*x,
                bg_bp.y() + bg_sz.height()* (1.0 - y));

}

/*---------------------------------------------------------------------------*/

QPointF QTransferFunctionEditorWidget::WidgetPos2listPos(QPoint p)
{
  return QPointF(qreal(p.x()-bg_bp.x())/(qreal)bg_sz.width(),
                 1.0 - qreal(p.y() - bg_bp.y())/(qreal)bg_sz.height());
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);


  // Draw Grid and Ticks
  QSize fsz = fontMetrics().size(0,QString("-0.00"));

  QPen bglnpen(Qt::white, pointSizePixel, Qt::DotLine);
  bglnpen.setWidth(1);

  QPen bgtxpen(Qt::white, pointSizePixel, Qt::SolidLine);

  if(this->isEnabled())
  {
    bglnpen.setColor(Qt::darkGray);
    bgtxpen.setColor(Qt::black);
  }
  else
  {
    bglnpen.setColor(Qt::lightGray);
    bgtxpen.setColor(Qt::darkGray);
  }

  int xnt = pow2(log2(bg_sz.width()/(2*fsz.width())));

  qreal xs = qreal(bg_sz.width())/qreal(xnt);

  for( int i = 0 ;i <= xnt ; ++i)
  {
    int x  = bg_bp.x() + qreal(i) * xs;
    int y0 = bg_bp.y();
    int y1 = bg_bp.y() + bg_sz.height();

    painter.setPen(bglnpen);
    painter.drawLine(QPoint(x,y0),QPoint(x,y1));

    painter.setPen(bgtxpen);
    QString xstr = opt_to_string(lerp<qreal>(qreal(i)/qreal(xnt),m_fmin,m_fmax));
    painter.drawText(x - fsz.width()/2,y1+fsz.height(),xstr);
  }

  int ynt = pow2(log2(bg_sz.height()/(2*fsz.height())));

  qreal ys = qreal(bg_sz.height())/qreal(ynt);

  for( int i = 0 ;i <= ynt ; ++i)
  {
    int y  = bg_bp.y() + qreal(i) * ys;
    int x0 = bg_bp.x();
    int x1 = bg_bp.x() + bg_sz.width();

    painter.setPen(bglnpen);
    painter.drawLine(QPoint(x0,y),QPoint(x1,y));

    painter.setPen(bgtxpen);
    QString ystr = opt_to_string(lerp<qreal>(qreal(i)/qreal(ynt),0,1));
    painter.drawText(0,height() - y +fsz.height()/3,ystr);
  }

  unsigned int bg_alpha = (isEnabled())?(0xbf000000):(0x2f000000);


  // Draw the color background
  for(int i=0 ; i < background->width() ; ++i)
  {
    qreal x = i / (qreal)background->width();

    QColor col = evalf(x);

    int jmin = (1.0-col.alphaF()) * background->height();

    unsigned int ucol = (col.rgb()&0x00ffffff)|bg_alpha;

    for(int j=0 ; j < jmin ; ++j)
    {
      background->setPixel(i,j,0x00000000);
    }
    for(int j=jmin ; j < background->height() ; ++j)
    {
      background->setPixel(i,j,ucol);
    }
  }

  painter.drawImage(bg_bp,*background);

  if(!this->isEnabled())
  {
    // call base event and get out
    QWidget::paintEvent(event);
    return;
  }

  // all points
  QPen pen(Qt::black, pointSizePixel, Qt::SolidLine);
  painter.setPen(pen);  
  for(int i=0 ; i<int(list.size()) ; ++i)
  {
    painter.drawPoint(listPos2WidgetPos(i));
  }

  // points interpolation line
  pen.setWidth(lineWidth);
  painter.setPen(pen);

  for(int p=1 ; p < int(list.size()) ; ++p)
    painter.drawLine(listPos2WidgetPos(p-1),listPos2WidgetPos(p));

  // selected point
  pen.setColor(Qt::red);
  painter.setPen(pen);
  if(selected >=0)
  {
    QPoint pt = listPos2WidgetPos(selected);
    pen.setColor(Qt::red);
    painter.setPen(pen);
    painter.drawEllipse(pt,circleSizePixel,circleSizePixel);

    QString xstr = opt_to_string(m_fmin+list[selected].f*(m_fmax-m_fmin));
    QString ystr = opt_to_string(list[selected].col.alphaF());

    pen.setColor(Qt::black);
    painter.setPen(pen);

    painter.drawText(QPoint(pt.x() - fsz.width()/2,fsz.height()),xstr);
    painter.drawText(QPoint(bg_bp.x()+bg_sz.width(),pt.y()+fsz.height()/2),
                            ystr);

    pen.setColor(Qt::black);
    pen.setStyle(Qt::SolidLine);
    painter.setPen(pen);

    painter.drawLine(pt.x(),bg_bp.y()+bg_sz.height()-tickLength,
                     pt.x(),bg_bp.y()+bg_sz.height());
    painter.drawLine(bg_bp.x()  ,pt.y(),
                     bg_bp.x()+tickLength,pt.y());

  }

  QWidget::paintEvent(event);
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::mousePressEvent( QMouseEvent * event )
{
  QPointF pf = WidgetPos2listPos(event->pos());

  if(event->button() == Qt::LeftButton)
  {
    pressed = true;
    if(selected == -1)
    {
      for(int i=1 ; i < int(list.size()) ; ++i)
      {
        if( list[i-1].f <= pf.x() && pf.x() < list[i].f )
        {
          pMin = list[i-1].f+0.01;
          pMax = list[i].f-0.01;

          QColor col = evalf(pf.x());
          col.setAlphaF(pf.y());

          list.insert(list.begin()+i,TransferFunctionPoint(pf.x(),col));

          selected = i;

          repaint();
          break;
        }
      }
    }
  }
  if(event->button() == Qt::RightButton && selected >= 0)
  {
    if(selected != 0 && selected != int(list.size())-1)
    {
      list.erase(list.begin()+selected);
      selected = -1;
      this->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::mouseDoubleClickEvent(QMouseEvent* event)
{
  if(event->button() == Qt::LeftButton)
  {
    if(selected >= 0)
    {
      int idx = selected; //selected will change to -1 when window pops up

      QColor col = QColorDialog::getColor(list[idx].col,this);

      if(col.isValid())
      {
        qreal a = list[idx].col.alphaF();
        list[idx].col = col;
        list[idx].col.setAlphaF(a);
      }

      repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::mouseReleaseEvent ( QMouseEvent * event )
{
  if(event->button() == Qt::LeftButton)
  {
    pressed = false;
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::mouseMoveEvent ( QMouseEvent * event )
{
  QPointF pf = WidgetPos2listPos(event->pos());

  if(pressed)
  {
    if(selected >= 0)
    {
      if(selected != 0 && selected != int(list.size())-1)
      {
        list[selected].f = clamp<qreal>(pf.x(),pMin,pMax);
      }

      list[selected].col.setAlphaF(clamp<qreal>(pf.y(),0,1));
      this->repaint();
      return;
    }
  }
  else
  {
    qreal d_min = circleSizePixel*circleSizePixel;
    qreal W = width()*width();
    qreal H = height()*height();
    int nearest = -1;

    for(int i=0 ; i<int(list.size()) ; ++i)
    {
      qreal x = list[i].f-pf.x();
      qreal y = list[i].col.alphaF()-pf.y();

      qreal d = x*x*W + y*y*H;
      if( d < d_min)
      {
        nearest = i;
        d_min = d;
        if(i==0)
        {
          pMin = 0.0;
          pMax = 0.0;
        }
        else if(i==int(list.size())-1)
        {
          pMin = 1.0;
          pMax = 1.0;
        }
        else
        {
          pMin = list[i-1].f+0.01;
          pMax = list[i+1].f-0.01;
        }
      }
    }

    if(nearest != selected)
    {
      selected = nearest;
      this->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorWidget::leaveEvent ( QEvent * event )
{
  selected = -1;
  repaint();
  QWidget::leaveEvent(event);
}

/*---------------------------------------------------------------------------*/

QColor QTransferFunctionEditorWidget::evalf(qreal t) const
{
  if(t<=0.0) return list[0].col;
  if(t>=1.0) return list[list.size()-1].col;


  for(int i=0 ; i<int(list.size())-1 ; ++i)
  {
    qreal x0 = list[i].f;
    qreal x1 = list[i+1].f;

    if(t < x0 || t > x1)
      continue;

    QColor y0 = list[i].col;
    QColor y1 = list[i+1].col;

    t = normalize<qreal>(t,x0,x1);

    qreal r = clamp<qreal>(lerp<qreal>(t,y0.redF(),y1.redF()),0,1);
    qreal g = clamp<qreal>(lerp<qreal>(t,y0.greenF(),y1.greenF()),0,1);
    qreal b = clamp<qreal>(lerp<qreal>(t,y0.blueF(),y1.blueF()),0,1);
    qreal a = clamp<qreal>(lerp<qreal>(t,y0.alphaF(),y1.alphaF()),0,1);

    return QColor::fromRgbF(r,g,b,a);
  }
  return list[0].col;
}

/*****************************************************************************/

#include <QGridLayout>
#include <QPushButton>
#include <QFileDialog>

/*****************************************************************************/

QTransferFunctionEditorDialog::QTransferFunctionEditorDialog
(QWidget *par,Qt::WindowFlags f):QDialog(par,f)
{
  gridl = new QGridLayout(this);
  setLayout(gridl);

  tf_editor_widget = new QTransferFunctionEditorWidget(this,f);
  gridl->addWidget(tf_editor_widget,0,0,6,6);

  QPushButton * apply = new QPushButton(tr("Apply"));
  QPushButton * ok    = new QPushButton(tr("Ok"));
  QPushButton * cancel = new QPushButton(tr("Cancel"));
  QPushButton * save = new QPushButton(tr("Save"));
  QPushButton * load = new QPushButton(tr("Load"));

  connect(apply,SIGNAL(clicked()),this,SLOT(apply()));
  connect(ok,SIGNAL(clicked()),this,SLOT(accept()));
  connect(ok,SIGNAL(clicked()),this,SLOT(apply()));
  connect(cancel,SIGNAL(clicked()),this,SLOT(reject()));
  connect(save,SIGNAL(clicked()),this,SLOT(save_tf()));
  connect(load,SIGNAL(clicked()),this,SLOT(load_tf()));

  gridl->addWidget(apply,6,3);
  gridl->addWidget(ok,6,4);
  gridl->addWidget(cancel,6,5);
  gridl->addWidget(save,6,0);
  gridl->addWidget(load,6,1);

  setWindowTitle(tr("Edit Transfer Function"));

  resize(600,300);
}

/*---------------------------------------------------------------------------*/

QTransferFunctionEditorDialog::~QTransferFunctionEditorDialog(){}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorDialog::save_tf()
{
  QString fn = QFileDialog::getSaveFileName
      (this,tr("Save Tf"),"",tr("Tfs (*.tf);; All Files(*)"));

  if(fn.size() != 0 )
  {
    TransferFunction tf;
    tf_editor_widget->getTransferFunction(tf);
    SaveTransferFunction(tf,fn);
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorDialog::load_tf()
{
  QString fn = QFileDialog::getOpenFileName
      (this,tr("Load Tf"),"",tr("Tfs (*.tf);; All Files(*)"));

  if(fn.size() != 0 )
  {
    TransferFunction tf;

    qreal fmin = tf_editor_widget->getRangeMin();
    qreal fmax = tf_editor_widget->getRangeMax();

    if(LoadTransferFunction(tf,fn,fmin,fmax))
    {
      tf_editor_widget->setTransferFunction(tf,fmin,fmax);
      tf_editor_widget->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorDialog::apply()
{
  emit applied();
}

/*---------------------------------------------------------------------------*/

int QTransferFunctionEditorDialog::editTransferFunction
(TransferFunction &tf, qreal minf, qreal maxf, bool normalized)
{
  QTransferFunctionEditorDialog dlg;

  if(normalized)
    dlg.setNormalizedTransferFunction(tf,minf,maxf);
  else
    dlg.setTransferFunction(tf,minf,maxf);

  dlg.setModal(true);
  int ret = dlg.exec();

  if(ret == QDialog::Accepted)
    dlg.getTransferFunction(tf);

  if(normalized)
    dlg.getNormalizedTransferFunction(tf);
  else
    dlg.getTransferFunction(tf);

  return ret;
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorDialog::getNormalizedTransferFunction
(TransferFunction &tf) const
{
  tf_editor_widget->getTransferFunction(tf);

  qreal m = tf.front().f;
  qreal M = tf.back().f;

  for(int i = 0 ; i < tf.size() ; ++i)
    tf[i].f = normalize(tf[i].f,m,M);
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionEditorDialog::setNormalizedTransferFunction
(const TransferFunction &_tf,qreal fmin,qreal fmax)
{
  TransferFunction tf = _tf;

  qSort(tf);

  if(fmin == 0 && fmax == 0)
  {
    fmin = 0;
    fmax = 1;
  }

  if(tf.size() != 0)
  {
    for(int i = 0 ; i < tf.size() ; ++i)
      tf[i].f = lerp(tf[i].f,fmin,fmax);
  }

  tf_editor_widget->setTransferFunction(tf);
}


/*****************************************************************************/




/*===========================================================================*/

#include <QGridLayout>
#include <QPushButton>
#include <QFileDialog>

/*---------------------------------------------------------------------------*/

QTransferFunctionWidget::QTransferFunctionWidget
(QWidget *par,Qt::WindowFlags f):QWidget(par,f)
{
  gridl = new QGridLayout(this);
  setLayout(gridl);

  tf_editor_widget = new QTransferFunctionEditorWidget(this,f);
  gridl->addWidget(tf_editor_widget,0,0,6,6);

  QPushButton * apply = new QPushButton(tr("Apply"));
  QPushButton * save = new QPushButton(tr("Save"));
  QPushButton * load = new QPushButton(tr("Load"));

  connect(apply,SIGNAL(clicked()),this,SLOT(apply()));
  connect(save,SIGNAL(clicked()),this,SLOT(save_tf()));
  connect(load,SIGNAL(clicked()),this,SLOT(load_tf()));

  gridl->addWidget(apply,6,0);
  gridl->addWidget(save,6,2);
  gridl->addWidget(load,6,4);

//  setWindowTitle(tr("Edit Transfer Function"));

//  resize(600,300);
}

/*---------------------------------------------------------------------------*/

QTransferFunctionWidget::~QTransferFunctionWidget(){}

/*---------------------------------------------------------------------------*/

void QTransferFunctionWidget::save_tf()
{
  QString fn = QFileDialog::getSaveFileName
      (this,tr("Save Tf"),"",tr("Tfs (*.tf);; All Files(*)"));

  if(fn.size() != 0 )
  {
    TransferFunction tf;
    tf_editor_widget->getTransferFunction(tf);
    SaveTransferFunction(tf,fn);
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionWidget::load_tf()
{
  QString fn = QFileDialog::getOpenFileName
      (this,tr("Load Tf"),"",tr("Tfs (*.tf);; All Files(*)"));

  if(fn.size() != 0 )
  {
    TransferFunction tf;

    qreal fmin = tf_editor_widget->getRangeMin();
    qreal fmax = tf_editor_widget->getRangeMax();

    if(LoadTransferFunction(tf,fn,fmin,fmax))
    {
      tf_editor_widget->setTransferFunction(tf,fmin,fmax);
      tf_editor_widget->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionWidget::apply()
{
  emit applied();
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionWidget::getNormalizedTransferFunction
(TransferFunction &tf) const
{
  tf_editor_widget->getTransferFunction(tf);

  qreal m = tf.front().f;
  qreal M = tf.back().f;

  for(int i = 0 ; i < tf.size() ; ++i)
    tf[i].f = normalize(tf[i].f,m,M);
}

/*---------------------------------------------------------------------------*/

void QTransferFunctionWidget::setNormalizedTransferFunction
(const TransferFunction &_tf,qreal fmin,qreal fmax)
{
  TransferFunction tf = _tf;

  qSort(tf);

  if(fmin == 0 && fmax == 0)
  {
    fmin = 0;
    fmax = 1;
  }

  if(tf.size() != 0)
  {
    for(int i = 0 ; i < tf.size() ; ++i)
      tf[i].f = lerp(tf[i].f,fmin,fmax);
  }

  tf_editor_widget->setTransferFunction(tf);
}

/*===========================================================================*/



/*****************************************************************************/

//#include <QApplication>

//int main(int argc, char** argv)
//{
//  QApplication app(argc,argv);

//  TransferFunction tf;

//  QTransferFunctionEditorDialog::editTransferFunction(tf,2,5);
//}

/*****************************************************************************/
