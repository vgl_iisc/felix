/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkOpenGLProjectedTetrahedraMapper.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*
 * Copyright 2003 Sandia Corporation.
 * Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
 * license for use of this work by or on behalf of the
 * U.S. Government. Redistribution and use in source and binary forms, with
 * or without modification, are permitted provided that this Notice and any
 * statement of authorship are reproduced on all copies.
 */

#include "OpenGLPTVolumeMapper.hpp"

#include "vtkCamera.h"
#include "vtkCellArray.h"
#include "vtkIdTypeArray.h"
#include "vtkObjectFactory.h"
#include "vtkRenderer.h"
#include "vtkOpenGLRenderWindow.h"
#include "vtkOpenGLExtensionManager.h"
#include "vtkTimerLog.h"
#include "vtkUnstructuredGrid.h"
#include "vtkVisibilitySort.h"
#include "vtkVolume.h"
#include "vtkVolumeProperty.h"
#include "vtkUnstructuredGridPartialPreIntegration.h"
#include "vtkUniformVariables.h"
#include "vtkgl.h"
#include "vtkShaderProgram2.h"
#include "vtkShader2.h"
#include "vtkShader2Collection.h"
#include "vtkPiecewiseFunction.h"
#include "vtkColorTransferFunction.h"
#include "vtkSmartPointer.h"


extern const char *PTVolumeMapper_VS;
extern const char *PTVolumeMapper_GS;
extern const char *PTVolumeMapper_FS;

#include <sstream>

//-----------------------------------------------------------------------------
class OpenGLPTVolumeMapper::vtkInternals
{
public:
  GLuint FrameBufferObjectId;
  GLuint RenderBufferObjectIds[2];

  GLuint PsiTableTexture;
  int    PsiTableTextureSize;
  GLuint TransferFunctionsTexture;
  GLuint TransferFunctionsTexture2;

  vtkSmartPointer<vtkShaderProgram2> Shader;
  vtkTimeStamp                       ShaderMtime;


  GLuint TetsVert;
  GLuint TetsVertScalar;
  GLuint TetsVertScalar2;
};

//-----------------------------------------------------------------------------

vtkStandardNewMacro(OpenGLPTVolumeMapper);

OpenGLPTVolumeMapper::OpenGLPTVolumeMapper()
{
  this->Initialized = false;
  this->CurrentFBOWidth = -1;
  this->CurrentFBOHeight = -1;
  this->FloatingPointFrameBufferResourcesAllocated = false;

  this->ScalarsRange[0] = 0;
  this->ScalarsRange[1] = 0;

  this->Scalars2Range[0] = 0;
  this->Scalars2Range[1] = 0;

  this->LastScalarData = NULL;
  this->TetsVert        = NULL;
  this->TetsVertScalar  = NULL;
  this->TetsVertScalar2 = NULL;
  this->UseGPUBuffers   = false;

  this->VolProp_IndependantComponents = true;
  this->UseScalar2OpacityExpDecay = false;
  this->Scalar2OpacityExpDecayVariance = 1.0;

  this->Brightness  = 8.0;

  this->Internals = new OpenGLPTVolumeMapper::vtkInternals;
  this->Internals->FrameBufferObjectId = 0;
  this->Internals->RenderBufferObjectIds[0] = 0;
  this->Internals->RenderBufferObjectIds[1] = 0;
  this->Internals->PsiTableTexture = 0;
  this->Internals->PsiTableTextureSize = 0;
  this->Internals->TransferFunctionsTexture = 0;
  this->Internals->TransferFunctionsTexture2 = 0;
  this->Internals->TetsVert = 0;
  this->Internals->TetsVertScalar = 0;
  this->Internals->TetsVertScalar2 = 0;

  this->UseFloatingPointFrameBuffer = true;
}

//-----------------------------------------------------------------------------

OpenGLPTVolumeMapper::~OpenGLPTVolumeMapper()
{
  this->ReleaseGraphicsResources(NULL);

  if (this->Internals)
  {
    if(this->Internals->Shader)
    {
      this->Internals->Shader->ReleaseGraphicsResources();
      this->Internals->Shader->Delete();
    }

    if(this->Internals->TetsVert)
      vtkgl::DeleteBuffers(1,&this->Internals->TetsVert);

    if(this->Internals->TetsVertScalar)
      vtkgl::DeleteBuffers(1,&this->Internals->TetsVertScalar);

    if(this->Internals->TetsVertScalar2)
      vtkgl::DeleteBuffers(1,&this->Internals->TetsVertScalar2);

    delete this->Internals;
  }

  if(this->TetsVert) delete []this->TetsVert;

  if(this->TetsVertScalar) delete []this->TetsVertScalar;

  if(this->TetsVertScalar2) delete []this->TetsVertScalar2;
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::PrintSelf(ostream &os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "VisibilitySort: " << this->VisibilitySort << endl;
  os << indent << "UseFloatingPointFrameBuffer: "
     << (this->UseFloatingPointFrameBuffer ? "True" : "False") << endl;
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::Initialize(vtkRenderer *renderer)
{
  this->Initialized = true;

  vtkOpenGLRenderWindow *renwin
      = vtkOpenGLRenderWindow::SafeDownCast(renderer->GetRenderWindow());
  vtkOpenGLExtensionManager *extensions = renwin->GetExtensionManager();

  this->CanDoFloatingPointFrameBuffer
      = extensions->ExtensionSupported("GL_ARB_framebuffer_object") &&
      extensions->ExtensionSupported("GL_ARB_texture_float");

  if (this->CanDoFloatingPointFrameBuffer)
  {
    extensions->LoadExtension("GL_ARB_framebuffer_object");
    extensions->LoadExtension("GL_ARB_texture_float");
  }
}

//-----------------------------------------------------------------------------

bool OpenGLPTVolumeMapper::InitShaders(vtkRenderer *renderer,vtkVolume *v)
{

  if(!this->Internals->Shader ||
      this->Internals->ShaderMtime < this->VolProp_IndependantComponents_MTime ||
      this->Internals->ShaderMtime < this->Scalar2OpacityExpDecayVariance_MTime ||
      this->Internals->ShaderMtime < this ->Brightness_MTime
      )
  {
    if(this->Internals->Shader)
    {
      this->Internals->Shader->ReleaseGraphicsResources();
      this->Internals->Shader->Delete();
    }

    vtkOpenGLRenderWindow *renwin
        = vtkOpenGLRenderWindow::SafeDownCast(renderer->GetRenderWindow());

    this->Internals->Shader=vtkShaderProgram2::New();
    this->Internals->Shader->SetContext(renwin);

    vtkShader2Collection *shaders=this->Internals->Shader->GetShaders();

    vtkStdString vs_code;
    vtkStdString gs_code;
    vtkStdString fs_code;

    if(!this->VolProp_IndependantComponents)
    {
      vs_code += "#define TWOSCALARS\n";
      gs_code += "#define TWOSCALARS\n";
      fs_code += "#define TWOSCALARS\n";

      if(this->UseScalar2OpacityExpDecay)
      {
        double s2diff = this->Scalars2Range[1] - this->Scalars2Range[0];

        if(s2diff != 0)
        {
          std::stringstream ss;

          ss<<"#define SCALAR2EXPDECAY_VAR "
            <<(this->Scalar2OpacityExpDecayVariance/(s2diff*s2diff))<<std::endl;

          fs_code += ss.str();
        }
        else
        {
          vtkErrorMacro(<<"Second component has 0 range"
                        <<"Cannot use exp decay")
        }
      }
    }

    std::stringstream  ss;

    ss<<"#define BRIGHTNESS "<<(this->Brightness)<<std::endl;
    fs_code += ss.str();

    vs_code += PTVolumeMapper_VS;
    gs_code += PTVolumeMapper_GS;
    fs_code += PTVolumeMapper_FS;

    vtkShader2 *vs=vtkShader2::New();
    vs->SetType(VTK_SHADER_TYPE_VERTEX);
    vs->SetContext(this->Internals->Shader->GetContext());
    vs->SetSourceCode(vs_code.c_str());
    shaders->AddItem(vs);
    vs->Delete();

    vtkShader2 *gs=vtkShader2::New();
    gs->SetType(VTK_SHADER_TYPE_GEOMETRY);
    gs->SetContext(this->Internals->Shader->GetContext());
    gs->SetSourceCode(gs_code.c_str());
    shaders->AddItem(gs);
    gs->Delete();

    vtkShader2 *fs=vtkShader2::New();
    fs->SetType(VTK_SHADER_TYPE_FRAGMENT);
    fs->SetContext(this->Internals->Shader->GetContext());
    fs->SetSourceCode(fs_code.c_str());
    shaders->AddItem(fs);
    fs->Delete();

    this->Internals->Shader->SetGeometryVerticesOut(8);
    this->Internals->Shader->SetGeometryTypeIn(VTK_GEOMETRY_SHADER_IN_TYPE_POINTS);
    this->Internals->Shader->SetGeometryTypeOut(VTK_GEOMETRY_SHADER_OUT_TYPE_TRIANGLE_STRIP);

    this->Internals->Shader->Build();

    this->Internals->Shader->Use();

    int uiValues[] = {0,1,this->Internals->PsiTableTextureSize,2};
    this->Internals->Shader->GetUniformVariables()->SetUniformi
        ("psiGammaTableTex",1, uiValues);
    this->Internals->Shader->GetUniformVariables()->SetUniformi
        ("tfTex",1, uiValues+1);
    this->Internals->Shader->GetUniformVariables()->SetUniformi
        ("preIntTexSize",1,uiValues+2);

    if(!this->VolProp_IndependantComponents && !this->UseScalar2OpacityExpDecay)
    {
      this->Internals->Shader->GetUniformVariables()->SetUniformi
          ("tf2Tex",1, uiValues+3);
    }

    this->Internals->Shader->Restore();

    this->Internals->ShaderMtime.Modified();

  }

  return true;
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::SetUseScalar2OpacityExpDecay(bool v)
{
  if(UseScalar2OpacityExpDecay !=v)
  {
    UseScalar2OpacityExpDecay = v;
    Scalar2OpacityExpDecayVariance_MTime.Modified();
  }
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::SetScalar2OpacityExpDecayVariance(double v)
{
  if(Scalar2OpacityExpDecayVariance !=v)
  {
    Scalar2OpacityExpDecayVariance = v;
    Scalar2OpacityExpDecayVariance_MTime.Modified();
  }
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::SetBrightness(double v)
{
  if(this->Brightness != v)
  {
    this->Brightness = v;
    this->Brightness_MTime.Modified();
  }
}

//-----------------------------------------------------------------------------

bool OpenGLPTVolumeMapper::CheckFBOResources(vtkRenderer *r)
{
  int *size = r->GetSize();
  if (!this->FloatingPointFrameBufferResourcesAllocated ||
      (size[0] != this->CurrentFBOWidth) ||
      (size[0] != this->CurrentFBOHeight))
  {
    this->CurrentFBOWidth = size[0];
    this->CurrentFBOHeight = size[1];

    if (!this->FloatingPointFrameBufferResourcesAllocated)
    {
      vtkgl::GenFramebuffers(1, &this->Internals->FrameBufferObjectId);
      vtkgl::GenRenderbuffers(2, this->Internals->RenderBufferObjectIds);

      vtkgl::BindFramebuffer(vtkgl::FRAMEBUFFER_EXT, 0);
      vtkgl::BindFramebuffer(vtkgl::DRAW_FRAMEBUFFER_EXT,
                             this->Internals->FrameBufferObjectId);

      this->FloatingPointFrameBufferResourcesAllocated = true;
    }


    vtkgl::BindFramebuffer(vtkgl::FRAMEBUFFER_EXT, 0);
    vtkgl::BindFramebuffer(vtkgl::DRAW_FRAMEBUFFER_EXT,
                           this->Internals->FrameBufferObjectId);

    vtkgl::FramebufferRenderbuffer(vtkgl::FRAMEBUFFER_EXT,
                                   vtkgl::COLOR_ATTACHMENT0_EXT,
                                   vtkgl::RENDERBUFFER_EXT, 0);

    vtkgl::FramebufferRenderbuffer(vtkgl::FRAMEBUFFER_EXT,
                                   vtkgl::DEPTH_ATTACHMENT_EXT,
                                   vtkgl::RENDERBUFFER_EXT, 0);

    vtkgl::BindRenderbuffer(vtkgl::RENDERBUFFER_EXT,
                            this->Internals->RenderBufferObjectIds[0]);
    vtkgl::RenderbufferStorage(vtkgl::RENDERBUFFER_EXT,
                               vtkgl::RGBA32F_ARB,
                               this->CurrentFBOWidth,
                               this->CurrentFBOHeight);

    vtkgl::BindRenderbuffer(vtkgl::RENDERBUFFER_EXT,
                            this->Internals->RenderBufferObjectIds[1]);
    vtkgl::RenderbufferStorage(vtkgl::RENDERBUFFER_EXT,
                               vtkgl::DEPTH_COMPONENT24,
                               this->CurrentFBOWidth,
                               this->CurrentFBOHeight);

    vtkgl::FramebufferRenderbuffer(vtkgl::FRAMEBUFFER_EXT,
                                   vtkgl::COLOR_ATTACHMENT0_EXT,
                                   vtkgl::RENDERBUFFER_EXT,
                                   this->Internals->RenderBufferObjectIds[0]);

    vtkgl::FramebufferRenderbuffer(vtkgl::FRAMEBUFFER_EXT,
                                   vtkgl::DEPTH_ATTACHMENT_EXT,
                                   vtkgl::RENDERBUFFER_EXT,
                                   this->Internals->RenderBufferObjectIds[1]);

    GLenum status = vtkgl::CheckFramebufferStatus(vtkgl::FRAMEBUFFER_EXT);
    if(status != vtkgl::FRAMEBUFFER_COMPLETE_EXT)
    {
      cerr << "Can't initialize FBO\n";
      return false;
    }
  }
  return true;
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::ReleaseGraphicsResources(vtkWindow *win)
{
  if (this->FloatingPointFrameBufferResourcesAllocated)
  {
    this->FloatingPointFrameBufferResourcesAllocated = false;

    vtkgl::DeleteFramebuffers(1, &this->Internals->FrameBufferObjectId);
    this->Internals->FrameBufferObjectId = 0;

    vtkgl::DeleteFramebuffers(2, this->Internals->RenderBufferObjectIds);
    this->Internals->RenderBufferObjectIds[0] = 0;
    this->Internals->RenderBufferObjectIds[1] = 0;
  }

  this->Superclass::ReleaseGraphicsResources(win);
}

//-----------------------------------------------------------------------------

bool OpenGLPTVolumeMapper::InitGridData(vtkRenderer *,vtkVolume *vol)
{
  if (this->Grid_MTime < this->GetInput()->GetMTime())
  {
    vtkDebugMacro(<<"Initializing Grid");

    this->MaxEdgeLength = 0;

    if(this->TetsVert) delete []this->TetsVert;

    this->NumberOfTetrahedra =  GetInput()->GetNumberOfCells();

    this->TetsVert = new float[this->NumberOfTetrahedra*4*3];

    for (int i = 0; i < this->NumberOfTetrahedra; ++i)
    {
      for (int j = 0; j < 4; ++j)
      {
        double pt[3];

        vtkIdType ptid =  this->GetInput()->GetCell(i)->GetPointId(j);

        GetInput()->GetPoint(ptid,pt);

        this->TetsVert[i*12 + j*3 + 0] = pt[0];
        this->TetsVert[i*12 + j*3 + 1] = pt[1];
        this->TetsVert[i*12 + j*3 + 2] = pt[2];
      }

      float d1 = vtkMath::Distance2BetweenPoints
          (this->TetsVert+i*12,this->TetsVert+i*12+3);

      float d2 = vtkMath::Distance2BetweenPoints
          (this->TetsVert+i*12,this->TetsVert+i*12+6);

      float d3 = vtkMath::Distance2BetweenPoints
          (this->TetsVert+i*12,this->TetsVert+i*12+9);

      float d4 = vtkMath::Distance2BetweenPoints
          (this->TetsVert+i*12+3,this->TetsVert+i*12+6);

      float d5 = vtkMath::Distance2BetweenPoints
          (this->TetsVert+i*12+3,this->TetsVert+i*12+9);

      float d6 = vtkMath::Distance2BetweenPoints
          (this->TetsVert+i*12+6,this->TetsVert+i*12+9);

      this->MaxEdgeLength = std::max(this->MaxEdgeLength,d1);
      this->MaxEdgeLength = std::max(this->MaxEdgeLength,d2);
      this->MaxEdgeLength = std::max(this->MaxEdgeLength,d3);
      this->MaxEdgeLength = std::max(this->MaxEdgeLength,d4);
      this->MaxEdgeLength = std::max(this->MaxEdgeLength,d5);
      this->MaxEdgeLength = std::max(this->MaxEdgeLength,d6);
    }

    this->MaxEdgeLength = sqrt(this->MaxEdgeLength);

    this->Grid_MTime.Modified();
  }

  if(GetUseGPUBuffers() && this->TetsVert)
  {
    if(!this->Internals->TetsVert)
      vtkgl::GenBuffers(1,&this->Internals->TetsVert);

    vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, this->Internals->TetsVert);

    vtkgl::BufferData
        (vtkgl::ARRAY_BUFFER, this->NumberOfTetrahedra * 12 *sizeof(GLfloat),
         this->TetsVert,vtkgl::STATIC_DRAW);

    vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER,0);

    delete []this->TetsVert;
    this->TetsVert = NULL;
  }

  return true;
}

bool OpenGLPTVolumeMapper::InitScalars(vtkRenderer *renderer,vtkVolume *vol)
{
  int UsingCellColors;

  vtkDataArray *scalarData = this->GetScalars
      (GetInput(), this->ScalarMode,this->ArrayAccessMode,this->ArrayId,
       this->ArrayName,UsingCellColors);

  if (!scalarData)
  {
    vtkErrorMacro(<< "Can't use projected tetrahedra without scalars!");
    return false;
  }

  bool use_scalar2 = !vol->GetProperty()->GetIndependentComponents() &&
      scalarData->GetNumberOfComponents() > 1;

  if ( this->Scalars_MTime < this->VolProp_IndependantComponents_MTime ||
      this->LastScalarData != scalarData ||
      this->Scalars_MTime < scalarData->GetMTime() ||
      this->Scalars_MTime < GetInput()->GetMTime())
  {
    vtkDebugMacro(<<"Initializing Scalars");

    if(this->TetsVertScalar) delete []this->TetsVert;
    if(this->TetsVertScalar2) delete []this->TetsVertScalar2;

    int NumberOfTetrahedra =  GetInput()->GetNumberOfCells();

    double diff = 0,diff2 = 0;    

    scalarData->GetRange(this->ScalarsRange,0);
    diff  = this->ScalarsRange[1]-this->ScalarsRange[0];

    if(diff == 0)
    {
      vtkErrorMacro(<<"Scalar component 0 has range 0");
      return false;
    }

    this->TetsVertScalar = new float[NumberOfTetrahedra*4];

    if(use_scalar2)
    {
      scalarData->GetRange(this->Scalars2Range,1);
      diff2  = this->Scalars2Range[1]-this->Scalars2Range[0];

      if(diff2 == 0)
      {
        vtkErrorMacro(<<"Scalar component 1 has range 0");
        return false;
      }

      this->TetsVertScalar2 = new float[NumberOfTetrahedra*4];
    }
    else
      this->TetsVertScalar2 = NULL;

    for (int i = 0; i < NumberOfTetrahedra; ++i)
    {
      for (int j = 0; j < 4; ++j)
      {
        vtkIdType ptid =
            (UsingCellColors)?(i):(GetInput()->GetCell(i)->GetPointId(j));

        this->TetsVertScalar[i*4+j] =
            ((float)scalarData->GetTuple(ptid)[0]-this->ScalarsRange[0])/diff;

        if(use_scalar2)
          this->TetsVertScalar2[i*4+j] =
            ((float)scalarData->GetTuple(ptid)[1]-this->Scalars2Range[0])/diff2;
      }
    }

    this->LastScalarData = scalarData;
    this->Scalars_MTime.Modified();
  }

  if(GetUseGPUBuffers())
  {
    if(this->TetsVertScalar)
    {

      if(!this->Internals->TetsVertScalar)
        vtkgl::GenBuffers(1,&this->Internals->TetsVertScalar);

      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, this->Internals->TetsVertScalar);

      vtkgl::BufferData
          (vtkgl::ARRAY_BUFFER, NumberOfTetrahedra * 4 *sizeof(GLfloat),
           this->TetsVertScalar,vtkgl::STATIC_DRAW);

      delete []this->TetsVertScalar;
      this->TetsVertScalar = NULL;
    }


    if(use_scalar2 && this->TetsVertScalar2)
    {
      if(!this->Internals->TetsVertScalar2)
        vtkgl::GenBuffers(1,&this->Internals->TetsVertScalar2);

      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, this->Internals->TetsVertScalar2);

      vtkgl::BufferData
          (vtkgl::ARRAY_BUFFER, NumberOfTetrahedra * 4 *sizeof(GLfloat),
           this->TetsVertScalar2,vtkgl::STATIC_DRAW);

      delete []this->TetsVertScalar2;
      this->TetsVertScalar2 = NULL;
    }

    vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER,0);
  }

  return true;
}

//-----------------------------------------------------------------------------

bool OpenGLPTVolumeMapper::InitTransferFunction(vtkRenderer *,vtkVolume *vol)
{
  vtkVolumeProperty *property = vol->GetProperty();


  // Create a 1D texture for transfer function look up
  if (   !this->Internals->TransferFunctionsTexture
      || (this->TransferFunctionTime < property->GetMTime())
      || (this->TransferFunctionTime < this->Scalars_MTime)
      || (!property->GetIndependentComponents() &&
          !this->Internals->TransferFunctionsTexture2))
  {
    if(this->Internals->TransferFunctionsTexture)
      glDeleteTextures(1,&this->Internals->TransferFunctionsTexture);

    if(this->Internals->TransferFunctionsTexture2)
      glDeleteTextures(1,&this->Internals->TransferFunctionsTexture2);

    const int TF_SIZE = 512;

    float TransferFunction[TF_SIZE*4];

    double x = ScalarsRange[0];
    double dx = 1.0/((float)TF_SIZE-1.0)*(ScalarsRange[1]-ScalarsRange[0]);

    vtkColorTransferFunction * color = NULL;
    vtkPiecewiseFunction     * gray  = NULL;
    vtkPiecewiseFunction     * alpha = property->GetScalarOpacity();

    if(property->GetColorChannels() == 1)
      gray = property->GetGrayTransferFunction();
    else
      color = property->GetRGBTransferFunction();

    double UnitDistance = property->GetScalarOpacityUnitDistance();

    double c[3], a ;

    for (int i = 0; i < TF_SIZE; i++)
    {
      if(gray)
      {
        c[0] = gray->GetValue(x);c[1] = c[0];c[2] = c[0];
      }
      else
      {
        color->GetColor(x,c);
      }

      a = alpha->GetValue(x);

      TransferFunction[i*4+0] = c[0];
      TransferFunction[i*4+1] = c[1];
      TransferFunction[i*4+2] = c[2];
      TransferFunction[i*4+3] = a / UnitDistance;

      x+=dx;
    }

    glGenTextures(1, &this->Internals->TransferFunctionsTexture);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_1D, this->Internals->TransferFunctionsTexture);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, vtkgl::CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, vtkgl::CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA,TF_SIZE,0,GL_RGBA,GL_FLOAT,TransferFunction);

    if(!property->GetIndependentComponents())
    {
      x = Scalars2Range[0];
      dx = 1.0/((float)TF_SIZE-1.0)*(Scalars2Range[1]-Scalars2Range[0]);

      if(property->GetColorChannels(1) == 1)
        gray = property->GetGrayTransferFunction(1);
      else
        color = property->GetRGBTransferFunction(1);

      alpha = property->GetScalarOpacity(1);

      UnitDistance = property->GetScalarOpacityUnitDistance(1);

      for (int i = 0; i < TF_SIZE; i++)
      {
        if(gray)
        {
          c[0] = gray->GetValue(x);c[1] = c[0];c[2] = c[0];
        }
        else
        {
          color->GetColor(x,c);
        }

        a = alpha->GetValue(x);

        TransferFunction[i*4+0] = c[0];
        TransferFunction[i*4+1] = c[1];
        TransferFunction[i*4+2] = c[2];
        TransferFunction[i*4+3] = a / UnitDistance;

        x+=dx;
      }

      glGenTextures(1, &this->Internals->TransferFunctionsTexture2);
      glActiveTexture(GL_TEXTURE2);
      glBindTexture(GL_TEXTURE_1D, this->Internals->TransferFunctionsTexture2);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, vtkgl::CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, vtkgl::CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA,TF_SIZE,0,GL_RGBA,GL_FLOAT,TransferFunction);
    }

    this->TransferFunctionTime.Modified();
  }

  return true;
}

//-----------------------------------------------------------------------------

bool OpenGLPTVolumeMapper::InitPsiTable(vtkRenderer *,vtkVolume *)
{
  if (!this->Internals->PsiTableTexture)
  {
    vtkUnstructuredGridPartialPreIntegration *ppi =
        vtkUnstructuredGridPartialPreIntegration::New();
    ppi->BuildPsiTable();

    float *psiTable = ppi->GetPsiTable(this->Internals->PsiTableTextureSize);

    // Create a 2D texture for the PSI lookup table
    glGenTextures(1, &this->Internals->PsiTableTexture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->Internals->PsiTableTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, vtkgl::CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, vtkgl::CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED,this->Internals->PsiTableTextureSize,
                 this->Internals->PsiTableTextureSize,0, GL_RED, GL_FLOAT, psiTable);
    ppi->Delete();
  }

  return true;
}

//-----------------------------------------------------------------------------

void OpenGLPTVolumeMapper::Render(vtkRenderer *ren,vtkVolume *vol)
{
  CheckSettingsChanges(ren,vol);

  if(!InitPsiTable(ren,vol)) return;
  this->CheckOpenGLError("Initalized Psi table");
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;

  if(!InitGridData(ren,vol)) return;
  this->CheckOpenGLError("Initalized Grid data");
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;  

  if(!InitScalars(ren,vol)) return;
  this->CheckOpenGLError("Initalized Scalar data");
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;

  if(!InitTransferFunction(ren,vol)) return;
  this->CheckOpenGLError("Initalized Transfer function");
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;

  if(!InitShaders(ren,vol)) return;
  this->CheckOpenGLError("Initalized Shaders");
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;

  this->Timer->StartTimer();

  this->ProjectTetrahedra_GPU(ren, vol);
  this->CheckOpenGLError("Projected tetrahedra");

  this->Timer->StopTimer();
  this->TimeToDraw = this->Timer->GetElapsedTime();
}

//-----------------------------------------------------------------------------
void OpenGLPTVolumeMapper::ProjectTetrahedra_GPU
(vtkRenderer *renderer,vtkVolume *volume)
{
  GLint drawbuffer_id;
  glGetIntegerv(GL_DRAW_BUFFER, &drawbuffer_id);

  if (! Initialized)Initialize(renderer);


  if (this->CanDoFloatingPointFrameBuffer && this->UseFloatingPointFrameBuffer)
  {
    this->CanDoFloatingPointFrameBuffer = CheckFBOResources(renderer);
    if (this->CanDoFloatingPointFrameBuffer)
    {
      vtkgl::BindFramebuffer(vtkgl::FRAMEBUFFER_EXT, 0);
      vtkgl::BindFramebuffer(vtkgl::DRAW_FRAMEBUFFER_EXT,
                             this->Internals->FrameBufferObjectId);

      glReadBuffer(drawbuffer_id);

      vtkgl::BlitFramebuffer(0, 0,
                             this->CurrentFBOWidth, this->CurrentFBOHeight,
                             0, 0,
                             this->CurrentFBOWidth, this->CurrentFBOHeight,
                             GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,
                             GL_NEAREST);
    }
  }

  vtkUnstructuredGridBase *input = this->GetInput();

  const int VSORT_MAXCELLS_PER_ITER = 4000;

  this->VisibilitySort->SetInput(input);
  this->VisibilitySort->SetDirectionToBackToFront();
  this->VisibilitySort->SetModelTransform(volume->GetMatrix());
  this->VisibilitySort->SetCamera(renderer->GetActiveCamera());
  this->VisibilitySort->SetMaxCellsReturned(VSORT_MAXCELLS_PER_ITER);

  this->VisibilitySort->InitTraversal();

  if (renderer->GetRenderWindow()->CheckAbortStatus()) return;


  glDisable(GL_LIGHTING);
  glDepthMask(GL_FALSE);

  glActiveTexture(GL_TEXTURE0);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, this->Internals->PsiTableTexture);
//  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  glActiveTexture(GL_TEXTURE1);
  glEnable(GL_TEXTURE_1D);
  glBindTexture(GL_TEXTURE_1D, this->Internals->TransferFunctionsTexture);
//  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  if(!volume->GetProperty()->GetIndependentComponents())
  {
    glActiveTexture(GL_TEXTURE2);
    glEnable(GL_TEXTURE_1D);
    glBindTexture(GL_TEXTURE_1D, this->Internals->TransferFunctionsTexture2);
  }

  glShadeModel(GL_SMOOTH);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDisable(GL_CULL_FACE);

  // save the default blend function.
  glPushAttrib(GL_COLOR_BUFFER_BIT);

  glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

  GLvoid * tvptr[] = {this->TetsVert,this->TetsVert + 3,
                    this->TetsVert + 6,this->TetsVert + 9};

  if(GetUseGPUBuffers())
  {
    tvptr[0] = 0;
    tvptr[1] = (GLvoid*)(sizeof(GL_FLOAT)*3);
    tvptr[2] = (GLvoid*)(sizeof(GL_FLOAT)*6);
    tvptr[3] = (GLvoid*)(sizeof(GL_FLOAT)*9);

    vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER,this->Internals->TetsVert);
  }

  glEnableClientState(GL_VERTEX_ARRAY);
  glVertexPointer(3,GL_FLOAT,12*sizeof(GL_FLOAT),tvptr[0]);

  glClientActiveTexture(GL_TEXTURE0 + 0);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glTexCoordPointer(3,GL_FLOAT,12*sizeof(GL_FLOAT),tvptr[1]);

  glClientActiveTexture(GL_TEXTURE0 + 1);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glTexCoordPointer(3,GL_FLOAT,12*sizeof(GL_FLOAT),tvptr[2]);

  glClientActiveTexture(GL_TEXTURE0 + 2);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glTexCoordPointer(3,GL_FLOAT,12*sizeof(GL_FLOAT),tvptr[3]);

  GLvoid * tvsptr[] = {this->TetsVertScalar,TetsVertScalar2};

  if(GetUseGPUBuffers())
  {
    tvsptr[0] = 0;
    tvsptr[1] = 0;

    vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER,this->Internals->TetsVertScalar);
  }

  glEnableClientState(GL_COLOR_ARRAY);
  glColorPointer(4,GL_FLOAT,0,tvsptr[0]);

  if(!volume->GetProperty()->GetIndependentComponents())
  {
    if(GetUseGPUBuffers())
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER,this->Internals->TetsVertScalar2);

    glClientActiveTexture(GL_TEXTURE0 + 3);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(4, GL_FLOAT,0,tvsptr[1]);
  }

  this->Internals->Shader->GetUniformVariables()->SetUniformf
      ("maxEdgeLength",1,&this->MaxEdgeLength);

  this->Internals->Shader->Use();

  int ntets_rendered = 0;

  unsigned int corder_buf[VSORT_MAXCELLS_PER_ITER];

  // Let's do it!
  for (vtkIdTypeArray *sorted_ids = this->VisibilitySort->GetNextCells();
       sorted_ids != NULL;
       sorted_ids = this->VisibilitySort->GetNextCells())
  {
    this->UpdateProgress((double)ntets_rendered/NumberOfTetrahedra);
    if (renderer->GetRenderWindow()->CheckAbortStatus()) break;

    int n = sorted_ids->GetNumberOfTuples();

    std::copy(sorted_ids->GetPointer(0),sorted_ids->GetPointer(0)+n,corder_buf);

    glDrawElements(GL_POINTS, n, GL_UNSIGNED_INT,corder_buf);

    ntets_rendered += n;
  }

  this->Internals->Shader->Restore();

  if(GetUseGPUBuffers())
    vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER,0);

  glDisableClientState(GL_VERTEX_ARRAY);

  glClientActiveTexture(GL_TEXTURE0 + 0);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glClientActiveTexture(GL_TEXTURE0 + 1);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glClientActiveTexture(GL_TEXTURE0 + 2);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glDisableClientState(GL_COLOR_ARRAY);

  if(!volume->GetProperty()->GetIndependentComponents())
  {
    glClientActiveTexture(GL_TEXTURE0 + 3);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  }

  // Restore the blend function.
  glPopAttrib();

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_1D, 0);
  glDisable(GL_TEXTURE_1D);

  if(!volume->GetProperty()->GetIndependentComponents())
  {
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_1D, 0);
    glDisable(GL_TEXTURE_1D);
  }

  glDepthMask(GL_TRUE);
  glEnable(GL_LIGHTING);

  if (this->CanDoFloatingPointFrameBuffer && this->UseFloatingPointFrameBuffer)
  {
    vtkgl::BindFramebuffer(vtkgl::FRAMEBUFFER_EXT, 0);
    vtkgl::BindFramebuffer(vtkgl::READ_FRAMEBUFFER_EXT,
                           this->Internals->FrameBufferObjectId);

    glDrawBuffer(drawbuffer_id);

    vtkgl::BlitFramebuffer(0, 0, this->CurrentFBOWidth, this->CurrentFBOHeight,
                         0, 0, this->CurrentFBOWidth, this->CurrentFBOHeight,
                         GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);

    vtkgl::BindFramebuffer(vtkgl::FRAMEBUFFER_EXT, 0);
  }

  this->UpdateProgress(1.0);
}

#define __SWITCH_CASE_TO_STR(c) case (c): return #c;

std::string OpenGLErrorToString(int err)
{
  switch(err)
  {
    __SWITCH_CASE_TO_STR(GL_INVALID_ENUM);
    __SWITCH_CASE_TO_STR(GL_INVALID_OPERATION);
  }
  return "Unknown";
}

void OpenGLPTVolumeMapper::CheckOpenGLError(const char * str)
{
  int err = glGetError(); (void)str;
  if ( err != GL_NO_ERROR)
  {
    vtkDebugMacro(<<"OpenGL Error: " << OpenGLErrorToString(err)<<endl);
    vtkDebugMacro(<<"App Message : "<<str<<endl);
  }
}

void OpenGLPTVolumeMapper::CheckSettingsChanges(vtkRenderer *r, vtkVolume *v)
{
  if(this->VolProp_IndependantComponents!=
      v->GetProperty()->GetIndependentComponents())
  {
    this->VolProp_IndependantComponents=
        v->GetProperty()->GetIndependentComponents();

    this->VolProp_IndependantComponents_MTime.Modified();
  }
}
