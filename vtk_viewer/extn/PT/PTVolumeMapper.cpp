/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkProjectedTetrahedraMapper.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*
 * Copyright 2003 Sandia Corporation.
 * Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
 * license for use of this work by or on behalf of the
 * U.S. Government. Redistribution and use in source and binary forms, with
 * or without modification, are permitted provided that this Notice and any
 * statement of authorship are reproduced on all copies.
 */

#include "PTVolumeMapper.hpp"

#include "vtkCellCenterDepthSort.h"
#include "vtkGarbageCollector.h"

#if VTK_MAJOR_VERSION <= 5
#include "vtkVolumeRenderingFactory.h"
#else
#include "vtkObjectFactory.h"
#endif

//-----------------------------------------------------------------------------

#if VTK_MAJOR_VERSION <= 5
vtkCxxSetObjectMacro(PTVolumeMapper,VisibilitySort, vtkVisibilitySort);

//-----------------------------------------------------------------------------

// Needed when we don't use the vtkStandardNewMacro.
vtkInstantiatorNewMacro(PTVolumeMapper);

PTVolumeMapper *PTVolumeMapper::New()
{
  vtkObject *ret
    = vtkVolumeRenderingFactory::CreateInstance("PTVolumeMapper");
  return (PTVolumeMapper *)ret;
}

#else
vtkAbstractObjectFactoryNewMacro(PTVolumeMapper)
#endif

//-----------------------------------------------------------------------------

PTVolumeMapper::PTVolumeMapper()
{
  this->VisibilitySort = vtkCellCenterDepthSort::New();
}

PTVolumeMapper::~PTVolumeMapper()
{
  this->SetVisibilitySort(NULL);
}

void PTVolumeMapper::PrintSelf(ostream &os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "VisibilitySort: " << this->VisibilitySort << endl;

}

//-----------------------------------------------------------------------------

void PTVolumeMapper::ReportReferences(vtkGarbageCollector *collector)
{
  this->Superclass::ReportReferences(collector);

  vtkGarbageCollectorReport(collector, this->VisibilitySort, "VisibilitySort");
}
