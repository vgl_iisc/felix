/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkOpenGLProjectedTetrahedraMapper.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/*
 * Copyright 2003 Sandia Corporation.
 * Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
 * license for use of this work by or on behalf of the
 * U.S. Government. Redistribution and use in source and binary forms, with
 * or without modification, are permitted provided that this Notice and any
 * statement of authorship are reproduced on all copies.
 */

// .NAME vtkOpenGLProjectedTetrahedraMapper - OpenGL implementation of PT
//
// .SECTION Bugs
// This mapper relies highly on the implementation of the OpenGL pipeline.
// A typical hardware driver has lots of options and some settings can
// cause this mapper to produce artifacts.
//

#ifndef __OpenGLPTVolumeMapper_h
#define __OpenGLPTVolumeMapper_h

#include "PTVolumeMapper.hpp"

class vtkVisibilitySort;

class OpenGLPTVolumeMapper : public PTVolumeMapper
{
public:
  vtkTypeMacro(OpenGLPTVolumeMapper,PTVolumeMapper);
  static OpenGLPTVolumeMapper *New();
  virtual void PrintSelf(ostream &os, vtkIndent indent);

  virtual void ReleaseGraphicsResources(vtkWindow *window);

  virtual void Render(vtkRenderer *renderer, vtkVolume *volume);

  // Description:
  // Set/get whether to use floating-point rendering buffers rather
  // than the default.
  vtkSetMacro(UseFloatingPointFrameBuffer,bool);
  vtkGetMacro(UseFloatingPointFrameBuffer,bool);
  vtkBooleanMacro(UseFloatingPointFrameBuffer,bool);

  vtkGetMacro(UseGPUBuffers,bool);
  vtkSetMacro(UseGPUBuffers,bool);
  vtkBooleanMacro(UseGPUBuffers,bool);

  vtkGetMacro(UseScalar2OpacityExpDecay,bool);
  void SetUseScalar2OpacityExpDecay(bool);
  vtkBooleanMacro(UseScalar2OpacityExpDecay,bool);

  vtkGetMacro(Scalar2OpacityExpDecayVariance,double);
  void SetScalar2OpacityExpDecayVariance(double);

  vtkGetMacro(Brightness,double);
  void SetBrightness(double);


protected:
  OpenGLPTVolumeMapper();
  ~OpenGLPTVolumeMapper();

  void Initialize(vtkRenderer *ren);
  bool Initialized;
  int  CurrentFBOWidth, CurrentFBOHeight;
  bool CheckFBOResources(vtkRenderer *ren);
  bool CanDoFloatingPointFrameBuffer;
  bool FloatingPointFrameBufferResourcesAllocated;
  bool UseFloatingPointFrameBuffer;

  double         ScalarsRange[2];
  double         Scalars2Range[2];

  bool           UseGPUBuffers;

  float         *TetsVert;
  int            NumberOfTetrahedra;
  vtkTimeStamp   Grid_MTime;
  float          MaxEdgeLength;

  float         *TetsVertScalar;
  float         *TetsVertScalar2;
  vtkTimeStamp   Scalars_MTime;
  vtkDataArray  *LastScalarData;


  bool           UseScalar2OpacityExpDecay;
  double         Scalar2OpacityExpDecayVariance;
  vtkTimeStamp   Scalar2OpacityExpDecayVariance_MTime;

  bool InitShaders(vtkRenderer *renderer,vtkVolume *volume);

  bool InitGridData(vtkRenderer *renderer,vtkVolume *volume);

  bool InitScalars(vtkRenderer *renderer,vtkVolume *volume);

  vtkTimeStamp TransferFunctionTime;
  bool InitTransferFunction(vtkRenderer *renderer,vtkVolume *volume);

  bool InitPsiTable(vtkRenderer *renderer,vtkVolume *volume);

  void ProjectTetrahedra_GPU(vtkRenderer *renderer, vtkVolume *volume);

  void CheckOpenGLError(const char * str);


  void CheckSettingsChanges(vtkRenderer *ren, vtkVolume *vol);

  int            VolProp_IndependantComponents;
  vtkTimeStamp   VolProp_IndependantComponents_MTime;

  double         Brightness;
  vtkTimeStamp   Brightness_MTime;


private:
  OpenGLPTVolumeMapper(const OpenGLPTVolumeMapper &);  // Not Implemented.
  void operator=(const OpenGLPTVolumeMapper &);  // Not Implemented.

  class vtkInternals;

  vtkInternals *Internals;
};

#endif
