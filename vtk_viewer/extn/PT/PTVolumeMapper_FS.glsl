/**
 */

uniform sampler1D tfTex; ///< Transfer Function Texture
uniform sampler2D psiGammaTableTex; ///< Psi Gamma Table (Pre-Integration) Texture
uniform int preIntTexSize; ///< Pre-Integration (Quad) Texture width

uniform float maxEdgeLength; ///< Maximum edge length

#ifdef TWOSCALARS
#ifndef SCALAR2EXPDECAY_VAR
uniform sampler1D tf2Tex; ///< Transfer Function Texture
#endif
varying float fs_depth;
#endif

#ifndef BRIGHTNESS
#define BRIGHTNESS 8f
#endif

// void main(void) 
// {
//   gl_FragColor = gl_Color;
// }

void main(void) 
{
  float sf = gl_Color.r; ///< Scalar front
  float sb = gl_Color.g; ///< Scalar back
  
#ifndef TWOSCALARS  
  float l = gl_Color.b; ///< Thickness or isoOpacity
#else
  float asf = gl_Color.b; ///< Scalar front
  float asb = gl_Color.a; ///< Scalar back
  float l = fs_depth; ///< Thickness or isoOpacity
#endif  
  
  l *= BRIGHTNESS; /// Brightness by thickness
  l /= maxEdgeLength; /// Normalize thickness [0, 1]	

  vec4 colorFront = texture1D(tfTex, sf);
  vec4 colorBack = texture1D(tfTex, sb);
  
#ifdef TWOSCALARS  
#ifdef SCALAR2EXPDECAY_VAR
  colorFront.a *= exp(-asf*asf/(2.0*SCALAR2EXPDECAY_VAR));
  colorBack.a *= exp(-asb*asb/(2.0*SCALAR2EXPDECAY_VAR));
#else
  colorFront.a *= texture1D(tf2Tex, asf).a;
  colorBack.a  *= texture1D(tf2Tex, asb).a;
#endif
#endif
  
  vec4 color;

  vec2 tau = vec2(colorFront.a, colorBack.a) * l;

  vec2 halfVec = vec2(0.5);

  float zeta = exp( -dot(tau, halfVec) );

  if (zeta == 1.0) /// No fragment color
    discard;

  vec2 gamma = tau / (1.0 + tau);

  float psi = texture2D(psiGammaTableTex, gamma + (halfVec / vec2(preIntTexSize))).x;

  color.rgb = colorFront.rgb*(1.0 - psi) + colorBack.rgb*(psi - zeta);
  color.a = 1.0 - zeta;
  
  gl_FragColor = color;

// composite front to back
// vec4 curcolor = texture2D(fboRenderTex,gl_FragCoord.xy*viewportSzInv);
// gl_FragData[0] = curcolor + color*(1.0 - curcolor.a);

}
