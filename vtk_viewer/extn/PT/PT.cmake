cmake_minimum_required(VERSION 2.8)

set(PT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/extn/PT)

set(PT_SOURCES
  ${PT_SOURCE_DIR}/PTVolumeMapper.hpp
  ${PT_SOURCE_DIR}/PTVolumeMapper.cpp

  ${PT_SOURCE_DIR}/OpenGLPTVolumeMapper.hpp
  ${PT_SOURCE_DIR}/OpenGLPTVolumeMapper.cpp
)

set(PT_SHADERS
  ${PT_SOURCE_DIR}/PTVolumeMapper_VS.glsl
  ${PT_SOURCE_DIR}/PTVolumeMapper_GS.glsl
  ${PT_SOURCE_DIR}/PTVolumeMapper_FS.glsl
  )
  
# -----------------------------------------------------------------------------
# Create custom commands to encode each glsl file into a C string literal
# in a header file
# -----------------------------------------------------------------------------

foreach(file ${PT_SHADERS})
  GET_FILENAME_COMPONENT(file_we ${file} NAME_WE)
  set(src ${file})
  set(res ${CMAKE_CURRENT_BINARY_DIR}/${file_we}.cpp)

  add_custom_command(
    OUTPUT ${res}
    DEPENDS ${src}
    COMMAND vtkEncodeString
    ARGS ${res} ${src} ${file_we}
    )
  set(PT_SOURCES ${PT_SOURCES} ${res})
endforeach(file)

