/**
 *   PT -- Projected Tetrahedra
 *
 *
 */

/// Shader Model 4.0 is required
#extension GL_EXT_geometry_shader4 : enable
#extension GL_EXT_gpu_shader4 : enable 

#ifndef TWOSCALARS

#define EMIT_VERTEX(_N,_D) \
gl_Position   = P##_N;\
gl_FrontColor = vec4(S##_N,_D,0);\
EmitVertex();\

#else

varying out float fs_depth;

#define EMIT_VERTEX(_N,_D) \
fs_depth = _D;\
gl_Position   = P ## _N;\
gl_FrontColor = vec4(S ## _N,AS ## _N);\
EmitVertex();\

#endif

float GetCorrectedDepth(vec3 thickpt, float back_z)
{
  mat4 inv = gl_ModelViewProjectionMatrixInverse;
  
  vec4 common = inv*vec4(thickpt.xy,0,1);
  
  vec4 eye1 = (common + inv[2]*thickpt.z);
  vec4 eye2 = (common + inv[2]*back_z);
  
  eye1 /= eye1.w;
  eye2 /= eye2.w;

  return length((eye1.xyz/eye1.w)-(eye2.xyz/eye2.w))*sign(eye1.z - eye2.z);
}

void main(void) 
{
  vec4  tet_points[5];
  tet_points[0] = gl_PositionIn[0];
  tet_points[1] = gl_TexCoordIn[0][0];
  tet_points[2] = gl_TexCoordIn[0][1];
  tet_points[3] = gl_TexCoordIn[0][2];
  
  tet_points[0] /= tet_points[0].w;
  tet_points[1] /= tet_points[1].w;
  tet_points[2] /= tet_points[2].w;
  tet_points[3] /= tet_points[3].w;
    
  vec2  tet_scalars[5];  
  tet_scalars[0] = gl_FrontColorIn[0].xx;
  tet_scalars[1] = gl_FrontColorIn[0].yy;
  tet_scalars[2] = gl_FrontColorIn[0].zz;
  tet_scalars[3] = gl_FrontColorIn[0].ww;
  
#ifdef TWOSCALARS
  vec2  tet_scalars2[5];
  tet_scalars2[0] = gl_BackColorIn[0].xx;
  tet_scalars2[1] = gl_BackColorIn[0].yy;
  tet_scalars2[2] = gl_BackColorIn[0].zz;
  tet_scalars2[3] = gl_BackColorIn[0].ww;
#endif

  
  // Do not render this cell if it is outside of the cutting planes.  For
  // most planes, cut if all points are outside.  For the near plane, cut if
  // any points are outside because things can go very wrong if one of the
  // points is behind the view.
  if (   (   (tet_points[0].x >  1.0f) && (tet_points[1].x >  1.0f)
	  && (tet_points[2].x >  1.0f) && (tet_points[3].x >  1.0f) )
      || (   (tet_points[0].x < -1.0f) && (tet_points[1].x < -1.0f)
	  && (tet_points[2].x < -1.0f) && (tet_points[3].x < -1.0f) )
      || (   (tet_points[0].y >  1.0f) && (tet_points[1].y >  1.0f)
	  && (tet_points[2].y >  1.0f) && (tet_points[3].y >  1.0f) )
      || (   (tet_points[0].y < -1.0f) && (tet_points[1].y < -1.0f)
	  && (tet_points[2].y < -1.0f) && (tet_points[3].y < -1.0f) )
      || (   (tet_points[0].z >  1.0f) && (tet_points[1].z >  1.0f)
	  && (tet_points[2].z >  1.0f) && (tet_points[3].z >  1.0f) )
      || (   (tet_points[0].z < -1.0f) || (tet_points[1].z < -1.0f)
	  || (tet_points[2].z < -1.0f) || (tet_points[3].z < -1.0f) ) )
  {
    return;
  }
  
  // The classic PT algorithm uses face normals to determine the
  // projection class and then do calculations individually.  However,
  // Wylie 2002 shows how to use the intersection of two segments to
  // calculate the depth of the thick part for any case.  Here, we use
  // face normals to determine which segments to use.  One segment
  // should be between two faces that are either both front facing or
  // back facing.  Obviously, we only need to test three faces to find
  // two such faces.  We test the three faces connected to point 0.


  ivec4 segments;

  vec2 v1, v2, v3;
  v1.x = tet_points[1].x - tet_points[0].x;
  v1.y = tet_points[1].y - tet_points[0].y;
  v2.x = tet_points[2].x - tet_points[0].x;
  v2.y = tet_points[2].y - tet_points[0].y;
  v3.x = tet_points[3].x - tet_points[0].x;
  v3.y = tet_points[3].y - tet_points[0].y;

  float face_dir1 = v3.x*v2.y - v3.y*v2.x;
  float face_dir2 = v1.x*v3.y - v1.y*v3.x;
  float face_dir3 = v2.x*v1.y - v2.y*v1.x;

  if (   (face_dir1 * face_dir2 >= 0.0)
      && (   (face_dir1 != 0.0)       // Handle a special case where 2 faces
	  || (face_dir2 != 0.0) ) )   // are perpendicular to the view plane.
  {
    segments.x = 0;  segments.y = 3;
    segments.z = 1;  segments.w = 2;
  }
  else if (face_dir1 * face_dir3 >= 0.0)
  {
    segments.x = 0;  segments.y = 2;
    segments.z = 1;  segments.w = 3;
  }
  else      // Unless the tet is degenerate, face_dir2*face_dir3 >= 0
  {
    segments.x = 0;  segments.y = 1;
    segments.z = 2;  segments.w = 3;
  }
  
#define P1 (tet_points[segments.x])
#define P2 (tet_points[segments.y])
#define P3 (tet_points[segments.z])
#define P4 (tet_points[segments.w])
#define P5 (tet_points[4])
#define S1 (tet_scalars[segments.x])
#define S2 (tet_scalars[segments.y])
#define S3 (tet_scalars[segments.z])
#define S4 (tet_scalars[segments.w])
#define S5 (tet_scalars[4])
#define AS1 (tet_scalars2[segments.x])
#define AS2 (tet_scalars2[segments.y])
#define AS3 (tet_scalars2[segments.z])
#define AS4 (tet_scalars2[segments.w])
#define AS5 (tet_scalars2[4])
   
  // Find the intersection of the projection of the two segments in the
  // XY plane.  This algorithm is based on that given in Graphics Gems
  // III, pg. 199-202.
  vec4 A,B,C;
  // We can define the two lines parametrically as:
  //        P1 + alpha(A)
  //        P3 + beta(B)
  // where A = P2 - P1
  // and   B = P4 - P3.
  // alpha and beta are in the range [0,1] within the line segment.
  A = P2 - P1;
  B = P4 - P3;
  // The lines intersect when the values of the two parameteric equations
  // are equal.  Setting them equal and moving everything to one side:
  //        0 = C + beta(B) - alpha(A)
  // where C = P3 - P1.
  C = P3-P1;
  // When we project the lines to the xy plane (which we do by throwing
  // away the z value), we have two equations and two unkowns.  The
  // following are the solutions for alpha and beta.
  float denominator = (A.x*B.y-A.y*B.x);
  if (denominator == 0.0)  return;   // Must be degenerated tetrahedra.
  float alpha = (B.y*C.x-B.x*C.y)/denominator;
  float beta = (A.y*C.x-A.x*C.y)/denominator;  
  
  if ((alpha >= 0.0) && (alpha <= 1.0) && 
      (beta >= 0.0) && (beta <= 1.0))
  {
    // The two segments intersect.  This corresponds to class 2 in
    // Shirley and Tuchman (or one of the degenerate cases).        
    
    vec4 ixn_a = P1 + alpha*A;
    vec4 ixn_b = P3 +  beta*B;
    
    // Make new point at intersection.
    P5 = ixn_a;
    
    // Find scalar at intersection... at this point all scalars have x and y the same
    S5.x = S1.x + alpha*(S2.x-S1.x);
    S5.y = S3.x +  beta*(S4.x-S3.x);    
    
#ifdef TWOSCALARS
    AS5.x = AS1.x + alpha*(AS2.x-AS1.x);
    AS5.y = AS3.y +  beta*(AS4.y-AS3.y);    
#endif
   
    float depth = GetCorrectedDepth(ixn_a.xyz,ixn_b.z);

    // Find depth at intersection.
    if(depth < 0.0)
    {
      depth *=-1.0;
      S5.xy = S5.yx;
#ifdef TWOSCALARS
      AS5.xy = AS5.yx;
#endif      
    }
   
    // Establish the order in which the points should be rendered.
    EMIT_VERTEX(1,0.0);
    EMIT_VERTEX(3,0.0);
    EMIT_VERTEX(5,depth);
    EMIT_VERTEX(2,0.0);
    EMIT_VERTEX(4,0.0);    
    EndPrimitive();
    EMIT_VERTEX(5,depth);
    EMIT_VERTEX(4,0.0);
    EMIT_VERTEX(1,0.0);
    EndPrimitive();
  }
  else
  {
    
    if(beta < 0.0 || beta > 1.0)
    {
      segments.xyzw = segments.zwxy;
      float tmp = alpha;
      alpha = beta;
      beta = tmp;
    }
    
    // The two segments do not intersect.  This corresponds to class 1
    // in Shirley and Tuchman.    
    if (alpha <= 0.0)
    {
      // Flip segments so that alpha is >= 1.  P1 and P2 are also
      // flipped as are C1-C2 and T1-T2.  Note that this will
      // invalidate A.  B and beta are unaffected.
      segments.xy = segments.yx;      
      alpha = 1.0 - alpha;
    }
   
    // From here on, we can assume P2 is the "thick" point.

    // Find the depth under the thick point.  Use the alpha and beta
    // from intersection to determine location of face under thick
    // point.
    float edgez = P3.z + beta*(P4.z - P3.z);
    float pointz = P1.z;
    float facez = (edgez + (alpha-1.0)*pointz)/alpha;
    float depth = GetCorrectedDepth(P2,facez);

    // Fix scalar at back of thick point.    
    S2.y = (S3.x + beta*(S4.x-S3.x) + (alpha-1.0)*S1.x)/alpha;    
#ifdef TWOSCALARS
    AS2.y = (AS3.x + beta*(AS4.x-AS3.x) + (alpha-1.0)*AS1.x)/alpha;
#endif

    if (depth < 0.0)
    {
      depth *= -1.0;
      S2.xy = S2.yx;
#ifdef TWOSCALARS
      AS2.xy = AS2.yx;
#endif
    }
    // Establish the order in which the points should be rendered.
    
    EMIT_VERTEX(1,0.0);
    EMIT_VERTEX(3,0.0);
    EMIT_VERTEX(2,depth);
    EMIT_VERTEX(4,0.0);
    EMIT_VERTEX(1,0.0);    
    EndPrimitive();
  }
}


// void main(void)
// {
//   // Establish the order in which the points should be rendered.    
//   gl_Position   = gl_ModelViewProjectionMatrix*vec4(gl_PositionIn[0].xyz,1);
//   gl_FrontColor = vec4(1,0,0,0);
//   EmitVertex();
//   
//   gl_Position   = gl_ModelViewProjectionMatrix*vec4(gl_PositionIn[2].xyz,1);
//   gl_FrontColor = vec4(1,0,0,1);
//   EmitVertex();
//   
//   gl_Position   = gl_ModelViewProjectionMatrix*vec4(gl_PositionIn[1].xyz,1);
//   gl_FrontColor = vec4(1,0,0,1);
//   EmitVertex();
//   
//   gl_Position   = gl_ModelViewProjectionMatrix*vec4(gl_PositionIn[3].xyz,1);
//   gl_FrontColor = vec4(1,0,0,1);
//   EmitVertex();
//   
//   gl_Position   = gl_ModelViewProjectionMatrix*vec4(gl_PositionIn[0].xyz,1);
//   gl_FrontColor = vec4(1,0,0,1);
//   EmitVertex();    
//   
//   EndPrimitive();
// }
// 

// void draw_line(vec4 v1,vec4 v2,vec4 v3, vec4 v4)
// {
//   gl_Position = v1;
//   gl_FrontColor = vec4(1,0,0,1);
//   EmitVertex();
//   
//   gl_Position = v2;
//   gl_FrontColor = vec4(0,0,0,1);
//   EmitVertex();  
//   
//   EndPrimitive();
//   
//   gl_Position = v3;
//   gl_FrontColor = vec4(0,0,1,1);
//   EmitVertex();
//   
//   gl_Position = v4;
//   gl_FrontColor = vec4(0,0,0,1);
//   EmitVertex();
//   
//   EndPrimitive();
// }
// 
// void draw_line2(vec4 v1,vec4 v2,vec4 v3, vec4 v4)
// {
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   gl_FrontColor = vec4(0,1,0,1);  
//   gl_Position = v2;
//   EmitVertex();  
//   
//   EndPrimitive();
//   
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   
//   gl_FrontColor = vec4(0,0,1,1);  
//   gl_Position = v3;
//   EmitVertex();  
//   
//   EndPrimitive();
//   
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   gl_FrontColor = vec4(1,1,0,1);
//   gl_Position = v4;
//   EmitVertex();  
//   
//   EndPrimitive();
// 
// }
// 
// 
// void draw_line3(vec4 v1,vec4 v2,vec4 v3, vec4 v4,vec4 v5)
// {
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   gl_FrontColor = vec4(0,1,0,1);  
//   gl_Position = v2;
//   EmitVertex();  
//   
//   EndPrimitive();
//   
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   
//   gl_FrontColor = vec4(0,0,1,1);  
//   gl_Position = v3;
//   EmitVertex();  
//   
//   EndPrimitive();
//   
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   gl_FrontColor = vec4(1,1,0,1);
//   gl_Position = v4;
//   EmitVertex();  
//   
//   EndPrimitive();
//   
//   gl_FrontColor = vec4(1,0,0,1);
//   gl_Position = v1;  
//   EmitVertex();
//   
//   gl_FrontColor = vec4(0,1,1,1);
//   gl_Position = v5;
//   EmitVertex();  
//   
//   EndPrimitive();
// 
// }