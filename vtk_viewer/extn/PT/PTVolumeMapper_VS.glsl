/**
 *   hapt.vert : Compute transformation for four vertices of one tetrahedron
 *               sent as a GL_POINT to the graphics pipeline using position and
 *               3 texCoords to store v_0, v_1, v_2 and v_3, respectively.
 *
 * GLSL code.
 *
 */
  

void main(void) 
{
  gl_Position = gl_ModelViewProjectionMatrix * vec4(gl_Vertex.xyz, 1.0);
  gl_TexCoord[0] = gl_ModelViewProjectionMatrix * vec4(gl_MultiTexCoord0.xyz, 1.0);
  gl_TexCoord[1] = gl_ModelViewProjectionMatrix * vec4(gl_MultiTexCoord1.xyz, 1.0);
  gl_TexCoord[2] = gl_ModelViewProjectionMatrix * vec4(gl_MultiTexCoord2.xyz, 1.0);
  
  gl_FrontColor =  gl_Color;
  
#ifdef TWOSCALARS
  gl_BackColor  = gl_MultiTexCoord3;
#endif  
}
