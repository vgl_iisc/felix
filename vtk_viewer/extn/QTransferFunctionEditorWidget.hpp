/******************************************************************************

Copyright (C) 2013 Nithin Shivashankar (nithin19484@gmail.com).
All rights reserved.

This was developed as a standalone Qt widget and a convenience Qt Dialog
to edit 1D transfer functions.

Code adapted from the Qtfe (Qt Transfer Function Editor) library by
Eric Heitz (er.heitz@gmail.com).

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License.
If not, see <http://www.gnu.org/licenses/>.

******************************************************************************/

#ifndef QTRANSFERFUNCTIONEDITORWIDGET
#define QTRANSFERFUNCTIONEDITORWIDGET

#include <QList>
#include <QColor>

struct TransferFunctionPoint
{
  qreal  f;
  QColor col;

  TransferFunctionPoint():f(0){}
  TransferFunctionPoint(qreal f,QColor col):f(f),col(col){}
  inline bool operator <(const TransferFunctionPoint &o) const{return f<o.f;}
};

typedef QList<TransferFunctionPoint> TransferFunction;

bool SaveTransferFunction(const TransferFunction&tf,const QString& fn);
bool LoadTransferFunction(      TransferFunction&tf,const QString& file,
                          qreal fmin=0,qreal fmax=0);
void ValidateTransferFunction(TransferFunction &tf,qreal fmin=0,qreal fmax=0);

/*****************************************************************************/



/*****************************************************************************/

#include <QWidget>
#include <QMouseEvent>

class QTransferFunctionEditorWidget: public QWidget
{
  Q_OBJECT

public:
  QTransferFunctionEditorWidget(QWidget *par=NULL,Qt::WindowFlags f=0);
  ~QTransferFunctionEditorWidget();

public slots:
  inline  void setRange(qreal m,qreal M){m_fmin = m; m_fmax = M;}
  inline qreal getRangeMin() const {return m_fmin;};
  inline qreal getRangeMax() const {return m_fmax;};

public:
  void getTransferFunction(TransferFunction &) const;
  void setTransferFunction(const TransferFunction &,qreal fmin=0,qreal fmax=0);

private:
  virtual void paintEvent(QPaintEvent* event);
  virtual void mousePressEvent(QMouseEvent* event);
  virtual void mouseDoubleClickEvent(QMouseEvent* event);
  virtual void mouseReleaseEvent(QMouseEvent* event);
  virtual void mouseMoveEvent(QMouseEvent * event);
  virtual void leaveEvent(QEvent* event);
  virtual void resizeEvent(QResizeEvent* event);

  QColor evalf(qreal x) const;

  // list of tf points
  TransferFunction list;

  // tf range
  qreal m_fmin,m_fmax;

  // mouse
  bool pressed;
  int  selected;
  qreal pMin, pMax;

  // paint
  QImage * background;
  QPoint  bg_bp;
  QSize   bg_sz;

  QPoint listPos2WidgetPos(int pos);
  QPointF WidgetPos2listPos(QPoint p);

private:

  static const int pointSizePixel;
  static const int circleSizePixel;
  static const int lineWidth;
  static const int tickLength;
};

/*****************************************************************************/



/*****************************************************************************/
#include <QDialog>

class QGridLayout;

/// \brief A dialog to edit a color tranfer function.
class QTransferFunctionEditorDialog: public QDialog
{
  Q_OBJECT

signals:
  /// \brief The user clicked Apply or Ok
  void applied();

public:
  /// \brief Constructor
  QTransferFunctionEditorDialog(QWidget *par=NULL,Qt::WindowFlags f=0);

  /// \brief Destructor
  ~QTransferFunctionEditorDialog();

  /// \brief Set the input transfer function
  /// \param[in]    tf   The input transfer function
  /// \param[in]    minf minimum Scalar function value (defaults to 0)
  /// \param[in]    maxf maximum Scalar function value (defaults to 0)
  ///
  /// \note If minf == 0 == maxf, it is assumed range is not given.
  ///       Then if tf is nonempty, the range of tf is taken.
  ///       If tf is empty then [0,1] is assumed and shown in the editor.
  ///
  /// \note If the range is given, then the tf will adhere to it.
  ///       If tf is non empty, the editor will show one that is rescaled to
  ///       given range, with a warning.
  inline void setTransferFunction(const TransferFunction &tf,qreal minf=0,qreal maxf=0)
  {tf_editor_widget->setTransferFunction(tf,minf,maxf);}

  /// \brief Get the edited tranfer function
  /// \param[out]    tf   The edited tf.
  ///
  /// \note previous contents of tf will be cleared
  inline void getTransferFunction(TransferFunction &tf) const
  {tf_editor_widget->getTransferFunction(tf);}

  /// \brief Pops up a modal dialog to edit a Transfer function
  ///
  /// \param[in,out] tf     The tf being edited
  /// \param[in]     minf   minimum Scalar function value (defaults to 0)
  /// \param[in]     maxf   maximum Scalar function value (defaults to 0)
  /// \param[in] normalized tf in/out is normalized to [0,1]
  ///
  /// \note see setTransferFunction() for convention on minf/maxf
  ///
  /// \note The given transfer function will be modified only if dialog is accepted
  static int editTransferFunction
    (TransferFunction& tf,qreal minf=0,qreal maxf=0,bool normalized=false);

  /// Get the normalized edited Tf. Range is in [0,1]
  ///
  /// \param[out] tf The tf which is normalized
  /// \note previous contents of tf will be cleared
  void getNormalizedTransferFunction(TransferFunction &tf) const;

  /// Set the normalized Tf. Range is in [0,1]. while editing it shown
  /// scaled from fmin to fmax
  ///
  /// \param[in] tf input Tf
  /// \note input has to be b/w [0,1] else it is enforced
  void setNormalizedTransferFunction
  (const TransferFunction &tf,qreal fmin=0,qreal fmax=0);


private:
  QGridLayout * gridl;
  QTransferFunctionEditorWidget  * tf_editor_widget;

private slots:
  void save_tf();
  void load_tf();
  void apply();
};



/// \brief A dialog to edit a color tranfer function.
class QTransferFunctionWidget: public QWidget
{
  Q_OBJECT

signals:
  /// \brief The user clicked Apply or Ok
  void applied();

public:
  /// \brief Constructor
  QTransferFunctionWidget(QWidget *par=NULL,Qt::WindowFlags f=0);

  /// \brief Destructor
  ~QTransferFunctionWidget();

  /// \brief Set the input transfer function
  /// \param[in]    tf   The input transfer function
  /// \param[in]    minf minimum Scalar function value (defaults to 0)
  /// \param[in]    maxf maximum Scalar function value (defaults to 0)
  ///
  /// \note If minf == 0 == maxf, it is assumed range is not given.
  ///       Then if tf is nonempty, the range of tf is taken.
  ///       If tf is empty then [0,1] is assumed and shown in the editor.
  ///
  /// \note If the range is given, then the tf will adhere to it.
  ///       If tf is non empty, the editor will show one that is rescaled to
  ///       given range, with a warning.
  inline void setTransferFunction(const TransferFunction &tf,qreal minf=0,qreal maxf=0)
  {tf_editor_widget->setTransferFunction(tf,minf,maxf);}

  /// \brief Get the edited tranfer function
  /// \param[out]    tf   The edited tf.
  ///
  /// \note previous contents of tf will be cleared
  inline void getTransferFunction(TransferFunction &tf) const
  {tf_editor_widget->getTransferFunction(tf);}

  /// Get the normalized edited Tf. Range is in [0,1]
  ///
  /// \param[out] tf The tf which is normalized
  /// \note previous contents of tf will be cleared
  void getNormalizedTransferFunction(TransferFunction &tf) const;

  /// Set the normalized Tf. Range is in [0,1]. while editing it shown
  /// scaled from fmin to fmax
  ///
  /// \param[in] tf input Tf
  /// \note input has to be b/w [0,1] else it is enforced
  void setNormalizedTransferFunction
  (const TransferFunction &tf,qreal fmin=0,qreal fmax=0);


private:
  QGridLayout * gridl;
  QTransferFunctionEditorWidget  * tf_editor_widget;

private slots:
  void save_tf();
  void load_tf();
  void apply();
};

/*****************************************************************************/
#endif
