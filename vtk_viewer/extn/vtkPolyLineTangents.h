/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPolyLineTangents.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef __vtkPolyLineTangents_h
#define __vtkPolyLineTangents_h

#include "vtkPolyDataAlgorithm.h"

class vtkPolyLineTangents: public vtkPolyDataAlgorithm
{
public:
  vtkTypeMacro(vtkPolyLineTangents,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  static vtkPolyLineTangents *New();

protected:
  vtkPolyLineTangents();
  ~vtkPolyLineTangents() {};

  // Usual data generation method
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);

private:
  vtkPolyLineTangents(const vtkPolyLineTangents&);  // Not implemented.
  void operator=(const vtkPolyLineTangents&);  // Not implemented.
};

#endif
