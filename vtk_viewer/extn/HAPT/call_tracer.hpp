#ifndef __CALL_TRACER_HPP
#define __CALL_TRACER_HPP

/*
  A utility to help tracking call sequences during run-time.
  Useful if you want to later instrument your code.
*/

struct __CALL_TRACER_SWITCH
{
  static bool traceon;
  bool old_traceon;

  __CALL_TRACER_SWITCH(bool new_traceon)
  {old_traceon = traceon;traceon = new_traceon;}
  ~__CALL_TRACER_SWITCH() {traceon = old_traceon;}
};

struct __CALL_TRACER_INDENTOR
{
  static int  indent;

  __CALL_TRACER_INDENTOR() {indent += 1;}
  ~__CALL_TRACER_INDENTOR() {indent -= 1;}
};

#ifdef CALL_TRACER_ENABLED
#include <sstream>


inline std::string __call_tracer_get_indent_string()
{
  std::stringstream ss;
  for(int i = 0 ; i < __CALL_TRACER_INDENTOR::indent; ++i)
    ss<<"--";
  return ss.str();
}

#define CALL_TRACER_MACRO __CALL_TRACER_INDENTOR __ct__indentor_object;\
  if(__CALL_TRACER_SWITCH::traceon)\
    std::cout<<"[Ctrace:]"<<__call_tracer_get_indent_string()<<__func__<<endl;

#define CALL_TRACER_SVAR(__val)\
  if(__CALL_TRACER_SWITCH::traceon)\
    std::cout<<"[Ctrace:]"<<__call_tracer_get_indent_string()<<"  "\
      <<(#__val)<<" = "<<(__val)<<endl;

#define CALL_TRACER_REC_TURNON  __CALL_TRACER_SWITCH __ct__switcher_object(true);
#define CALL_TRACER_REC_TURNOFF __CALL_TRACER_SWITCH __ct__switcher_object(false);
#else

#define CALL_TRACER_MACRO
#define CALL_TRACER_SVAR(__val)
#define CALL_TRACER_REC_TURNON
#define CALL_TRACER_REC_TURNOFF

#endif // CALL_TRACER_ENABLED

#endif
