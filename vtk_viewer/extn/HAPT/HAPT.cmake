cmake_minimum_required(VERSION 2.8)

set(HAPT_SOURCE_DIR ${VTK_VIEWER_SOURCE_DIR}/HAPT)
set(HAPT_BINARY_DIR ${VTK_VIEWER_BINARY_DIR})

set(HAPT_SOURCES
  ${HAPT_SOURCE_DIR}/HAPTVolumeMapper.hpp
  ${HAPT_SOURCE_DIR}/HAPTVolumeMapper.cpp

  ${HAPT_SOURCE_DIR}/HAPTVolumeMapper_tables.hpp

  ${HAPT_SOURCE_DIR}/OpenGLHAPTVolumeMapper.hpp
  ${HAPT_SOURCE_DIR}/OpenGLHAPTVolumeMapper.cpp

  ${HAPT_SOURCE_DIR}/call_tracer.hpp
  ${HAPT_SOURCE_DIR}/call_tracer.cpp
)

set(HAPT_SHADERS
  HAPTVolumeMapper_VS.glsl
  HAPTVolumeMapper_GS.glsl
  HAPTVolumeMapper_FS.glsl
  )

# -----------------------------------------------------------------------------
# Create custom commands to encode each glsl file into a C string literal
# in a header file
# -----------------------------------------------------------------------------

foreach(file ${HAPT_SHADERS})
  GET_FILENAME_COMPONENT(file_we ${file} NAME_WE)
  set(src ${HAPT_SOURCE_DIR}/${file})
  set(res ${HAPT_BINARY_DIR}/${file_we}.cpp)

  add_custom_command(
    OUTPUT ${res} ${resh}
    DEPENDS ${src}
    COMMAND vtkEncodeString
    ARGS ${res} ${src} ${file_we}
    )
  set(HAPT_SOURCES ${HAPT_SOURCES} ${res})
endforeach(file)

