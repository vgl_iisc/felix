/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkOpenGLHAPTVolumeMapper.h

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef __OpenGLHAPTVolumeMapper_h
#define __OpenGLHAPTVolumeMapper_h

#include "HAPTVolumeMapper.hpp"

#include <vtkWeakPointer.h> // to cache the vtkRenderWindow
class vtkRenderer;
class vtkRenderWindow;
class vtkShaderProgram2;

/// \brief OpenGL implementation of the HAPT Volume mapper
class OpenGLHAPTVolumeMapper : public HAPTVolumeMapper
{
public:
  static OpenGLHAPTVolumeMapper *New();

  vtkTypeMacro(OpenGLHAPTVolumeMapper,HAPTVolumeMapper);

  virtual void PrintSelf(ostream& os, vtkIndent indent);
  
  /// \brief
  /// Render the volume
  virtual void Render(vtkRenderer *ren, vtkVolume *vol);

  /// \brief
  /// Release any graphics resources that are being consumed by this volume
  /// renderer.
  virtual void ReleaseGraphicsResources(vtkWindow *);

  /// \brief
  /// Set whether or not the data structures should be stored on the GPU
  /// for better peformance.
  virtual void SetGPUDataStructures(bool);

  /// \brief
  /// Check hardware support for the HAPT algorithm.
  ///
  /// Necessary features include off-screen rendering, 32-bit fp textures and
  /// framebuffer objects.
  /// Subclasses must override this method to indicate if supported by Hardware.
  virtual bool SupportedByHardware(vtkRenderer *r);
protected:

  OpenGLHAPTVolumeMapper();
  ~OpenGLHAPTVolumeMapper();
  virtual int FillInputPortInformation(int port, vtkInformation* info);

//BTX
  void LoadRequiredExtensions(vtkRenderer *ren);

  void InitializeLookupTables(vtkVolume *vol);
  void ReleaseLookupTables();

  void InitializeGPUDataStructures();
  void ReleaseGPUDataStructures();

  void InitializeShaders();
  void ReleaseShaders();

  void InitializeFramebufferObject();
  void ReleaseFramebufferObject();

  void RenderHAPT(vtkRenderer *ren,vtkVolume *vol);

  void SetupFBOZBuffer(int screenWidth, int screenHeight,
                       float depthNear, float depthFar, float *zbuffer);
  void DrawFBOInit    (int screenWidth, int screenHeight,
                       float depthNear, float depthFar);
  void DrawFBO        (int screenWidth, int screenHeight,
                       float depthNear, float depthFar);
  void DrawFBOFlush   (int screenWidth, int screenHeight,
                       float depthNear, float depthFar);
  void DrawBlend      (int screenWidth, int screenHeight,
                       float depthNear, float depthFar);

  void CheckOpenGLError(const char *str);

  // GPU
  unsigned int FramebufferObject;
  int FramebufferObjectSize;
  unsigned int FramebufferTexture;
  unsigned int DepthRenderBuffer;

  bool         UseGPUDataStructures_changed;
#ifndef HAPT_USE_AUX_SCALARS
  unsigned int TetraVertPositionBufObj[4];
#else
  unsigned int TetraVertPositionBufObj[5];
#endif

  vtkShaderProgram2 *  Shader;

#ifdef HAPT_USE_AUX_SCALARS
  int                  AuxShaderTransferFunctionType;
  vtkShaderProgram2 *  AuxShader;
#endif

  // Lookup Tables
  unsigned int PsiTableTexture;
  int          PsiTableSize;
  unsigned int TransferFunctionTexture;
  unsigned int OrderTableTexture;
  unsigned int TriangleFanOrderTableTexture;
#ifdef HAPT_USE_AUX_SCALARS
  unsigned int AuxTransferFunctionTexture;
#endif

  vtkWeakPointer<vtkRenderWindow> RenderWindow;
//ETX

private:
  OpenGLHAPTVolumeMapper(const OpenGLHAPTVolumeMapper&);  // Not implemented.
  void operator=(const OpenGLHAPTVolumeMapper&);  // Not implemented.
};

#endif
