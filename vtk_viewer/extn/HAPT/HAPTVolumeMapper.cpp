/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkHAPTVolumeMapper.cxx

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/* Copyright 2005, 2006 by University of Utah. */

#include "HAPTVolumeMapper.hpp"

#include "vtkCell.h"
#include "vtkCellArray.h"
#include "vtkColorTransferFunction.h"
#include "vtkDataArray.h"
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkPiecewiseFunction.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkVolume.h"
#include "vtkVolumeProperty.h"
#include "vtkVolumeRenderingFactory.h"
#include "vtkCellCenterDepthSort.h"
#include "vtkGarbageCollector.h"

#include <algorithm>
#include <set>
#include <vector>

#include <math.h>

// Needed when we don't use the vtkStandardNewMacro.
vtkInstantiatorNewMacro(HAPTVolumeMapper);

#include "call_tracer.hpp"

/*****************************************************************************/

HAPTVolumeMapper *HAPTVolumeMapper::New()
{
  CALL_TRACER_MACRO;
  vtkObject *ret
    = vtkVolumeRenderingFactory::CreateInstance("HAPTVolumeMapper");
  return (HAPTVolumeMapper *)ret;
}

/*---------------------------------------------------------------------------*/
vtkCxxSetObjectMacro(HAPTVolumeMapper,VisibilitySort, vtkVisibilitySort);
 
/*---------------------------------------------------------------------------*/
HAPTVolumeMapper::HAPTVolumeMapper()
{
  CALL_TRACER_MACRO;
  this->TetraVertPosition[0]       = NULL;
  this->TetraVertPosition[1]       = NULL;
  this->TetraVertPosition[2]       = NULL;
  this->TetraVertPosition[3]       = NULL;
  this->Scalars                    = NULL;
  this->MaxCellSize                = 0;
  this->ScalarRange[0]             = 0.0;
  this->ScalarRange[1]             = 1.0;
  this->NumberOfVertices           = 0;
  this->NumberOfTetrahedra         = 0;
  this->NumberOfScalars            = 0;
  this->GPUDataStructures          = true;
  this->TransferFunctionSize       = 256;
  this->TransferFunction           = new float[this->TransferFunctionSize*4];  
  this->UnitDistance               = 1.0;  
  this->Initialized                = 0;
  this->FrameNumber                = 0;
  this->TotalRenderTime            = 0.0;
  this->LastVolume                 = NULL;  
  this->VisibilitySort             = vtkCellCenterDepthSort::New();

#ifdef HAPT_USE_AUX_SCALARS
  this->UseAuxScalars              = false;
  this->TetraVertPosition[4]       = NULL;
  this->AuxTransferFunction        = new float[this->TransferFunctionSize];
  this->AuxTransferFunctionType    = AUX_TRANSFER_FUNCTION_OPACITY_MUL;
  this->AuxUnitDistance            = 1.0;
  this->AuxScalars                 = 0;
  this->AuxScalarRange[0]          = 0.0;
  this->AuxScalarRange[1]          = 1.0;  
#endif
}

/*---------------------------------------------------------------------------*/
HAPTVolumeMapper::~HAPTVolumeMapper()
{
  CALL_TRACER_MACRO;
  if (this->Scalars) { delete [] this->Scalars; }
  if (this->TetraVertPosition[0]) { delete [] this->TetraVertPosition[0]; }
  if (this->TetraVertPosition[1]) { delete [] this->TetraVertPosition[1]; }
  if (this->TetraVertPosition[2]) { delete [] this->TetraVertPosition[2]; }
  if (this->TetraVertPosition[3]) { delete [] this->TetraVertPosition[3]; }
  if (this->TransferFunction) { delete [] this->TransferFunction; }
  this->SetVisibilitySort(NULL);

#ifdef HAPT_USE_AUX_SCALARS
  if (this->TetraVertPosition[4]) { delete [] this->TetraVertPosition[4]; }
  if (this->AuxTransferFunction){ delete [] this->AuxTransferFunction; };
#endif
}

/*---------------------------------------------------------------------------*/
bool HAPTVolumeMapper::InitializePrimitives()
{
  CALL_TRACER_MACRO;

  if (this->Grid_MTime >= this->GetInput()->GetMTime() &&
      (this->Grid_MTime >= this->MTime))
    return false;

  this->Grid_MTime.Modified();

  // Check for valid input
  vtkUnstructuredGrid *ugrid = this->GetInput();
  vtkIdType numTetrahedra = ugrid->GetNumberOfCells();
  if (!numTetrahedra)
  {
    this->InitializationError = HAPTVolumeMapper::NO_CELLS;
    return false;
  }
  bool tetrahedra = true;
  for (vtkIdType i = 0; i < numTetrahedra; i++)
  {
    vtkCell *c = ugrid->GetCell(i);
    if (c->GetNumberOfPoints() != 4)
    {
      tetrahedra = false;
    }
  }
  if (!tetrahedra)
  {
    this->InitializationError = HAPTVolumeMapper::NON_TETRAHEDRA;
    return false;
  }

  // Put away the vertices in a CPU buffer
  this->NumberOfTetrahedra = numTetrahedra;
  this->NumberOfVertices = ugrid->GetNumberOfPoints();

#ifdef HAPT_USE_AUX_SCALARS
  const int numbufs = 5;
#else
  const int numbufs = 4;
#endif
  for (int j = 0; j < numbufs; ++j)
  {
    if (this->TetraVertPosition[j]) {delete [] this->TetraVertPosition[j];}
    this->TetraVertPosition[j] = new float[numTetrahedra * 4];
  }

  this->MaxCellSize = ugrid->GetMaxCellSize();

  for (int i = 0; i < numTetrahedra; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      double pt[3];

      vtkIdType ptid =  ugrid->GetCell(i)->GetPointId(j);

      ugrid->GetPoint(ptid,pt);

      this->TetraVertPosition[j][i*4 + 0] = pt[0];
      this->TetraVertPosition[j][i*4 + 1] = pt[1];
      this->TetraVertPosition[j][i*4 + 2] = pt[2];
//      this->TetraVertPosition[j][i*4 + 3] = ptid;
    }
  }

  return true;
}

/*---------------------------------------------------------------------------*/
bool HAPTVolumeMapper::InitializeScalars(vtkVolume * vol)
{
  // Check to see if we need to update the scalars
  if (this->Scalars_MTime >= this->GetInput()->GetMTime() &&
      (this->Scalars_MTime >= this->MTime))
    return false;

  CALL_TRACER_MACRO;
  vtkUnstructuredGrid *ugrid = this->GetInput();

  // Fill up scalars
  int UsingCellColor;
  vtkDataArray *scalarData = this->GetScalars(ugrid, this->ScalarMode,
                                              this->ArrayAccessMode,
                                              this->ArrayId,
                                              this->ArrayName,
                                              UsingCellColor);

  this->Scalars_MTime.Modified();

  if (!scalarData)
  {
    this->InitializationError = HAPTVolumeMapper::NO_SCALARS;
    return false;
  }
  if (UsingCellColor)
  {
    this->InitializationError = HAPTVolumeMapper::CELL_DATA;
    return false;
  }
#ifdef HAPT_USE_AUX_SCALARS
  if (this->UseAuxScalars && scalarData->GetNumberOfComponents() < 2)
  {
    this->InitializationError = HAPTVolumeMapper::NO_AUX_SCALARS;
  }
#endif


  this->NumberOfScalars = scalarData->GetNumberOfTuples();

  if(this->Scalars) delete []this->Scalars;

  this->Scalars    = new float[this->NumberOfScalars];

  for (unsigned int i = 0; i < this->NumberOfScalars; i++)
    this->Scalars[i]    = (float)scalarData->GetTuple(i)[0];

  // Normalize scalars
  if (this->NumberOfScalars)
  {
    scalarData->GetRange(this->ScalarRange,0);
    double diff  = this->ScalarRange[1]-this->ScalarRange[0];

    for (unsigned int i = 0; i < this->NumberOfScalars; i++)
      this->Scalars[i]   = (this->Scalars[i] - this->ScalarRange[0])/diff;
  }

  // Copy the scalar value to the tets 4th coord
  // .. For HAPT   compat.. remove it later
  for (int i = 0; i < NumberOfTetrahedra; ++i)
    for (int j = 0; j < 4; ++j)
    {
      vtkIdType ptid =  ugrid->GetCell(i)->GetPointId(j);
      this->TetraVertPosition[j][i*4 + 3] = this->Scalars[ptid];
    }


#ifdef HAPT_USE_AUX_SCALARS
  if(this->UseAuxScalars)
  {
    if(this->AuxScalars) delete []this->AuxScalars;

    this->AuxScalars    = new float[this->NumberOfScalars];

    for (unsigned int i = 0; i < this->NumberOfScalars; i++)
      this->AuxScalars[i]    = (float)scalarData->GetTuple(i)[1];

    // Normalize Aux scalars
    if (this->NumberOfScalars)
    {
      scalarData->GetRange(this->AuxScalarRange,1);
      double diff  = this->AuxScalarRange[1]-this->AuxScalarRange[0];

      for (unsigned int i = 0; i < this->NumberOfScalars; i++)
        this->AuxScalars[i]   = (this->AuxScalars[i] - this->AuxScalarRange[0])/diff;
    }

    // Copy the scalar value to the tets 4th coord
    // .. For HAPT   compat.. remove it later
    for (int i = 0; i < NumberOfTetrahedra; ++i)
      for (int j = 0; j < 4; ++j)
      {
        vtkIdType ptid =  ugrid->GetCell(i)->GetPointId(j);
        this->TetraVertPosition[4][i*4 + j] = this->AuxScalars[ptid];
      }
  }
#endif

  return true;
}

/*---------------------------------------------------------------------------*/
bool HAPTVolumeMapper::InitializeTransferFunction(vtkVolume *vol)
{
  CALL_TRACER_MACRO;

  vtkVolumeProperty *prop          = vol->GetProperty();
  vtkColorTransferFunction * color = NULL;
  vtkPiecewiseFunction     * gray  = NULL;
  vtkPiecewiseFunction     * alpha = prop->GetScalarOpacity();
#ifdef HAPT_USE_AUX_SCALARS
  vtkPiecewiseFunction * aux_alpha = prop->GetScalarOpacity(1);
#endif

  if(prop->GetColorChannels() == 1)
    gray = prop->GetGrayTransferFunction();
  else
    color = prop->GetRGBTransferFunction();

  // Check to see if we need to update the lookup table
  if ((!color || this->TF_MTime >= color->GetMTime())&&
      (!gray  || this->TF_MTime >= gray->GetMTime())&&
      (this->TF_MTime >=    alpha->GetMTime())&&
#ifdef HAPT_USE_AUX_SCALARS
      (this->TF_MTime >= aux_alpha->GetMTime())&&
      (this->AuxUnitDistance == prop->GetScalarOpacityUnitDistance(1))&&
#endif
      (this->UnitDistance    == prop->GetScalarOpacityUnitDistance())
      )
    return false;

  this->TF_MTime.Modified();

  double x = this->ScalarRange[0];
  double dx = 1.0/((float)this->TransferFunctionSize-1.0)*
      (this->ScalarRange[1]-this->ScalarRange[0]);

  this->UnitDistance = prop->GetScalarOpacityUnitDistance();

  double c[3], a ,g ;

  if(gray)
  {
    for (int i = 0; i < this->TransferFunctionSize; i++)
    {
      g = gray->GetValue(x);
      a = alpha->GetValue(x);

      this->TransferFunction[i*4+0] = g;
      this->TransferFunction[i*4+1] = g;
      this->TransferFunction[i*4+2] = g;
      this->TransferFunction[i*4+3] = a / this->UnitDistance;

      x+=dx;
    }
  }
  else
  {
    for (int i = 0; i < this->TransferFunctionSize; i++)
    {
      color->GetColor(x,c);
      a = alpha->GetValue(x);

      this->TransferFunction[i*4+0] = c[0];
      this->TransferFunction[i*4+1] = c[1];
      this->TransferFunction[i*4+2] = c[2];
      this->TransferFunction[i*4+3] = a / this->UnitDistance;

      x+=dx;
    }
  }

#ifdef HAPT_USE_AUX_SCALARS
  x = this->AuxScalarRange[0];
  dx = 1.0/((float)this->TransferFunctionSize-1.0)*
      (this->AuxScalarRange[1]-this->AuxScalarRange[0]);

  this->AuxUnitDistance = prop->GetScalarOpacityUnitDistance(1);

  for (int i = 0; i < this->TransferFunctionSize; i++)
  {
    a = aux_alpha->GetValue(x);
    a = (a+1.0)/3.0; // change from [-1,2] to [0,1]
    this->AuxTransferFunction[i] = a / this->AuxUnitDistance;
    x+=dx;
  }
#endif
  return true;
}

/*---------------------------------------------------------------------------*/
bool HAPTVolumeMapper::CheckInitializationError()
{
  CALL_TRACER_MACRO;
  if (this->InitializationError ==
      HAPTVolumeMapper::NO_INIT_ERROR)
  {
    return false;
  }

  if (this->InitializationError ==
      HAPTVolumeMapper::NON_TETRAHEDRA)
  {
    vtkErrorMacro(<< "Non-tetrahedral cells not supported!");
  }
  else if (this->InitializationError ==
           HAPTVolumeMapper::UNSUPPORTED_EXTENSIONS)
  {
    vtkErrorMacro(<< "Required OpenGL extensions not supported!" );
  }
  else if (this->InitializationError ==
           HAPTVolumeMapper::NO_SCALARS)
  {
    vtkErrorMacro(<< "Can't use HAPT without scalars!");
  }
#ifdef HAPT_USE_AUX_SCALARS
  else if (this->InitializationError ==
           HAPTVolumeMapper::NO_AUX_SCALARS)
  {
    vtkErrorMacro(<< "This HAPT version needs auxilliary Scalars!");
  }
#endif
  else if (this->InitializationError ==
           HAPTVolumeMapper::CELL_DATA)
  {
    vtkErrorMacro(<< "Can't use HAPT with cell data!");
  }
  else if (this->InitializationError ==
           HAPTVolumeMapper::NO_CELLS)
  {
    vtkErrorMacro(<< "No Cells!");
  }
  return true;
}

/*---------------------------------------------------------------------------*/
void HAPTVolumeMapper::PrintSelf(ostream& os, vtkIndent indent)
{
  CALL_TRACER_MACRO;
  os << indent << "Initialized " << this->Initialized << endl;
  os << indent << "Unit Distance: " << this->UnitDistance << endl;
  os << indent << "TransferFunction Size: " << this->TransferFunctionSize << endl;
  os << indent << "GPU Data Structures: " << this->GPUDataStructures << endl;
  os << indent << "VisibilitySort: " << this->VisibilitySort << endl;

  this->Superclass::PrintSelf(os,indent);
}

/*---------------------------------------------------------------------------*/

void HAPTVolumeMapper::ReportReferences(vtkGarbageCollector *collector)
{
  this->Superclass::ReportReferences(collector);

  vtkGarbageCollectorReport(collector, this->VisibilitySort, "VisibilitySort");
}

/*****************************************************************************/
