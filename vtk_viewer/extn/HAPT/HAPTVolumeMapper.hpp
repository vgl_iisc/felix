/*=========================================================================

Program:   Visualization Toolkit
Module:    HAPTVolumeMapper.h

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/



#ifndef __HAPTVolumeMapper_h
#define __HAPTVolumeMapper_h

#include "vtkUnstructuredGridVolumeMapper.h"

class vtkUnstructuredGrid;
class vtkVisibilitySort;

#define HAPT_USE_AUX_SCALARS
/// \brief HAPT Volume mapper base class
class HAPTVolumeMapper : public vtkUnstructuredGridVolumeMapper
{
public:
  static HAPTVolumeMapper *New();

  vtkTypeMacro(HAPTVolumeMapper,vtkUnstructuredGridVolumeMapper);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  virtual bool SupportedByHardware(vtkRenderer *vtkNotUsed(r)){return false; }

  /// \brief Set whether or not the data structures should be stored on the GPU
  /// for better peformance.
  virtual void SetGPUDataStructures(bool) = 0;

  /// \brief Get whether or not the data structures are being stored on the GPU
  /// for better peformance.
  vtkGetMacro(GPUDataStructures, bool);

  /// \brief Set visibility sorter. Default by depth of cellids
  virtual void SetVisibilitySort(vtkVisibilitySort *sort);

  /// \brief Set visibility sorter.
  vtkGetObjectMacro(VisibilitySort, vtkVisibilitySort);


#ifdef HAPT_USE_AUX_SCALARS
  /// \brief Set whether or not to use the auxilliary scalar to influence the
  /// Volume rendering.
  vtkSetMacro(UseAuxScalars,bool);

  /// \brief Get whether or not the auxilliary scalars influence the
  /// Volume rendering.
  vtkGetMacro(UseAuxScalars,bool);

  /// \brief
  /// Set/get how the auxilliary scalars influence the color and opacity
  /// of the primary scalar .
  ///
  /// i.e color for a sample with scalar 's' and aux scalar 'a' is given
  /// by Tf(s) + Tf_aux(a). The blending operation is configureable as
  /// detailed below.
  ///
  /// Available Options:\n
  /// AUX_TRANSFER_FUNCTION_OPACITY_ADD: the aux's opacity will be added
  ///                                    to primary scalar's opacity\n
  /// AUX_TRANSFER_FUNCTION_OPACITY_MUL: the aux's opacity will be multiplied
  ///                                    to primary scalar's opacity\n

  vtkSetClampMacro(AuxTransferFunctionType,int,0,__AUX_TRANSFER_FUNCTION_CT-1);
  vtkGetMacro(AuxTransferFunctionType,int);

  enum {
    AUX_TRANSFER_FUNCTION_OPACITY_MUL=0,
    AUX_TRANSFER_FUNCTION_OPACITY_ADD,
    __AUX_TRANSFER_FUNCTION_CT,// Not a real option
  };

#endif
  
protected:  
  HAPTVolumeMapper();
  ~HAPTVolumeMapper();

//BTX
  bool InitializePrimitives();
  bool InitializeScalars(vtkVolume * vol);
  bool InitializeTransferFunction(vtkVolume *vol);

  bool CheckInitializationError();

  /// \brief The visibility sort will probably make a reference loop by
  /// holding a reference to the input.
  virtual void ReportReferences(vtkGarbageCollector *collector);

  enum
  {
    NO_INIT_ERROR=0,
    NON_TETRAHEDRA=1,
    UNSUPPORTED_EXTENSIONS=2,
    NO_SCALARS=3,
    CELL_DATA=4,
    NO_CELLS=5,
#ifdef HAPT_USE_AUX_SCALARS
    NO_AUX_SCALARS=6,
#endif
  };

  // Mesh
  float *Scalars;
  double ScalarRange[2];

#ifdef HAPT_USE_AUX_SCALARS
  float *AuxScalars;
  double AuxScalarRange[2];
#endif

#ifndef HAPT_USE_AUX_SCALARS
  float        *TetraVertPosition[4];
#else
  float        *TetraVertPosition[5];
#endif
  unsigned int  NumberOfVertices;
  unsigned int  NumberOfTetrahedra;
  unsigned int  NumberOfScalars;

  vtkVisibilitySort *VisibilitySort;
  float              MaxCellSize;

  // To GPU or to not GPU
  bool GPUDataStructures;

  // Lookup Tables
  float *TransferFunction;
  int TransferFunctionSize;
  float UnitDistance;

#ifdef HAPT_USE_AUX_SCALARS
  float *AuxTransferFunction;
  float AuxUnitDistance;
  bool  UseAuxScalars;
  int   AuxTransferFunctionType;
#endif

  // State and Timing Stats
  bool Initialized;
  int InitializationError;
  int FrameNumber;
  float TotalRenderTime;
  vtkTimeStamp TF_MTime;
  vtkTimeStamp Grid_MTime;
  vtkTimeStamp Scalars_MTime;
  vtkVolume *LastVolume;  
//ETX

private:
  HAPTVolumeMapper(const HAPTVolumeMapper&);  // Not implemented.
  void operator=(const HAPTVolumeMapper&);  // Not implemented.
};
#endif
