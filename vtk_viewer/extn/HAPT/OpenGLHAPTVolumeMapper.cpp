/*=========================================================================

Program:   Visualization Toolkit
Module:    vtkOpenGLHAPTVolumeMapper.cxx

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/* Copyright 2005, 2006 by University of Utah. */

#include "OpenGLHAPTVolumeMapper.hpp"

#include "vtkCamera.h"
#include "vtkColorTransferFunction.h"
#include "vtkgl.h"
#include "vtkInformation.h"
#include "vtkObjectFactory.h"
#include "vtkOpenGL.h"
#include "vtkOpenGLExtensionManager.h"
#include "vtkPiecewiseFunction.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkTimerLog.h"
#include "vtkTransform.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridPartialPreIntegration.h"
#include "vtkVolumeProperty.h"
#include "vtkOpenGLRenderWindow.h"
#include "vtkShaderProgram2.h"
#include "vtkShader2.h"
#include "vtkShader2Collection.h"
#include "vtkUniformVariables.h"
#include "vtkVisibilitySort.h"
#include "vtkIdTypeArray.h"

vtkStandardNewMacro(OpenGLHAPTVolumeMapper);

extern const char *HAPTVolumeMapper_VS;
extern const char *HAPTVolumeMapper_GS;
extern const char *HAPTVolumeMapper_FS;

#include "HAPTVolumeMapper_tables.hpp"

#include "call_tracer.hpp"

const char * const gAuxTransferFunctionType_str[]=
{
  "AUX_TRANSFER_FUNCTION_OPACITY_MUL",
  "AUX_TRANSFER_FUNCTION_OPACITY_ADD",
};

const int HAPT_VisibilitySort_MaxCells = 1024;

//----------------------------------------------------------------------------
// return the correct type of UnstructuredGridVolumeMapper 
OpenGLHAPTVolumeMapper::OpenGLHAPTVolumeMapper()
{
  CALL_TRACER_MACRO;

  this->Initialized                = false;

  this->FramebufferObjectSize      = 0;
  this->FramebufferObject          = 0;
  this->RenderWindow               = 0;
  this->FramebufferTexture         = 0;
  this->DepthRenderBuffer          = 0;

  this->UseGPUDataStructures_changed = false;
  this->TetraVertPositionBufObj[0] = 0;
  this->TetraVertPositionBufObj[1] = 0;
  this->TetraVertPositionBufObj[2] = 0;
  this->TetraVertPositionBufObj[3] = 0;

  this->PsiTableTexture            = 0;
  this->PsiTableSize               = 0;
  this->TransferFunctionTexture    = 0;
  this->OrderTableTexture          = 0;
  this->TriangleFanOrderTableTexture  = 0;

  this->Shader                     = NULL;

#ifdef HAPT_USE_AUX_SCALARS
  this->TetraVertPositionBufObj[4] = 0;
  this->AuxTransferFunctionTexture = 0;
  this->AuxShader                  = NULL;
  this->AuxShaderTransferFunctionType  = this->AuxTransferFunctionType;
#endif
}

//----------------------------------------------------------------------------
// return the correct type of UnstructuredGridVolumeMapper 
OpenGLHAPTVolumeMapper::~OpenGLHAPTVolumeMapper()
{
  CALL_TRACER_MACRO;
  ReleaseShaders();
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::CheckOpenGLError(const char * str)
{
  CALL_TRACER_MACRO;
  int err = glGetError(); (void)str;
  if ( err != GL_NO_ERROR && this->GetDebug() )
    vtkDebugMacro( << "OpenGL Error: " << str );
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::ReleaseGraphicsResources(vtkWindow *renWin)
{
  CALL_TRACER_MACRO;
  this->Superclass::ReleaseGraphicsResources(renWin);

  if (this->Initialized)
  {
    ReleaseLookupTables();
    ReleaseGPUDataStructures();
    ReleaseFramebufferObject();
//    ReleaseShaders();
    // If ReleaseShaders called here it goes crazy when it is reinitialized
    // by Render() until a complete redraw of the window is done
    // Some internal state is being violated..
    // Leave it for now..
  }

  this->Initialized                = false;
  this->RenderWindow               = 0;
}


//----------------------------------------------------------------------------
// Change GPU data structures state
void OpenGLHAPTVolumeMapper::SetGPUDataStructures(bool gpu)
{
  CALL_TRACER_MACRO;
  UseGPUDataStructures_changed = (gpu != this->GPUDataStructures);
  this->GPUDataStructures = gpu;
}

//----------------------------------------------------------------------------
// Store data structures on GPU if possible
void OpenGLHAPTVolumeMapper::InitializeGPUDataStructures()
{
  CALL_TRACER_MACRO;
  if(this->GPUDataStructures)
  {
#ifndef HAPT_USE_AUX_SCALARS
  const int numbufs = 4;
#else
  const int numbufs = 5;
#endif
    if(this->TetraVertPositionBufObj[0] == 0)
      vtkgl::GenBuffers(numbufs, this->TetraVertPositionBufObj);

    for (GLuint j = 0; j < numbufs; ++j)
    {
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, this->TetraVertPositionBufObj[j]);

      vtkgl::BufferData
          (vtkgl::ARRAY_BUFFER, this->NumberOfTetrahedra * 4 *sizeof(GLfloat),
           this->TetraVertPosition[j],vtkgl::STATIC_DRAW);

    }
  }
  else
  {
    ReleaseGPUDataStructures();
  }
}

//----------------------------------------------------------------------------
// Store data structures on GPU if possible
void OpenGLHAPTVolumeMapper::ReleaseGPUDataStructures()
{
  CALL_TRACER_MACRO;
  vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, 0);

  if(this->TetraVertPositionBufObj[0])
    vtkgl::DeleteBuffers(1,&this->TetraVertPositionBufObj[0]);
  this->TetraVertPositionBufObj[0] = 0;

  if(this->TetraVertPositionBufObj[1])
    vtkgl::DeleteBuffers(1,&this->TetraVertPositionBufObj[1]);
  this->TetraVertPositionBufObj[1] = 0;

  if(this->TetraVertPositionBufObj[2])
    vtkgl::DeleteBuffers(1,&this->TetraVertPositionBufObj[2]);
  this->TetraVertPositionBufObj[2] = 0;

  if(this->TetraVertPositionBufObj[3])
    vtkgl::DeleteBuffers(1,&this->TetraVertPositionBufObj[3]);
  this->TetraVertPositionBufObj[3] = 0;

#ifdef HAPT_USE_AUX_SCALARS
  if(this->TetraVertPositionBufObj[4])
    vtkgl::DeleteBuffers(1,&this->TetraVertPositionBufObj[4]);
  this->TetraVertPositionBufObj[4] = 0;
#endif

}

//----------------------------------------------------------------------------
// Vertex and Fragment shaders
void OpenGLHAPTVolumeMapper::InitializeShaders()
{
  CALL_TRACER_MACRO;
  if(!this->Shader)
  {
    this->Shader=vtkShaderProgram2::New();
    this->Shader->SetContext(dynamic_cast<vtkOpenGLRenderWindow *>
                             (RenderWindow.GetPointer()));

    vtkShader2Collection *shaders=this->Shader->GetShaders();

    vtkShader2 *vs=vtkShader2::New();
    vs->SetType(VTK_SHADER_TYPE_VERTEX);
    vs->SetContext(this->Shader->GetContext());
    vs->SetSourceCode(HAPTVolumeMapper_VS);
    shaders->AddItem(vs);
    vs->Delete();

    vtkShader2 *gs=vtkShader2::New();
    gs->SetType(VTK_SHADER_TYPE_GEOMETRY);
    gs->SetContext(this->Shader->GetContext());
    gs->SetSourceCode(HAPTVolumeMapper_GS);
    shaders->AddItem(gs);
    gs->Delete();

    vtkShader2 *fs=vtkShader2::New();
    fs->SetType(VTK_SHADER_TYPE_FRAGMENT);
    fs->SetContext(this->Shader->GetContext());
    fs->SetSourceCode(HAPTVolumeMapper_FS);
    shaders->AddItem(fs);
    fs->Delete();

    this->Shader->SetGeometryVerticesOut(8);
    this->Shader->SetGeometryTypeIn(VTK_GEOMETRY_SHADER_IN_TYPE_POINTS);
    this->Shader->SetGeometryTypeOut(
          VTK_GEOMETRY_SHADER_OUT_TYPE_TRIANGLE_STRIP);

    this->Shader->Build();

    this->Shader->Use();

    int uiValues[] = {0,1,2,3,4};
    this->Shader->GetUniformVariables()->SetUniformi
        ("orderTableTex",1, uiValues+0);
    this->Shader->GetUniformVariables()->SetUniformi
        ("tfanOrderTableTex",1, uiValues+1);
    this->Shader->GetUniformVariables()->SetUniformi
        ("psiGammaTableTex",1, uiValues+2);
    this->Shader->GetUniformVariables()->SetUniformi
        ("tfTex",1, uiValues+3);
    this->Shader->GetUniformVariables()->SetUniformi
        ("fboRenderTex",1, uiValues+4);

    float ufValues[] = {this->PsiTableSize,this->MaxCellSize,1.0};

    this->Shader->GetUniformVariables()->SetUniformf
        ("preIntTexSize",1,ufValues+0);
    this->Shader->GetUniformVariables()->SetUniformf
        ("maxEdgeLength",1,ufValues+1);
    this->Shader->GetUniformVariables()->SetUniformf
        ("brightness",1,ufValues+2);

    this->Shader->Restore();
  }

#ifdef HAPT_USE_AUX_SCALARS

  if(this->AuxShaderTransferFunctionType != this->AuxTransferFunctionType )
  {
    if(this->AuxShader)
    {
      this->AuxShader->ReleaseGraphicsResources();
      this->AuxShader->Delete();
      this->AuxShader = NULL;
    }
    this->AuxShaderTransferFunctionType = this->AuxTransferFunctionType;
  }

  if(!this->AuxShader)
  {
    this->AuxShader=vtkShaderProgram2::New();
    this->AuxShader->SetContext(dynamic_cast<vtkOpenGLRenderWindow *>
                             (RenderWindow.GetPointer()));

    vtkShader2Collection *shaders=this->AuxShader->GetShaders();

    vtkStdString vscode("#define HAPT_USE_AUX_SCALARS\n");
    vtkStdString gscode("#define HAPT_USE_AUX_SCALARS\n");
    vtkStdString fscode("#define HAPT_USE_AUX_SCALARS\n");

    fscode += "#define HAPT_";
    fscode += gAuxTransferFunctionType_str[this->AuxShaderTransferFunctionType];
    fscode += "\n";

    vscode += HAPTVolumeMapper_VS;
    gscode += HAPTVolumeMapper_GS;
    fscode += HAPTVolumeMapper_FS;

    vtkShader2 *vs=vtkShader2::New();
    vs->SetType(VTK_SHADER_TYPE_VERTEX);
    vs->SetContext(this->AuxShader->GetContext());
    vs->SetSourceCode(vscode);
    shaders->AddItem(vs);
    vs->Delete();

    vtkShader2 *gs=vtkShader2::New();
    gs->SetType(VTK_SHADER_TYPE_GEOMETRY);
    gs->SetContext(this->AuxShader->GetContext());
    gs->SetSourceCode(gscode);
    shaders->AddItem(gs);
    gs->Delete();

    vtkShader2 *fs=vtkShader2::New();
    fs->SetType(VTK_SHADER_TYPE_FRAGMENT);
    fs->SetContext(this->AuxShader->GetContext());
    fs->SetSourceCode(fscode);
    shaders->AddItem(fs);
    fs->Delete();

    this->AuxShader->SetGeometryVerticesOut(8);
    this->AuxShader->SetGeometryTypeIn(VTK_GEOMETRY_SHADER_IN_TYPE_POINTS);
    this->AuxShader->SetGeometryTypeOut(
          VTK_GEOMETRY_SHADER_OUT_TYPE_TRIANGLE_STRIP);

    this->AuxShader->Build();

    this->AuxShader->Use();

    int uiValues[] = {0,1,2,3,4,5};
    this->AuxShader->GetUniformVariables()->SetUniformi
        ("orderTableTex",1, uiValues+0);
    this->AuxShader->GetUniformVariables()->SetUniformi
        ("tfanOrderTableTex",1, uiValues+1);
    this->AuxShader->GetUniformVariables()->SetUniformi
        ("psiGammaTableTex",1, uiValues+2);
    this->AuxShader->GetUniformVariables()->SetUniformi
        ("tfTex",1, uiValues+3);
    this->AuxShader->GetUniformVariables()->SetUniformi
        ("fboRenderTex",1, uiValues+4);
    this->AuxShader->GetUniformVariables()->SetUniformi
        ("auxTfTex",1, uiValues+5);

    float ufValues[] = {this->PsiTableSize,this->MaxCellSize,1.0};

    this->AuxShader->GetUniformVariables()->SetUniformf
        ("preIntTexSize",1,ufValues+0);
    this->AuxShader->GetUniformVariables()->SetUniformf
        ("maxEdgeLength",1,ufValues+1);
    this->AuxShader->GetUniformVariables()->SetUniformf
        ("brightness",1,ufValues+2);

    this->AuxShader->Restore();
  }
#endif
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::ReleaseShaders()
{
  CALL_TRACER_MACRO;

  if(this->Shader)
  {
    this->Shader->ReleaseGraphicsResources();
    this->Shader->Delete();
  }

  this->Shader     = NULL;

#ifdef HAPT_USE_AUX_SCALARS
  if(this->AuxShader)
  {
    this->AuxShader->ReleaseGraphicsResources();
    this->AuxShader->Delete();
  }

  this->AuxShader     = NULL;
#endif
}

//----------------------------------------------------------------------------
// Build the lookup tables used for partial pre-integration
void OpenGLHAPTVolumeMapper::InitializeLookupTables(vtkVolume *vol)
{
  CALL_TRACER_MACRO;  

  // Order Table Texture
  if(!this->OrderTableTexture)
  {
    GLint *orderTableBuffer;
    orderTableBuffer = new GLint[81*4];

    for (GLuint i = 0; i < 81; ++i)
      for (GLuint j = 0; j < 4; ++j)
        orderTableBuffer[i*4 + j] = (GLint)(order_table[i][j]);


    glGenTextures(1, &this->OrderTableTexture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_1D,  this->OrderTableTexture);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage1D(GL_TEXTURE_1D, 0, vtkgl::RGBA16I_EXT, 81, 0,
                 vtkgl::RGBA_INTEGER_EXT, GL_INT, orderTableBuffer);

    delete [] orderTableBuffer;
  }

  // Triangle Fan/Strip Order Table Texture
  if(! this->TriangleFanOrderTableTexture)
  {
    GLint *tfanOrderTableBuffer;
    tfanOrderTableBuffer = new GLint[81*4];

    GLint tfan, k;

    for (GLuint i = 0; i < 81; ++i) {

      k = 0;

      for (GLuint j = 0; j < 5; ++j) {

        tfan = triangle_fan_order_table[i][j];
        if( tfan == -1 ) continue;
        if( k == 4 ) continue;
        tfanOrderTableBuffer[i*4 + k] = tfan;
        ++k;

      }
    }

    glGenTextures(1, &this->TriangleFanOrderTableTexture);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_1D,  this->TriangleFanOrderTableTexture);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage1D(GL_TEXTURE_1D, 0, vtkgl::RGBA16I_EXT, 81, 0,
                 vtkgl::RGBA_INTEGER_EXT, GL_INT, tfanOrderTableBuffer);

    delete [] tfanOrderTableBuffer;
  }

  if (!this->PsiTableTexture)
  {
    vtkUnstructuredGridPartialPreIntegration *ppi =
        vtkUnstructuredGridPartialPreIntegration::New();
    ppi->BuildPsiTable();

    float *psiTable = ppi->GetPsiTable(this->PsiTableSize);

    // Create a 2D texture for the PSI lookup table
    glGenTextures(1, reinterpret_cast<GLuint *>(&this->PsiTableTexture));
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, this->PsiTableTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, vtkgl::CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, vtkgl::CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, this->PsiTableSize,
                 this->PsiTableSize,0, GL_RED, GL_FLOAT, psiTable);
    ppi->Delete();
  }
  // Create a 1D texture for transfer function look up
  if(this->TransferFunctionTexture)
    glDeleteTextures(1, reinterpret_cast<GLuint *>(&this->TransferFunctionTexture));

  glGenTextures(1, reinterpret_cast<GLuint *>(&this->TransferFunctionTexture));
  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_1D, this->TransferFunctionTexture);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, vtkgl::CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, this->TransferFunctionSize,0,
               GL_RGBA,GL_FLOAT, this->TransferFunction);


#ifdef HAPT_USE_AUX_SCALARS
  // Create a 1D texture for transfer function look up
  if(this->AuxTransferFunctionTexture)
    glDeleteTextures(1, reinterpret_cast<GLuint *>(&this->AuxTransferFunctionTexture));

  glGenTextures(1, reinterpret_cast<GLuint *>(&this->AuxTransferFunctionTexture));
  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_1D, this->AuxTransferFunctionTexture);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, vtkgl::CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RED, this->TransferFunctionSize,0,
               GL_RED,GL_FLOAT, this->AuxTransferFunction);
#endif

}

//----------------------------------------------------------------------------
// Release the textures when not needed
void OpenGLHAPTVolumeMapper::ReleaseLookupTables()
{
  CALL_TRACER_MACRO;
  if(this->OrderTableTexture)
    glDeleteTextures(1,reinterpret_cast<GLuint *>(&this->OrderTableTexture));
  this->OrderTableTexture             = 0;

  if(this->TriangleFanOrderTableTexture)
    glDeleteTextures(1,reinterpret_cast<GLuint *>(&this->TriangleFanOrderTableTexture));
  this->TriangleFanOrderTableTexture  = 0;

  if(this->PsiTableTexture)
    glDeleteTextures(1,reinterpret_cast<GLuint *>(&this->PsiTableTexture));
  this->PsiTableTexture               = 0;

  if(this->TransferFunctionTexture)
    glDeleteTextures(1,reinterpret_cast<GLuint *>(&this->TransferFunctionTexture));
  this->TransferFunctionTexture       = 0;

#ifdef HAPT_USE_AUX_SCALARS
  if(this->AuxTransferFunctionTexture)
    glDeleteTextures(1,reinterpret_cast<GLuint *>(&this->AuxTransferFunctionTexture));
  this->AuxTransferFunctionTexture    = 0;
#endif
}


//----------------------------------------------------------------------------
// Initialize FBO and attach color and depth textures.


void OpenGLHAPTVolumeMapper::InitializeFramebufferObject()
{
  CALL_TRACER_MACRO;
  GLint maxRB;
  glGetIntegerv(vtkgl::MAX_RENDERBUFFER_SIZE_EXT, &maxRB);
  int texSize = (maxRB > 1024)? 1024 : maxRB;

  // Create FBO
  if (!this->FramebufferObject)
    vtkgl::GenFramebuffersEXT
        (1,reinterpret_cast<GLuint *>(&this->FramebufferObject));

  if(this->FramebufferTexture)
    glDeleteTextures
        (1,reinterpret_cast<GLuint *>(&this->FramebufferTexture));

  if(this->DepthRenderBuffer)
    vtkgl::DeleteRenderbuffersEXT
        (1,reinterpret_cast<GLuint *>(&this->DepthRenderBuffer));

  this->CheckOpenGLError("creating FBO");

  // Create FBO textures
  glGenTextures(1, reinterpret_cast<GLuint *>(&this->FramebufferTexture));
  glBindTexture(GL_TEXTURE_2D, this->FramebufferTexture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, vtkgl::RGBA32F_ARB, texSize, texSize, 0,
               GL_RGBA, GL_FLOAT, 0);

  this->CheckOpenGLError("creating fbo textures");

  // Bind framebuffer object
  GLint savedFrameBuffer;
  glGetIntegerv(vtkgl::FRAMEBUFFER_BINDING_EXT,&savedFrameBuffer);
  vtkgl::BindFramebufferEXT(vtkgl::FRAMEBUFFER_EXT, this->FramebufferObject);
  this->CheckOpenGLError("binding FBO");
  
  // Generate depth buffer texture for framebuffer
  vtkgl::GenRenderbuffersEXT(1, reinterpret_cast<GLuint *>
                             (&this->DepthRenderBuffer));

  // Attach texture to framebuffer color buffer
  vtkgl::FramebufferTexture2DEXT( vtkgl::FRAMEBUFFER_EXT, 
                                 vtkgl::COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D,
                                 this->FramebufferTexture, 0 );

  // Attach depth RenderBuffer to framebuffer
  vtkgl::BindRenderbufferEXT(vtkgl::RENDERBUFFER_EXT, this->DepthRenderBuffer);
  vtkgl::RenderbufferStorageEXT
      (vtkgl::RENDERBUFFER_EXT,vtkgl::DEPTH_COMPONENT24,texSize, texSize);
  vtkgl::FramebufferRenderbufferEXT
      (vtkgl::FRAMEBUFFER_EXT,vtkgl::DEPTH_ATTACHMENT_EXT,
       vtkgl::RENDERBUFFER_EXT,this->DepthRenderBuffer);

  this->CheckOpenGLError("attach textures to FBO");

  // Validate FBO after attaching textures
  if (vtkgl::CheckFramebufferStatusEXT(vtkgl::FRAMEBUFFER_EXT) != 
      vtkgl::FRAMEBUFFER_COMPLETE_EXT && this->GetDebug())
  {
    vtkDebugMacro( << "FBO incomplete" );
  }

  // Disable FBO rendering
  vtkgl::BindFramebufferEXT(vtkgl::FRAMEBUFFER_EXT,savedFrameBuffer);
  
  this->FramebufferObjectSize = texSize;
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::ReleaseFramebufferObject()
{
  CALL_TRACER_MACRO;
  if(this->FramebufferTexture)
    glDeleteTextures
        (1, reinterpret_cast<GLuint *>(&this->FramebufferTexture));

  if(this->DepthRenderBuffer)
    vtkgl::DeleteRenderbuffersEXT
        (1, reinterpret_cast<GLuint *>(&this->DepthRenderBuffer));

  if( this->FramebufferObject)
    vtkgl::DeleteFramebuffersEXT
        (1,reinterpret_cast<GLuint *>(&this->FramebufferObject));

  this->FramebufferObjectSize      = 0;
  this->FramebufferObject          = 0;
  this->FramebufferTexture         = 0;
  this->DepthRenderBuffer          = 0;
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::Render(vtkRenderer *ren,vtkVolume *vol)
{
  CALL_TRACER_MACRO;  

  this->InitializationError = HAPTVolumeMapper::NO_INIT_ERROR;
  this->RenderWindow=ren->GetRenderWindow();

  ren->GetRenderWindow()->MakeCurrent();
  LoadRequiredExtensions(ren);

  //Reality Check
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;
  if (this->CheckInitializationError()) return;
  this->UpdateProgress(0.0);

  bool tetras_changed  = this->Superclass::InitializePrimitives();
  bool scalars_changed = this->Superclass::InitializeScalars(vol);
  bool tf_changed      = this->Superclass::InitializeTransferFunction(vol);

  if (ren->GetRenderWindow()->CheckAbortStatus()) return;
  if (this->CheckInitializationError()) return;
  this->UpdateProgress(0.2);

  // Initialize FBOs
  if(!Initialized)
    this->InitializeFramebufferObject();

  if(tf_changed || !Initialized)
    this->InitializeLookupTables(vol);

  // Order is important
  this->InitializeShaders();

  if(scalars_changed||tetras_changed || !Initialized || UseGPUDataStructures_changed)
    this->InitializeGPUDataStructures();

  //Reality Check
  if (ren->GetRenderWindow()->CheckAbortStatus()) return;
  if (this->CheckInitializationError()) return;
  this->UpdateProgress(0.4);

  Initialized = true;

  this->Timer->StartTimer();
  this->RenderHAPT(ren,vol);
  this->LastVolume = vol;

  this->Timer->StopTimer();
  this->TimeToDraw = static_cast<float>(this->Timer->GetElapsedTime());
}

//----------------------------------------------------------------------------
// The OpenGL rendering
void OpenGLHAPTVolumeMapper::RenderHAPT(vtkRenderer *ren,vtkVolume *vol)
{
  CALL_TRACER_MACRO;
  glPushAttrib(GL_ENABLE_BIT         |
               GL_CURRENT_BIT        |
               GL_COLOR_BUFFER_BIT   |
               GL_STENCIL_BUFFER_BIT |
               GL_DEPTH_BUFFER_BIT   | 
               GL_POLYGON_BIT        | 
               GL_TEXTURE_BIT        |
               GL_LIGHTING_BIT       |
               GL_TRANSFORM_BIT      |
               GL_VIEWPORT_BIT);

  // Setup OpenGL state
  glShadeModel(GL_SMOOTH);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);
  glDisable(GL_NORMALIZE);
  glDisable(GL_BLEND);  
  glDisable(GL_SCISSOR_TEST);
  glDisable(GL_STENCIL_TEST);

  int screenWidth, screenHeight;
  ren->GetTiledSize(&screenWidth, &screenHeight);

  int vpWidth = screenWidth;
  int vpHeight = screenHeight;

  if (screenWidth > this->FramebufferObjectSize)
    vpWidth = this->FramebufferObjectSize;

  if (screenHeight > this->FramebufferObjectSize)
    vpHeight = this->FramebufferObjectSize;

  this->UpdateProgress(0.4);

  if (ren->GetRenderWindow()->CheckAbortStatus()) return;

  // Get depth range from OpenGL state for correct z
  GLfloat depthRange[2];
  glGetFloatv(GL_DEPTH_RANGE, depthRange);

  // Get the current z-buffer
  float *zbuf =
    ren->GetRenderWindow()->GetZbufferData(0,0,screenWidth-1,screenHeight-1);

  // Enable FBO Rendering
  GLint savedFrameBuffer;
  glGetIntegerv(vtkgl::FRAMEBUFFER_BINDING_EXT,&savedFrameBuffer);
  vtkgl::BindFramebufferEXT(vtkgl::FRAMEBUFFER_EXT, this->FramebufferObject);  

  // Setup z-buffer
  this->SetupFBOZBuffer(vpWidth, vpHeight, depthRange[0], depthRange[1],zbuf);
  this->UpdateProgress(0.5);
  delete [] zbuf;

  // Setup textures and clear Render target
  this->DrawFBOInit(vpWidth,vpHeight,depthRange[0],depthRange[1]);

  // Sort according to centroid Z
  vtkUnstructuredGrid *input = this->GetInput();
  this->VisibilitySort->SetInput(input);
  this->VisibilitySort->SetDirectionToFrontToBack();
  this->VisibilitySort->SetModelTransform(vol->GetMatrix());
  this->VisibilitySort->SetCamera(ren->GetActiveCamera());
  this->VisibilitySort->SetMaxCellsReturned(HAPT_VisibilitySort_MaxCells);
  this->VisibilitySort->InitTraversal();

  // Render all the tetrahedra
  this->UpdateProgress(0.7);
  this->DrawFBO(vpWidth,vpHeight,depthRange[0], depthRange[1]);

  // Draw Flushing pass  
  this->UpdateProgress(0.9);
  this->DrawFBOFlush(vpWidth, vpHeight, depthRange[0], depthRange[1]);
  
  // Disable FBO rendering
  vtkgl::BindFramebufferEXT(vtkgl::FRAMEBUFFER_EXT, savedFrameBuffer);
  
  glPopAttrib();

  // Blend Result into framebuffer
  this->DrawBlend(vpWidth, vpHeight, depthRange[0], depthRange[1]);
  this->UpdateProgress(1.0);
}

//----------------------------------------------------------------------------
// Draw the current z-buffer into the FBO z-buffer for correct compositing
// with existing geometry or widgets.
void OpenGLHAPTVolumeMapper::SetupFBOZBuffer(int screenWidth,
                                                int screenHeight,
                                                float depthNear,
                                                float depthFar,
                                                float *zbuffer)
{
  CALL_TRACER_MACRO;
  // Setup view for z-buffer copy
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0.0, static_cast<GLdouble>(screenWidth), 0.0,
          static_cast<GLdouble>(screenHeight),
          static_cast<GLdouble>(depthNear), static_cast<GLdouble>(depthFar));
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Enable FBO z-buffer
  glEnable(GL_DEPTH_TEST);
  glClearDepth(depthFar);
  glClear(GL_DEPTH_BUFFER_BIT);
  glDepthFunc(GL_LESS);

  glRasterPos2i(0,0);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glDrawPixels(screenWidth, screenHeight, GL_DEPTH_COMPONENT, GL_FLOAT,
               zbuffer);
  glFlush();

  // Make z-buffer Read only
  glDepthMask(GL_FALSE);

  // Reset view state after z-buffer copy
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}

//----------------------------------------------------------------------------
// Draw a screen-aligned plane with the init fragment shader enabled.  The
// init fragment shader clears the framebuffer to 0 and the k-buffers to -1.
void OpenGLHAPTVolumeMapper::DrawFBOInit(int screenWidth, int screenHeight,
                                            float depthNear, float depthFar)
{
  CALL_TRACER_MACRO;

  // Setup ortho view
  GLenum buffers[] = {vtkgl::COLOR_ATTACHMENT0_EXT};
  vtkgl::DrawBuffers(1, buffers);

  glEnable(GL_TEXTURE_1D);
  vtkgl::ActiveTexture(vtkgl::TEXTURE0);
  glBindTexture(GL_TEXTURE_1D, this->OrderTableTexture);

  glEnable(GL_TEXTURE_1D);
  vtkgl::ActiveTexture(vtkgl::TEXTURE1);
  glBindTexture(GL_TEXTURE_1D, this->TriangleFanOrderTableTexture);

  glEnable(GL_TEXTURE_2D);
  vtkgl::ActiveTexture(vtkgl::TEXTURE2);
  glBindTexture(GL_TEXTURE_2D,this->PsiTableTexture);

  glEnable(GL_TEXTURE_1D);
  vtkgl::ActiveTexture(vtkgl::TEXTURE3);
  glBindTexture(GL_TEXTURE_1D,this->TransferFunctionTexture);

  glEnable(GL_TEXTURE_2D);
  vtkgl::ActiveTexture(vtkgl::TEXTURE4);
  glBindTexture(GL_TEXTURE_2D,this->FramebufferTexture);

#ifdef HAPT_USE_AUX_SCALARS
  if(UseAuxScalars)  {
    glEnable(GL_TEXTURE_1D);
    vtkgl::ActiveTexture(vtkgl::TEXTURE5);
    glBindTexture(GL_TEXTURE_1D,this->AuxTransferFunctionTexture);
  }
#endif

  this->CheckOpenGLError("setup Textures ..Done");

  // Setup ortho view
  glViewport(0,0,screenWidth,screenHeight);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0.0, static_cast<GLdouble>(screenWidth), 0.0,
          static_cast<GLdouble>(screenHeight),
          static_cast<GLdouble>(depthNear), static_cast<GLdouble>(depthFar));
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Draw a quad to initialize the FBO
  glColor4f(0,0,0,0.0);
  glBegin(GL_QUADS);
  glVertex3f(0.0, 0.0, 0.0);
  glVertex3f(static_cast<float>(screenWidth), 0.0, 0.0);
  glVertex3f(static_cast<float>(screenWidth),
             static_cast<float>(screenHeight), 0.0);
  glVertex3f(0.0, static_cast<float>(screenHeight), 0.0);
  glEnd();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}


//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::DrawFBO(int , int ,float ,float )
{
  CALL_TRACER_MACRO;

  glEnableClientState(GL_VERTEX_ARRAY);

  glDisable(GL_CULL_FACE);

  if( GPUDataStructures ) {
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, TetraVertPositionBufObj[0]);
      glVertexPointer(4, GL_FLOAT, 0, 0);
  } else
      glVertexPointer(4, GL_FLOAT, 0, TetraVertPosition[0]);

  glClientActiveTexture(GL_TEXTURE0 + 0);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  if( GPUDataStructures ) {
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, TetraVertPositionBufObj[1]);
      glTexCoordPointer(4, GL_FLOAT, 0, 0);
  } else
      glTexCoordPointer(4, GL_FLOAT, 0, TetraVertPosition[1]);

  glClientActiveTexture(GL_TEXTURE0 + 1);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  if( GPUDataStructures ) {
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, TetraVertPositionBufObj[2]);
      glTexCoordPointer(4, GL_FLOAT, 0, 0);
  } else
      glTexCoordPointer(4, GL_FLOAT, 0, TetraVertPosition[2]);

  glClientActiveTexture(GL_TEXTURE0 + 2);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  if( GPUDataStructures ) {
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, TetraVertPositionBufObj[3]);
      glTexCoordPointer(4, GL_FLOAT, 0, 0);
  } else
      glTexCoordPointer(4, GL_FLOAT, 0, TetraVertPosition[3]);

#ifdef HAPT_USE_AUX_SCALARS
  if( UseAuxScalars) {
    glClientActiveTexture(GL_TEXTURE0 + 3);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    if( GPUDataStructures ) {
      vtkgl::BindBuffer(vtkgl::ARRAY_BUFFER, TetraVertPositionBufObj[4]);
      glTexCoordPointer(4, GL_FLOAT, 0, 0);
    } else
      glTexCoordPointer(4, GL_FLOAT, 0, TetraVertPosition[4]);
  }

  vtkShaderProgram2 *shader = (UseAuxScalars)?(this->AuxShader):(this->Shader);
#else
  vtkShaderProgram2 *shader = this->Shader;
#endif

  shader->Use();

  float viewportSzInv[] = {1.0/this->FramebufferObjectSize,
                           1.0/this->FramebufferObjectSize};

  shader->GetUniformVariables()->SetUniformf
      ("viewportSzInv",2,viewportSzInv);

  unsigned int corder_buf[HAPT_VisibilitySort_MaxCells];

  for (vtkIdTypeArray *corder = this->VisibilitySort->GetNextCells();
       corder != NULL;corder = this->VisibilitySort->GetNextCells())
  {
    int n = corder->GetNumberOfTuples();

    std::copy(corder->GetPointer(0),corder->GetPointer(0)+n,corder_buf);

    glDrawElements(GL_POINTS, n, GL_UNSIGNED_INT,corder_buf);
  }

  shader->Restore();


  glDisableClientState(GL_VERTEX_ARRAY);

  glClientActiveTexture(GL_TEXTURE0 + 0);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glClientActiveTexture(GL_TEXTURE0 + 1);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glClientActiveTexture(GL_TEXTURE0 + 2);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

#ifdef HAPT_USE_AUX_SCALARS
  if( UseAuxScalars) {
    glClientActiveTexture(GL_TEXTURE0 + 3);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  }
#endif
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::DrawFBOFlush(int screenWidth,
                                             int screenHeight,
                                             float depthNear, float depthFar)
{
  CALL_TRACER_MACRO;
  // Disable Textures  
  vtkgl::ActiveTexture(vtkgl::TEXTURE0);
  glDisable(GL_TEXTURE_1D);

  vtkgl::ActiveTexture(vtkgl::TEXTURE1);
  glDisable(GL_TEXTURE_1D);

  vtkgl::ActiveTexture(vtkgl::TEXTURE2);
  glDisable(GL_TEXTURE_2D);

  vtkgl::ActiveTexture(vtkgl::TEXTURE3);
  glDisable(GL_TEXTURE_1D);

  vtkgl::ActiveTexture(vtkgl::TEXTURE4);
  glDisable(GL_TEXTURE_2D);

#ifdef HAPT_USE_AUX_SCALARS
  if(UseAuxScalars) {
    vtkgl::ActiveTexture(vtkgl::TEXTURE5);
    glDisable(GL_TEXTURE_1D);
  }
#endif
  glDisable(GL_DEPTH_TEST);

  glFinish();  

  this->CheckOpenGLError("Flushed FBO");
}

//----------------------------------------------------------------------------
// Blend the result from the off screen rendering into the framebuffer by
// drawing a textured screen-aligned plane.  This avoids expensive data
// transfers between GPU and CPU.
void OpenGLHAPTVolumeMapper::DrawBlend(int screenWidth, int screenHeight,
                                          float depthNear, float depthFar)
{
  CALL_TRACER_MACRO;
  // Setup draw buffer
  glDrawBuffer(GL_BACK);

  // Setup 2D view
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0.0, screenWidth, 0.0, screenHeight, depthNear, depthFar);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  // Bind resulting texture
  vtkgl::ActiveTexture(vtkgl::TEXTURE0);

  glBindTexture(GL_TEXTURE_2D, this->FramebufferTexture);
  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  float max_u =static_cast<float>(screenWidth)/
      static_cast<float>(this->FramebufferObjectSize);

  float max_v = static_cast<float>(screenHeight)/
      static_cast<float>(this->FramebufferObjectSize);

  if (max_u > 1.0) { max_u = 1.0; }
  if (max_v > 1.0) { max_v = 1.0; }

  // Setup blending.  Use the same non-standard blending function as PT to get
  // similar images.
  glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

  // Draw textured screen-aligned plane
  glColor4f(0.0, 0.0, 0.0, 0.0);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0);
  glVertex2f(0.0, 0.0); 
  glTexCoord2f(max_u, 0.0);
  glVertex2f(screenWidth, 0.0);
  glTexCoord2f(max_u, max_v);
  glVertex2f(screenWidth, screenHeight);
  glTexCoord2f(0.0, max_v);
  glVertex2f(0.0, screenHeight);
  glEnd();

  // Reset view
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // Restore OpenGL state
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDisable(GL_TEXTURE_2D);
  
  CheckOpenGLError("Final Blend");
}

//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::PrintSelf(ostream& os, vtkIndent indent)
{
  CALL_TRACER_MACRO;
  if(this->RenderWindow!=0)
    {
    vtkOpenGLExtensionManager *extensions=
      static_cast<vtkOpenGLRenderWindow *>(this->RenderWindow.GetPointer())
      ->GetExtensionManager();
    if ( this->Initialized )
      {
      os << indent << "Supports GL_VERSION_1_3 (edge_clamp (1.2) and"
         << " multitexture (1.3) minimal version required by"
         << " GL_ARB_draw_buffers): "
         << extensions->ExtensionSupported( "GL_VERSION_1_3" );

      os << indent << "Supports GL_VERSION_2_0 (GL_ARB_draw_buffers is a core"
         << "feature): "
         << extensions->ExtensionSupported( "GL_VERSION_2_0" );

      os << indent << "Supports GL_ARB_draw_buffers: "
         << extensions->ExtensionSupported( "GL_ARB_draw_buffers" );

      os << indent << "Supports GL_EXT_framebuffer_object: " 
         << extensions->ExtensionSupported( "GL_EXT_framebuffer_object" )
         << endl;
      os << indent << "Supports GL_ARB_vertex_program: "
         << extensions->ExtensionSupported( "GL_ARB_vertex_program" ) << endl;
      os << indent << "Supports GL_ARB_fragment_program: "
         << extensions->ExtensionSupported( "GL_ARB_fragment_program" ) << endl;
      os << indent << "Supports GL_ARB_texture_float"
         << extensions->ExtensionSupported( "GL_ARB_texture_float" ) << endl;
      os << indent << "Supports GL_ATI_texture_float: "
         << extensions->ExtensionSupported( "GL_ATI_texture_float" ) << endl;

      os << indent << "Supports GL_VERSION_1_5 (for optional core feature VBO): "
         << extensions->ExtensionSupported( "GL_VERSION_1_5" ) <<endl;

      os << indent << "Supports (optional) GL_ARB_vertex_buffer_object: "
         << extensions->ExtensionSupported( "GL_ARB_vertex_buffer_object" )
         <<endl;
      }
    }
  
  os << indent << "Framebuffer Object Size: " 
     << this->FramebufferObjectSize << endl;
  
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
// Check the OpenGL extension manager for GPU features necessary for the
// HAPT algorithm.
bool OpenGLHAPTVolumeMapper::SupportedByHardware(vtkRenderer *r)
{
  CALL_TRACER_MACRO;
  vtkOpenGLExtensionManager *extensions=
    static_cast<vtkOpenGLRenderWindow *>(r->GetRenderWindow())
    ->GetExtensionManager();

  // Temporarily filter out the Macs, as this mapper makes the ATI driver crash
  // (RogueResearch2 on VTK, ATI Radeon X1600 OpenGL Engine 2.0 ATI-1.4.56) and
  // makes the Nvidia driver render some corrupted image (kamino on ParaView3
  // dashboard NVIDIA GeForce 7300 GT 2.0 NVIDIA-1.4.56).
  // This mapper does not actually use texture3D but it is known that Macs
  // only support texture3d through OpenGL 1.2 API, not as an extension, so
  // this is a good way to filter them out. 
  int iAmAMac=!extensions->ExtensionSupported("GL_EXT_texture3D");
  
  // OpenGL 1.3 is required by GL_ARB_draw_buffers, GL_ARB_fragment_program
  // and GL_ARB_vertex_program
  // CLAMP_TO_EGDE is core feature of OpenGL 1.2 and
  // multitexture is core feature of OpenGL 1.3.
  int supports_GL_1_3=extensions->ExtensionSupported( "GL_VERSION_1_3" );
   
  
  int supports_GL_2_0=extensions->ExtensionSupported( "GL_VERSION_2_0" );
  int supports_draw_buffers;
  if(supports_GL_2_0)
    {
    supports_draw_buffers=1;
    }
  else
    {
    supports_draw_buffers=
      extensions->ExtensionSupported( "GL_ARB_draw_buffers" );
    }
  
  int supports_GL_ARB_fragment_program =
    extensions->ExtensionSupported( "GL_ARB_fragment_program" );
  int supports_GL_ARB_vertex_program = 
    extensions->ExtensionSupported( "GL_ARB_vertex_program" );
  
  int supports_GL_EXT_framebuffer_object = 
    extensions->ExtensionSupported( "GL_EXT_framebuffer_object");
   
   // GL_ARB_texture_float or GL_ATI_texture_float introduce new tokens but
   // no new function: don't need to call LoadExtension.
   
  int supports_GL_ARB_texture_float = 
    extensions->ExtensionSupported( "GL_ARB_texture_float" );
  int supports_GL_ATI_texture_float = 
    extensions->ExtensionSupported( "GL_ATI_texture_float" );
  
  return !iAmAMac && supports_GL_1_3 && supports_draw_buffers &&
    supports_GL_ARB_fragment_program && supports_GL_ARB_vertex_program &&
    supports_GL_EXT_framebuffer_object &&
    ( supports_GL_ARB_texture_float || supports_GL_ATI_texture_float);
}


//----------------------------------------------------------------------------
void OpenGLHAPTVolumeMapper::LoadRequiredExtensions(vtkRenderer *ren)
{
  CALL_TRACER_MACRO;

  // Check for the required extensions only.
  if (!Initialized && !this->SupportedByHardware(ren))
  {
    this->InitializationError = HAPTVolumeMapper::UNSUPPORTED_EXTENSIONS;
    return;
  }

  vtkOpenGLExtensionManager *extensions=static_cast<vtkOpenGLRenderWindow *>
      (ren->GetRenderWindow())->GetExtensionManager();

  // Load required extensions

  // supports_GL_1_3=1 as checked by this->SupportedByHardware()
  // OpenGL 1.3 is required by GL_ARB_draw_buffers, GL_ARB_fragment_program
  //  and GL_ARB_vertex_program.
  // CLAMP_TO_EGDE is core feature of OpenGL 1.2 and
  // multitexture is core feature of OpenGL 1.3.
  extensions->LoadExtension("GL_VERSION_1_3"); // multitexture

  // supports_draw_buffers as checked by this->SupportedByHardware()
  int supports_GL_2_0=extensions->ExtensionSupported( "GL_VERSION_2_0" );

  if(supports_GL_2_0)
  {
    extensions->LoadExtension("GL_VERSION_2_0");
  }
  else
  {
    extensions->LoadCorePromotedExtension( "GL_ARB_draw_buffers" );
  }

  // supports_fragment_program && supports_vertex_program as checked
  // by this->SupportedByHardware()
  extensions->LoadExtension( "GL_ARB_fragment_program" );
  extensions->LoadExtension( "GL_ARB_vertex_program" );

  // supports_GL_EXT_framebuffer_object==1 as checked
  // by this->SupportedByHardware()
  extensions->LoadExtension("GL_EXT_framebuffer_object");

  // GL_ARB_texture_float or GL_ATI_texture_float introduce new tokens but
  // no new function: don't need to call LoadExtension.

  // Optional extension.
  int supports_GL_1_5=extensions->ExtensionSupported( "GL_VERSION_1_5" );
  int supports_vertex_buffer_object;

  if(supports_GL_1_5)
  {
    supports_vertex_buffer_object=1;
  }
  else
  {
    supports_vertex_buffer_object =
        extensions->ExtensionSupported( "GL_ARB_vertex_buffer_object" );
  }

  if(supports_vertex_buffer_object)
  {
    if(supports_GL_1_5)
    {
      extensions->LoadExtension("GL_VERSION_1_5");
    }
    else
    {
      extensions->LoadCorePromotedExtension( "GL_ARB_vertex_buffer_object" );
    }
  }

  if (!supports_vertex_buffer_object)
  {
    this->SetGPUDataStructures(false);
  }
}


//----------------------------------------------------------------------------
int OpenGLHAPTVolumeMapper::FillInputPortInformation(int port,
                                                        vtkInformation* info)
{
  CALL_TRACER_MACRO;
  if(!this->Superclass::FillInputPortInformation(port, info))
    {
    return 0;
    }
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
  return 1;
}
