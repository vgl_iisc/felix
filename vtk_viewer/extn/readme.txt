This directory contains classes/modules that extend/modify the
standard libraries/toolkits used in the application (vtk, Qt, PythonQt etc). 

The dependancies of these classes are ensured to be minimally depenendant on 
the parent toolkit/library as far as possible so that they can be reused elsewhere. 

-Nithin Shivashankar (19 Nov 2013). 