/******************************************************************************

Copyright (C) 2013 Nithin Shivashankar (nithin19484@gmail.com).
All rights reserved.

This was developed as a standalone Qt widget to edit 1D Piecewise functions
Code adapted from the Qtfe (Qt Transfer Function Editor) library by
Eric Heitz (er.heitz@gmail.com).

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License.
If not, see <http://www.gnu.org/licenses/>.

******************************************************************************/

#include "QPiecewiseFunctionEditorWidget.hpp"

#include <QPainter>
#include <QColorDialog>
#include <QDebug>
#include <QFile>

/*****************************************************************************/

template <typename T> T lerp(T t,T m, T M) {return (1-t)*m + t*M;}
template <typename T> T normalize(T t,T m, T M){return (t-m)/(M-m);}
template <typename T> T clamp(T t,T m, T M){return(t<=m)?(m):((t<=M)?(t):(M));}

inline int log2(int i){int j = 0;while (i >>= 1) ++j;  return j;}
inline int pow2(int i){return 1 << i;}

/*---------------------------------------------------------------------------*/


template <class Iterator,class Comparator>
Iterator max_element ( Iterator b, Iterator e ,Comparator cmp)
{
  if (b==e) return e;
  Iterator largest = b;

  while (++b!=e)
    if (cmp(*largest,*b))
      largest=b;
  return largest;
}

/*---------------------------------------------------------------------------*/

template <class Iterator,class Comparator>
Iterator min_element ( Iterator b, Iterator e ,Comparator cmp)
{
  if (b==e) return e;
  Iterator smallest = b;

  while (++b!=e)
    if (cmp(*b,*smallest))
      smallest=b;
  return smallest;
}

/*---------------------------------------------------------------------------*/

inline QString opt_to_string(qreal r)
{
  if (r >= 100)
    return QString().sprintf("%d.",int(r));
  else if (r >= 10)
    return QString().sprintf("%.1f",(r));
  else if (r >= 1)
    return QString().sprintf("%.2f",(r));
  else
    return QString().sprintf("%.2f",(r));
}
/*****************************************************************************/



/*****************************************************************************/

void ValidatePiecewiseFunction(PiecewiseFunction &pwf,
                               qreal &minx,qreal &maxx,
                               qreal &miny,qreal &maxy)
{
  if(!(minx < maxx))
  {
    qWarning()
        <<"Given X Range is inconsistent\n"
        <<"minx = "<<minx<<"\n"
        <<"maxx = "<<maxx<<"\n"
        <<"Assuming [0,1]\n";

    minx = 0;
    maxx = 1;

  }

  if(!(miny < maxy))
  {
    if(!(miny == 0 && maxy == 0))
    {
      qWarning()
          <<"Given Y Range is inconsistent\n"
          <<"miny = "<<miny<<"\n"
          <<"maxy = "<<maxy<<"\n"
          <<"Assuming [0,1]\n";
    }

    miny = 0;
    maxy = 1;
  }


  if(pwf.size() == 1)
  {
    qWarning()
        <<"Given a Piecewise function with only 1 point\n"
        <<"Clearing it out and entering default values\n";
    pwf.clear();
  }

  if(pwf.size() == 0)
  {
    pwf.push_back(PiecewiseFunctionPoint(minx,miny));
    pwf.push_back(PiecewiseFunctionPoint(maxx,maxy));
  }

  qSort(pwf);

  qreal _minx = pwf.front().x;
  qreal _maxx = pwf.back().x;

  if(!(qAbs(minx - _minx) < 0.001*qAbs(maxx-minx)) ||
     !(qAbs(maxx - _maxx) < 0.001*qAbs(maxx-minx)))
  {
    qWarning()
        <<"Warning: Given pwf's X range does not match with given X range\n"
        <<"Pwf's range\n"
        <<"minx = "<<_minx<<"\n"
        <<"maxx = "<<_maxx<<"\n"
        <<"Given Range\n"
        <<"minx = "<<minx<<"\n"
        <<"maxx = "<<maxx<<"\n"
        <<"Pwf x will be normalized and rescaled linearly to given X Range\n";

    for( int i = 0; i < int(pwf.size()) ;++i)
      pwf[i].x = lerp<qreal>(normalize<qreal>(pwf[i].x,_minx,_maxx),minx,maxx);
  }

  qreal _miny = min_element(pwf.begin(),pwf.end(),PiecewiseFunctionPoint::cmp_y)->y;
  qreal _maxy = max_element(pwf.begin(),pwf.end(),PiecewiseFunctionPoint::cmp_y)->y;

  if( _miny < miny || _maxx > maxx)
  {
    qWarning()
        <<"Warning: Given pwf's Y range does fit within with given Y range\n"
        <<"pwf's range\n"
        <<"minx = "<<_minx<<"\n"
        <<"maxx = "<<_maxx<<"\n"
        <<"Given Range\n"
        <<"minx = "<<minx<<"\n"
        <<"maxx = "<<maxx<<"\n"
        <<"Pwf y will be normalized and rescaled linearly to given Y Range\n";

    for( int i = 0; i < int(pwf.size()) ;++i)
      pwf[i].y = lerp<qreal>(normalize<qreal>(pwf[i].y,_miny,_maxy),miny,maxy);
  }
}

/*---------------------------------------------------------------------------*/

bool SavePiecewiseFunction(const PiecewiseFunction&_pwf,const QString& fn,
                           qreal xmin,qreal xmax,qreal ymin,qreal ymax)
{
  PiecewiseFunction pwf(_pwf);

  if(pwf.size() < 2)
  {
    qWarning()<<"Piecewise function function has too few values!!"
              <<"Not saving!!!";
    return false;
  }

  ValidatePiecewiseFunction(pwf,xmin,xmax,ymin,ymax);

  QFile f(fn);
  if(!f.open(QIODevice::WriteOnly))
  {
    qWarning()<<"Unable to open file!!"<<fn;
    return false;
  }
  QTextStream ts(&f);

  ts<<xmin<<"\t"
    <<xmax<<"\t"
    <<ymin<<"\t"
    <<ymax<<"\n";

  for(int i = 0 ; i < int(pwf.size()); ++i)
  {
    ts<<pwf[i].x<<"\t"<<pwf[i].y<<"\n";
  }

  f.close();

  return true;

}

/*---------------------------------------------------------------------------*/

bool LoadPiecewiseFunction(PiecewiseFunction &pwf,const QString& fn,
                           qreal xmin,qreal xmax,qreal ymin,qreal ymax)
{
  QFile f(fn);
  if(!f.open(QIODevice::ReadOnly))
  {
    qWarning()<<"Unable to open file!!"<<fn;
    return false;
  }

  QTextStream ts(&f);

  PiecewiseFunction _pwf;

  qreal _xmin,_xmax,_ymin,_ymax;

  ts>>_xmin>>_xmax>>_ymin>>_ymax;

  while(true)
  {
    qreal x,y;

    ts>>x>>y;

    if(ts.status() != QTextStream::Ok)
      break;

    _pwf.push_back(PiecewiseFunctionPoint(x,y));
  }
  f.close();

  if(_pwf.size() < 2)
  {
    qWarning()<<"Transfer function read with less that two values\n"
              <<"Not updating\n";
    return false;
  }

  ValidatePiecewiseFunction(_pwf,xmin,xmax,ymin,ymax);

  // All good
  pwf.clear();
  pwf += _pwf;

  return true;
}


/*****************************************************************************/



/*****************************************************************************/

const int QPiecewiseFunctionEditorWidget::pointSizePixel = 5;
const int QPiecewiseFunctionEditorWidget::circleSizePixel = 9;
const int QPiecewiseFunctionEditorWidget::lineWidth = 2;
const int QPiecewiseFunctionEditorWidget::tickLength = 5;

/*---------------------------------------------------------------------------*/

QPiecewiseFunctionEditorWidget::QPiecewiseFunctionEditorWidget
(QWidget *par,Qt::WindowFlags f):QWidget(par,f),m_xmin(0),m_xmax(1),
  m_ymin(0),m_ymax(1),
  pressed(false),selected(-1)

{
  this->setMouseTracking(true);

  list.push_back(PiecewiseFunctionPoint(m_xmin,m_ymin));
  list.push_back(PiecewiseFunctionPoint(m_xmax,m_ymax));

  setMinimumSize(fontMetrics().width("0")*10,fontMetrics().height()*5);
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::setPiecewiseFunction
(const PiecewiseFunction &pwf, qreal minx, qreal maxx, qreal miny, qreal maxy)
{
  list.clear();
  list += pwf;

  m_xmin = minx;
  m_xmax = maxx;

  m_ymin = miny;
  m_ymax = maxy;

  ValidatePiecewiseFunction(list,m_xmin,m_xmax,m_ymin,m_ymax);

  for( int i = 0; i < int(list.size()) ;++i)
  {
    list[i].x = normalize<qreal>(list[i].x,m_xmin,m_xmax);
    list[i].y = normalize<qreal>(list[i].y,m_ymin,m_ymax);
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::getPiecewiseFunction
(PiecewiseFunction &tf) const
{
  tf.clear();
  tf += list;

  for(int i = 0 ; i < int(tf.size()); ++i)
  {
    tf[i].x = lerp<qreal>(tf[i].x,m_xmin,m_xmax);
    tf[i].y = lerp<qreal>(tf[i].y,m_ymin,m_ymax);
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::resizeEvent ( QResizeEvent * event )
{
  QSize sz   = event->size();
  QSize tksz = fontMetrics().size(0,QString("-00.0"));

  bg_sz = sz - (tksz*2);
  bg_bp.setX(tksz.width());
  bg_bp.setY(tksz.height());
}

/*---------------------------------------------------------------------------*/

QPoint QPiecewiseFunctionEditorWidget::listPos2WidgetPos(int pos)
{
  qreal x = list[pos].x;
  qreal y = list[pos].y;

  return QPoint(bg_bp.x() + bg_sz.width()*x,
                bg_bp.y() + bg_sz.height()* (1.0 - y));
}

/*---------------------------------------------------------------------------*/

QPointF QPiecewiseFunctionEditorWidget::WidgetPos2listPos(QPoint p)
{
  return QPointF(qreal(p.x()-bg_bp.x())/(qreal)bg_sz.width(),
                 1.0 - qreal(p.y() - bg_bp.y())/(qreal)bg_sz.height());
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::paintEvent(QPaintEvent *event)
{
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing, true);

  // A white background
  QBrush bgbrush(Qt::white);
  painter.setBrush(bgbrush);
  painter.drawRect(QRect(bg_bp,bg_sz));

  // Draw Grid and Ticks
  QSize fsz = fontMetrics().size(0,QString("-0.00"));

  QPen bglnpen(Qt::white, pointSizePixel, Qt::DotLine);
  bglnpen.setColor(Qt::darkGray);
  bglnpen.setWidth(1);

  QPen bgtxpen(Qt::white, pointSizePixel, Qt::SolidLine);
  bgtxpen.setColor(Qt::black);

  int xnt = pow2(log2(bg_sz.width()/(2*fsz.width())));

  qreal xs = qreal(bg_sz.width())/qreal(xnt);

  for( int i = 0 ;i <= xnt ; ++i)
  {
    int x  = bg_bp.x() + qreal(i) * xs;
    int y0 = bg_bp.y();
    int y1 = bg_bp.y() + bg_sz.height();

    painter.setPen(bglnpen);
    painter.drawLine(QPoint(x,y0),QPoint(x,y1));

    painter.setPen(bgtxpen);
    QString xstr = opt_to_string(lerp<qreal>(qreal(i)/qreal(xnt),m_xmin,m_xmax));
    painter.drawText(x - fsz.width()/2,y1+fsz.height(),xstr);
  }

  int ynt = pow2(log2(bg_sz.height()/(2*fsz.height())));

  qreal ys = qreal(bg_sz.height())/qreal(ynt);

  for( int i = 0 ;i <= ynt ; ++i)
  {
    int y  = bg_bp.y() + qreal(i) * ys;
    int x0 = bg_bp.x();
    int x1 = bg_bp.x() + bg_sz.width();

    painter.setPen(bglnpen);
    painter.drawLine(QPoint(x0,y),QPoint(x1,y));

    painter.setPen(bgtxpen);
    QString ystr = opt_to_string(lerp<qreal>(qreal(i)/qreal(ynt),m_ymin,m_ymax));
    painter.drawText(0,height() - y +fsz.height()/3,ystr);
  }


  painter.setBrush(Qt::NoBrush);


  // all points
  QPen pen(Qt::black, pointSizePixel, Qt::SolidLine);
  painter.setPen(pen);
  for(int i=0 ; i<int(list.size()) ; ++i)
  {
    painter.drawPoint(listPos2WidgetPos(i));
  }

  // points interpolation line
  pen.setWidth(lineWidth);
  painter.setPen(pen);

  for(int p=1 ; p < int(list.size()) ; ++p)
    painter.drawLine(listPos2WidgetPos(p-1),listPos2WidgetPos(p));

  // selected point
  if(selected >=0)
  {
    QPoint pt = listPos2WidgetPos(selected);
    pen.setColor(Qt::red);
    painter.setPen(pen);
    painter.drawEllipse(pt,circleSizePixel,circleSizePixel);

    QString xstr = opt_to_string(m_xmin+list[selected].x*(m_xmax-m_xmin));
    QString ystr = opt_to_string(m_ymin+list[selected].y*(m_ymax-m_ymin));

    pen.setColor(Qt::black);
    painter.setPen(pen);

    painter.drawText(QPoint(pt.x() - fsz.width()/2,fsz.height()),xstr);
    painter.drawText(QPoint(bg_bp.x()+bg_sz.width(),pt.y()+fsz.height()/2),
                            ystr);

    pen.setColor(Qt::black);
    pen.setStyle(Qt::SolidLine);
    painter.setPen(pen);

    painter.drawLine(pt.x(),bg_bp.y()+bg_sz.height()-tickLength,
                     pt.x(),bg_bp.y()+bg_sz.height());
    painter.drawLine(bg_bp.x()  ,pt.y(),
                     bg_bp.x()+tickLength,pt.y());
  }


  QWidget::paintEvent(event);
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::mousePressEvent( QMouseEvent * event )
{
  QPointF pf = WidgetPos2listPos(event->pos());

  if(event->button() == Qt::LeftButton)
  {
    pressed = true;
    if(selected == -1)
    {
      for(int i=1 ; i < int(list.size()) ; ++i)
      {
        if( list[i-1].x <= pf.x() && pf.x() < list[i].x )
        {
          pMin = list[i-1].x+0.001;
          pMax = list[i].x-0.001;

          qreal y = evalf(pf.x());

          list.insert(list.begin()+i,PiecewiseFunctionPoint(pf.x(),y));

          selected = i;

          repaint();
          break;
        }
      }
    }
  }
  if(event->button() == Qt::RightButton && selected >= 0)
  {
    if(selected != 0 && selected != int(list.size())-1)
    {
      list.erase(list.begin()+selected);
      selected = -1;
      this->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::mouseReleaseEvent ( QMouseEvent * event )
{
  if(event->button() == Qt::LeftButton)
  {
    pressed = false;
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::mouseMoveEvent ( QMouseEvent * event )
{
  QPointF pf = WidgetPos2listPos(event->pos());

  if(pressed)
  {
    if(selected >= 0)
    {
      if(selected != 0 && selected != int(list.size())-1)
      {
        list[selected].x = clamp<qreal>(pf.x(),pMin,pMax);
      }

      list[selected].y = clamp<qreal>(pf.y(),0,1);
      this->repaint();
      return;
    }
  }
  else
  {
    qreal d_min = circleSizePixel*circleSizePixel;
    qreal W = width()*width();
    qreal H = height()*height();
    int nearest = -1;

    for(int i=0 ; i<int(list.size()) ; ++i)
    {
      qreal x = list[i].x-pf.x();
      qreal y = list[i].y-pf.y();

      qreal d = x*x*W + y*y*H;
      if( d < d_min)
      {
        nearest = i;
        d_min = d;
        if(i==0)
        {
          pMin = 0.0;
          pMax = 0.0;
        }
        else if(i==int(list.size())-1)
        {
          pMin = 1.0;
          pMax = 1.0;
        }
        else
        {
          pMin = list[i-1].x+0.001;
          pMax = list[i+1].x-0.001;
        }
      }
    }

    if(nearest != selected)
    {
      selected = nearest;
      this->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorWidget::leaveEvent ( QEvent * event )
{
  selected = -1;
  repaint();
  QWidget::leaveEvent(event);
}

/*---------------------------------------------------------------------------*/

qreal QPiecewiseFunctionEditorWidget::evalf(qreal t) const
{
  if(t<=0.0) return list[0].y;
  if(t>=1.0) return list[list.size()-1].y;


  for(int i=0 ; i<int(list.size())-1 ; ++i)
  {
    qreal x0 = list[i].x;
    qreal x1 = list[i+1].x;

    if(t < x0 || t > x1)
      continue;

    qreal y0 = list[i].y;
    qreal y1 = list[i+1].y;

    t = normalize<qreal>(t,x0,x1);

    return clamp<qreal>(lerp<qreal>(t,y0,y1),0,1);
  }
  return list[0].y;
}

/*****************************************************************************/



/*****************************************************************************/

#include <QGridLayout>
#include <QPushButton>
#include <QFileDialog>

/*---------------------------------------------------------------------------*/

QPiecewiseFunctionEditorDialog::QPiecewiseFunctionEditorDialog
(QWidget *par,Qt::WindowFlags f):QDialog(par,f)
{
  gridl = new QGridLayout(this);
  setLayout(gridl);

  pwf_editor_widget = new QPiecewiseFunctionEditorWidget(this,f);
  gridl->addWidget(pwf_editor_widget,0,0,6,6);

  QPushButton * apply = new QPushButton(tr("Apply"));
  QPushButton * ok = new QPushButton(tr("Ok"));
  QPushButton * cancel = new QPushButton(tr("Cancel"));
  QPushButton * save = new QPushButton(tr("Save"));
  QPushButton * load = new QPushButton(tr("Load"));

  connect(apply,SIGNAL(clicked()),this,SLOT(apply()));
  connect(ok,SIGNAL(clicked()),this,SLOT(apply()));
  connect(ok,SIGNAL(clicked()),this,SLOT(accept()));
  connect(cancel,SIGNAL(clicked()),this,SLOT(reject()));
  connect(save,SIGNAL(clicked()),this,SLOT(save_tf()));
  connect(load,SIGNAL(clicked()),this,SLOT(load_tf()));

  gridl->addWidget(apply,6,3);
  gridl->addWidget(ok,6,4);
  gridl->addWidget(cancel,6,5);
  gridl->addWidget(save,6,0);
  gridl->addWidget(load,6,1);

  setWindowTitle(tr("Edit Piecewise Function"));

  resize(600,300);
}

/*---------------------------------------------------------------------------*/

QPiecewiseFunctionEditorDialog::~QPiecewiseFunctionEditorDialog(){}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorDialog::save_tf()
{
  QString fn = QFileDialog::getSaveFileName
      (this,tr("Save Pwf"),"",tr("Pwfs (*.pwf);; All Files(*)"));

  if(fn.size() != 0 )
  {
    PiecewiseFunction pwf;
    pwf_editor_widget->getPiecewiseFunction(pwf);
    SavePiecewiseFunction(pwf,fn,
                          pwf_editor_widget->getMinX(),
                          pwf_editor_widget->getMaxX(),
                          pwf_editor_widget->getMinY(),
                          pwf_editor_widget->getMaxY());
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorDialog::load_tf()
{
  QString fn = QFileDialog::getOpenFileName
      (this,tr("Load Pwf"),"",tr("Pwfs (*.pwf);; All Files(*)"));

  if(fn.size() != 0 )
  {
    PiecewiseFunction pwf;

    qreal minx = pwf_editor_widget->getMinX();
    qreal miny = pwf_editor_widget->getMinY();
    qreal maxx = pwf_editor_widget->getMaxX();
    qreal maxy = pwf_editor_widget->getMaxY();

    if(LoadPiecewiseFunction(pwf,fn,minx,miny,maxx,maxy))
    {
      pwf_editor_widget->setPiecewiseFunction(pwf,minx,maxx,miny,maxy);
      pwf_editor_widget->repaint();
    }
  }
}

/*---------------------------------------------------------------------------*/

void QPiecewiseFunctionEditorDialog::apply()
{
  emit applied();
}

/*---------------------------------------------------------------------------*/

int QPiecewiseFunctionEditorDialog::editPiecewiseFunction
(PiecewiseFunction &pwf, qreal minx, qreal maxx,qreal miny,qreal maxy)

{
  QPiecewiseFunctionEditorDialog dlg;

  dlg.setPiecewiseFunction(pwf,minx,maxx,miny,maxy);
  dlg.setModal(true);
  int ret = dlg.exec();

  if(ret == QDialog::Accepted)
  {
    pwf.clear();
    dlg.getPiecewiseFunction(pwf);
  }

  return ret;
}

/*****************************************************************************/



/*****************************************************************************/

//#include <QApplication>

//int main(int argc, char** argv)
//{
//  QApplication app(argc,argv);

//  PiecewiseFunction tf;

//  QPiecewiseFunctionEditorDialog::editPiecewiseFunction(tf,2,5,-1,1);
//}

/*****************************************************************************/
