/******************************************************************************

Copyright (C) 2013 Nithin Shivashankar (nithin19484@gmail.com).
All rights reserved.

This was developed as a standalone Qt widget and a convenience Qt Dialog
to edit 1D Piecewise functions.

Code adapted from the Qtfe (Qt Piecewise Function Editor) library by
Eric Heitz (er.heitz@gmail.com).

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License.
If not, see <http://www.gnu.org/licenses/>.

******************************************************************************/

#ifndef QPIECEWISEFUNCTIONEDITORWIDGET_INCLUDED
#define QPIECEWISEFUNCTIONEDITORWIDGET_INCLUDED

#include <QList>
#include <QColor>

struct PiecewiseFunctionPoint
{
  qreal  x;
  qreal  y;

  PiecewiseFunctionPoint():x(0),y(0){}
  PiecewiseFunctionPoint(qreal x,qreal y):x(x),y(y){}
  inline bool operator <(const PiecewiseFunctionPoint &o) const{return x<o.x;}

  inline static bool cmp_y(PiecewiseFunctionPoint a,PiecewiseFunctionPoint b)
  {return a.y < b.y;}
};

typedef QList<PiecewiseFunctionPoint> PiecewiseFunction;

bool SavePiecewiseFunction(const PiecewiseFunction&tf,const QString& fn,
                           qreal xmin,qreal xmax,qreal ymin,qreal ymax);
bool LoadPiecewiseFunction(      PiecewiseFunction&tf,const QString& file,
                           qreal xmin,qreal xmax,qreal ymin,qreal ymax);


/*****************************************************************************/



/*****************************************************************************/

#include <QWidget>
#include <QMouseEvent>

class QPiecewiseFunctionEditorWidget: public QWidget
{
  Q_OBJECT

public:
  QPiecewiseFunctionEditorWidget(QWidget *par=NULL,Qt::WindowFlags f=0);
  ~QPiecewiseFunctionEditorWidget(){}

public:
  void getPiecewiseFunction(PiecewiseFunction &) const;
  void setPiecewiseFunction(const PiecewiseFunction &,qreal minx,qreal maxx,
                            qreal miny,qreal maxy);

  inline qreal getMinX() {return m_xmin;}
  inline qreal getMinY() {return m_ymin;}
  inline qreal getMaxX() {return m_xmax;}
  inline qreal getMaxY() {return m_ymax;}


private:
  virtual void paintEvent(QPaintEvent* event);
  virtual void mousePressEvent(QMouseEvent* event);
  virtual void mouseReleaseEvent(QMouseEvent* event);
  virtual void mouseMoveEvent(QMouseEvent * event);
  virtual void leaveEvent(QEvent* event);
  virtual void resizeEvent(QResizeEvent* event);

  qreal evalf(qreal x) const;

  // list of tf points
  PiecewiseFunction list;

  // pwf range
  qreal m_xmin,m_xmax;
  qreal m_ymin,m_ymax;

  // mouse
  bool pressed;
  int  selected;
  qreal pMin, pMax;

  // paint
  QPoint bg_bp;
  QSize  bg_sz;
  QPoint listPos2WidgetPos(int pos);
  QPointF WidgetPos2listPos(QPoint p);

private:

  static const int pointSizePixel;
  static const int circleSizePixel;
  static const int lineWidth;
  static const int tickLength;
};

/*****************************************************************************/



/*****************************************************************************/
#include <QDialog>

class QGridLayout;
/// \brief A dialog to edit a Piecewise Function.
class QPiecewiseFunctionEditorDialog: public QDialog
{
  Q_OBJECT

signals:
  /// \brief The user clicked Apply or Ok
  void applied();

public:

  /// \brief Constructor.
  QPiecewiseFunctionEditorDialog(QWidget *par=NULL,Qt::WindowFlags f=0);

  /// \brief Destructor.
  ~QPiecewiseFunctionEditorDialog();

  /// \brief Set the Piecewise Function
  /// \param[in]    pwf  The input Piecewise function
  /// \param[in]    minx minimum Domain value
  /// \param[in]    maxx maximum Domain value
  /// \param[in]    miny minimum Range value
  /// \param[in]    maxy maximum Range value
  ///
  /// \note If pwf is non empty, it has to adhere to the domain and range.
  ///       If not, the editor will show one that is rescaled to
  ///       given domain and range, with a warning.
  inline void setPiecewiseFunction
    (const PiecewiseFunction &pwf,qreal minx, qreal maxx,qreal miny,qreal maxy)
  {pwf_editor_widget->setPiecewiseFunction(pwf,minx,maxx,miny,maxy);}

  /// \brief Get the edited Piecewise function
  /// \param[out]    pwf   The edited pwf.
  ///
  /// \note previous contents of pwf will be cleared
  inline void getPiecewiseFunction(PiecewiseFunction &pwf)
  {pwf_editor_widget->getPiecewiseFunction(pwf);}

  /// \brief Edit the given piecewise function
  /// \param[in,out] pwf  The pwf function being edited
  /// \param[in]     minx minimum Domain value
  /// \param[in]     maxx maximum Domain value
  /// \param[in]     miny minimum Range value
  /// \param[in]     maxy maximum Range value
  ///
  /// \note see setPiecewiseFunction() on conditions on pwf and the ranges.
  static int editPiecewiseFunction(PiecewiseFunction &pwf, qreal minx, qreal maxx,
                                   qreal miny,qreal maxy);

private:
  QGridLayout * gridl;
  QPiecewiseFunctionEditorWidget  * pwf_editor_widget;

private slots:
  void save_tf();
  void load_tf();
  void apply();
};

/*****************************************************************************/
#endif
