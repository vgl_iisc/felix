cmake_minimum_required(VERSION 2.8)

set(HAVS_SOURCE_DIR ${VTK_VIEWER_SOURCE_DIR}/HAVS)
set(HAVS_BINARY_DIR ${VTK_VIEWER_BINARY_DIR})

set(HAVS_SOURCES
  ${HAVS_SOURCE_DIR}/OpenGLHAVSVolumeMapper.hpp
  ${HAVS_SOURCE_DIR}/OpenGLHAVSVolumeMapper.cpp
  ${HAVS_SOURCE_DIR}/HAVSVolumeMapper.hpp
  ${HAVS_SOURCE_DIR}/HAVSVolumeMapper.cpp
)

set(HAVS_SHADERS
  HAVSVolumeMapper_kbufferVP.asm
  HAVSVolumeMapper_k2FP.asm
  HAVSVolumeMapper_k2BeginFP.asm
  HAVSVolumeMapper_k2EndFP.asm
  HAVSVolumeMapper_k6FP.asm
  HAVSVolumeMapper_k6BeginFP.asm
  HAVSVolumeMapper_k6EndFP.asm
  )

# -----------------------------------------------------------------------------
# Create custom commands to encode each glsl file into a C string literal
# in a header file
# -----------------------------------------------------------------------------

foreach(file ${HAVS_SHADERS})
  GET_FILENAME_COMPONENT(file_we ${file} NAME_WE)
  set(src ${HAVS_SOURCE_DIR}/${file})
  set(res ${HAVS_BINARY_DIR}/${file_we}.cpp)
  set(resh ${HAVS_BINARY_DIR}/${file_we}.h)

  add_custom_command(
    OUTPUT ${res} ${resh}
    DEPENDS ${src}
    COMMAND vtkEncodeString
    ARGS ${res} ${src} ${file_we} --build-header " " vtkSystemIncludes.h
    )
  set(HAVS_SOURCES ${HAVS_SOURCES} ${res} ${resh})
endforeach(file)

