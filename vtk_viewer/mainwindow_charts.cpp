#include <vtkContextView.h>
#include <vtkRenderer.h>
#include <vtkContextScene.h>
#include <vtkChart.h>
#include <vtkAxis.h>
#include <vtkRenderWindow.h>
#include <vtkPlotPoints.h>
#include <vtkFloatArray.h>
#include <vtkTable.h>
#include <vtkChartXY.h>
#include <vtkContextMouseEvent.h>
#include <vtkStringArray.h>
#include <vtkIdTypeArray.h>
#include <vtkImageData.h>
#include <vtkColorTransferFunction.h>
#include <vtkChartHistogram2D.h>
#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
#include <vtkTextProperty.h>
#include <vtkPen.h>
#include <vtkPlotLine.h>

#include <QGridLayout>
#include <QVTKWidget.h>
#include <QGroupBox>
#include <QRadioButton>
#include <QHBoxLayout>
#include <QDebug>

#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <mainwindow_charts.hpp>
#include <tet_mscomplex.hpp>

#define VTK_CREATE(class, variable)\
  vtkSmartPointer<class> variable = vtkSmartPointer<class>::New();

using namespace tet;

const unsigned char ub_red   [] = {255,0,0};
const unsigned char ub_green [] = {0,255,0};
const unsigned char ub_blue  [] = {0,0,255};
const unsigned char ub_yellow[] = {255,255,0};
const unsigned char ub_cyan  [] = {0,255,255};
const unsigned char ub_gray  [] = {32,32,32};


/*********************************************************************/

ChartMatrix::ChartMatrix(QWidget *p,Qt::WindowFlags f):QWidget(p,f)
{
  gridl = new QGridLayout(this);
  this->setLayout(gridl);
}

/*-------------------------------------------------------------------*/

void ChartMatrix::addChart
  (vtkSmartPointer<vtkChart> chart,int r,int c,bool sync)
{
  VTK_CREATE(vtkContextView,view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  view->GetScene()->AddItem(chart);

  QVTKWidget *widget = new QVTKWidget;
  gridl->addWidget(widget,r,c);
  view->SetInteractor(widget->GetInteractor());
  widget->SetRenderWindow(view->GetRenderWindow());

  chart_data_t cd;
  cd.chart  = chart;
  cd.widget = widget;
  chart_data.push_back(cd);

  if(sync)
  {
    chart->AddObserver(vtkChart::UpdateRange,this,
                       &ChartMatrix::ranges_changed);
    synced_charts.push_back(chart_data.size()-1);
  }
}

/*-------------------------------------------------------------------*/

void ChartMatrix::ranges_changed(vtkObject *cobj,unsigned long ,void *)
{
  vtkChart *schart = dynamic_cast<vtkChart*>(cobj);

  static bool chart_range_update_in_progress = false;

  if(chart_range_update_in_progress)
    return;

  chart_range_update_in_progress = true;

  double xrng[2],yrng[2];

  schart->GetAxis(vtkAxis::BOTTOM)->GetRange(xrng);
  schart->GetAxis(vtkAxis::LEFT)->GetRange(yrng);

  BOOST_FOREACH(int i, synced_charts)
  {
    vtkSmartPointer<vtkChart> chart = chart_data[i].chart;
    QVTKWidget  *widget             = chart_data[i].widget;

    double oxrng[2],oyrng[2];

    chart->GetAxis(vtkAxis::BOTTOM)->GetRange(oxrng);
    chart->GetAxis(vtkAxis::LEFT)->GetRange(oyrng);

    if(oxrng[0] != xrng[0] || oxrng[1] != xrng[1])
      chart->GetAxis(vtkAxis::BOTTOM)->SetRange(xrng);

    if(oyrng[0] != yrng[0] || oyrng[1] != yrng[1])
      chart->GetAxis(vtkAxis::LEFT)->SetRange(yrng);

    widget->GetRenderWindow()->Render();
  }

  chart_range_update_in_progress = false;

  return;
}

/*-------------------------------------------------------------------*/

void ChartMatrix::vtkRender()
{
  for(int i = 0 ; i < chart_data.size(); ++i)
  {
    QVTKWidget  *widget             = chart_data[i].widget;
    widget->GetRenderWindow()->Render();
  }
}

/*********************************************************************/





/*********************************************************************/

PersistenceCharts::PersistenceCharts(QWidget *p, Qt::WindowFlags f)
  :ChartMatrix(p,f),m_selection_mode(SELECTION_ADDITION),m_show_unpaired_cps(true)
{
  QGroupBox *groupBox = new QGroupBox(tr("Selection Mode"));

  QRadioButton *radio1 = new QRadioButton(tr("&None"));
  QRadioButton *radio2 = new QRadioButton(tr("&Addition"));
  QRadioButton *radio3 = new QRadioButton(tr("&Subtraction"));
  QRadioButton *radio4 = new QRadioButton(tr("&Toggle"));

  connect(radio1,SIGNAL(clicked()),this,SLOT(none_clicked()));
  connect(radio2,SIGNAL(clicked()),this,SLOT(addition_clicked()));
  connect(radio3,SIGNAL(clicked()),this,SLOT(subtraction_clicked()));
  connect(radio4,SIGNAL(clicked()),this,SLOT(toggle_clicked()));

  QHBoxLayout *hbox = new QHBoxLayout;
  hbox->addWidget(radio1);
  hbox->addWidget(radio2);
  hbox->addWidget(radio3);
  hbox->addWidget(radio4);

  radio2->setChecked(true);

  groupBox->setLayout(hbox);

  gridl->addWidget(groupBox,0,0,1,2);

  for(int i = 0 ; i < 4; ++i)
  {
    VTK_CREATE(vtkChartXY, chart);

    chart->SetActionToButton(vtkChart::PAN,vtkContextMouseEvent::LEFT_BUTTON);
    chart->SetActionToButton(vtkChart::SELECT,vtkContextMouseEvent::RIGHT_BUTTON);

    chart->AddObserver(vtkCommand::SelectionChangedEvent,this,
                       &PersistenceCharts::pers_end_selection);

    addChart(chart,i/2+1,i%2);
  }

  set_show_axis_labels(true);
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::create(tet::mscomplex_cptr_t msc)
{
  m_selected_cps.clear();
  m_selected_cps_MTime.Modified();
  m_pers_pts.clear();
  m_cp_pers_pt.clear();

  m_msc = msc;

  vtkSmartPointer<vtkTable>       *tables = m_pers_pts_tables;
  vtkSmartPointer<vtkStringArray> *labels = m_pers_pts_labels;
  vtkSmartPointer<vtkPlotPoints>  *points = m_pers_pts_plotpts;

  const unsigned char *pt_color[] =
  {ub_blue,ub_green,ub_red,ub_gray,ub_gray,ub_gray};

  const int pt_marker_style[] =
  {vtkPlotPoints::CIRCLE,vtkPlotPoints::CIRCLE,vtkPlotPoints::CIRCLE,
   vtkPlotPoints::PLUS,vtkPlotPoints::PLUS,vtkPlotPoints::PLUS
  };

  const double pt_marker_size[] = {4,4,4,8,8,8};

  br::copy(m_msc->m_canc_list,std::back_inserter(m_pers_pts));

  for(int i = 0 ; i < m_msc->get_num_critpts(); ++i)
    if(m_msc->is_not_paired(i))
      m_pers_pts.push_back(std::make_pair(-1,i));

  m_cp_pers_pt.resize(m_msc->get_num_critpts(),-1);

  for(int i = 0 ; i < m_pers_pts.size(); ++i)
  {
    int p = m_pers_pts[i].first;
    int q = m_pers_pts[i].second;

    if( p != -1)
      m_cp_pers_pt[p] = i;

    m_cp_pers_pt[q] = i;
  }

  for( int i = 0 ; i < 6; ++i)
  {
    VTK_CREATE(vtkFloatArray,arr_p);
    VTK_CREATE(vtkFloatArray,arr_q);

    arr_p->SetName("p");
    arr_q->SetName("q");

    tables[i] = vtkSmartPointer<vtkTable>::New();
    labels[i] = vtkSmartPointer<vtkStringArray>::New();
    tables[i]->AddColumn(arr_p);
    tables[i]->AddColumn(arr_q);

    points[i] = vtkSmartPointer<vtkPlotPoints>::New();
#if VTK_MAJOR_VERSION <= 5
    points[i]->SetInput(tables[i], 0, 1);
#else
    points[i]->SetInputData(tables[i], 0, 1);
#endif
    points[i]->SetColor(pt_color[i][0],pt_color[i][1],pt_color[i][2],255);
    points[i]->SetTooltipLabelFormat("%i [%x,%y]");
    points[i]->SetIndexedLabels(labels[i]);
    points[i]->SetMarkerStyle(pt_marker_style[i]);
    points[i]->SetMarkerSize(pt_marker_size[i]);
  }

//  m_pers_chart_matrix = new ChartMatrix(ui->pers_tabWidget);
//  ui->pers_tabWidget->addTab(m_pers_chart_matrix,tr("Pers"));

  for(int i = 0 ; i < 4; ++i)
  {
    vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(i).chart);

    int nplots = chart->GetNumberOfPlots();

    for(int j = 0 ; j < nplots; ++j)
      chart->RemovePlot(0);

    if(i < 3)
    {
      chart->AddPlot(points[i]);
      chart->AddPlot(points[i+3]);
    }
    else
    {
      for(int j = 0 ; j < 6; ++j)
        chart->AddPlot(points[j]);
    }
  }

  double rng[] = {msc->m_fmin,msc->m_fmax};

  vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(0).chart);

  chart->GetAxis(vtkAxis::BOTTOM)->SetRange(rng);
  chart->GetAxis(vtkAxis::LEFT)->SetRange(rng);

  create_diagonal();
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::create_diagonal()
{
  VTK_CREATE(vtkFloatArray,xcol);
  VTK_CREATE(vtkFloatArray,ycol);

  xcol->InsertNextValue(m_msc->m_fmin);
  ycol->InsertNextValue(m_msc->m_fmin);

  xcol->InsertNextValue(m_msc->m_fmax);
  ycol->InsertNextValue(m_msc->m_fmax);


  VTK_CREATE(vtkTable,cornerpts);
  cornerpts->AddColumn(xcol);
  cornerpts->AddColumn(ycol);

  xcol->SetName("p");
  ycol->SetName("q");

  VTK_CREATE(vtkPlotLine,diag)

#if VTK_MAJOR_VERSION <= 5
  diag->SetInput(cornerpts,0,1);
#else
  diag->SetInputData(cornerpts,0,1);
#endif

  diag->SetColor(0.5,0.5,0.5);

  for(int i = 0 ; i< 4 ; ++i)
  {
    vtkChartXY * chart = vtkChartXY::SafeDownCast
        (chart_data.at(i).chart);

    chart->AddPlot(diag);
  }

  m_pers_diagonal = diag;
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::update()
{
  update_display();
//  update_selection_display();
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::set_show_unpaired_cps(bool val)
{
  if(val != m_show_unpaired_cps)
  {
    m_show_unpaired_cps = val;
    update();
  }
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::set_show_axis_labels(bool val)
{
  if(val)
  {
    for(int i = 0 ; i < 3; ++i)
    {
      vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(i).chart);
      chart->GetAxis(vtkAxis::BOTTOM)->SetTitle(g_cp_type_names[i]);
      chart->GetAxis(vtkAxis::LEFT)->SetTitle(g_cp_type_names[i+1]);
    }

    vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(3).chart);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("min-1saddle-2saddle");
    chart->GetAxis(vtkAxis::LEFT)->SetTitle("1saddle-2saddle-max");
  }
  else
  {
    for(int i = 0 ; i < 4; ++i)
    {
      vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(i).chart);
      chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("");
      chart->GetAxis(vtkAxis::LEFT)->SetTitle("");
    }
  }

}

/*-------------------------------------------------------------------*/

void PersistenceCharts::set_font_size(int s)
{
  for(int i = 0 ; i < 4; ++i)
  {
    vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(i).chart);
    chart->GetAxis(vtkAxis::BOTTOM)->GetLabelProperties()->SetFontSize(s);
    chart->GetAxis(vtkAxis::LEFT)->GetLabelProperties()->SetFontSize(s);
    chart->GetAxis(vtkAxis::BOTTOM)->GetLabelProperties()->SetFontFamilyToArial();
    chart->GetAxis(vtkAxis::LEFT)->GetLabelProperties()->SetFontFamilyToArial();
  }
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::set_line_size(double s)
{
  for(int i = 0 ; i < 4; ++i)
  {
    vtkChartXY * chart = vtkChartXY::SafeDownCast(chart_data.at(i).chart);
    chart->GetAxis(vtkAxis::BOTTOM)->GetPen()->SetWidth(s);
    chart->GetAxis(vtkAxis::LEFT)->GetPen()->SetWidth(s);
  }

  m_pers_diagonal->SetWidth(s);
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::set_point_size(double s)
{
  for(int i = 0 ; i < 6; ++i)
  {
    m_pers_pts_plotpts[i]->SetMarkerStyle(vtkPlotPoints::CIRCLE);
    m_pers_pts_plotpts[i]->SetMarkerSize(s);
  }
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::update_display()
{
  vtkSmartPointer<vtkTable>       *tables = m_pers_pts_tables;
  vtkSmartPointer<vtkStringArray> *labels = m_pers_pts_labels;

  for(int i = 0 ; i < 3; ++i)
  {
    tables[i]->SetNumberOfRows(0);
    labels[i]->Resize(0);
    tables[i]->Modified();
    labels[i]->Modified();
  }

  for (int i = m_cutoff; i < m_pers_pts.size(); ++i)
  {
    int_pair_t pr = m_pers_pts[i];

    if(pr.first == -1 && !m_show_unpaired_cps)
      continue;

    int tidx = std::min<int>(m_msc->index(pr.second),2);
    int rno  = tables[tidx]->InsertNextBlankRow();

    fn_t x = m_msc->fn(pr.second);
    fn_t y = (pr.first == -1)?(m_msc->m_fmax):(m_msc->fn(pr.first));

    tables[tidx]->SetValue(rno,0,x);
    tables[tidx]->SetValue(rno,1,y);
    labels[tidx]->InsertNextValue(utl::to_string(i));
  }

  update_selection_display();
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::update_selection_display()
{
  vtkSmartPointer<vtkTable>       *tables = m_pers_pts_tables;
  vtkSmartPointer<vtkStringArray> *labels = m_pers_pts_labels;

  for(int i = 3 ; i < 6; ++i)
  {
    tables[i]->SetNumberOfRows(0);
    labels[i]->Resize(0);
    tables[i]->Modified();
    labels[i]->Modified();
  }

  BOOST_FOREACH(int_pair_t s_pr, m_selected_cps)
  {
    int pers_pt = m_cp_pers_pt[s_pr.first];

    if(!is_in_range(pers_pt,m_cutoff,m_pers_pts.size()))
      continue;

    int_pair_t pr = m_pers_pts[pers_pt];

    if(pr.first == -1 && !m_show_unpaired_cps)
      continue;

    int tidx = 3 + std::min<int>(m_msc->index(pr.second),2);
    int rno  = tables[tidx]->InsertNextBlankRow();

    fn_t x = m_msc->fn(pr.second);
    fn_t y = (pr.first == -1)?(m_msc->m_fmax):(m_msc->fn(pr.first));

    tables[tidx]->SetValue(rno,0,x);
    tables[tidx]->SetValue(rno,1,y);
    labels[tidx]->InsertNextValue(utl::to_string(pr.first));
  }

  vtkRender();
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::pers_end_selection
  (vtkObject *caller,unsigned long ,void *)
{
  vtkChartXY *chart = dynamic_cast<vtkChartXY*>(caller);

  for(int i = 0 ; i < chart->GetNumberOfPlots()/2; ++i)
  {
    vtkPlotPoints  *plt = dynamic_cast<vtkPlotPoints*>
        (chart->GetPlot(i));
    vtkIdTypeArray *sel = plt->GetSelection();

    for(int j = 0 ; j < sel->GetSize(); ++j)
    {
      vtkStdString str =  plt->GetIndexedLabels()->
          GetValue(sel->GetValue(j));

      _select_pers_pt(atoi(str.c_str()));
    }
  }

  for(int i = 0 ; i < chart->GetNumberOfPlots(); ++i)
  {
    chart->GetPlot(i)->GetSelection()->Resize(0);
  }

  m_selected_cps_MTime.Modified();

  update_selection_display();
}

/*-------------------------------------------------------------------*/

bool PersistenceCharts::_select_pers_pt(int i, int pres,int qres)
{
  int_pair_t pr = m_pers_pts[i];

  bool pin=false,qin=false;

  if( pr.first == -1) pr.first = pr.second;

  ASSERT(pr.first >=0 && pr.second >=0);

  switch(m_selection_mode)
  {
  case SELECTION_ADDITION:
  {
    BOOST_AUTO(pret,m_selected_cps.insert(int_pair_t(pr.first,pres)));
    BOOST_AUTO(qret,m_selected_cps.insert(int_pair_t(pr.second,qres)));

    pret.first->second = pres;
    qret.first->second = qres;

    pin = pret.second;
    qin = qret.second;
    break;
  }
  case SELECTION_SUBTRACTION:
    pin = m_selected_cps.erase(pr.first) != 0;
    qin = m_selected_cps.erase(pr.second) != 0;
    break;
  case SELECTION_TOGGLE:
//    // if either is not in the put both in .. otherwise toss em both.
//    if(m_selected_cps.count(pr.first)  == 0 ||
//        m_selected_cps.count(pr.second) == 0)
//    {
//      pin = m_selected_cps.insert(pr.first).second;
//      qin = m_selected_cps.insert(pr.second).second;
//    }
//    else
//    {
//      pin = m_selected_cps.erase(pr.first) != 0;
//      qin = m_selected_cps.erase(pr.second) != 0;
//    }
    break;
  }

  return pin || qin;
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::select_pers_pts_in_rect
(double l,double b, double w, double h)
{
  QRectF r(l,b,w,h);

  for(int i = m_cutoff ; i< m_pers_pts.size();++i)
  {
    int_pair_t pr = m_pers_pts[i];

    fn_t x = m_msc->fn(pr.second);
    fn_t y = (pr.first == -1)?(m_msc->m_fmax):(m_msc->fn(pr.first));

    if(r.contains(x,y))
      _select_pers_pt(i);
  }

  m_selected_cps_MTime.Modified();

  update_selection_display();
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::select_pers_pts_above_line
(double a, double b, double c)
{
  for(int i = m_cutoff ; i< m_pers_pts.size();++i)
  {
    int_pair_t pr = m_pers_pts[i];

    fn_t x = m_msc->fn(pr.second);
    fn_t y = (pr.first == -1)?(m_msc->m_fmax):(m_msc->fn(pr.first));

    if(a*x + b*y + c >= 0 )
      _select_pers_pt(i);
  }

  m_selected_cps_MTime.Modified();

  update_selection_display();
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::select_pers_pt(int i)
{
  ENSURE_OR_RETURN(0 <= i && i < m_pers_pts.size()) <<"value out of range";

  if(_select_pers_pt(i))
  {
    m_selected_cps_MTime.Modified();

    update_selection_display();
  }
}

/*-------------------------------------------------------------------*/

void PersistenceCharts::select_pers_pts_range(double l,double u)
{
//  int nc = m_msc->get_num_critpts();

//  for(int i = 0 ; i < nc; ++i)
//  {
//    if(is_in_range(m_msc->fn(i),l,u) &&
//       is_in_range(m_msc->cancno(i),m_cutoff,nc))
//      _select_pers_pt(m_msc->cancno(i));
//  }

//  m_selected_cps_MTime.Modified();

//  update_selection_display();

}

/*---------------------------------------------------------------------------*/

bool PersistenceCharts::select_cp(int i, int res)
{
  BOOST_AUTO(inret,m_selected_cps.insert(int_pair_t(i,res)));

  if(!inret.second && inret.first->second == res)
    return false;

  inret.first->second = res;

  m_selected_cps_MTime.Modified();
  return true;
}

/*---------------------------------------------------------------------------*/

bool PersistenceCharts::deselect_cp(int i)
{
  if(m_selected_cps.erase(i) == 0)
    return false;

  m_selected_cps_MTime.Modified();
  return true;
}

/*---------------------------------------------------------------------------*/

void PersistenceCharts::deselect_all_cps()
{
  m_selected_cps.clear();
  m_selected_cps_MTime.Modified();
}

/*---------------------------------------------------------------------------*/

void PersistenceCharts::save_screenshot(QString  fn_qt,int mag) const
{
  VTK_CREATE(vtkWindowToImageFilter,windowToImageFilter);

  std::string fn = fn_qt.toStdString();

  if(fn_qt.endsWith(".png",Qt::CaseInsensitive))
    fn= fn.substr(0,fn.size()-4);

  if(mag != 1)
  {
    qWarning()<<__func__<<"\n"
    <<"Magnification does not behave well with charts"<<"\n"
    <<"Ignoring Mag";
  }

  for(int i = 0 ; i < this->chart_data.size(); ++i)
  {
    const chart_data_t &cd = chart_data[i];

    windowToImageFilter->SetInput(cd.widget->GetRenderWindow());
//    windowToImageFilter->SetMagnification(mag);
    windowToImageFilter->SetInputBufferTypeToRGBA();
    windowToImageFilter->Update();

    std::string fni = (fn + utl::to_string<int>(i)+".png");

    VTK_CREATE(vtkPNGWriter,writer);
    writer->SetFileName(fni.c_str());
    writer->SetInputConnection(windowToImageFilter->GetOutputPort());
    writer->Write();

    // The rendered view gets messed up if you use magnifications
    cd.widget->GetRenderWindow()->Render();
  }
}

/*---------------------------------------------------------------------------*/

QList<int> PersistenceCharts::get_selected_dcps(int d) const
{
  QList<int> ret;

  br::copy(get_selected_cps()|badpt::map_keys|
           badpt::filtered(bind(&mscomplex_t::is_index_i_cp,m_msc,d,_1)),
           std::back_inserter(ret));

  return ret;
}


/*********************************************************************/




/*********************************************************************/

const int g_histX = 50;
const int g_histY = 50;

/*-------------------------------------------------------------------*/

PersistenceHistogramCharts::PersistenceHistogramCharts
(QWidget *p, Qt::WindowFlags f)
  :ChartMatrix(p,f){}

/*-------------------------------------------------------------------*/

void PersistenceHistogramCharts::create(tet::mscomplex_ptr_t msc)
{
  m_msc = msc;

  for( int i = 0 ; i < 3; ++i)
  {
    VTK_CREATE(vtkImageData,hist);
    hist->SetExtent(0, g_histX-1, 0, g_histY-1, 0, 0);    
#if VTK_MAJOR_VERSION <= 5
    hist->SetNumberOfScalarComponents(1);
    hist->SetScalarTypeToDouble();
    hist->AllocateScalars();
#else
    hist->AllocateScalars(VTK_DOUBLE,1);
#endif
    hist->SetOrigin(m_msc->m_fmin, m_msc->m_fmin, 0.0);
    hist->SetSpacing((m_msc->m_fmax-m_msc->m_fmin)/(g_histX),
                     (m_msc->m_fmax-m_msc->m_fmin)/(g_histY),1.0);

    VTK_CREATE(vtkColorTransferFunction, tf);
    tf->AddRGBPoint(0,0,0,0);
    tf->AddRGBPoint(1,1,1,1);

    VTK_CREATE(vtkChartHistogram2D, chart);
#if VTK_MAJOR_VERSION <= 5
    chart->SetInput(hist);
#else
    chart->SetInputData(hist);
#endif
    chart->SetTransferFunction(tf);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle(g_cp_type_names[i]);
    chart->GetAxis(vtkAxis::LEFT)->SetTitle(g_cp_type_names[i+1]);

    addChart(chart,i/2,i%2);

    m_pers_hist_data[i].hist   = hist;
    m_pers_hist_data[i].tf     = tf;
  }
}

/*-------------------------------------------------------------------*/

void PersistenceHistogramCharts::update_cutoff(int cutoff)
{
  m_cutoff = cutoff;

  double *dptrs[3];
  double  dmax[3] = {0,0,0};

  for(int i = 0 ; i < 3; ++i)
  {
    pers_hist_data_t &hd = m_pers_hist_data[i];
    dptrs[i] = static_cast<double *>(hd.hist->GetScalarPointer(0,0,0));
    std::fill_n(dptrs[i],g_histX*g_histY,0);
  }

  for( int i = m_cutoff; i < m_msc->m_canc_list.size(); ++i)
  {
    int_pair_t pr = m_msc->m_canc_list[i];

    fn_t f1 = m_msc->fn(pr.second);
    fn_t f2 = m_msc->fn(pr.first);

    int bx  = ((f1 - m_msc->m_fmin)/(m_msc->m_fmax- m_msc->m_fmin))*
        double(g_histX);
    int by  = ((f2 - m_msc->m_fmin)/(m_msc->m_fmax- m_msc->m_fmin))*
        double(g_histY);

    bx      = std::min(bx,g_histX-1);
    by      = std::min(by,g_histY-1);

    int didx = m_msc->index(pr.second);

    dptrs[didx][bx + by*g_histX] += 1;
    dmax[didx] = std::max(dmax[didx],dptrs[didx][bx + by*g_histX]);
  }

  for(int i = 0 ; i < 3; ++i)
  {
    pers_hist_data_t &hd = m_pers_hist_data[i];

    hd.tf->RemoveAllPoints();
    hd.tf->AddRGBPoint(          0, 1,1,1);
    hd.tf->AddRGBPoint(dmax[i]*1/3, 1,1,0);
    hd.tf->AddRGBPoint(dmax[i]*2/3, 1,0,0);
    hd.tf->AddRGBPoint(dmax[i]    , 0,0,0);

    hd.tf->Build();
    hd.tf->Modified();

    hd.hist->Modified();
  }

  vtkRender();
}

/*********************************************************************/



/*********************************************************************/
#include <vtkIntArray.h>
#include <vtkPlotLine.h>

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>

/*-------------------------------------------------------------------*/

PersistentBettiNumbersPlot::PersistentBettiNumbersPlot
(QWidget *p, Qt::WindowFlags f)
  :QVTKWidget(p,f){}

/*-------------------------------------------------------------------*/

void PersistentBettiNumbersPlot::create(tet::mscomplex_ptr_t msc)
{
  m_msc = msc;

  const unsigned char *color[] =
  {ub_blue,ub_green,ub_red};
  const char *label[]={"Betti 0","Betti 1","Betti 2"};

  vtkSmartPointer<vtkTable> *tables = m_pers_betti_tables;

  VTK_CREATE(vtkChartXY,chart);
  chart->SetActionToButton
      (vtkChart::PAN,vtkContextMouseEvent::LEFT_BUTTON);
  chart->SetActionToButton
      (vtkChart::SELECT,vtkContextMouseEvent::RIGHT_BUTTON);

  for( int i = 0 ; i < 3; ++i)
  {
    VTK_CREATE(vtkFloatArray,arr_fn);
    VTK_CREATE(vtkIntArray,arr_binct);

    arr_fn->SetName("fn");
    arr_binct->SetName("Betti");

    tables[i] = vtkSmartPointer<vtkTable>::New();
    tables[i]->AddColumn(arr_fn);
    tables[i]->AddColumn(arr_binct);

    VTK_CREATE(vtkPlotLine, lineplot);
#if VTK_MAJOR_VERSION <= 5
    lineplot->SetInput(tables[i], 0, 1);
#else
    lineplot->SetInputData(tables[i], 0, 1);
#endif
    lineplot->SetColor(color[i][0],color[i][1],color[i][2],255);
    lineplot->SetLabel(label[i]);

    chart->AddPlot(lineplot);
  }

  chart->SetShowLegend(true);
  chart->GetAxis(vtkAxis::LEFT)->SetTitle("Betti No");
  chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Function");

  VTK_CREATE(vtkContextView,view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  view->GetScene()->AddItem(chart);
  view->SetInteractor(this->GetInteractor());
  SetRenderWindow(view->GetRenderWindow());
}

/*-------------------------------------------------------------------*/

void PersistentBettiNumbersPlot::update_cutoff(int cutoff)
{
  m_cutoff = cutoff;

  typedef boost::tuples::tuple<fn_t,int,bool,int> flt_event_t;

  std::vector<flt_event_t> flt_events;

  vtkSmartPointer<vtkTable> *tables = m_pers_betti_tables;

  for(int i = 0 ; i < 3; ++i)
  {
    tables[i]->SetNumberOfRows(0);
    tables[i]->Modified();
  }

  for( int i = m_cutoff; i<m_msc->m_canc_list.size() ; ++i)
  {
    int_pair_t pr = m_msc->m_canc_list[i];

    int  blwr = m_msc->index(pr.second);

    fn_t flwr = m_msc->fn(pr.second);
    fn_t fupr = m_msc->fn(pr.first);

    flt_events.push_back(boost::tuples::make_tuple(flwr,blwr,true,i));
    flt_events.push_back(boost::tuples::make_tuple(fupr,blwr,false,i));
  }

  int betti[] = {0,0,0};

  br::sort(flt_events);

  BOOST_FOREACH(flt_event_t &evt,flt_events)
  {
    fn_t f   = boost::tuples::get<0>(evt);
    int  b   = boost::tuples::get<1>(evt);
    bool inc = boost::tuples::get<2>(evt);

    if(inc)
      ++betti[b];
    else
      --betti[b];

    int rno = tables[b]->InsertNextBlankRow();
    tables[b]->SetValue(rno,0,f);
    tables[b]->SetValue(rno,1,betti[b]);
  }

  GetRenderWindow()->Render();
}

/*********************************************************************/



/*********************************************************************/

#include <vtkPlotBar.h>

const int g_num_hist_bins = 50;

CriticalPointsHistograms::CriticalPointsHistograms
(QWidget *p, Qt::WindowFlags f)
  :ChartMatrix(p,f){}

/*-------------------------------------------------------------------*/

void CriticalPointsHistograms::create(tet::mscomplex_ptr_t msc)
{
  m_msc = msc;

  const unsigned char *stat_color[] =
  {ub_blue,ub_green,ub_yellow,ub_red,ub_gray,};

  vtkSmartPointer<vtkTable> *tables = m_cp_hist_tables;

  for( int i = 0 ; i < 5; ++i)
  {
    VTK_CREATE(vtkFloatArray,arr_fn);
    VTK_CREATE(vtkIntArray,arr_binct);

    arr_fn->SetName("fn");
    arr_binct->SetName("bin count");

    tables[i] = vtkSmartPointer<vtkTable>::New();
    tables[i]->AddColumn(arr_fn);
    tables[i]->AddColumn(arr_binct);
    tables[i]->SetNumberOfRows(g_num_hist_bins);

    VTK_CREATE(vtkPlotBar, points);
#if VTK_MAJOR_VERSION <= 5
    points->SetInput(tables[i], 0, 1);
#else
    points->SetInputData(tables[i], 0, 1);
#endif
    points->SetColor(stat_color[i][0],stat_color[i][1],stat_color[i][2],255);
    points->SetLabel(g_cp_type_names[i]);

    VTK_CREATE(vtkChartXY,chart);
    chart->AddPlot(points);
    chart->SetActionToButton
        (vtkChart::PAN,vtkContextMouseEvent::LEFT_BUTTON);
    chart->SetActionToButton
        (vtkChart::SELECT,vtkContextMouseEvent::RIGHT_BUTTON);
    chart->GetAxis(vtkAxis::LEFT)->SetTitle(g_cp_type_names[i]);
    chart->GetAxis(vtkAxis::BOTTOM)->SetTitle("Function");

    addChart(chart,i/2,i%2,false);
  }



}

/*-------------------------------------------------------------------*/

void CriticalPointsHistograms::update_cutoff(int cutoff)
{
  m_cutoff = cutoff;

  int bin_ct[5][g_num_hist_bins];
  std::fill_n(bin_ct[0],g_num_hist_bins*5,0);

  fn_t bin_width = (m_msc->m_fmax - m_msc->m_fmin)/
      fn_t(g_num_hist_bins);

  for(int i = 0 ; i < m_msc->get_num_critpts(); ++i)
  {
    if(m_msc->is_canceled(i))
      continue;

    int  idx   = m_msc->index(i);
    fn_t binno = (m_msc->fn(i) - m_msc->m_fmin)/bin_width;

    bin_ct[idx][int(binno)] +=1;
    bin_ct[4][int(binno)] +=1;
  }

  for( int i = 0 ; i < 5; ++i)
  {
    for( int j = 0 ; j < g_num_hist_bins; ++j)
    {
      m_cp_hist_tables[i]->SetValue(j,0,m_msc->m_fmin + bin_width*j);
      m_cp_hist_tables[i]->SetValue(j,1,bin_ct[i][j]);
    }
    m_cp_hist_tables[i]->Modified();
  }

  vtkRender();
}

/*********************************************************************/
