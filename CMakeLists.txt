#Define the project we are working on
project(mscomplex-tet)

# Look for various software we need
cmake_minimum_required(VERSION 2.8.3)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# Common Properties
set(LOG_LEVEL 2 CACHE STRING
      "log level value trace=0,debug=1,info=2,warning=3,error=4,fatal=5")

set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -fpic -DUTL_LOG_LEVEL=${LOG_LEVEL}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpic -DUTL_LOG_LEVEL=${LOG_LEVEL}")

# Handle installation paths
set(INSTALL_DIR "" CACHE PATH "installation dir")

if(INSTALL_DIR)
  set(INSTALL_DIR_BIN   ${INSTALL_DIR})
  set(INSTALL_DIR_LIB   ${INSTALL_DIR})
  set(INSTALL_DIR_SHARE ${INSTALL_DIR})
else(INSTALL_DIR)
  set(INSTALL_DIR_BIN   bin)
  set(INSTALL_DIR_LIB   lib)
  set(INSTALL_DIR_SHARE share/mscomplex-tet)
endif(INSTALL_DIR)

add_subdirectory(core)

option(BUILD_PYMSTET "pymstet python module" OFF)
if(BUILD_PYMSTET OR BUILD_VTK_VIEWER)
add_subdirectory(pymstet)
endif()

option(BUILD_VTK_VIEWER "build the mscomplex viewer" OFF)
if(BUILD_VTK_VIEWER)
add_subdirectory(vtk_viewer)
endif(BUILD_VTK_VIEWER)

option(BUILD_DTFE_MESHER "build the dtfe tet mesh builder" OFF)
if(BUILD_DTFE_MESHER)
add_subdirectory(dtfe)
endif(BUILD_DTFE_MESHER)

option(BUILD_QML_VTK_VIEWER "build the mscomplex viewer" OFF)
if(BUILD_QML_VTK_VIEWER)
add_subdirectory(qmlVtkViewer)
endif(BUILD_QML_VTK_VIEWER)



option(BUILD_DOCS "generate project documentation " OFF)
if(BUILD_DOCS)
add_subdirectory(doxygen)
endif(BUILD_DOCS)


# option(BUILD_SUBCOMPLEX_EXTRACTOR "a tool to extract subcomplexes from a tetra" OFF)
# 
# if(BUILD_SUBCOMPLEX_EXTRACTOR)
# include(subcomplex/subcomplex.cmake)
# endif(BUILD_SUBCOMPLEX_EXTRACTOR)


