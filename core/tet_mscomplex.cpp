#include <cmath>
#include <queue>
#include <limits>
#include <numeric>

#include <boost/typeof/typeof.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/assign.hpp>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <tet_mscomplex.hpp>
#include <tet_dataset.hpp>

using namespace std;

namespace ba = boost::adaptors;

/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/

mscomplex_t::mscomplex_t()
  :m_des_conn(m_conn[0]),m_asc_conn(m_conn[1]),
    m_des_mfolds(m_mfolds[0]),m_asc_mfolds(m_mfolds[1]){init();}

/*---------------------------------------------------------------------------*/

mscomplex_t::~mscomplex_t(){init();}

/*---------------------------------------------------------------------------*/

void mscomplex_t::set_critpt(int i, cellid_t c, char idx, fn_t f, cellid_t v, bool b)
{
  m_cp_cellid[i]     = c;
  m_cp_vertid[i]     = v;
  m_cp_index[i]      = idx;
  m_cp_fn[i]         = f;
  m_cp_is_boundry[i] = b;

  m_fmax = std::max(f,m_fmax);
  m_fmin = std::min(f,m_fmin);
}

/*---------------------------------------------------------------------------*/

void  mscomplex_t::init(int n)
{
  m_grad_conv = DES;

  m_cp_cellid.clear();
  m_cp_vertid.clear();
  m_cp_pair_idx.clear();
  m_cp_index.clear();
  m_cp_is_boundry.clear();
  m_cp_is_cancelled.clear();
  m_cp_fn.clear();
  m_cp_hversion.clear();

  m_fmax = std::numeric_limits<fn_t>::min();
  m_fmin = std::numeric_limits<fn_t>::max();

  m_hversion = 0;
  m_canc_list.clear();

  for(int dir = 0; dir < GDIR_CT; ++ dir)
    for(int dim = 0 ; dim <= 3 ;++ dim)
      m_geom_hversion[dir][dim] = get_hversion();

  m_des_conn.clear();
  m_asc_conn.clear();

  m_des_mfolds.clear();
  m_asc_mfolds.clear();

  m_merge_dag.reset(new merge_dag_t);

  if(n !=0 )
  {
    m_cp_cellid.resize(n,invalid_cellid);
    m_cp_vertid.resize(n,invalid_cellid);
    m_cp_pair_idx.resize(n,-1);
    m_cp_index.resize(n,-1);
    m_cp_is_boundry.resize(n,false);
    m_cp_is_cancelled.resize(n,false);
    m_cp_fn.resize(n);
    m_cp_hversion.resize(n,n);

    m_des_conn.resize(n);
    m_asc_conn.resize(n);

    m_asc_mfolds.resize(n);
    m_des_mfolds.resize(n);

    m_merge_dag->init(n);
  }
}

/*---------------------------------------------------------------------------*/

mscomplex_t::mscomplex_t(const mscomplex_t &o):
  m_des_conn(m_conn[0]),m_asc_conn(m_conn[1]),
  m_des_mfolds(m_mfolds[0]),m_asc_mfolds(m_mfolds[1])
{
//  init(o.get_num_critpts());

//  br::copy(o.m_cp_cellid,m_cp_cellid.begin());
//  br::copy(o.m_cp_vertid,m_cp_vertid.begin());
//  br::copy(o.m_cp_pair_idx,m_cp_pair_idx.begin());
//  br::copy(o.m_cp_index,m_cp_index.begin());
//  br::copy(o.m_cp_is_boundry,m_cp_is_boundry.begin());
//  br::copy(o.m_cp_is_cancelled,m_cp_is_cancelled.begin());
//  br::copy(o.m_cp_fn,m_cp_fn.begin());
//  br::copy(o.m_cp_hversion,m_cp_hversion.begin());

//  br::copy(o.m_des_conn,m_des_conn.begin());
//  br::copy(o.m_asc_conn,m_asc_conn.begin());

//  m_canc_list.resize(o.m_canc_list.size());
////  m_canc_dep_tree.resize(o.m_canc_dep_tree.size());

//  br::copy(o.m_canc_list,m_canc_list.begin());
////  br::copy(o.m_canc_dep_tree,m_canc_dep_tree.begin());

//  br::copy(o.m_des_mfolds,m_des_mfolds.begin());
//  br::copy(o.m_asc_mfolds,m_asc_mfolds.begin());

//  m_fmax = o.m_fmax;
//  m_fmin = o.m_fmin;
  ENSURE(false,"Very buggy !! fix and then use");
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::save_bin(ostream &os) const
{
  utl::bin_write(os,m_grad_conv);
  utl::bin_write(os,m_fmax);
  utl::bin_write(os,m_fmin);

  utl::bin_write_vec(os,m_cp_cellid);
  utl::bin_write_vec(os,m_cp_vertid);
  utl::bin_write_vec(os,m_cp_pair_idx);
  utl::bin_write_vec(os,m_cp_index);
  utl::bin_write_vec(os,m_cp_is_boundry);
  utl::bin_write_vec(os,m_cp_is_cancelled);
  utl::bin_write_vec(os,m_cp_fn);
  utl::bin_write_vec(os,m_cp_hversion);

  int N = m_cp_cellid.size();

  int_list_t nconn(2*N);
  int_pair_list_t adj;

  for(int i = 0 ; i < N; ++i)
  {
    nconn[2*i]   = m_des_conn[i].size();
    nconn[2*i+1] = m_asc_conn[i].size();

    br::copy(m_des_conn[i],back_inserter(adj));
    br::copy(m_asc_conn[i],back_inserter(adj));
  }

  utl::bin_write_vec(os,nconn);
  utl::bin_write_vec(os,adj);

  utl::bin_write(os,m_hversion);
  utl::bin_write_vec(os,m_canc_list);
  utl::bin_write(os,m_geom_hversion);

  for(int i = 0 ; i < N; ++i)
  {
    utl::bin_write_vec(os,m_des_mfolds[i]);
    utl::bin_write_vec(os,m_asc_mfolds[i]);
  }

  m_merge_dag->save_bin(os);
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::load_bin(istream &is)
{
  init();

  utl::bin_read(is,m_grad_conv);
  utl::bin_read(is,m_fmax);
  utl::bin_read(is,m_fmin);

  utl::bin_read_vec(is,m_cp_cellid);
  utl::bin_read_vec(is,m_cp_vertid);
  utl::bin_read_vec(is,m_cp_pair_idx);
  utl::bin_read_vec(is,m_cp_index);
  utl::bin_read_vec(is,m_cp_is_boundry);
  utl::bin_read_vec(is,m_cp_is_cancelled);
  utl::bin_read_vec(is,m_cp_fn);
  utl::bin_read_vec(is,m_cp_hversion);

  int_list_t      nconn;
  int_pair_list_t adj;

  utl::bin_read_vec(is,nconn);
  utl::bin_read_vec(is,adj);

  int N = m_cp_cellid.size();

  m_des_conn.resize(N);
  m_asc_conn.resize(N);

  int_pair_list_t::iterator a,b,c = adj.begin();

  for(int i = 0 ; i < N; ++i)
  {
    a = c;
    b = a + (nconn[2*i]);
    c = b + (nconn[2*i+1]);

    std::copy(a,b,inserter(m_des_conn[i],m_des_conn[i].begin()));
    std::copy(b,c,inserter(m_asc_conn[i],m_asc_conn[i].begin()));
  }

  utl::bin_read(is,m_hversion);
  utl::bin_read_vec(is,m_canc_list);
  utl::bin_read(is,m_geom_hversion);

  m_des_mfolds.resize(N);
  m_asc_mfolds.resize(N);

  for(int i = 0 ; i < N; ++i)
  {
    utl::bin_read_vec(is,m_des_mfolds[i]);
    utl::bin_read_vec(is,m_asc_mfolds[i]);
  }

  m_merge_dag->load_bin(is);
}

/*---------------------------------------------------------------------------*/

std::string mscomplex_t::cp_conn (int i) const
{
  std::stringstream ss;

  ss<<std::endl<<"des = ";

  br::copy(m_des_conn[i]|ba::map_keys|ba::transformed(bind(&mscomplex_t::cellid,this,_1)),
           ostream_iterator<cellid_t>(ss));

  ss<<std::endl<<"asc = ";

  br::copy(m_asc_conn[i]|ba::map_keys|ba::transformed(bind(&mscomplex_t::cellid,this,_1)),
           ostream_iterator<cellid_t>(ss));

  ss<<std::endl;

  return ss.str();
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::connect_cps(int p, int q, long m)
{
  order_pr_by_cp_index(*this,p,q);

  ENSURES(index(p) == index(q)+1);

  // if a d-cp hits a d+-1 cp and the d+-1 cp is paired
  // then the connection is useful iff the dimension of the pair is d

//  ASSERT(!(is_paired(p) && index(pair_idx(p))!= index(q)));
//  ASSERT(!(is_paired(q) && index(pair_idx(q))!= index(p)));



  if( m_des_conn[p].count(q) == 0)
  {
    ASSERT(m_asc_conn[q].count(p) == 0);
    m_des_conn[p][q] = 0;
    m_asc_conn[q][p] = 0;
  }

  ASSERTS(m_des_conn[p][q] == m_asc_conn[q][p])
      << SVAR(p) << SVAR(m_des_conn[p][q])
      << SVAR(q) << SVAR(m_asc_conn[q][p]);

  m_des_conn[p][q] += m;
  m_asc_conn[q][p] += m;

  if(m_des_conn[p][q] == 0)
  {
    m_des_conn[p].erase(q);
    m_asc_conn[q].erase(p);
  }
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::cancel_pair ( int p, int q)
{
  order_pr_by_cp_index(*this,p,q);

  ENSURE(m_hversion == m_canc_list.size(),
         "Cannot cancel pair !! Ms complex resolution is not coarsest.");
  ENSURE(index(p) == index(q)+1,
         "indices do not differ by 1");
  ENSURE(m_cp_pair_idx[p] == -1 && m_cp_pair_idx[q] == -1,
         "p/q has already been paired");
  ENSURE(m_des_conn[p].count(q) == 1 && m_asc_conn[q].count(p) == 1,
         "p is not connected to q");
  ENSURE(m_des_conn[p][q] == 1 && m_asc_conn[q][p] == 1,
         "p and q are multiply connected");
  ENSURE(m_cp_is_boundry[p] == m_cp_is_boundry[q],
         "boundary cps cant be paired with interior cps");

  m_cp_pair_idx[p] = q;
  m_cp_pair_idx[q] = p;
  m_canc_list.push_back(int_pair_t(p,q));    
  m_cp_hversion[p] = m_hversion;
  m_cp_hversion[q] = m_hversion;

  cancel_pair();

}

/*---------------------------------------------------------------------------*/

void mscomplex_t::cancel_pair ()
{
  ENSURE(is_in_range(m_hversion,0,m_canc_list.size()),
         "invalid cancellation position");

  int p = m_canc_list[m_hversion].first;
  int q = m_canc_list[m_hversion].second;

  m_hversion++;

  ASSERT(index(p) == index(q)+1);
  ASSERT(m_cp_pair_idx[p] == q);
  ASSERT(m_cp_pair_idx[q] == p);
  ASSERT(m_des_conn[p].count(q) == 1);
  ASSERT(m_asc_conn[q].count(p) == 1);
  ASSERT(m_des_conn[p][q] == 1);
  ASSERT(m_asc_conn[q][p] == 1);

  m_des_conn[p].erase(q);
  m_asc_conn[q].erase(p);

  BOOST_FOREACH(int_pair_t i,m_des_conn[p])
      BOOST_FOREACH(int_pair_t j,m_asc_conn[q])
  {
    int u = i.first;
    int v = j.first;
    long int m = i.second*j.second;

    ASSERT(is_canceled(u) == false);
    ASSERT(is_canceled(v) == false);

    BTRACE_ERROR(connect_cps(u,v,m));
  }

  BOOST_FOREACH(int_pair_t pr,m_des_conn[p]) m_asc_conn[pr.first].erase(p);
  BOOST_FOREACH(int_pair_t pr,m_asc_conn[p]) m_des_conn[pr.first].erase(p);
  BOOST_FOREACH(int_pair_t pr,m_des_conn[q]) m_asc_conn[pr.first].erase(q);
  BOOST_FOREACH(int_pair_t pr,m_asc_conn[q]) m_des_conn[pr.first].erase(q);

  m_cp_is_cancelled[p] =true;
  m_cp_is_cancelled[q] =true;
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::anticancel_pair()
{
  ENSURE(is_in_range(m_hversion-1,0,m_canc_list.size()),
         "invalid cancellation position");

  m_hversion--;

  int p = m_canc_list[m_hversion].first;
  int q = m_canc_list[m_hversion].second;

  ASSERT(index(p) == index(q)+1);
  ASSERT(m_cp_pair_idx[p] == q);
  ASSERT(m_cp_pair_idx[q] == p);

  BOOST_FOREACH(int_pair_t pr,m_des_conn[p]) m_asc_conn[pr.first][p] = pr.second;
  BOOST_FOREACH(int_pair_t pr,m_asc_conn[p]) m_des_conn[pr.first][p] = pr.second;
  BOOST_FOREACH(int_pair_t pr,m_des_conn[q]) m_asc_conn[pr.first][q] = pr.second;
  BOOST_FOREACH(int_pair_t pr,m_asc_conn[q]) m_des_conn[pr.first][q] = pr.second;

  // cps in lower of u except l
  BOOST_FOREACH(int_pair_t i,m_des_conn[p])
      BOOST_FOREACH(int_pair_t j,m_asc_conn[q])
  {
    int u = i.first;
    int v = j.first;
    int m = i.second*j.second;

    ASSERT(is_canceled(u) == false);
    ASSERT(is_canceled(v) == false);

    BTRACE_ERROR(connect_cps(u,v,-m));
  }

  BTRACE_ERROR(connect_cps(p,q,1));

  ASSERT(m_des_conn[p].count(q) == 1);
  ASSERT(m_asc_conn[q].count(p) == 1);
  ASSERT(m_des_conn[p][q] == 1);
  ASSERT(m_asc_conn[q][p] == 1);

  m_cp_is_cancelled[p] =false;
  m_cp_is_cancelled[q] =false;
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::set_hversion(int hver)
{
  int hversion_old = m_hversion;
  for(int i = m_hversion; i>hver && i>0; --i)  anticancel_pair();
  for(int i = m_hversion; i<hver && i<m_canc_list.size(); ++i)cancel_pair();
  TLOG << SVAR(hver) <<SVAR(hversion_old) << SVAR(m_hversion);
}

/*---------------------------------------------------------------------------*/

int mscomplex_t::get_hversion_nextrema(int nmax, int nmin) const
{
  int ns_max = boost::distance(cpno_range()
    |ba::filtered(bind(&mscomplex_t::is_index_i_cp,this,3,_1)));

  int ns_min = boost::distance(cpno_range()
    |ba::filtered(bind(&mscomplex_t::is_index_i_cp,this,0,_1)));

  int hver = 0;

  for(hver = 0 ; hver < m_canc_list.size() ; ++hver)
  {
    int p = m_canc_list[hver].first,q = m_canc_list[hver].second;

    order_pr_by_cp_index(*this,p,q);

    if (ns_max <= nmax || ns_min <= nmin)
      break;

    if(index(p) == 3 ) --ns_max;
    if(index(q) == 0 ) --ns_min;

  }

  TLOG << SVAR(nmax) <<SVAR(nmin) << SVAR(hver);

  return hver;
}

/*---------------------------------------------------------------------------*/

inline bool is_valid_canc_edge
(const mscomplex_t &msc, int_pair_t e,fn_t thr )
{
  order_pr_by_cp_index(msc,e.first,e.second);

//  if(e == std::make_pair(57579,30053) || e == std::make_pair(30053,57579))
//  {
//    cerr << SVAR (msc.is_canceled(e.first)||msc.is_canceled(e.second)) << endl;
//    cerr << SVAR (msc.is_paired(e.first) || msc.is_paired(e.second)) << endl;
//    cerr << SVAR (msc.is_boundry(e.first) != msc.is_boundry(e.second)) << endl;
//    cerr << SVAR (msc.m_des_conn[e.first][e.second] != 1) << endl;
//  }


  if(msc.is_canceled(e.first)||msc.is_canceled(e.second))
    return false;

  if(msc.is_paired(e.first) || msc.is_paired(e.second))
    return false;

  if(msc.is_boundry(e.first) != msc.is_boundry(e.second))
    return false;

  ASSERT(msc.m_des_conn[e.first].count(e.second) == 1);
  ASSERT(msc.m_asc_conn[e.second].count(e.first) == 1);
  ASSERT(msc.m_des_conn[e.first][e.second] == msc.m_asc_conn[e.second][e.first]);

  if(msc.m_des_conn[e.first][e.second] != 1)
    return false;

  bool   is_epsilon_persistent = (msc.vertid(e.first) == msc.vertid(e.second));
  bool   is_pers_lt_t          = std::abs(msc.fn(e.first) - msc.fn(e.second)) < thr;

  return (is_epsilon_persistent || is_pers_lt_t);
}

/*---------------------------------------------------------------------------*/

bool mscomplex_t::persistence_cmp(int_pair_t p0,int_pair_t p1) const
{
  order_pr_by_cp_index(*this,p0.first,p0.second);
  order_pr_by_cp_index(*this,p1.first,p1.second);

  cellid_t v00 = vertid(p0.first);
  cellid_t v01 = vertid(p0.second);
  cellid_t v10 = vertid(p1.first);
  cellid_t v11 = vertid(p1.second);

  cellid_t c00 = cellid(p0.first);
  cellid_t c01 = cellid(p0.second);
  cellid_t c10 = cellid(p1.first);
  cellid_t c11 = cellid(p1.second);

  if( (v00 == v01 ) != (v10 == v11))
    return (v00 == v01 );

  if( (v00 == v01 ) &&(v10 == v11))
  {
    if(v00 == v10)
    {
      if(c00 != c10)
        return c00 < c10;
      else
        return c01 < c11;
    }
    else
    {
      return (v00 < v10);
    }
  }

  fn_t f00 = fn(p0.first);
  fn_t f01 = fn(p0.second);
  fn_t f10 = fn(p1.first);
  fn_t f11 = fn(p1.second);

  fn_t d1 = std::abs(f01-f00);
  fn_t d2 = std::abs(f11-f10);

  if(d1 != d2)
    return d1 < d2;

  if(c00 != c10)
    return c00 < c10;

  return c01 < c11;
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::simplify_pers(double thresh, bool is_nrm, int nmax, int nmin)
{
  DLOG << "Entered :" << SVAR(thresh) <<SVAR(is_nrm) << SVAR(nmax)<<SVAR(nmin);

  BOOST_AUTO(cmp,bind(&mscomplex_t::persistence_cmp,this,_2,_1));

  priority_queue<int_pair_t,int_pair_list_t,typeof(cmp)> pq(cmp);

  if(is_nrm) thresh *= (m_fmax - m_fmin);

  for(int i = 0 ;i < get_num_critpts();++i)
  {
    BOOST_FOREACH(int_pair_t j,m_des_conn[i])
    {
      int_pair_t pr(i,j.first);

      if(is_valid_canc_edge(*this,pr,thresh))
        pq.push(pr);
    }
  }

  int ns_max = boost::distance(cpno_range()
    |ba::filtered(bind(&mscomplex_t::is_index_i_cp,this,3,_1))
    |ba::filtered(bind(&mscomplex_t::is_not_canceled,this,_1)));
  int ns_min = boost::distance(cpno_range()
    |ba::filtered(bind(&mscomplex_t::is_index_i_cp,this,0,_1))
    |ba::filtered(bind(&mscomplex_t::is_not_canceled,this,_1)));

  fn_t last_pers = -1;

  while (pq.size() !=0)
  {
    int_pair_t pr = pq.top();

    pq.pop();

    if(!is_valid_canc_edge(*this,pr,thresh))
      continue;

    if (ns_max <= nmax || ns_min <= nmin)
      break;

    ASSERT(order_pair<DES>(shared_from_this(),pr) == pr);

    if(index(pr.first)  == 3) --ns_max;
    if(index(pr.second) == 0) --ns_min;

    fn_t pr_pers = (fn(pr.first) - fn(pr.second));

    ENSURES(last_pers <= pr_pers )
        <<"Persistence Monotonicity Violated !!! "
        << SVAR(pr.first) <<SVAR(pr.second)
        << SVAR(last_pers) << SVAR(pr_pers);

    cancel_pair(pr.first,pr.second);

    last_pers = pr_pers;

    BOOST_FOREACH(int_pair_t i,m_des_conn[pr.first])
    BOOST_FOREACH(int_pair_t j,m_asc_conn[pr.second])
    {
      int_pair_t e(j.first,i.first);

      if(is_valid_canc_edge(*this,e,thresh))
        pq.push(e);
    }
  }

  m_merge_dag->update(shared_from_this());

  DLOG << "Exited  :" << SVAR(m_hversion) <<SVAR(ns_max) <<SVAR(ns_min);
}

/*---------------------------------------------------------------------------*/

bool is_tresh_lt_pers(const mscomplex_t &msc,fn_t t, int_pair_t pr)
{return t<std::abs<fn_t>(msc.fn(pr.first)-msc.fn(pr.second));}

int mscomplex_t::get_hversion_pers(double t, bool is_nrm) const
{
  BOOST_AUTO(cmp,bind(is_tresh_lt_pers,boost::cref(*this),_1,_2));

  int hver = int(br::upper_bound
                 (m_canc_list,(is_nrm)?(t*std::abs(m_fmax-m_fmin)):(t),cmp)
                 - m_canc_list.begin());

  TLOG << SVAR(t) << SVAR(is_nrm)<< SVAR(hver);
  return hver;
}

/*---------------------------------------------------------------------------*/

typedef std::vector<int_list_t> contrib_list_t;

template <int dim,int odim>
inline bool is_dim_pair(mscomplex_ptr_t msc,int_pair_t pr)
{return (msc->index(pr.first) == dim) && (msc->index(pr.second) == odim);}

/// \brief For each canceled dim-cp get a list of surviving cps it
///        contributes its dir-geometry
///
/// \note The first entry in each list is the id of the canceled cp
template<eGDIR dir,int dim>
void getCanceledCpContrib(mscomplex_ptr_t msc,contrib_list_t& ccp_contrib)
{
  const eGDIR odir = (dir == ASC)?(DES):(ASC);
  const int   odim = (dir == ASC)?(dim +1):(dim -1);

  // Stage 1:
  // I)   Obtain a sequence of cancellations so that
  //      a) each pair is ordered by dir ..
  //         i.e if dir is DES then look at a (2,1) as (2,1) and not (1,2)
  //      b) pairs that have not yet been cancelled are removed
  //      c) pairs whose first element have index == dim
  //
  // II) Construct an index mapping for the relevant canceled cps
  //
  // III)  Allocate data and ddd in the id of the canceled cp at the last

  std::map<int,int> ccp_map;

  BOOST_AUTO(ccp_rng,msc->m_canc_list
             |ba::sliced(0,msc->m_hversion)
             |ba::transformed(bind(order_pair<dir>,msc,_1))
             |ba::filtered(bind(is_dim_pair<dim,odim>,msc,_1))
             );

  BOOST_FOREACH(int_pair_t pr,ccp_rng)
      ccp_map.insert(int_pair_t(pr.first,ccp_map.size()));

  ccp_contrib.resize(ccp_map.size());

  BOOST_FOREACH(int_pair_t pr,ccp_map)
    ccp_contrib[pr.second].push_back(pr.first);


  // Stage 2:
  // This part computes for each cancelled critical point,
  // the surviving critical points to which it contributes its
  // finest resolution geometry .
  BOOST_FOREACH(int_pair_t pr,ccp_rng|ba::reversed)
  {
    int p = pr.first,q = pr.second;

    int_list_t & pcontrib = ccp_contrib[ccp_map.at(p)];

    // for each qa in the asc conn of q:
    BOOST_FOREACH(int qa, msc->m_conn[odir][q]|ba::map_keys)
    {
      // a) if qa is not canceled ..
      if(msc->is_not_canceled(qa))
      {
        // .. p contributes to qa.
        pcontrib.push_back(qa);
      }
      // b) if qa is paired and qa's pair and q have same index ..
      else if(msc->index(q) == msc->index(msc->pair_idx(qa)))
      {
        int_list_t &qa_contrib = ccp_contrib[ccp_map.at(qa)];

        // .. then foreach qaqa that qa contributes to  ..
        BOOST_FOREACH(int qaqa,qa_contrib|ba::sliced(1,qa_contrib.size()))
        {
          // .. p contributes to qaqa.
          pcontrib.push_back(qaqa);

          // pdpd has to be a surviving cp
          ASSERT(msc->is_not_canceled(qaqa));
        }
      }
    }
  }

  // Stage 3:
  // Debug sanity checks
  BOOST_FOREACH(int_list_t & ccp_l, ccp_contrib)
  {
    int ccp = ccp_l.front();

    ASSERTS (msc->index(ccp) == dim) << "incorrect dim";
    ASSERTS (msc->is_canceled(ccp))    << ccp << " should be cancelled";

    BOOST_FOREACH(int scp, ccp_l|ba::sliced(1,ccp_l.size()))
    {
      ASSERTS(msc->index(scp) == dim)  << "incorrect dim";
      ASSERTS(msc->is_not_canceled(scp)) << scp << " should be calcelled";
    }
  }
}

/* -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - */

/// \brief For each survigin dim-cp get a list of canceled cps it
///        contributes its dir-geometry to it
///
/// \note The last entry in each list is the id of the surviving cp
template<eGDIR dir,int dim>
void getSurvivingCpContrib(mscomplex_ptr_t msc,contrib_list_t& scp_contrib)
{
  // Stage 1:
  // Construct an index mapping for each surviving dim-cp and allocate data
  std::map<int,int> scp_map;

  BOOST_FOREACH(int cp,msc->cpno_range()
                |ba::filtered(bind(&mscomplex_t::is_not_canceled,msc,_1))
                |ba::filtered(bind(&mscomplex_t::is_index_i_cp,msc,dim,_1)))
      scp_map.insert(int_pair_t(cp,scp_map.size()));

  scp_contrib.resize(scp_map.size());

  BOOST_FOREACH(int_pair_t pr,scp_map)
    scp_contrib[pr.second].push_back(pr.first);


  // Stage 2:
  // For each canceld dim cp, get a list of surviging cps it contributes to
  contrib_list_t ccp_contrib;
  getCanceledCpContrib<dir,dim>(msc,ccp_contrib);

  // Stage 3:
  // Invert the above data i.e.  for each surviving cp get a list of
  // canceled cps that contribute to it.
  BOOST_FOREACH(int_list_t & ccp_l, ccp_contrib)
  {
    int ccp = ccp_l.front();

    BOOST_FOREACH(int scp, ccp_l|ba::sliced(1,ccp_l.size()))
    {
      scp_contrib[scp_map.at(scp)].push_back(ccp);
    }
  }

  // Stage 4:
  // Debug sanity checks
  BOOST_FOREACH(int_list_t & scp_l, scp_contrib)
  {
    int scp = scp_l.front();

    ASSERTS (msc->index(scp) == dim)   << "incorrect dim";
    ASSERTS (msc->is_not_canceled(scp))  << scp << " should be cancelled";

    BOOST_FOREACH(int ccp, scp_l|ba::sliced(1,scp_l.size()))
    {
      ASSERTS(msc->index(ccp) == dim)     << "incorrect dim";
      ASSERTS(msc->is_canceled(ccp)) << ccp << " should be calcelled";
    }
  }
}

/* -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - */

template <eGDIR dir, int dim>
inline void __collect_mfolds(mscomplex_ptr_t msc, dataset_ptr_t ds)
{
  contrib_list_t contrib;
  getSurvivingCpContrib<dir,dim>(msc,contrib);

  #pragma omp parallel for
  for(int i = 0 ; i < contrib.size() ; ++i)
  {
    cellid_t c = contrib[i][0];

    msc->m_mfolds[dir][c].clear();

    cellid_list_t contrib_cells;

    br::copy(contrib[i]|ba::transformed(bind(&mscomplex_t::cellid,msc,_1)),
             back_inserter(contrib_cells));

    contrib[i].clear();

    ds->get_manifold(dir,contrib_cells,msc->m_mfolds[dir][c]);
  }

  msc->m_geom_hversion[dir][dim] = msc->get_hversion();
}

/* -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - */

void mscomplex_t::collect_mfolds(eGDIR d, int k, dataset_ptr_t ds)
{
  ENSURES(d==DES || d==ASC || d==GDIR_CT) <<SVAR(d);
  ENSURES(k == -1 || (0<=k && k <= 3) )   << SVAR(k);

  if (d == ASC || d == GDIR_CT)
  {
    if(k==0 || k==-1)__collect_mfolds<ASC,0>(shared_from_this(),ds);
    if(k==1 || k==-1)__collect_mfolds<ASC,1>(shared_from_this(),ds);
    if(k==2 || k==-1)__collect_mfolds<ASC,2>(shared_from_this(),ds);
  }

  if (d == DES || d == GDIR_CT)
  {
    if(k==1 || k==-1)__collect_mfolds<DES,1>(shared_from_this(),ds);
    if(k==2 || k==-1)__collect_mfolds<DES,2>(shared_from_this(),ds);
    if(k==3 || k==-1)__collect_mfolds<DES,3>(shared_from_this(),ds);
  }
}

/* -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  - */

void mscomplex_t::collect_mfolds(dataset_ptr_t ds)
{
  __collect_mfolds<ASC,0>(shared_from_this(),ds);
  __collect_mfolds<ASC,1>(shared_from_this(),ds);
  __collect_mfolds<ASC,2>(shared_from_this(),ds);

  __collect_mfolds<DES,1>(shared_from_this(),ds);
  __collect_mfolds<DES,2>(shared_from_this(),ds);
  __collect_mfolds<DES,3>(shared_from_this(),ds);
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::get_mfold(mfold_t & m,eGDIR d,int c,int hv) const
{
  if (hv == -1) hv = m_hversion;
  int gv = m_geom_hversion[d][index(c)];

  int_list_t contrib;

  m_merge_dag->get_contrib_cps(contrib,d,c,hv,gv);

  for(int i = 0 ; i < contrib.size();++i)
    br::copy(m_mfolds[d][contrib[i]],std::back_inserter(m));
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::get_contrib_cps(int_list_t &o,eGDIR d,int c,int hv,int gv) const
{
  if(hv == -1) hv = get_hversion();
  if(gv == -1) gv = m_geom_hversion[d][index(c)];

  m_merge_dag->get_contrib_cps(o,d,c,hv,gv);
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::get_contrib_cps(int_list_t &o,eGDIR d,const int_pair_list_t &c,int gv) const
{
  if(gv == -1) gv = m_geom_hversion[d][index(c[0].first)];

  m_merge_dag->get_contrib_cps(o,d,c,gv);
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::flip_gradient_convention()
{
  m_grad_conv = (m_grad_conv == DES)?(ASC):(DES);

  for(int i = 0 ; i < get_num_critpts(); ++i)
  {
    m_cp_index[i] = 3 - m_cp_index[i];
    m_cp_fn[i] = m_cp_fn[i]*-1;

    std::swap(m_des_conn[i],m_asc_conn[i]);
    std::swap(m_des_mfolds[i],m_asc_mfolds[i]);
  }

  for(int i = 0 ; i < m_canc_list.size(); ++i)
  {
    std::swap(m_canc_list[i].first,m_canc_list[i].second);
  }

  for(int i = 0 ; i < g_max_dim + 1; ++i)
    std::swap(m_geom_hversion[0][i],m_geom_hversion[1][i]);

  std::swap(m_fmin,m_fmax);

  std::swap(m_merge_dag->m_cp_geom[0],m_merge_dag->m_cp_geom[1]);

  m_fmax *= -1;
  m_fmin *= -1;
}

/*---------------------------------------------------------------------------*/

//void mscomplex_t::build_canc_dep_tree()
//{
//  int_list_t cp_canc_no(get_num_critpts());

//  m_canc_dep_tree.resize(m_canc_list.size(),-1);

//  for(int i = m_canc_list.size()-1; i>=0 ; --i)
//  {
//    int p = m_canc_list[i].first;
//    int q = m_canc_list[i].second;

//    cp_canc_no[p] = i;
//    cp_canc_no[q] = i;

//    BOOST_FOREACH(int_pair_t r,m_des_conn[p])
//    BOOST_FOREACH(int_pair_t s,m_asc_conn[q])
//    {
//      int u = r.first;
//      int v = s.first;
//      int m = r.second*s.second;

//      if(m == 1 && is_paired(u) && is_paired(v))
//      {
//        if(pair_idx(u) == v)
//          m_canc_dep_tree[i] = cp_canc_no[u];
//        else if(m_canc_dep_tree[cp_canc_no[u]] == cp_canc_no[v])
//          m_canc_dep_tree[i] = cp_canc_no[v];
//        else if(m_canc_dep_tree[cp_canc_no[v]] == cp_canc_no[u])
//          m_canc_dep_tree[i] = cp_canc_no[u];
//      }
//    }
//  }
//}


//void mscomplex_t::collect_arc_geom(dataset_ptr_t ds)
//{
//  typedef std::map<int_pair_t,int_pair_list_t,utl::uo_pair_cmp<int> > arc_pcs_t;
//  typedef std::map<int_pair_t,int_list_t,utl::uo_pair_cmp<int> >      arc_geom_t;

//  arc_pcs_t arc_merge_dag;

//  for(int i = 0 ; i < m_canc_list.size(); ++i)
//  {
//    int p = m_canc_list[i].first;
//    int q = m_canc_list[i].second;

//    order_pr_by_cp_index(*this,p,q);

//    BOOST_FOREACH(int pd, m_des_conn[p]|badpt::map_keys)
//    {
//      if(pd  == q)
//        continue;

//      BOOST_FOREACH(int qa, m_asc_conn[q]|badpt::map_keys)
//      {
//        if(qa  == p)
//          continue;

//        arc_merge_dag[int_pair_t(qa,pd)].push_back(int_pair_t(qa,q));
//        arc_merge_dag[int_pair_t(qa,pd)].push_back(int_pair_t(q,p));
//        arc_merge_dag[int_pair_t(qa,pd)].push_back(int_pair_t(p,pd));
//      }
//    }
//  }

//  arc_pcs_t arc_pcs;

//  arc_geom_t finest_res_arc_geom;

//  BOOST_FOREACH(int p, cpno_range()|badpt::filtered(bind(&mscomplex_t::is_not_paired,this,_1)))
//  {
//    BOOST_FOREACH(int q, m_des_conn[p]|badpt::map_keys)
//    {
//      int_pair_list_t &arc_pc = arc_pcs[int_pair_t(p,q)];

//      std::stack<int_pair_t> arc_stk;

//      arc_stk.push(int_pair_t(p,q));

//      while(!arc_stk.empty())
//      {
//        int_pair_t pr = arc_stk.top();arc_stk.pop();

//        if(arc_merge_dag.count(pr) != 0)
//        {
//          if(pr == order_pr_by_cp_index(*this,pr) )
//            BOOST_FOREACH(int_pair_t pc, arc_merge_dag[pr]|badpt::reversed)
//                arc_stk.push(pc);
//          else
//            BOOST_FOREACH(int_pair_t pc, arc_merge_dag[pr])
//                arc_stk.push(int_pair_t(pc.second,pc.first));

//        }
//        else
//        {
//          arc_pc.push_back(pr);
//          finest_res_arc_geom[pr].resize(0);
//        }
//      }
//    }
//  }

//  BOOST_FOREACH(arc_geom_t::value_type &pr, finest_res_arc_geom)
//  {
//    int p = pr.first.first;
//    int q = pr.first.second;

//    order_pr_by_cp_index(*this,p,q);

//    int_pair_list_t  desm;
//    int_pair_list_t  ascm;

//    {
//      int_pair_list_t ls;
//      ls.push_back(int_pair_t(cellid(p),1));
//      ds->get_manifold(DES,ls,desm);
//    }

//    {
//      int_pair_list_t ls;
//      ls.push_back(int_pair_t(cellid(q),1));
//      ds->get_manifold(ASC,ls,ascm);
//    }

//    ds->get_arc_intersection(desm,ascm,pr.second);
//  }

//  BOOST_FOREACH(arc_pcs_t::value_type &pr, arc_pcs)
//  {
//    int_list_t &arc_geom = m_arc_geom[pr.first];

//    BOOST_FOREACH(int_pair_t arc_pc,pr.second)
//    {
//      arc_geom.push_back(cellid(arc_pc.first));
//      br::copy(finest_res_arc_geom[arc_pc],back_inserter(arc_geom));
//    }
//    arc_geom.push_back(cellid(pr.first.second));
//  }
//}

/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/




/*===========================================================================*/
namespace tet {
/*---------------------------------------------------------------------------*/

inline mscomplex_t::merge_dag_t::node_t
mscomplex_t::merge_dag_t::get_node(int i) const
{


  if(i < get_ncps())
    return node_t(i);

  return m_nodes[i-get_ncps()];
}

/*---------------------------------------------------------------------------*/

mscomplex_t::merge_dag_t::merge_dag_t():m_last_hversion(0){}

/*---------------------------------------------------------------------------*/

inline int mscomplex_t::merge_dag_t::get_ncps() const
{  return m_cp_geom[0].size();}

/*---------------------------------------------------------------------------*/

void mscomplex_t::merge_dag_t::init(int ncps)
{
  m_cp_geom[ASC].resize(ncps);
  m_cp_geom[DES].resize(ncps);

//  #pragma omp parallel for
  for(int i = 0; i < ncps; ++i)
  {
    m_cp_geom[ASC][i] = i;
    m_cp_geom[DES][i] = i;
  }

}

/*---------------------------------------------------------------------------*/

void mscomplex_t::merge_dag_t::clear()
{
  m_cp_geom[DES].clear();
  m_cp_geom[ASC].clear();
  m_nodes.clear();
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::merge_dag_t::update(mscomplex_ptr_t msc)
{
  ENSURES(msc->get_hversion() >= m_last_hversion)
      << "Updates to merge_dag may only be performed on the coarsest version";

  for(; m_last_hversion < msc->get_hversion();)
  {
    int_pair_t pr = msc->m_canc_list[m_last_hversion++]; // ++ is deliberate

    int p = pr.first,q = pr.second;

    ASSERT(order_pair<DES>(msc,pr) == pr);

    for(int dir = 0,odir=1 ; dir < 2; ++dir,--odir)
    {
      int pnode = m_cp_geom[dir][p];

      ENSURES(get_node(pnode).hversion < m_last_hversion)
         <<"earlier pnode has formed from a later cancellation"
         <<SVAR(get_node(pnode).hversion) << SVAR(m_last_hversion);

      BOOST_FOREACH(int r,msc->m_conn[odir][q]|ba::map_keys)
      {
        int rnode         = m_cp_geom[dir][r];
        m_cp_geom[dir][r] = get_ncps() + m_nodes.size();
        m_nodes.push_back(node_t(r,rnode,pnode,m_last_hversion));

        ENSURES(get_node(rnode).hversion < m_last_hversion)
            <<"earlier rnode has formed from a later cancellation";
      }
      std::swap(p,q);
    }
  }

  int maxnode = get_ncps() + m_nodes.size();

  for(int i = 0 ; i < get_ncps(); ++i)
  {
    ASSERTS(is_in_range(m_cp_geom[0][i],0,maxnode));
    ASSERTS(is_in_range(m_cp_geom[1][i],0,maxnode));
  }

  for(int i = 0 ; i < m_nodes.size() ; ++i)
  {
    ASSERTS(is_in_range(m_nodes[i].base,0,maxnode));
    ASSERTS(is_in_range(m_nodes[i].other,0,maxnode));
  }

}

/*---------------------------------------------------------------------------*/

void mscomplex_t::merge_dag_t::get_contrib_cps
(int_list_t &l, eGDIR dir, int cp, int hv, int gv) const
{
  int g = m_cp_geom[dir][cp];

  while(get_node(g).hversion > hv )
    g = get_node(g).base;

  std::set<int>    visited;

  std::stack<int> stk;

  stk.push(g);

  while(stk.size() != 0 )
  {
    int g = stk.top();
    stk.pop();

    if(visited.count(g) != 0 )
      continue;

    visited.insert(g);

    node_t gnode = get_node(g);

    if( gnode.hversion <= gv)
      l.push_back(gnode.cpid);
    else if(gnode.base != -1)
    {
      stk.push(gnode.base);
      stk.push(gnode.other);
    }
  }
}

/*---------------------------------------------------------------------------*/
void mscomplex_t::merge_dag_t::get_contrib_cps
(int_list_t &o, eGDIR d, const int_pair_list_t &cp_list, int gv) const
{
  std::stack<int>  stk;
  std::set<int>    visited;

  for(int  i = 0 ; i < cp_list.size(); ++i)
  {
    int cp = cp_list[i].first,hv = cp_list[i].second;

    int g = m_cp_geom[d][cp];

    while(get_node(g).hversion > hv )
      g = get_node(g).base;

    stk.push(g);
  }

  while(stk.size() != 0 )
  {
    int g = stk.top();
    stk.pop();

    if(visited.count(g) != 0 )
      continue;

    visited.insert(g);

    node_t gnode = get_node(g);

    if( gnode.hversion <= gv)
      o.push_back(gnode.cpid);
    else if(gnode.base != -1)
    {
      stk.push(gnode.base);
      stk.push(gnode.other);
    }
  }
}

/*---------------------------------------------------------------------------*/

void mscomplex_t::merge_dag_t::save_bin(ostream &os) const
{
  utl::bin_write_vec(os,m_nodes);
  utl::bin_write_vec(os,m_cp_geom[DES]);
  utl::bin_write_vec(os,m_cp_geom[ASC]);
  utl::bin_write(os,m_last_hversion);
}

//*--------------------------------------------------------------------------*/

void mscomplex_t::merge_dag_t::load_bin(istream &is)
{
  utl::bin_read_vec(is,m_nodes);
  utl::bin_read_vec(is,m_cp_geom[DES]);
  utl::bin_read_vec(is,m_cp_geom[ASC]);
  utl::bin_read(is,m_last_hversion);
}

//*--------------------------------------------------------------------------*/
}
/*===========================================================================*/
