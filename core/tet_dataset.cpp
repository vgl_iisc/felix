#include <stack>
#include <map>
#include <queue>

#include <boost/typeof/typeof.hpp>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <tet_dataset.hpp>
#include <tet_mscomplex.hpp>

using namespace std;

/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/

dataset_t::dataset_t(const fn_list_t &fns,base_cc_ptr_t tcc)
  :m_vert_fns(fns),m_tcc(tcc)
{
  ENSURES(m_vert_fns.size() == m_tcc->num_dcells(0)) << "Size MisMatch";

  int N = m_tcc->num_cells();

  m_cell_mxfct.resize(N,invalid_cellid);
  m_cell_pairs.resize(N,invalid_cellid);

  for(int i = 0 ; i < fns.size(); i++)
      ENSURES(!std::isnan(fns[i]));
}

/*---------------------------------------------------------------------------*/

template <size_t dim,typename rng_t>
inline void assign_max_facets(dataset_t &ds,rng_t rng)
{
  BOOST_AUTO(cmp,bind(&dataset_t::compare_cells<dim-1>,&ds,_1,_2));

  cellid_t b = *boost::begin(rng), e = *boost::end(rng);

  #pragma omp parallel for
  for(cellid_t c = b; c < e; ++c)
  {
    cellid_t fcts[DMAX];
    int n = ds.m_tcc->get_fcts(c,fcts);
    ds.m_cell_mxfct[c] = *max_element(fcts,fcts+n,cmp);
  }
}

/*---------------------------------------------------------------------------*/

template <typename cmp_t>inline void get_lstar
(std::set<cellid_t,cmp_t> &oset, cellid_t c,const dataset_t & ds)
{
  cellid_t cf[DMAX];

  std::set<cellid_t>::iterator beg = oset.insert(c).first;
  std::set<cellid_t>::iterator end = beg;

  for(int i = 0; i < 3; ++i)
  {
    do
    {
      int ct = ds.m_tcc->get_cofs(*beg,cf);

      for( int j = 0 ; j < ct; ++j)
        if(ds.max_vert<-1>(cf[j]) == c)
          oset.insert(cf[j]);

    }
    while (beg++ != end);

    beg = ++end;
    end = --oset.end();
  }
}

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

template <typename cmp_t> inline cellid_t get_singular_facet
(const std::set<cellid_t,cmp_t> &oset,cellid_t c, const base_cc_t &tcc)
{
  cellid_t f[DMAX],ret = invalid_cellid;

  int ct = tcc.get_fcts(c,f),nixn = 0;

  for(int i = 0 ; i < ct; ++i)
    if(oset.count(f[i]))
      ret = f[i],++nixn;

  return (nixn == 1)?(ret):(invalid_cellid);
}

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

inline void assign_pairs_vr(dataset_t &ds)
{
  BOOST_AUTO(cmp,boost::bind(&dataset_t::compare_cells_nd,&ds,_1,_2));

  typedef std::set<cellid_t,BOOST_TYPEOF(cmp)> lsset_t;

  int n = ds.m_tcc->num_dcells(0);

  #pragma omp parallel for schedule(dynamic,100)
  for(int c = 0 ; c <n ; ++c)
  {
    lsset_t lstar(cmp);

    get_lstar(lstar,c,ds);

    while(lstar.size() != 0)
    {
      bool found_pairs = true;

      while(found_pairs)
      {
        found_pairs = false;

        for(lsset_t::iterator b = lstar.begin(); b != lstar.end();)
        {
          cellid_t f = get_singular_facet(lstar,*b,*ds.m_tcc);

          if(f != invalid_cellid)
          {
            if(ds.m_tcc->is_bnd(*b) == ds.m_tcc->is_bnd(f)  )
              ds.pair(*b,f);

            found_pairs = true;

            lstar.erase(b++);
            lstar.erase(f);
          }
          else
          {
            ++b;
          }
        }// end for
      }//end while

      if(lstar.begin() != lstar.end())
        lstar.erase(lstar.begin());
    }// end while
  }
}

/*---------------------------------------------------------------------------*/

template<int dim,eGDIR dir,typename range_t>
inline void mark_reachable(const range_t &rng,dataset_t &ds,std::vector<bool> &vmarks)
{
 cellid_t cets[DMAX];

 cellid_list_t stk(boost::begin(rng),boost::end(rng));

 while(stk.size() != 0 )
 {
   cellid_t c = stk.back(); stk.pop_back();

   ASSERT(ds.m_tcc->get_dim(c) == dim);

   vmarks[c] = true;

   for(cellid_t * b = cets, *e = cets + ds.get_cets<dir>(c,cets);b!=e;++b)
   {
     if(!ds.is_critical(*b))
     {
       cellid_t p = ds.pair(*b);
       if(ds.m_tcc->get_dim(p) == dim && !vmarks[p] )
       {
         stk.push_back(p);
       }
     }
   }
 }
}

/*---------------------------------------------------------------------------*/

template <int dim,eGDIR dir>
struct pq_cmp
{
  const dataset_t &ds;
  pq_cmp(const dataset_t &_ds):ds(_ds){}
  bool operator()(const int_pair_t &c1,const int_pair_t &c2);
};

template <int dim>
struct pq_cmp<dim,DES>
{
  const dataset_t &ds;
  pq_cmp(const dataset_t &_ds):ds(_ds){}
  bool operator()(const int_pair_t &c1,const int_pair_t &c2)
  {return ds.compare_cells_pp<dim>(c1.first,c2.first);}
};

template <int dim>
struct pq_cmp<dim,ASC>
{
  const dataset_t &ds;
  pq_cmp(const dataset_t &_ds):ds(_ds){}
  bool operator()(const int_pair_t &c1,const int_pair_t &c2)
  {return ds.compare_cells_pp<dim>(c2.first,c1.first);}
};

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

template <int dim,eGDIR dir>
inline void compute_inc_pairs_pq
(cellid_t s, const dataset_t &ds,const vector<bool> &vmarks, int_pair_list_t &inc)
{
  const int pdim = (dir == DES)?(dim - 1):(dim + 1);
  const bool no_vcheck =
      ((dim==1) && (dir==DES)) || (dim==0) ||
      ((dim==2) && (dir==ASC)) || (dim==3);

  pq_cmp<dim,dir>  cmp_dim(ds);
  pq_cmp<pdim,dir> cmp_pdim(ds);

  priority_queue<int_pair_t,int_pair_list_t,pq_cmp<dim,dir> >  pq(cmp_dim);
  priority_queue<int_pair_t,int_pair_list_t,pq_cmp<pdim,dir> > inc_pq(cmp_pdim);

  cellid_t f[DMAX];

  pq.push(make_pair((int)s,1));

  while(pq.size() != 0 )
  {
    cellid_t c = pq.top().first;

    ASSERT(ds.m_tcc->get_dim(c) == dim);

    int n = 0 ;

    do {n += pq.top().second; pq.pop();}
    while(pq.size() != 0 && pq.top().first == c);

    for(cellid_t *b = f,*e = f + ds.get_cets<dir>(c,f);b != e; ++b)
    {
      if(ds.is_critical(*b))
      {
        ASSERT(ds.m_tcc->get_dim(*b) == pdim);
        inc_pq.push(make_pair(*b,n));
      }
      else
      {
        cellid_t p = ds.pair(*b);

        if (p != c && (no_vcheck||vmarks[*b]) && ds.m_tcc->get_dim(p) == dim )
        {
          pq.push(make_pair(p,n));
        }
      }
    }
  }

  while(inc_pq.size() != 0 )
  {
    cellid_t p = inc_pq.top().first;

    ASSERT(ds.m_tcc->get_dim(p) == pdim);

    int n = 0 ;

    do {n += inc_pq.top().second; inc_pq.pop();}
    while(inc_pq.size() != 0 && inc_pq.top().first == p);

    inc.push_back(make_pair(p,n));
  }
}

/*---------------------------------------------------------------------------*/

template <int dim,eGDIR dir,bool no_vcheck,typename range_t >
inline void collect_reachable (cellid_list_t &mfold,const range_t &rng,
                               const dataset_t &ds,const vector<bool> &vmarks)
{
  const int pdim = (dir == DES)?(dim - 1):(dim + 1);

  pq_cmp<dim,dir>  cmp_dim(ds);

  priority_queue<int_pair_t,int_pair_list_t,pq_cmp<dim,dir> >
      pq(cmp_dim);

  BOOST_FOREACH(cellid_t c,rng)
      pq.push(int_pair_t(c,1));

  cellid_t f[DMAX];

  while(pq.size() != 0 )
  {
    cellid_t c = pq.top().first;

    ASSERT(ds.m_tcc->get_dim(c) == dim);

    int n = 0 ;

    do {n += pq.top().second; pq.pop();}
    while(pq.size() != 0 && pq.top().first == c);

    mfold.push_back(c);

    for(cellid_t *b = f,*e = f + ds.get_cets<dir>(c,f);b != e; ++b)
    {
      if(!ds.is_critical(*b))
      {
        cellid_t p = ds.pair(*b);

        if (p != c && (no_vcheck||vmarks[*b]) && ds.m_tcc->get_dim(p) == dim )
        {
          pq.push(make_pair(p,n));
        }
      }
    }
  }
}

/*---------------------------------------------------------------------------*/

bool is_twocell(const base_cc_t &tcc, cellid_t c){return tcc.get_dim(c) == 2;}

void dataset_t::work(mscomplex_ptr_t msc)
{
  assign_max_facets<1>(*this,m_tcc->cel_rng(1));
  assign_max_facets<2>(*this,m_tcc->cel_rng(2));
  assign_max_facets<3>(*this,m_tcc->cel_rng(3));

//  assign_pairs<0>(*this,m_tcc->cel_rng(0));
//  assign_pairs<1>(*this,m_tcc->cel_rng(1));
//  assign_pairs<2>(*this,m_tcc->cel_rng(2));

//  assign_pairs2<1>(*this,m_tcc->cel_rng(1));
//  assign_pairs2<2>(*this,m_tcc->cel_rng(2));

  assign_pairs_vr(*this);

  cellid_list_t ccells;

  br::copy(m_tcc->cel_rng()
           |badpt::filtered(boost::bind(&dataset_t::is_critical,this,_1)),
           back_inserter(ccells));

  std::vector<bool> vmarks(m_tcc->num_cells(),false);

  mark_reachable<2,DES>
      (ccells|badpt::filtered(bind(is_twocell,boost::cref(*m_tcc),_1)),
       *this,vmarks);

  std::map<cellid_t,int> id_cp_map;

  msc->init(ccells.size());

  BOOST_FOREACH(cellid_t c, ccells)
  {
    int i = id_cp_map.size();
    id_cp_map[c] = i;
    msc->set_critpt(i,c,m_tcc->get_dim(c),fn(c),
                    max_vert<-1>(c),m_tcc->is_bnd(c));
  }

  #pragma omp parallel for schedule(dynamic,10)
  for (int i = 0 ; i < ccells.size() ; ++i)
  {
    cellid_t c = ccells[i];

    int_pair_list_t inc;

    if( m_tcc->get_dim(c) == 1)
    {
      compute_inc_pairs_pq<1,DES>(c,*this,vmarks,inc);
      compute_inc_pairs_pq<1,ASC>(c,*this,vmarks,inc);
    }
    else if( m_tcc->get_dim(c) == 2)
    {
      compute_inc_pairs_pq<2,ASC>(c,*this,vmarks,inc);
    }

    #pragma omp critical(mscomplex_connectivity_update)
    {
      BOOST_FOREACH(int_pair_t p, inc)
      {
        msc->connect_cps(id_cp_map[c],id_cp_map[p.first],p.second);
      }
    }
  }
}

/*---------------------------------------------------------------------------*/

void  dataset_t::get_manifold(eGDIR dir, const cellid_list_t &s, cellid_list_t &m) const
{
  int dim = m_tcc->get_dim(s[0]);

  std::vector<bool> vm;

  if( dim == 0 && dir == ASC) collect_reachable<0,ASC,true>(m,s,*this,vm);
  if( dim == 1 && dir == ASC) collect_reachable<1,ASC,true>(m,s,*this,vm);
  if( dim == 2 && dir == ASC) collect_reachable<2,ASC,true>(m,s,*this,vm);

  if( dim == 1 && dir == DES) collect_reachable<1,DES,true>(m,s,*this,vm);
  if( dim == 2 && dir == DES) collect_reachable<2,DES,true>(m,s,*this,vm);
  if( dim == 3 && dir == DES) collect_reachable<3,DES,true>(m,s,*this,vm);
}

/*---------------------------------------------------------------------------*/

//inline cellid_t * filter_elst(cellid_t *b,cellid_t *e, cellid_t *r, cellid_t c,const dataset_t &ds)
//{for(;b!=e; ++b) if( ds.max_fct(*b) == c) *r++ = *b; return r;}

/*---------------------------------------------------------------------------*/

//template <int dim,typename rng_t>
//inline void assign_pairs(dataset_t &ds,rng_t rng)
//{
//  cellid_t cf[DMAX],*cfe;

//  BOOST_AUTO(cmp,bind(&dataset_t::compare_cells<dim+1>,&ds,_1,_2));

//  BOOST_FOREACH(cellid_t c,rng)
//  {
//    int ct = ds.get_cets<ASC>(c,cf);

//    cfe = cf + ct;
//    cfe = filter_elst(cf,cfe,cf,c,ds);

//    cellid_t *mcf = min_element(cf,cfe,cmp);

//    if( mcf != cfe && ds.m_tcc->is_bnd(*mcf) == ds.m_tcc->is_bnd(c))
//      ds.pair(c,*mcf);
//  }
//}

/*---------------------------------------------------------------------------*/

//inline cellid_t * filter_elst2(cellid_t *b,cellid_t *e, cellid_t *r, cellid_t c,const dataset_t &ds)
//{
//  for(;b!=e; ++b)
//    if( !ds.is_paired(*b) && ds.max_fct(*b) != c && ds.max_fct(ds.max_fct(*b)) == ds.max_fct(c))
//      *r++ = *b;

//  return r;
//}

/*---------------------------------------------------------------------------*/

//template <int dim,typename rng_t>
//inline void assign_pairs2(dataset_t &ds,rng_t rng)
//{
//  cellid_t cf[DMAX],*cfe;

//  BOOST_AUTO(cmp,bind(&dataset_t::compare_cells<dim+1>,&ds,_1,_2));

//  BOOST_FOREACH(cellid_t c,rng)
//  {
//    if(!ds.is_paired(c))
//    {
//      cfe = cf + ds.get_cets<ASC>(c,cf);
//      cfe = filter_elst2(cf,cfe,cf,c,ds);

//      cellid_t *mcf = min_element(cf,cfe,cmp);

//      if( mcf != cfe && ds.m_tcc->is_bnd(*mcf) == ds.m_tcc->is_bnd(c))
//        ds.pair(c,*mcf);
//    }
//  }
//}

/*---------------------------------------------------------------------------*/

//void  dataset_t::get_arc_intersection
//(const int_pair_list_t &desm,const int_pair_list_t &ascm,int_list_t &arc_geom)
//{
//  BOOST_AUTO(cmp,bind(&dataset_t::compare_cells_pp_nd,this,_1,_2));

//  std::set<cellid_t,BOOST_TYPEOF(cmp)> des_cells(cmp),asc_cells(cmp);

//  br::copy(desm|badpt::map_keys,
//           inserter(des_cells,des_cells.end()));
//  br::copy(desm|badpt::map_keys
//           |badpt::filtered(boost::bind(&dataset_t::is_paired,this,_1))
//           |badpt::transformed(boost::bind(&dataset_t::pair,this,_1)),
//           inserter(des_cells,des_cells.end()));

//  br::copy(ascm|badpt::map_keys,
//           inserter(asc_cells,asc_cells.end()));
//  br::copy(ascm|badpt::map_keys
//           |badpt::filtered(boost::bind(&dataset_t::is_paired,this,_1))
//           |badpt::transformed(boost::bind(&dataset_t::pair,this,_1)),
//           inserter(asc_cells,asc_cells.end()));

//  br::set_intersection(des_cells,asc_cells,back_inserter(arc_geom),cmp);
//}

/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/

