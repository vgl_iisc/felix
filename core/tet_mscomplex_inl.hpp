#ifndef TET_MSCOMPLEX_INL_HPP_INCLUDED
#define TET_MSCOMPLEX_INL_HPP_INCLUDED

#include <tet_mscomplex.hpp>

namespace tet
{

/*===========================================================================*/

inline int  mscomplex_t::get_num_critpts() const
{return m_cp_cellid.size();}

/*---------------------------------------------------------------------------*/

inline int8_t mscomplex_t::index(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_index.size()),i);
  return m_cp_index[i];
}

/*---------------------------------------------------------------------------*/

inline int mscomplex_t::pair_idx(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_pair_idx.size()),i);
  ASSERTV(is_in_range(m_cp_pair_idx[i],0,(int)m_cp_pair_idx.size()),i);
  return m_cp_pair_idx[i];
}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_paired(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_pair_idx.size()),i);
  return (m_cp_pair_idx[i] != -1);
}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_not_paired(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_pair_idx.size()),i);

  return (m_cp_pair_idx[i] == -1);
}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_canceled(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_is_cancelled.size()),i);
  return m_cp_is_cancelled[i];
}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_not_canceled(int i) const
{
  return !is_canceled(i);
}

/*---------------------------------------------------------------------------*/

inline cellid_t mscomplex_t::cellid(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_cellid.size()),i);
  return m_cp_cellid[i];
}

/*---------------------------------------------------------------------------*/

inline cellid_t mscomplex_t::vertid(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_vertid.size()),i);
  return m_cp_vertid[i];
}

/*---------------------------------------------------------------------------*/

inline fn_t mscomplex_t::fn(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_fn.size()),i);
  return m_cp_fn[i];
}

/*---------------------------------------------------------------------------*/

inline int mscomplex_t::hversion(int i) const
{
  ASSERTV(is_in_range(i,0,(int)m_cp_hversion.size()),i);
  return m_cp_hversion[i];
}


/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_boundry(int i) const
{
  ASSERT(is_in_range(i,0,(int)m_cp_is_boundry.size()));
  return m_cp_is_boundry[i];
}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_extrema(int i) const
{return (index(i) == 0 || index(i) == 3);}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_saddle(int i) const
{return (index(i) == 1 || index(i) == 2);}

/*---------------------------------------------------------------------------*/

inline bool mscomplex_t::is_index_i_cp(int i,int cp) const
{return (index(cp) == i);}

/*---------------------------------------------------------------------------*/

inline int mscomplex_t::surv_extrema(int i) const
{
  ASSERT(is_extrema(i));

  if(is_canceled(i) == false)
    return i;

  eGDIR dir = (index(i) == 3)?(ASC):(DES);

  ASSERT(m_conn[dir][pair_idx(i)].size() == 1);

  int j = m_conn[dir][pair_idx(i)].begin()->first;

  ASSERT(is_not_canceled(j));

  return j;
}

/*---------------------------------------------------------------------------*/

inline fn_t mscomplex_t::fn_min() const
{return m_fmin;}

/*---------------------------------------------------------------------------*/

inline fn_t mscomplex_t::fn_max() const
{return m_fmax;}

/*---------------------------------------------------------------------------*/

inline std::string mscomplex_t::cp_info (int cp_no) const
{
  std::stringstream ss;

  ss<<std::endl;
  ss<<"cp_no        ::"<<cp_no<<std::endl;
  ss<<"cellid       ::"<<cellid(cp_no)<<std::endl;
  ss<<"vert cell    ::"<<vertid(cp_no)<<std::endl;
  ss<<"index        ::"<<(int)index(cp_no)<<std::endl;
  ss<<"fn           ::"<<fn(cp_no)<<std::endl;
  ss<<"is_cancelled ::"<<is_canceled(cp_no)<<std::endl;
  ss<<"is_paired    ::"<<is_paired(cp_no)<<std::endl;
//  ss<<"pair_idx     ::"<<pair_idx(cp_no)<<std::endl;
  ss<<"is_boundry   ::"<<(int)is_boundry(cp_no)<<std::endl;

  return ss.str();
}

/*---------------------------------------------------------------------------*/

inline std::string mscomplex_t::info() const
{
  std::stringstream ss;

  ss<<"num cps      : "<<get_num_critpts()<<std::endl;
  ss<<"num maxima   : "<<br::count(m_cp_index,3)<<std::endl;
  ss<<"num 2-saddle : "<<br::count(m_cp_index,2)<<std::endl;
  ss<<"num 1-saddle : "<<br::count(m_cp_index,1)<<std::endl;
  ss<<"num minima   : "<<br::count(m_cp_index,0)<<std::endl;

  return ss.str();
}

/*---------------------------------------------------------------------------*/

inline boost::iterator_range<mscomplex_t::iterator_t> mscomplex_t::cpno_range() const
{return boost::make_iterator_range(iterator_t(0),iterator_t(get_num_critpts()));}

/*---------------------------------------------------------------------------*/

inline void mscomplex_t::save(const std::string &f) const
{
  std::fstream fs(f.c_str(),std::ios::out|std::ios::binary);
  ENSUREV(fs.is_open(),"file not found!!",f);
  save_bin(fs);
}

/*---------------------------------------------------------------------------*/

inline void mscomplex_t::load(const std::string &f)
{
  std::fstream fs(f.c_str(),std::ios::in|std::ios::binary);
  ENSUREV(fs.is_open(),"file not found!!",f);
  load_bin(fs);
}

/*---------------------------------------------------------------------------*/

inline int mscomplex_t::get_hversion() const {return m_hversion;}

/*===========================================================================*/





/*===========================================================================*/

inline void order_pr_by_cp_index(const mscomplex_t &msc,int &p,int &q)
{if(msc.index(p) < msc.index(q))std::swap(p,q);}

/*---------------------------------------------------------------------------*/

template<> inline int_pair_t order_pair<DES>(mscomplex_ptr_t msc,int_pair_t pr)
{if(msc->index(pr.first) < msc->index(pr.second))
    std::swap(pr.first,pr.second);return pr;}

template<> inline int_pair_t order_pair<ASC>(mscomplex_ptr_t msc,int_pair_t pr)
{if(msc->index(pr.first) > msc->index(pr.second))
    std::swap(pr.first,pr.second); return pr;}

/*===========================================================================*/

}
#endif
