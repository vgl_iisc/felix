#include <boost/typeof/typeof.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <set>
#include <stack>

#include <tet_cc.hpp>

// dart map for tet a b c d
// 0 : b c d
// 1 : c d b
// 2 : d b c
//
// 3 : c a d
// 4 : a d c
// 5 : d c a
//
// 6 : a b d
// 7 : b d a
// 8 : d a b
//
// 9 : b a c
//10 : a c b
//11 : c b a

using namespace std;

typedef tet::tet_cc_t::ege_t ege_t;
typedef tet::tet_cc_t::tri_t tri_t;
typedef tet::tet_cc_t::tet_t tet_t;
typedef tet::tet_cc_t::tet_list_t tet_list_t;

namespace tet
{

inline tet_t order_tet(const tet_t &t)
{
  tet_t r = t;

  if(r[0]< r[1] ) swap(r[0],r[1]);
  if(r[0]< r[2] ) swap(r[0],r[2]);
  if(r[0]< r[3] ) swap(r[0],r[3]);
  if(r[1]< r[2] ) swap(r[1],r[2]);
  if(r[1]< r[3] ) swap(r[1],r[3]);
  if(r[2]< r[3] ) swap(r[2],r[3]);

  return r;
}

inline tri_t order_tri(const tri_t &t)
{
  tri_t r = t;

  if(r[0]< r[1] ) swap(r[0],r[1]);
  if(r[0]< r[2] ) swap(r[0],r[2]);
  if(r[1]< r[2] ) swap(r[1],r[2]);

  return r;
}

inline ege_t order_ege(const ege_t &t)
{
  ege_t r = t;

  if(r[0]< r[1] ) swap(r[0],r[1]);

  return r;
}

inline int neg_dart(int d) {return int(d/24)*24 + (d+12)%24;}

inline int rot_dart(int d) {return int(d/3)*3 + (d+1)%3;}

inline tri_t neg(tri_t t)
{return mk_cell(t[0],t[2],t[1]);}

inline tri_t rot(tri_t t)
{return mk_cell(t[1],t[2],t[0]);}

const tri_t d_to_t[] =
{
  mk_cell(1,2,3),
  rot(d_to_t[0]),
  rot(d_to_t[1]),

  mk_cell(0,3,2),
  rot(d_to_t[3]),
  rot(d_to_t[4]),

  mk_cell(0,1,3),
  rot(d_to_t[6]),
  rot(d_to_t[7]),

  mk_cell(0,2,1),
  rot(d_to_t[9]),
  rot(d_to_t[10]),

  neg(d_to_t[0]),
  neg(d_to_t[1]),
  neg(d_to_t[2]),

  neg(d_to_t[3]),
  neg(d_to_t[4]),
  neg(d_to_t[5]),

  neg(d_to_t[6]),
  neg(d_to_t[7]),
  neg(d_to_t[8]),

  neg(d_to_t[9]),
  neg(d_to_t[10]),
  neg(d_to_t[11]),
};

inline tri_t dart_to_tri(int d, const tet_list_t &tets)
{
  tri_t tri = d_to_t[d%24];

  tet_t tet = tets[d/24];

  return mk_cell(tet[tri[0]],tet[tri[1]],tet[tri[2]]);
}

inline ege_t dart_to_ege(int d, const tet_list_t &tets)
{
  tri_t tri = d_to_t[d%24];

  tet_t tet = tets[d/24];

  return mk_cell(tet[tri[0]],tet[tri[1]]);
}

inline int dart_to_vrt(int d, const tet_list_t &tets)
{
  tri_t tri = d_to_t[d%24];

  tet_t tet = tets[d/24];

  return tet[tri[0]];
}

inline int get_dart_mapping(int src_dart, int dst_dart,const tet_list_t &tets)
{
  tri_t src_tri = dart_to_tri(neg_dart(src_dart),tets);

  ASSERT(order_tri(src_tri) == order_tri(dart_to_tri(dst_dart,tets)));

  if(src_tri == dart_to_tri(dst_dart,tets))
    return dst_dart;

  dst_dart = rot_dart(dst_dart);

  if(src_tri == dart_to_tri(dst_dart,tets))
    return dst_dart;

  dst_dart = rot_dart(dst_dart);

  if(src_tri == dart_to_tri(dst_dart,tets))
    return dst_dart;

  dst_dart = neg_dart(dst_dart);

  if(src_tri == dart_to_tri(dst_dart,tets))
    return dst_dart;

  dst_dart = rot_dart(dst_dart);

  if(src_tri == dart_to_tri(dst_dart,tets))
    return dst_dart;

  dst_dart = rot_dart(dst_dart);

  if(src_tri == dart_to_tri(dst_dart,tets))
    return dst_dart;

  throw std::logic_error("this should never ever happen");

  return -1;
}

bool cmp_tri(tri_t t1, tri_t t2)
{
  t1 = order_tri(t1);
  t2 = order_tri(t2);

  if( t1[0] != t2[0])
    return t1[0] < t2[0];

  if( t1[1] != t2[1])
    return t1[1] < t2[1];

  return t1[2] < t2[2];
}

bool cmp_ege(ege_t e1, ege_t e2)
{
  e1 = order_ege(e1);
  e2 = order_ege(e2);

  if( e1[0] != e2[0])
    return e1[0] < e2[0];

  return e1[1] < e2[1];
}



typedef std::map<tri_t,cellid_t,bool(*)(tri_t,tri_t) > tri_map_t;
typedef std::map<ege_t,cellid_t,bool(*)(ege_t,ege_t) > ege_map_t;

struct max_tet_idx
{
  typedef int result_type;

  int operator()(tet_t t) const
  {
    return max(max(t[0],t[1]),max(t[2],t[3]));
  }
};

struct min_tet_idx
{
  typedef int result_type;

  int operator()(tet_t t) const
  {
    return min(min(t[0],t[1]),min(t[2],t[3]));
  }
};

inline int drt_beta0(int drt)
{
  return rot_dart(drt);
}

// edgeno indiacates which edge of tet verts (0,1,2,3)
// 0 : (1,2)
// 1 : (2,3)
// 2 : (3,1)
// 3 : (0,3)
// 4 : (2,0)
// 5 : (0,1)

// egeno_to_drt: for a given edgeno (modulo 6)
// which dart (of the tet) has first 2 verts as above
// first 6 +orientation forward edge
// next  6 +orientation reverse edge
// next  6 -orientation forward edge
// next  6 -orientation reverse edge

const int egeno_to_drt[] = { 0, 1, 2, 3, 5, 6,
                            10, 4, 7, 8, 9,11,
                            23,17,20,18,22,12,
                            13,14,12,16,15,19};
// drt_to_egeno: for a given dart which edge it coressponds to

const int drt_to_egeno[] = { 0, 1, 2, 3, 7, 4,
                             5, 8, 9,10, 6,11,
                             8, 6, 7,10, 9, 1,
                             3,11, 2, 5, 4, 0};

inline int drt_beta1(int drt)
{
  int i = drt/24;
  int j = drt%24;

  int e = drt_to_egeno[j];
  e = e%6 + int(j/12)*12 + ((int(e/6)+1)*6)%12;

  return i*24 + egeno_to_drt[e] ;
}

inline int drt_beta2(int drt, const tet_list_t &opp_drt)
{
  int i = drt/24;
  int j = drt%24;

  int ndrt = opp_drt[i][(j/3)%4];

  if(ndrt == -1)
    return -1;

  int ni = ndrt/24;
  int nj = ndrt%24;

  nj = int(nj/3)*3 + (nj + (j%3)*(( int(nj/12) == 1)?(1):(2)))%3;
  nj = ((j/12)*12 + nj)%24;

  return ni*24 + nj;
}

bool check_opp_drt(const tet_list_t &tet_vrt , const tet_list_t &opp_drt)
{
  for( int i = 0 ; i < tet_vrt.size(); ++i)
  {
    for(int j = 0; j < 24; ++j)
    {
      int drt    = i*24+j;
      int drt_b2 = drt_beta2(drt,opp_drt);

      if( drt_b2 != -1)
      {
        tri_t t1 = dart_to_tri(drt,   tet_vrt);
        tri_t t2 = dart_to_tri(drt_b2,tet_vrt);

        ASSERT(drt == drt_beta2(drt_b2,opp_drt));
        ASSERT(t1[0] == t2[0] && t1[1] == t2[2] && t1[2] == t2[1]);
      }
    }
  }

  return true;
}

bool tet_cc_t::test() const
{
  for(int i = 0 ;i < m_cel_drt.size(); ++i)
  {
    ASSERT(m_cel_drt[i] != -1);
  }

  for(int i = 0 ; i < m_tet_vrt.size(); ++i)
  {
    for(int j = 0 ; j < 4; ++j)
    {
      int d = m_cel_drt[m_tet_vrt[i][j]];

      ASSERT(m_tet_vrt[i][j] == dart_to_tri(d,m_tet_vrt)[0]);
    }

    for(int j = 0 ; j < 6; ++j)
    {
      int d  = m_cel_drt[m_tet_ege[i][j]];
      int d1 = i*24 + egeno_to_drt[j];

      ASSERT(order_ege(dart_to_ege(d1,m_tet_vrt)) ==
             order_ege(dart_to_ege(d,m_tet_vrt)));

    }

    for(int j = 0 ; j < 4; ++j)
    {
      int d  = m_cel_drt[m_tet_tri[i][j]];
      int d1 = i*24 + j*3;

      ASSERT(order_tri(dart_to_tri(d1,m_tet_vrt)) ==
             order_tri(dart_to_tri(d,m_tet_vrt)));
    }
  }

  return true;
}

void tet_cc_t::init(const tet_list_t &tets)
{
  br::copy(tets,back_inserter(m_tet_vrt));

  int num_tet = m_tet_vrt.size();

  m_tet_ege.resize(num_tet,mk_cell(-1,-1,-1,-1,-1,-1));
  m_tet_tri.resize(num_tet,mk_cell(-1,-1,-1,-1));
  m_opp_drt.resize(num_tet,mk_cell(-1,-1,-1,-1));

  ASSERT(*br::min_element(m_tet_vrt|badpt::transformed(min_tet_idx())) == 0);

  int num_verts = *br::max_element(m_tet_vrt|badpt::transformed(max_tet_idx()))+1;

  int num_cel  = num_verts;

  ege_map_t ege_map(cmp_ege);

  for( int i = 0 ; i < num_tet ;++i)
  {
    for (int j = 0 ; j < 6; ++j)
    {
      int drt = 24*i + egeno_to_drt[j];

      ege_t ege = dart_to_ege(drt,m_tet_vrt);

      ege_map_t::iterator it = ege_map.find(ege);

      if( it != ege_map.end())
      {
        int odrt = it->second;

        int oi = odrt/24;
        int oj = drt_to_egeno[odrt%24]%6;

        m_tet_ege[i][j] = m_tet_ege[oi][oj];

      }
      else
      {
        ege_map[ege]    = drt;
        m_tet_ege[i][j] = num_cel++;
      }
    }
  }

  int num_edges = num_cel - num_verts;
  ege_map.clear();

  tri_map_t tri_map(cmp_tri);

  for( int i = 0 ; i < num_tet ;++i)
  {
    for( int j = 0 ; j < 4 ; ++j)
    {
      int   drt = i*24 + j*3;
      tri_t tri = dart_to_tri(drt,m_tet_vrt);

      tri_map_t::iterator it = tri_map.find(tri);

      if(it != tri_map.end())
      {
        int odrt = it->second;

        ASSERT(is_in_range(odrt,0,i*24));

        int oi = odrt/24;
        int oj = (odrt%24)/3;

        m_opp_drt[ i][ j] = get_dart_mapping(drt,odrt,m_tet_vrt);
        m_opp_drt[oi][oj] = get_dart_mapping(odrt,drt,m_tet_vrt);

        m_tet_tri[i][j]   = m_tet_tri[oi][oj];

        it->second = -1;

      }
      else
      {
        m_tet_tri[i][j] = num_cel++;
        tri_map[tri]    = drt;
      }
    }
  }

  int num_tris = num_cel - num_edges - num_verts;
  tri_map.clear();

  ASSERT(check_opp_drt(m_tet_vrt,m_opp_drt));

  num_cel     += num_tet;

  base_cc_t::init(num_verts,num_edges,num_tris,num_tet);

  m_cel_drt.resize(num_verts+num_edges+num_tris,-1);

  int vrtno_to_drt [] = { 3, 0, 1, 2,
                          6, 7, 5, 4,
                          9,11,10, 8};

  for(int i = 0 ; i < num_tet; ++i)
  {
    for(int j = 0 ; j < 12 ; ++j)
    {
      int  d  = i*24 + vrtno_to_drt[j];
      int &od = m_cel_drt[m_tet_vrt[i][j%4]] ;

      if(od == -1 || beta2(od) != -1)
        od = d;
    }

    for(int j = 0 ; j < 6 ; ++j)
    {
      m_cel_drt[m_tet_ege[i][j]]  = i*24 + egeno_to_drt[j];
    }

    for(int j = 0 ; j < 4 ; ++j)
    {
      m_cel_drt[m_tet_tri[i][j]] = i*24 + j*3;
    }
  }

  // for an edge cell thats on the boundry ..
  // ensure that the rep dart is on a boundry triangle

  iterator e_b = boost::begin(cel_rng(1));
  iterator e_e = boost::end(cel_rng(1));

  for(; e_b != e_e; ++e_b)
  {
    int  i = *e_b;
    int st = m_cel_drt[i];
    int d  = st;

    do
    {
      int db2 = beta2(beta0(d));

      if(db2 == -1)
        break;

      d = beta1(db2);

      ASSERT(dart_to_ege(d,m_tet_vrt) == dart_to_ege(st,m_tet_vrt));
    }
    while(st != d);

    ASSERT(st == d || beta2(d) == -1);

    m_cel_drt[i] = d;
  }

  ASSERT(this->test());
}

//void tet_cc_t::init_verts(const vert_list_t &verts)
//{
//  m_verts.resize(num_cells());

//  br::copy(verts,m_verts.begin());

//  BOOST_FOREACH(cellid_t c,cel_rng(1))
//  {
//    ege_t e = get_ege(c);
//    m_verts[c] = (m_verts[e[0]]+m_verts[e[1]])/2;
//  }

//  BOOST_FOREACH(cellid_t c,cel_rng(2))
//  {
//    tri_t t = get_tri(c);
//    m_verts[c] = (m_verts[t[0]]+m_verts[t[1]] +m_verts[t[2]])/3;
//  }

//  BOOST_FOREACH(cellid_t c,cel_rng(3))
//  {
//    tet_t t = get_tet(c);
//    m_verts[c] = (m_verts[t[0]]+m_verts[t[1]] +m_verts[t[2]]+m_verts[t[3]])/4;
//  }
//}

int tet_cc_t::beta0(int drt) const
{return drt_beta0(drt);}
int tet_cc_t::beta1(int drt) const
{return drt_beta1(drt);}
int tet_cc_t::beta2(int drt) const
{return drt_beta2(drt,m_opp_drt);}

int tet_cc_t::get_vrt_ege(cellid_t c,cellid_t* f ) const
{
  ASSERT(get_dim(c) == 0);

  int d   = m_cel_drt[c];
  int db2 = 0;

  map<int,int>   vrt_tets;
  stack<int>     dstk;

  dstk.push(d);
  vrt_tets[d/24] = d;

  ASSERT(dart_to_vrt(d,m_tet_vrt) == c);

  db2 = beta2(d);

  if(db2 != -1)
  {
    dstk.push(db2);
    vrt_tets[db2/24] = db2;
    ASSERT(dart_to_vrt(db2,m_tet_vrt) == c);
  }

  while(!dstk.empty())
  {
    d = dstk.top();
    dstk.pop();

    for( int i = 0 ; i < 2 ; ++i)
    {
      d = beta0(beta1(d));

      ENSURE(dart_to_vrt(d,m_tet_vrt) == c,"");
      db2 = beta2(d);

      if(db2 != -1 && vrt_tets.find(db2/24) == vrt_tets.end())
      {
        dstk.push(db2);
        vrt_tets[db2/24] = db2;

        ASSERT(dart_to_vrt(db2,m_tet_vrt) == c);
      }
    }
  }

  set<int> vrt_eges;

  for(map<int,int>::iterator it = vrt_tets.begin() ; it != vrt_tets.end(); ++it)
  {
    int d = it->second;

    for(int i = 0 ; i < 3; ++i)
    {
      vrt_eges.insert(m_tet_ege[d/24][drt_to_egeno[d%24]%6]);
      d = beta0(beta1(d));
    }
  }

  try{

  ASSERT(vrt_eges.size() < DMAX);
  }catch (exception e)
  {
    cerr<<SVAR(vrt_eges.size())<<SVAR(c)<<endl;

    br::copy(vrt_eges|badpt::transformed(bind(&tet_cc_t::get_ege,this,_1)) ,
             ostream_iterator<ege_t>(cerr,"\t"));
    throw;
  }

  br::copy(vrt_eges,f);

  return vrt_eges.size();
}


int tet_cc_t::get_ege_tri(cellid_t c,cellid_t* f ) const
{
  ASSERT(get_dim(c) == 1);

  int st = m_cel_drt[c];
  int d  = st;
  int ct = 0;
  do
  {
    f[ct++] = m_tet_tri[d/24][(d%12)/3];

    d = beta1(d);    

    if(beta2(d) == -1)
    {
      f[ct++] = m_tet_tri[d/24][(d%12)/3];
      break;
    }

    d = beta0(beta0(beta2(d)));

    ASSERT(dart_to_ege(d,m_tet_vrt) == dart_to_ege(st,m_tet_vrt));
  }
  while(st != d);

  ASSERT(st == d || beta2(d) == -1);

  return ct;
}

int tet_cc_t::get_tri_tet(cellid_t c,cellid_t* f ) const
{
  ASSERT(get_dim(c) == 2);

  int d  = m_cel_drt[c];
  int ct = 0;

  f[ct++] = num_upto_dcells(2) + d/24;

  d = beta2(d);

  if( d != -1)
    f[ct++] = num_upto_dcells(2) + d/24;

  return ct;
}

ege_t tet_cc_t::get_ege(cellid_t c) const
{
  ASSERT(get_dim(c) == 1);

  return dart_to_ege(m_cel_drt[c], m_tet_vrt);
}

tri_t tet_cc_t::get_tri(cellid_t c) const
{
  ASSERT(get_dim(c) == 2);

  return dart_to_tri(m_cel_drt[c], m_tet_vrt);
}

tet_t tet_cc_t::get_tet(cellid_t c) const
{
  ASSERT(get_dim(c) == 3);

  return m_tet_vrt[c-num_upto_dcells(2)];
}

tri_t tet_cc_t::get_tri_ege(cellid_t c) const
{
  ASSERT(get_dim(c) == 2);

  tri_t t;

  int i = m_cel_drt[c]/24;
  int j = m_cel_drt[c]%24;

  t[0] = m_tet_ege[i][drt_to_egeno[j]%6]; j = beta0(j);
  t[1] = m_tet_ege[i][drt_to_egeno[j]%6]; j = beta0(j);
  t[2] = m_tet_ege[i][drt_to_egeno[j]%6];

  return t;
}

tet_t tet_cc_t::get_tet_tri(cellid_t c) const
{
  ASSERT(get_dim(c) == 3);

  return m_tet_tri[c-num_upto_dcells(2)];
}


int ct_tet_diff(tet_t t1,tet_t t2)
{
  int ct = 0;

  t1 = order_tet(t1);
  t2 = order_tet(t2);

  BOOST_AUTO(__first1,t1.data());
  BOOST_AUTO(__first2,t2.data());

  BOOST_AUTO(__last1,t1.data()+4);
  BOOST_AUTO(__last2,t2.data()+4);


  while (__first1 != __last1 && __first2 != __last2)
    if (*__first1 > *__first2)
    {
      ++ct;
      ++__first1;
    }
    else if (*__first2 > *__first1)
      ++__first2;
    else
    {
      ++__first1;
      ++__first2;
    }
  return ct + std::distance(__first1,__last1);
}



cellid_t tet_cc_t::get_opp_cell(cellid_t c,cellid_t cf) const
{
  tet_t cv = mk_cell(-1,-1,-1,-1),cfv = mk_cell(-1,-1,-1,-1);

  ASSERT(get_dim(c) +1 == get_dim(cf));

  switch(get_dim(c))
  {
  case 0:cv[0] = c;                 cfv.head<2>() = get_ege(cf); break;
  case 1:cv.head<2>() = get_ege(c); cfv.head<3>() = get_tri(cf); break;
  case 2:cv.head<3>() = get_tri(c); cfv           = get_tet(cf); break;
  };

  cv  = order_tet(cv);
  cfv = order_tet(cfv);

  ASSERT(ct_tet_diff(cv,cfv) == 1);

  for( int i = 0 ; i < 4; ++i)
  {
    if(cfv[i] != cv[i])
      return cfv[i];
  }

  return -1;
}

int tet_cc_t::get_edge_star(cellid_t c,cellid_t* f ) const
{
  ASSERT(get_dim(c) == 1);

  int st = m_cel_drt[c];
  int d  = st;
  int ct = 0;

  do
  {
    f[ct++] = m_tet_tri[d/24][(d%12)/3];
    f[ct++] = num_upto_dcells(2) + d/24;

    d = beta1(d);

    if(beta2(d) == -1)
      break;

    d = beta0(beta0(beta2(d)));

    ASSERT(dart_to_ege(d,m_tet_vrt) == dart_to_ege(st,m_tet_vrt));
  }
  while(st != d);

  f[ct++] = m_tet_tri[d/24][(d%12)/3];

  ASSERT(st == d || beta2(d) == -1);

  return ct;
}

/*---------------------------------------------------------------------------*/

void tet_cc_t::load(std::istream &is)
{
  base_cc_t::load(is);

  utl::bin_read_vec(is,m_tet_vrt);
  utl::bin_read_vec(is,m_tet_ege);
  utl::bin_read_vec(is,m_tet_tri);
  utl::bin_read_vec(is,m_opp_drt);
  utl::bin_read_vec(is,m_cel_drt);
}

/*---------------------------------------------------------------------------*/

void tet_cc_t::save(std::ostream &os) const
{
  base_cc_t::save(os);

  utl::bin_write_vec(os,m_tet_vrt);
  utl::bin_write_vec(os,m_tet_ege);
  utl::bin_write_vec(os,m_tet_tri);
  utl::bin_write_vec(os,m_opp_drt);
  utl::bin_write_vec(os,m_cel_drt);
}

/*****************************************************************************/



/*****************************************************************************/

template <typename T, typename RT,int n>
inline RT nth_elem(const T& arr){return arr[n];}

double get_edge_length(tet_geom_cc_t *tgcc,cellid_t c)
{
  ege_t e = tgcc->get_ege(c);
  return (tgcc->get_vert(e[0]) - tgcc->get_vert(e[1])).norm2();
}

tet_geom_cc_t::tet_geom_cc_t
(const tet_list_t &tets,const vert_list_t &verts)  
{
  tet_cc_t::init(tets);

  ASSERT(verts.size() == num_dcells(0));
  m_verts.resize(verts.size());
  br::copy(verts,m_verts.begin());

  const vert_list_t &vl = m_verts;

  m_bounds[0] = *br::min_element(vl|badpt::transformed(nth_elem<vert_t,double,0>));
  m_bounds[2] = *br::min_element(vl|badpt::transformed(nth_elem<vert_t,double,1>));
  m_bounds[4] = *br::min_element(vl|badpt::transformed(nth_elem<vert_t,double,2>));

  m_bounds[1] = *br::max_element(vl|badpt::transformed(nth_elem<vert_t,double,0>));
  m_bounds[3] = *br::max_element(vl|badpt::transformed(nth_elem<vert_t,double,1>));
  m_bounds[5] = *br::max_element(vl|badpt::transformed(nth_elem<vert_t,double,2>));


  // Stupid Gcc 4.6.3 does not compile
  // *br::min_element(cel_rng(0)|badpt::transformed(bind(get_edge_length,this,_1)))

  m_min_edge_length = std::numeric_limits<double>::max();
  m_max_edge_length = std::numeric_limits<double>::min();

  BOOST_FOREACH(cellid_t c,cel_rng(1))
  {
    m_min_edge_length = std::min(m_min_edge_length,get_edge_length(this,c));
    m_max_edge_length = std::max(m_min_edge_length,get_edge_length(this,c));
  }
}

/*---------------------------------------------------------------------------*/

tet_geom_cc_t::tet_geom_cc_t(std::istream &is)
{
  tet_cc_t::load(is);

  utl::bin_read(is,m_bounds[0]);
  utl::bin_read(is,m_bounds[1]);
  utl::bin_read(is,m_bounds[2]);
  utl::bin_read(is,m_bounds[3]);
  utl::bin_read(is,m_bounds[4]);
  utl::bin_read(is,m_bounds[5]);

  utl::bin_read_vec(is,m_verts);

  utl::bin_read(is,m_min_edge_length);
  utl::bin_read(is,m_max_edge_length);
}

/*---------------------------------------------------------------------------*/

void tet_geom_cc_t::save(std::ostream &os) const
{
  tet_cc_t::save(os);

  utl::bin_write(os,m_bounds[0]);
  utl::bin_write(os,m_bounds[1]);
  utl::bin_write(os,m_bounds[2]);
  utl::bin_write(os,m_bounds[3]);
  utl::bin_write(os,m_bounds[4]);
  utl::bin_write(os,m_bounds[5]);

  utl::bin_write_vec(os,m_verts);

  utl::bin_write(os,m_min_edge_length);
  utl::bin_write(os,m_max_edge_length);
}
/*****************************************************************************/



/*****************************************************************************/

ptet_cc_t::ptet_cc_t(const tet_list_t &tets,const tet_pdomain_list_t &ptets)  
{
  tet_cc_t::init(tets);
  ASSERT(tets.size() == ptets.size());
  m_ptets.resize(ptets.size());
  br::copy(ptets,m_ptets.begin());
}

/*---------------------------------------------------------------------------*/

template<>
ptet_cc_t::cell_pdomain_t<2>::type ptet_cc_t::get_pdomain<2>(cellid_t c) const
{
  ASSERT(get_dim(c)+1 == 2);

  cell_pdomain_t<2>::type ret;

  int tno  = m_cel_drt[c]/24;
  int tver = m_cel_drt[c]%24;

  ret[0] = m_ptets[tno][d_to_t[tver][0]];
  ret[1] = m_ptets[tno][d_to_t[tver][1]];

  // just to check
  ege_t ege = get_ege(c);
  ASSERT(m_tet_vrt[tno][d_to_t[tver][0]] == ege[0]);
  ASSERT(m_tet_vrt[tno][d_to_t[tver][1]] == ege[1]);

  return ret;
}

template<>
ptet_cc_t::cell_pdomain_t<3>::type ptet_cc_t::get_pdomain<3>(cellid_t c) const
{
  ASSERT(get_dim(c)+1 == 3);

  cell_pdomain_t<3>::type ret;

  int tno  = m_cel_drt[c]/24;
  int tver = m_cel_drt[c]%24;

  ret[0] = m_ptets[tno][d_to_t[tver][0]];
  ret[1] = m_ptets[tno][d_to_t[tver][1]];
  ret[2] = m_ptets[tno][d_to_t[tver][2]];

  tri_t tri = get_tri(c);
  ASSERT(m_tet_vrt[tno][d_to_t[tver][0]] == tri[0]);
  ASSERT(m_tet_vrt[tno][d_to_t[tver][1]] == tri[1]);
  ASSERT(m_tet_vrt[tno][d_to_t[tver][2]] == tri[2]);

  return ret;
}

template<>
ptet_cc_t::cell_pdomain_t<4>::type ptet_cc_t::get_pdomain<4>(cellid_t c) const
{
  ASSERT(get_dim(c)+1 == 4);
  return m_ptets[c-num_upto_dcells(2)];
}

/*---------------------------------------------------------------------------*/

template <>
ptet_cc_t::pcell_t<2>::type ptet_cc_t::align_cwrtf<2>(cellid_t c,cellid_t f) const
{
  ASSERT(get_dim(c) == 1);
  ASSERT(get_dim(f) < get_dim(c));

  pcell_t<2>::type c_pcell = get_pcell<2>(c);

  int fv0_in_c = (f == c_pcell[0].cid)?(0):(1);

  ASSERT(c_pcell[fv0_in_c].cid == f);

  utl::ivec3 diff = c_pcell[fv0_in_c].pd_org();

  c_pcell[0].set_pd_org(c_pcell[0].pd_org() - diff);
  c_pcell[1].set_pd_org(c_pcell[1].pd_org() - diff);

  return c_pcell;
}

template <>
ptet_cc_t::pcell_t<3>::type ptet_cc_t::align_cwrtf<3>(cellid_t c,cellid_t f) const
{
  ASSERT(get_dim(c) == 2);
  ASSERT(get_dim(f) < get_dim(c));

  pcellid_t    fv0;
  int          fdrt    = m_cel_drt[f];

  fv0.cid = m_tet_vrt[fdrt/24][d_to_t[fdrt%24][0]];
  fv0.did = m_ptets  [fdrt/24][d_to_t[fdrt%24][0]];

  pcell_t<3>::type c_pcell = get_pcell<3>(c);

  int fv0_in_c = (fv0.cid == c_pcell[0].cid)?(0):
                ((fv0.cid == c_pcell[1].cid)?(1):(2));

  ASSERT(c_pcell[fv0_in_c].cid == fv0.cid);

  utl::ivec3 diff = fv0.pd_org() - c_pcell[fv0_in_c].pd_org();

  c_pcell[0].set_pd_org(c_pcell[0].pd_org() + diff);
  c_pcell[1].set_pd_org(c_pcell[1].pd_org() + diff);
  c_pcell[2].set_pd_org(c_pcell[2].pd_org() + diff);

  return c_pcell;
}

template <>
ptet_cc_t::pcell_t<4>::type ptet_cc_t::align_cwrtf<4>(cellid_t c,cellid_t f) const
{
  ASSERT(get_dim(c) == 3);
  ASSERT(get_dim(f) < get_dim(c));

  pcellid_t    fv0;
  int          fdrt    = m_cel_drt[f];

  fv0.cid = m_tet_vrt[fdrt/24][d_to_t[fdrt%24][0]];
  fv0.did = m_ptets  [fdrt/24][d_to_t[fdrt%24][0]];

  pcell_t<4>::type c_pcell = get_pcell<4>(c);

  int fv0_in_c = (fv0.cid == c_pcell[0].cid)?(0):
                ((fv0.cid == c_pcell[1].cid)?(1):
                ((fv0.cid == c_pcell[2].cid)?(2):(3)));

  ASSERT(c_pcell[fv0_in_c].cid == fv0.cid);

  utl::ivec3 diff = fv0.pd_org() - c_pcell[fv0_in_c].pd_org();

  c_pcell[0].set_pd_org(c_pcell[0].pd_org() + diff);
  c_pcell[1].set_pd_org(c_pcell[1].pd_org() + diff);
  c_pcell[2].set_pd_org(c_pcell[2].pd_org() + diff);
  c_pcell[3].set_pd_org(c_pcell[3].pd_org() + diff);

  return c_pcell;
}

/*---------------------------------------------------------------------------*/

ptet_cc_t::ptet_cc_t(std::istream &is)
{
  tet_cc_t::load(is);
  utl::bin_read_vec(is,m_ptets);
}

/*---------------------------------------------------------------------------*/

void ptet_cc_t::save(std::ostream &os) const
{
  tet_cc_t::save(os);
  utl::bin_write_vec(os,m_ptets);
}

/*****************************************************************************/



/*****************************************************************************/

double get_edge_length(ptet_geom_cc_t *ptgcc,cellid_t c)
{
  ptet_cc_t::pege_t e = ptgcc->get_pcell<2>(c);
  return (ptgcc->get_vert(e[0]) - ptgcc->get_vert(e[1])).norm2();
}

ptet_geom_cc_t::ptet_geom_cc_t
(const tet_list_t &tets, const tet_pdomain_list_t &ptets,
 const vert_list_t &verts, const vert_t &dom_sz)
  :ptet_cc_t(tets,ptets),m_dom_sz(dom_sz)
{
  ASSERT(verts.size() == num_dcells(0));
  m_verts.resize(verts.size());
  br::copy(verts,m_verts.begin());

  const vert_list_t &vl = m_verts;

  vert_t lc,uc;

  lc[0] = *br::min_element(vl|badpt::transformed(nth_elem<vert_t,double,0>));
  lc[1] = *br::min_element(vl|badpt::transformed(nth_elem<vert_t,double,1>));
  lc[2] = *br::min_element(vl|badpt::transformed(nth_elem<vert_t,double,2>));

  uc[0] = *br::max_element(vl|badpt::transformed(nth_elem<vert_t,double,0>));
  uc[1] = *br::max_element(vl|badpt::transformed(nth_elem<vert_t,double,1>));
  uc[2] = *br::max_element(vl|badpt::transformed(nth_elem<vert_t,double,2>));

  init_bb(lc,uc);


  // Stupid Gcc 4.6.3 does not compile
  // *br::min_element(cel_rng(0)|badpt::transformed(bind(get_edge_length,this,_1)))

  m_min_edge_length = std::numeric_limits<double>::max();
  m_max_edge_length = std::numeric_limits<double>::min();

  BOOST_FOREACH(cellid_t c,cel_rng(1))
  {
    m_min_edge_length = std::min(m_min_edge_length,get_edge_length(this,c));
    m_max_edge_length = std::max(m_min_edge_length,get_edge_length(this,c));
  }

}

/*---------------------------------------------------------------------------*/

template<>
double ptet_geom_cc_t::get_cell_size<DES,1>(cellid_t c) const{return 0;}

template<>
double ptet_geom_cc_t::get_cell_size<DES,2>(cellid_t c) const
{
  pege_t e = get_pcell<2>(c);

  vert_t v1 = get_vert(e[0]);
  vert_t v2 = get_vert(e[1]);

  return (v1-v2).norm2();
}

template<>
double ptet_geom_cc_t::get_cell_size<DES,3>(cellid_t c) const
{
  ptri_t t = get_pcell<3>(c);

  vert_t v1 = get_vert(t[0]);
  vert_t v2 = get_vert(t[1]);
  vert_t v3 = get_vert(t[2]);

  return (utl::cross(v1-v2,v1-v3)).norm2()/2;
}

template<>
double ptet_geom_cc_t::get_cell_size<DES,4>(cellid_t c) const
{
  ptet_t t = get_pcell<4>(c);

  vert_t v1 = get_vert(t[0]);
  vert_t v2 = get_vert(t[1]);
  vert_t v3 = get_vert(t[2]);
  vert_t v4 = get_vert(t[3]);

  return std::abs(utl::dot(utl::cross(v1-v2,v1-v3),v1-v4))/6;
}


template<>
double ptet_geom_cc_t::get_cell_size<ASC,2>(cellid_t e) const
{
  double sz = 0;

  cellid_t link[DMAX];
  int ct = get_edge_star(e,link);

  vert_t ecnt = get_centroid<2>(e);
  vert_t pcnt = get_centroid<3>(align_cwrtf<3>(link[0],e));

  for(int i = 0 ; i < ct-2 ; i+= 2)
  {
    vert_t tcnt = get_centroid<4>(align_cwrtf<4>(link[i+1],e));
    sz += utl::cross(ecnt-pcnt,ecnt-tcnt).norm2();

    pcnt = get_centroid<3>(align_cwrtf<3>(link[i+2],e));
    sz += utl::cross(ecnt-pcnt,ecnt-tcnt).norm2();
  }

  return sz;
}

template<>
double ptet_geom_cc_t::get_cell_size<ASC,4>(cellid_t c) const{return 0;}

/*---------------------------------------------------------------------------*/

ptet_geom_cc_t::ptet_geom_cc_t(std::istream &is):ptet_cc_t(is)
{
  utl::bin_read(is,m_dom_sz);

  vert_t lc,uc;

  utl::bin_read(is,lc);
  utl::bin_read(is,uc);

  init_bb(lc,uc);

  utl::bin_read_vec(is,m_verts);

  utl::bin_read(is,m_min_edge_length);
  utl::bin_read(is,m_max_edge_length);
}

/*---------------------------------------------------------------------------*/

void ptet_geom_cc_t::save(std::ostream &os) const
{
  ptet_cc_t::save(os);

  utl::bin_write(os,m_dom_sz);

  utl::bin_write(os,get_lc());
  utl::bin_write(os,get_uc());

  utl::bin_write_vec(os,m_verts);

  utl::bin_write(os,m_min_edge_length);
  utl::bin_write(os,m_max_edge_length);
}


/*---------------------------------------------------------------------------*/

vert_t ptet_geom_cc_t::get_vertex(cellid_t c) const
{return get_vert(c);}

/*****************************************************************************/
}
