#ifndef CUBE_CC_HPP_INCLUDED
#define CUBE_CC_HPP_INCLUDED

#include <set>
#include <iterator>

#include <boost/iterator/counting_iterator.hpp>
#include <boost/array.hpp>

#include <vec.hpp>

#include <base_cc.hpp>

namespace tet
{

/// \brief A class to represent 3-dimensional cubical complexes.
class cube_cc_t : public base_cc_geom_t
{
public:
  /// \brief Constructor
  cube_cc_t();

  /// \brief init grid
  void init(int X, int Y, int Z);
  void init(int X, int Y, int Z,vert_t lc, vert_t uc);


public:
  int      get_fcts(cellid_t c,cellid_t* f )    const;
  int      get_cofs(cellid_t c,cellid_t* cf)    const;
  int      get_vrts(cellid_t c,cellid_t* v )    const;
  int      is_bnd(cellid_t c)                   const;
  cellid_t get_opp_cell(cellid_t c,cellid_t cf) const;
  bool     test()                               const;

  vert_t   get_vertex(cellid_t c)               const;
  vert_t   get_centroid(cellid_t c)             const;


protected:
  /// \brief coordinate type
  typedef utl::ivec3 coords_t;

  /// \brief Axis aligned bounding rectangles (boundary inclusive)
  struct rect_t
  {
    /// \brief lower corner
    coords_t lc;

    /// \brief upper corner
    coords_t uc;

    /// \brief ctor
    rect_t(coords_t lc,coords_t uc);

    /// \brief copy ctor
    rect_t(const rect_t & r);

    /// \brief default ctor
    rect_t();
  };

  /// \brief A rect for each cell type (boundary inclusive,scaled by 2)
  ///
  /// \note (0,0,0)  (2*X-2,2*Y-2,2*Z-2) --> verts
  ///       (1,0,0)  (2*X-3,2*Y-2,2*Z-2) --> edges in xdir
  ///       (0,1,0)  (2*X-2,2*Y-3,2*Z-2) --> edges in ydir
  ///       (0,0,1)  (2*X-2,2*Y-2,2*Z-3) --> edges in zdir
  ///       (0,1,1)  (2*X-2,2*Y-3,2*Z-3) --> faces in xdir
  ///       (1,0,1)  (2*X-3,2*Y-2,2*Z-3) --> faces in ydir
  ///       (1,1,0)  (2*X-3,2*Y-3,2*Z-2) --> faces in zdir
  ///       (1,1,1)  (2*X-3,2*Y-3,2*Z-3) --> cubes
  boost::array<rect_t,1+3+3+1>     m_cell_rects;

  /// \brief id offsets for each cell_rect's cells
  boost::array<cellid_t,1+1+3+3+1> m_cell_rect_idoffsets;

  /// \brief convert a cellid to coords
  coords_t cellid_to_coords(cellid_t id) const;

  /// \brief convert coords to cellid
  cellid_t coords_to_cellid(coords_t crd) const;
};
}

#endif
