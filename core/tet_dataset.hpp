/***************************************************************************
 *   Copyright (C) 2009 by nithin,,,   *
 *   nithin@gauss   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef TET_DATASET_HPP_INCLUDED
#define TET_DATASET_HPP_INCLUDED

#include <boost/iterator/counting_iterator.hpp>

#include <tet.hpp>
#include <base_cc.hpp>

/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/
/// \brief A class to compute a discrete gradient field and mscomplex
class dataset_t
{
public:

  /// \brief Ctor
  dataset_t (const fn_list_t &fns,base_cc_ptr_t tcc);

public:
  /// \brief work
  void  work(mscomplex_ptr_t);

  /// \brief get manifold
  void  get_manifold(eGDIR d, const cellid_list_t &s, cellid_list_t &m) const;

public:
  /// \privatesection
  template<eGDIR dir> int get_cets(cellid_t c,cellid_t *cets) const;
  template<eGDIR dir> int get_co_cets(cellid_t c,cellid_t *cets) const;

  const cellid_t& max_fct(cellid_t ) const;
  template <int dim> cellid_t max_vert(cellid_t c) const;
  fn_t           fn(cellid_t c) const;

  template <int dim>
  bool compare_cells   (const cellid_t & c1, const cellid_t &c2) const;
  bool compare_cells_nd(const cellid_t & c1, const cellid_t &c2) const;

  template <int dim>
  bool compare_cells_pp   (const cellid_t & c1, const cellid_t &c2) const;
  bool compare_cells_pp_nd(const cellid_t & c1, const cellid_t &c2) const;

  const cellid_t& pair(cellid_t ) const;
  void pair(cellid_t,cellid_t );

  bool is_paired(cellid_t) const;
  bool is_critical(cellid_t) const;

public:
  /// \privatesection
  const fn_list_t    &m_vert_fns;
  cellid_list_t       m_cell_pairs;
  cellid_list_t       m_cell_mxfct;
  base_cc_ptr_t       m_tcc;
};
/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/





/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/

template<> inline int dataset_t::get_cets<DES>(cellid_t c,cellid_t *cts) const
{return m_tcc->get_fcts(c,cts);}
template<> inline int dataset_t::get_cets<ASC>(cellid_t c,cellid_t *cts) const
{return m_tcc->get_cofs(c,cts);}

/*---------------------------------------------------------------------------*/

template<eGDIR dir> inline int dataset_t::get_co_cets(cellid_t c,cellid_t *cts)
const {return get_cets<(dir == DES)?(ASC):(DES)>(c,cts);}

/*---------------------------------------------------------------------------*/

inline const cellid_t& dataset_t::pair(cellid_t c) const
{ASSERT(m_cell_pairs[c] != invalid_cellid);return m_cell_pairs[c];}

/*---------------------------------------------------------------------------*/

inline void dataset_t::pair(cellid_t c,cellid_t p)
{
  ASSERT(m_cell_pairs[c] == invalid_cellid);m_cell_pairs[c] = p;
  ASSERT(m_cell_pairs[p] == invalid_cellid);m_cell_pairs[p] = c;
}

/*---------------------------------------------------------------------------*/

inline bool dataset_t::is_paired(cellid_t c) const
{return m_cell_pairs[c] != invalid_cellid;}

/*---------------------------------------------------------------------------*/

inline bool dataset_t::is_critical(cellid_t c) const
{return m_cell_pairs[c] == invalid_cellid;}

/*---------------------------------------------------------------------------*/

inline const cellid_t& dataset_t::max_fct(cellid_t c) const
{ASSERT(m_cell_mxfct[c] != invalid_cellid);return m_cell_mxfct[c];}

/*---------------------------------------------------------------------------*/

template <int dim> inline cellid_t dataset_t::max_vert(cellid_t c) const
{return max_vert<dim-1>(max_fct(c));}
template <> inline cellid_t dataset_t::max_vert<0>(cellid_t c) const
{return c;}
template <> inline cellid_t dataset_t::max_vert<-1>(cellid_t c) const
{
  switch(m_tcc->get_dim(c))
  {
    case 3: c = max_fct(c);
    case 2: c = max_fct(c);
    case 1: c = max_fct(c);
    case 0: return c;
  }
  ASSERT(false&&"invalid celldim");
  return c;
}

/*---------------------------------------------------------------------------*/

inline fn_t dataset_t::fn(cellid_t c) const
{return m_vert_fns[max_vert<-1>(c)];}

/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/





/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/

template <int dim>
inline bool dataset_t::compare_cells(const cellid_t & c1, const cellid_t &c2) const
{
  cellid_t f1 = max_fct(c1);
  cellid_t f2 = max_fct(c2);

  if(f1 != f2)
    return compare_cells<dim-1>(f1,f2);

  f1 = m_tcc->get_opp_cell(f1,c1);
  f2 = m_tcc->get_opp_cell(f2,c2);

  int is_bnd_f1 = m_tcc->is_bnd(f1);
  int is_bnd_f2 = m_tcc->is_bnd(f2);

  if(is_bnd_f1 != is_bnd_f2)
    return (is_bnd_f2 < is_bnd_f1);

  switch(m_tcc->get_dim(f1))
  {
    case 2: return compare_cells<2>(f1,f2);
    case 1: return compare_cells<1>(f1,f2);
    case 0: return compare_cells<0>(f1,f2);
    default:
      ENSURES(false) << "Should not reach here" ;
      return false;
  }
}

/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*/

template <>
inline bool dataset_t::compare_cells<0>(const cellid_t & c1, const cellid_t &c2) const
{
  ASSERT(m_tcc->get_dim(c1) == 0);
  ASSERT(m_tcc->get_dim(c2) == 0);

  fn_t f1 = m_vert_fns[c1];
  fn_t f2 = m_vert_fns[c2];

  if (f1 != f2)
    return f1 < f2;

  return c1 < c2;
}

/*---------------------------------------------------------------------------*/

inline bool dataset_t::compare_cells_nd(const cellid_t & c1, const cellid_t &c2) const
{
  int f1 = c1;
  int f2 = c2;

  int d1 = m_tcc->get_dim(c1);
  int d2 = m_tcc->get_dim(c2);

  for(int i = d1 ; i < d2 ; ++i)
    f2 = max_fct(f2);

  for(int i = d2 ; i < d1 ; ++i)
    f1 = max_fct(f1);

  if( f1 == f2)
    return (d1 < d2);

  switch(std::min(d1,d2))
  {
    case 3: return compare_cells<3>(f1,f2);
    case 2: return compare_cells<2>(f1,f2);
    case 1: return compare_cells<1>(f1,f2);
    case 0: return compare_cells<0>(f1,f2);
  }
  ASSERT(false);
  return false;
}

/*---------------------------------------------------------------------------*/

template <int dim>
inline bool dataset_t::compare_cells_pp(const cellid_t & c1, const cellid_t &c2) const
{
  cellid_t oc1 = c1,oc2 = c2;

  if(is_paired(c1) && m_tcc->get_dim(pair(c1)) == dim+1)
    oc1 = max_fct(pair(c1));

  if(is_paired(c2) && m_tcc->get_dim(pair(c2)) == dim+1)
    oc2 = max_fct(pair(c2));

  return compare_cells<dim>(oc1,oc2);
}

/*---------------------------------------------------------------------------*/

inline bool dataset_t::compare_cells_pp_nd(const cellid_t & c1, const cellid_t &c2) const
{
  //   if c1,c2 is a pair then post pairing the lower dim cell is higher
  if( is_paired(c1) && c2 == pair(c1))
    return m_tcc->get_dim(c2) < m_tcc->get_dim(c1);

  // if they are not pairs, then if either is the lower dim cell of a
  // pair, it is represented by the higher dim cell of the pair (oc)
  cellid_t oc1 = c1,oc2 = c2;

  if(is_paired(c1) && m_tcc->get_dim(pair(c1)) == m_tcc->get_dim(c1)+1)
    oc1 = pair(c1);

  if(is_paired(c2) && m_tcc->get_dim(pair(c2)) == m_tcc->get_dim(c2)+1)
    oc2 = pair(c2);

  return compare_cells_nd(oc1,oc2);
}
/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/
#endif
