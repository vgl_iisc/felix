#include <boost/range/iterator_range.hpp>
#include <boost/range/difference_type.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/bind/bind.hpp>

#include <fstream>

#include <tet.hpp>
#include <tet_cc.hpp>

using namespace std;

namespace ba = boost::algorithm;

namespace tet
{

const char * const g_cp_type_names[] = {
  "minimum",
  "1-saddle",
  "2-saddle",
  "maximum",
  "critical points"
};

const char * const g_gdir_names[] = {
  "descending",
  "ascending"
};

void read_ts(std::string f,tet_cc_t::tet_list_t *tets,
             int compno,fn_list_t *fns,
             vert_list_t *verts,
             ptet_cc_t::tet_pdomain_list_t *ptets,
             vert_t *sz)
{
  fstream fs(f.c_str(),ios::in);
  ENSURE(fs.is_open(),"unable to open file");

  string l;

  getline(fs,l);

  ba::trim(l);

  vector<string> strs;

  ba::split(strs,l, ba::is_any_of("\t \n"));

  int nv = atoi(strs[0].c_str());
  int nt = atoi(strs[1].c_str());

  if(verts)
    verts->resize(nv);

  if(fns)
    fns->resize(nv);

  for( int i = 0 ; i < nv; ++i)
  {
    strs.clear();l.clear();

    getline(fs,l);

    ba::trim(l);ba::split(strs,l, ba::is_any_of("\t \n"));

    if(verts)
    {
      verts->at(i)[0] = atof(strs[0].c_str());
      verts->at(i)[1] = atof(strs[1].c_str());
      verts->at(i)[2] = atof(strs[2].c_str());
    }

    if(fns)
    {
      ENSURE(is_in_range(compno,0,strs.size()),"missing info!!");
      fns->at(i) = atof(strs[compno].c_str());
    }
  }

  tets->resize(nt);


  for( int i = 0 ; i < nt; ++i)
  {
    strs.clear();l.clear();

    getline(fs,l);

    ba::trim(l);ba::split(strs,l, ba::is_any_of("\t \n"));

    tets->at(i)[0] = atoi(strs[0].c_str());
    tets->at(i)[1] = atoi(strs[1].c_str());
    tets->at(i)[2] = atoi(strs[2].c_str());
    tets->at(i)[3] = atoi(strs[3].c_str());
  }

  l.clear();

  if(!fs.eof())
    getline(fs,l);

  if(!ptets || l != "PERIODIC TETRAHEDRA INFO BEGIN")
    return;

  ptets->resize(nt);

  l.clear();
  getline(fs,l);

  strs.clear();
  ba::split(strs,l, ba::is_any_of("\t \n"));

  vert_t lc,uc;

  lc[0] = atof(strs[1].c_str());
  uc[0] = atof(strs[2].c_str());
  lc[1] = atof(strs[3].c_str());
  uc[1] = atof(strs[4].c_str());
  lc[2] = atof(strs[5].c_str());
  uc[2] = atof(strs[6].c_str());

  *sz = uc-lc;

  l.clear();
  getline(fs,l);

  for( int i = 0 ; i < nt; ++i)
  {
    strs.clear();l.clear();

    getline(fs,l);

    ba::trim(l);ba::split(strs,l, ba::is_any_of("\t \n"));

    ptets->at(i)[0] = atoi(strs[0].c_str());
    ptets->at(i)[1] = atoi(strs[1].c_str());
    ptets->at(i)[2] = atoi(strs[2].c_str());
    ptets->at(i)[3] = atoi(strs[3].c_str());
  }

  l.clear();
  getline(fs,l);
}



}
