#include <cube_cc.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/foreach.hpp>

typedef utl::ivec3 coords_t;

namespace br = boost::range;

/*===========================================================================*/
namespace tet {
/*---------------------------------------------------------------------------*/

cube_cc_t::rect_t::rect_t():lc(utl::mk_vec(0,0,0)),uc(utl::mk_vec(0,0,0)){}
cube_cc_t::rect_t::rect_t(const rect_t &r):lc(r.lc),uc(r.uc){}
cube_cc_t::rect_t::rect_t(coords_t lc, coords_t uc):lc(lc),uc(uc){}

/*---------------------------------------------------------------------------*/
cube_cc_t::cube_cc_t()
{
  for(int i = 0; i <= 8 ; ++i)
    m_cell_rect_idoffsets[i] = 0;
}

/*---------------------------------------------------------------------------*/

void cube_cc_t::init(int X, int Y, int Z, vert_t lc, vert_t uc)
{
  m_cell_rects[0] = rect_t(utl::mk_vec(0,0,0),utl::mk_vec(X*2-2,Y*2-2,Z*2-2));
  m_cell_rects[1] = rect_t(utl::mk_vec(1,0,0),utl::mk_vec(X*2-3,Y*2-2,Z*2-2));
  m_cell_rects[2] = rect_t(utl::mk_vec(0,1,0),utl::mk_vec(X*2-2,Y*2-3,Z*2-2));
  m_cell_rects[3] = rect_t(utl::mk_vec(0,0,1),utl::mk_vec(X*2-2,Y*2-2,Z*2-3));
  m_cell_rects[4] = rect_t(utl::mk_vec(0,1,1),utl::mk_vec(X*2-2,Y*2-3,Z*2-3));
  m_cell_rects[5] = rect_t(utl::mk_vec(1,0,1),utl::mk_vec(X*2-3,Y*2-2,Z*2-3));
  m_cell_rects[6] = rect_t(utl::mk_vec(1,1,0),utl::mk_vec(X*2-3,Y*2-3,Z*2-2));
  m_cell_rects[7] = rect_t(utl::mk_vec(1,1,1),utl::mk_vec(X*2-3,Y*2-3,Z*2-3));

  m_cell_rect_idoffsets[0] = 0;

  for(int i = 1; i <= 8 ; ++i)
  {
    coords_t l = m_cell_rects[i-1].lc;
    coords_t u = m_cell_rects[i-1].uc;
    coords_t s = (u - l)/2 + 1;

    m_cell_rect_idoffsets[i] =
        m_cell_rect_idoffsets[i-1] + s[0]*s[1]*s[2];
  }

  base_cc_geom_t::init
      ( m_cell_rect_idoffsets[1] - m_cell_rect_idoffsets[0],
        m_cell_rect_idoffsets[4] - m_cell_rect_idoffsets[1],
        m_cell_rect_idoffsets[7] - m_cell_rect_idoffsets[4],
        m_cell_rect_idoffsets[8] - m_cell_rect_idoffsets[7]);

  init_bb(lc,uc);
}

/*---------------------------------------------------------------------------*/
void cube_cc_t::init(int X, int Y, int Z)
{init(X,Y,Z,utl::mk_vec(0,0,0),utl::mk_vec(X-1,Y-1,Z-1));}


/*---------------------------------------------------------------------------*/

int cube_cc_t::get_fcts(cellid_t id,cellid_t* f ) const
{
  coords_t crd = cellid_to_coords(id);

  int pos = 0;

  for(int d = 0; d< 3; ++d)
    for(int i = 0 ; i < (crd[d]&1);++i)
      for(int j = -1 ; j <=1; j+=2)
      {
        coords_t f_crd = crd;
        f_crd[d]      += j;
        f[pos++]       = coords_to_cellid(f_crd);
      }

  return pos;
}

/*---------------------------------------------------------------------------*/

int cube_cc_t::get_cofs(cellid_t id,cellid_t* cf) const
{
  coords_t crd = cellid_to_coords(id);

  coords_t l = m_cell_rects[0].lc;
  coords_t u = m_cell_rects[0].uc;

  int pos = 0;

  for(    int d =  0 ; d < 3; ++d)
    for(  int i =  0 ; i < ((crd[d]+1)&1);++i)
      for(int j = -1 ; j <=1; j+=2)
      {
        coords_t cf_crd = crd;
        cf_crd[d]      += j;

        if(l[d] <= cf_crd[d] &&  cf_crd[d] <= u[d])
          cf[pos++]     = coords_to_cellid(cf_crd);
      }

  return pos;
}

/*---------------------------------------------------------------------------*/

int cube_cc_t::get_vrts(cellid_t id,cellid_t* v ) const
{
  int pos = 0;

  coords_t crd = cellid_to_coords(id) , i;

  for(    i[2] = -(crd[2]&1) ; i[2] <= (crd[2]&1) ;i[2]+=2)
    for(  i[1] = -(crd[1]&1) ; i[1] <= (crd[1]&1) ;i[1]+=2)
      for(i[0] = -(crd[0]&1) ; i[0] <= (crd[0]&1) ;i[0]+=2)
        v[pos++] = coords_to_cellid(crd+i);

  return pos;
}

/*---------------------------------------------------------------------------*/

int cube_cc_t::is_bnd(cellid_t c) const
{
  coords_t crd = cellid_to_coords(c);

  coords_t l = m_cell_rects[0].lc;
  coords_t u = m_cell_rects[0].uc;

  return ((crd[0] == l[0] ||crd[0] == u[0])?(1):(0))
      +  ((crd[1] == l[1] ||crd[1] == u[1])?(1):(0))
      +  ((crd[2] == l[2] ||crd[2] == u[2])?(1):(0));

}

/*---------------------------------------------------------------------------*/

cube_cc_t::cellid_t cube_cc_t::get_opp_cell(cellid_t c_id,cellid_t cf_id) const
{
  coords_t c_crd  = cellid_to_coords(c_id);
  coords_t cf_crd = cellid_to_coords(cf_id);
  return coords_to_cellid(2*cf_crd - c_crd);
}

/*---------------------------------------------------------------------------*/

cube_cc_t::coords_t cube_cc_t::cellid_to_coords(cellid_t id) const
{
  int r_no = br::upper_bound(m_cell_rect_idoffsets,id) -
      m_cell_rect_idoffsets.begin()-1;

  coords_t l = m_cell_rects[r_no].lc;
  coords_t u = m_cell_rects[r_no].uc;
  coords_t s = (u - l)/2 + 1;

  id -= m_cell_rect_idoffsets[r_no];

  coords_t crd;

  crd[0] = l[0] + ((id)            %s[0])*2;
  crd[1] = l[1] + ((id/ s[0])      %s[1])*2;
  crd[2] = l[2] + ((id/(s[0]*s[1]))%s[2])*2;

  return crd;
}

/*---------------------------------------------------------------------------*/

cube_cc_t::cellid_t cube_cc_t::coords_to_cellid(cube_cc_t::coords_t crd) const
{
  static int r_no_tab[2][2][2] = {{{0,3},{2,4}},{{1,5},{6,7}}};

  int r_no = r_no_tab[crd[0]&1][crd[1]&1][crd[2]&1];

  coords_t l = m_cell_rects[r_no].lc;
  coords_t u = m_cell_rects[r_no].uc;
  coords_t s = (u - l)/2 + 1;

  cellid_t id = (
      (crd[0] - l[0]) +
      (crd[1] - l[1])*s[0] +
      (crd[2] - l[2])*s[0]*s[1])/2;

  id += m_cell_rect_idoffsets[r_no];

  return id;
}

/*---------------------------------------------------------------------------*/

bool cube_cc_t::test() const
{
  bool ret = base_cc_t::test();

  BOOST_FOREACH(cellid_t id,cel_rng())
  {
    ASSERT(id == coords_to_cellid(cellid_to_coords(id)));
  }

  return ret;
}

/*---------------------------------------------------------------------------*/

cube_cc_t::vert_t   cube_cc_t::get_vertex(cube_cc_t::cellid_t c)   const
{ vert_t r = (cellid_to_coords(c) - m_cell_rects[0].lc)*(get_uc() - get_lc());
  r /= (m_cell_rects[0].uc - m_cell_rects[0].lc );
  return r + get_lc();
}

/*---------------------------------------------------------------------------*/

cube_cc_t::vert_t   cube_cc_t::get_centroid(cube_cc_t::cellid_t c) const
{ vert_t r = (cellid_to_coords(c) - m_cell_rects[0].lc)*(get_uc() - get_lc());
  r /= (m_cell_rects[0].uc - m_cell_rects[0].lc );
  return r + get_lc();}


/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/




/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/

base_cc_geom_t::vert_t base_cc_geom_t::get_centroid(cellid_t c) const
{
  cellid_t pts[20];

  int n = get_vrts(c,pts);

  vert_t r = utl::mk_vec(0,0,0);

  for(int i = 0; i < n; ++i) r += get_vertex(pts[i]);

  return r/n;
}
/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/
