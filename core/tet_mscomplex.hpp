/***************************************************************************
 *   Copyright (C) 2009 by nithin,,,   *
 *   nithin@gauss   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TET_MSCOMPLEX_HPP_INCLUDED
#define TET_MSCOMPLEX_HPP_INCLUDED

#include <set>
#include <map>

#include <iostream>
#include <fstream>

#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <boost/functional.hpp>

#include <tet.hpp>

namespace tet
{
typedef std::map<long,long>                conn_t;
typedef std::vector<conn_t>               conn_list_t;

typedef std::vector<cellid_t>             mfold_t;
typedef std::vector<mfold_t>              mfold_list_t;


/*===========================================================================*/

class mscomplex_t:public boost::enable_shared_from_this<mscomplex_t>
{
public:
  class merge_dag_t;

  eGDIR           m_grad_conv;
  fn_t            m_fmax;
  fn_t            m_fmin;

  cellid_list_t   m_cp_cellid;
  cellid_list_t   m_cp_vertid;
  int_list_t      m_cp_pair_idx;
  char_list_t     m_cp_index;
  bool_list_t     m_cp_is_boundry;
  bool_list_t     m_cp_is_cancelled;
  fn_list_t       m_cp_fn;  
  int_list_t      m_cp_hversion;

  int             m_hversion;
  int_pair_list_t m_canc_list;
  int             m_geom_hversion[GDIR_CT][3+1];

  conn_list_t     m_conn[GDIR_CT];
  conn_list_t    &m_des_conn;
  conn_list_t    &m_asc_conn;

  mfold_list_t    m_mfolds[GDIR_CT];
  mfold_list_t   &m_des_mfolds;
  mfold_list_t   &m_asc_mfolds;

  boost::shared_ptr<merge_dag_t>
                  m_merge_dag;

public:

  mscomplex_t();
  mscomplex_t(const mscomplex_t &);
  ~mscomplex_t();
  void  init(int i=0);

  // save/load data
  void save(const std::string &f) const;
  void load(const std::string &f);

  void save_bin(std::ostream &os) const;
  void load_bin(std::istream &is);


  // mscomplex basic query functions
  int       get_num_critpts()              const;
  int       pair_idx(int i)                const;
  cellid_t  cellid(int i)                  const;
  cellid_t  vertid(int i)                  const;
  int8_t    index(int i)                   const;
  fn_t      fn(int i)                      const;
  int       hversion(int i)                const;
  int       surv_extrema(int i)            const;
  bool      is_paired(int i)               const;
  bool      is_not_paired(int i)           const;
  bool      is_extrema(int i)              const;
  bool      is_saddle(int i)               const;
  bool      is_canceled(int i)             const;
  bool      is_boundry(int i)              const;
  bool      is_not_canceled(int i)         const;
  fn_t      fn_min()                       const;
  fn_t      fn_max()                       const;
  bool      is_index_i_cp(int i,int cp)    const;

  // iterator range to go over the set of critical points
  typedef boost::counting_iterator<int> iterator_t;
  boost::iterator_range<iterator_t> cpno_range() const;

  // functions to create a mscomplex from a dataset  
  void  set_critpt(int i,cellid_t c,char idx,fn_t f,cellid_t v,bool b);
  void  connect_cps(int p, int q,long int m=1);

  // hierarchical Ms complex related stuff
  void cancel_pair();
  void cancel_pair(int p, int q);
  void anticancel_pair();
  void set_hversion(int hver);
  int  get_hversion() const;
  int  get_hversion_nextrema(int nmax=0,int nmin=0) const;

  // persistence based simplification related stuff
  bool persistence_cmp(int_pair_t p0,int_pair_t p1) const;
  void simplify_pers(double thresh=1.0,bool is_nrm=true,int nmax=0,int nmin=0);
  int  get_hversion_pers(double thresh=1.0,bool is_nrm=true) const;







  /// \brief collect Descending/Ascending k-manifolds
  void collect_mfolds(eGDIR d, int k, dataset_ptr_t ds);

  /// \brief collect all Descending/Ascending manifolds
  void collect_mfolds(dataset_ptr_t ds);

  /// \brief get the Descending/Ascending manifold of the given cp
  inline const mfold_t & get_mfold(eGDIR d,int c) const{return m_mfolds[d][c];}

  /// \brief get the Descending/Ascending manifold of the cp in the given
  ///        hierarchical version.
  void get_mfold(mfold_t & m,eGDIR d,int c,int hv) const;

  /// \brief Get a unique list of cps that contribute its geometry to the given
  ///        cp in the given hierarchical version.
  ///
  /// \param o  [out] unique list of contributing cps
  /// \param d  [in]  Descending/Ascending direction
  /// \param c  [in]  input critical point
  /// \param hv [in]  hierachical version.  -1 indicates current version.
  /// \param gv [in]  geometry version. -1 default. See note below.
  ///
  ///\note contributing cps that are not present in versions less
  /// than gv are not included in output. If gv=-1 then the version at which
  /// the last call to collect_geom for (dir,index(c)) was made is used.
  ///
  void get_contrib_cps(int_list_t &o,eGDIR d,int c,int hv=-1,int gv=-1) const;


  /// \brief Get a unique list of cps that contribute their geometry to the
  ///        given list of cps, in each cps' hiearchcial version.
  ///
  /// \param c  [in]  list (cpid,hversion) pairs
  ///
  void get_contrib_cps(int_list_t &o,eGDIR d,const int_pair_list_t &c,int gv=-1)const;






  // misc functions
  std::string info() const;
  std::string cp_info (int cp_no) const;
  std::string cp_conn (int cp_no) const;

//  void build_canc_dep_tree();
//  void collect_arc_geom(dataset_ptr_t ds);
  void flip_gradient_convention();
};

/*===========================================================================*/




/*===========================================================================*/

void order_pr_by_cp_index(const mscomplex_t &msc,int &p,int &q);

template<eGDIR dir> int_pair_t order_pair(mscomplex_ptr_t msc,int_pair_t pr);

/*===========================================================================*/




/*===========================================================================*/

/// \brief Data structure to extract MSC geometry in hierarchical versions
class mscomplex_t::merge_dag_t
{
public:
  /// \brief ctor
  merge_dag_t();

  /// \brief init
  void init(int ncps);

  /// \brief clear
  void clear();



  /// \brief see mscomplex_t::get_contrib_cps
  void get_contrib_cps(int_list_t &o, eGDIR d, int c, int hv, int gv) const;

  /// \brief see mscomplex_t::get_contrib_cps
  void get_contrib_cps(int_list_t &o,eGDIR d,const int_pair_list_t &c,int gv) const;

  /// \brief update merge_dag after new cancellations
  void update(mscomplex_ptr_t msc);



  /// \brief save into a binary stream
  void save_bin(std::ostream &os) const;

  /// \brief load from a binary stream
  void load_bin(std::istream &is);


private:

  friend class mscomplex_t;

  struct node_t
  {
    int base;
    int other;
    int hversion;
    int cpid;

    node_t():cpid(-1),base(-1),other(-1),hversion(0){}
    node_t(int c):cpid(c),base(-1),other(-1),hversion(0){}
    node_t(int c,int b,int o,int h):cpid(c),base(b),other(o),hversion(h){}
  };

  std::vector<node_t>  m_nodes;
  std::vector<int>     m_cp_geom[GDIR_CT];
  int                  m_last_hversion;

  node_t get_node(int i) const;
  int    get_ncps() const;
};

/*===========================================================================*/


}

#include <tet_mscomplex_inl.hpp>
#endif
