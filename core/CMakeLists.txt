#Define the project we are working on
project(mscomplex-tet-core)

# Look for various software we need
cmake_minimum_required(VERSION 2.8.3)

find_package(Boost 1.48 COMPONENTS program_options system date_time REQUIRED)

find_package(OpenMP)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}  ${Boost_INCLUDE_DIR})

set(SRCS
  utl.hpp
  vec.hpp
  tet.hpp
  tet_cc.hpp
  tet_cc_inl.hpp
  base_cc.hpp
  cube_cc.hpp
  tet_dataset.hpp
  tet_mscomplex.hpp
  tet_mscomplex_inl.hpp
  cube_mask_cc_t.h


  utl.cpp
  tet.cpp
  tet_cc.cpp
  cube_cc.cpp
  tet_dataset.cpp
  tet_mscomplex.cpp
  cube_mask_cc_t.cpp
)

add_library(mscomplex-tet-core OBJECT ${SRCS})

if(OPENMP_FOUND)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()
 
option(BUILD_TOOL "command line tool" ON)
if(BUILD_TOOL)
    add_executable(mscomplex-tet-main  $<TARGET_OBJECTS:mscomplex-tet-core> main.cpp ${${PROJECT_NAME}_OPENCL_SRCS})
    target_link_libraries(mscomplex-tet-main  ${Boost_LIBRARIES})
    install(TARGETS mscomplex-tet-main DESTINATION ${INSTALL_DIR_BIN})
endif(BUILD_TOOL)
