#include <boost/typeof/typeof.hpp>
#include <boost/program_options.hpp>

#include <tet_cc.hpp>
#include <cube_cc.hpp>
#include <tet_dataset.hpp>
#include <tet_mscomplex.hpp>
#include <cube_mask_cc_t.h>

using namespace std;

namespace bpo = boost::program_options ;

using namespace std;
using namespace tet;

int main(int ac , char **av)
{
    string file;
    std::string mask_file;
    int compno;
    double simp_tresh;
    double pre_simp_thresh;
    string mscfile;
    string ts_cache_file;
    bool arc_geom;
    int gdir_conv;
    utl::ivec3 gdim = utl::mk_vec(0,0,0);

    bpo::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce help message")
            ("mask_file,m",bpo::value(&mask_file)->default_value(".msk"),
             ".msk for mask structure grid\n")
            ("file,f",bpo::value(&file)->required(),
             "data file \n"
             ".ts  for tetrahedra mesh\n"
             ".raw/.bin for brick of binary floats (structured grid)\n"
             "     grid-dim must also be specified\n")
            ("grid-dim,d",bpo::value(&gdim)->default_value(gdim),
             "structured grid size")
            ("vert-comp,c",bpo::value(&compno)->default_value(3),
             "vert comp in ts file to read")
            ("simp-tresh,t",bpo::value(&simp_tresh)->default_value(0.0),
             "simplification treshold: A threshold upto which the combinatorial ms complex is"\
             "simplified using persistence.")
            ("pre-simp-tresh,p",bpo::value(&pre_simp_thresh)->default_value(0.0),
             "pre simplification treshold: a threshold upto which the ms complex is simplified "\
             "using persistence. The geometry returned is at this threshold.")
            ("output-name,o",bpo::value(&mscfile)->default_value("mscomplex.bin"),
             "Mscomplex output file name ")
            ("arc-geom,a",bpo::value(&arc_geom)->default_value(false),
             "enable collection of the geometry of arcs")
            ("ts-cache-file",bpo::value(&ts_cache_file)->default_value(""),
             "Load ts file from binary cache file for faster loads."\
             "If it does not exist one is created if given")
            ("grad-dir-conv,g",bpo::value(&gdir_conv)->default_value(tet::DES),
             "Gradient direction convention, 0-->Des (default), 1-->Asc.")
            ;


    bpo::variables_map vm;
    bpo::store(bpo::parse_command_line(ac, av, desc), vm);

    if (vm.count("help"))
    {
        cout << desc << endl;
        return 0;
    }
    try
    {
        bpo::notify(vm);
    }
    catch(bpo::required_option e)
    {
        cout<<e.what()<<endl;
        cout<<desc<<endl;
        return 1;
    }

    cout<<"==================start======================================"<<endl;

    fn_list_t     fns;
    mask_list_t   mask;
    base_cc_ptr_t cc;

    bool is_raw     = (file.find(".raw") == int(file.size()) - 4);
    bool is_bin     = (file.find(".bin") == int(file.size()) - 4);
    bool is_msk     = (mask_file.find(".msk") != 0);
    bool is_sgrid   = ((gdim != mk_cell(0,0,0) ) && (is_bin || is_raw));
    bool is_tetgrid =  (file.find(".ts") == int(file.size()) - 3);


    if( is_tetgrid )
        cc.reset(new tet_cc_t);
    else if (is_sgrid && is_msk)
        cc.reset(new cube_mask_cc_t);
    else if (is_sgrid && !is_msk)
        cc.reset(new cube_cc_t);

    tet_cc_ptr_t  tcc = boost::dynamic_pointer_cast<tet_cc_t>(cc);
    cube_mask_cc_ptr_t mcc = boost::dynamic_pointer_cast<cube_mask_cc_t>(cc);
    cube_cc_ptr_t ccc = boost::dynamic_pointer_cast<cube_cc_t>(cc);

    if(tcc)
    {
        bool tcc_loaded = false;

        if(!ts_cache_file.empty())
        {
            ifstream ifs(ts_cache_file.c_str(),ios::in|ios::binary);

            if(ifs)
            {
                cout<<"Reading triangulation from cached data"<<endl;
                utl::bin_read_vec(ifs,fns);
                tcc->load(ifs);
                tcc_loaded = true;
            }
        }

        if(! tcc_loaded)
        {           
            tet_cc_t::tet_list_t tets;
            cout<<"Reading Triangulation"<<endl;
            read_ts(file.c_str(),&tets,compno,&fns,NULL,NULL,NULL);
            cout<<"Setting up Triangulation"<<endl;
            tcc->init(tets);

            if(!ts_cache_file.empty())
            {
                ofstream ofs(ts_cache_file.c_str(),ios::out|ios::binary);
                if(ofs)
                {
                    cout<<"Caching Triangulation for future loads"<<endl;
                    utl::bin_write_vec(ofs,fns);
                    tcc->save(ofs);
                }
            }
        }
    }

    else if(mcc)
    {
        cout<<"reading msk file data"<<endl;

        ifstream mas;
        mas.open(mask_file.c_str(),ios::in|ios::binary);
        ENSURE(mas.is_open(),"Unable to open file");
        mask.resize(gdim[0]*gdim[1]*gdim[2]);
        mas.read((char*)(void*)mask.data(),sizeof(char)*gdim[0]*gdim[1]*gdim[2]);
        mcc->init(mask,gdim[0],gdim[1],gdim[2]);
        mcc->read_fns_with_mask(fns,file.c_str());
        mas.close();
    }

    else if(ccc)
    {
        cout<<"Reading Raw file data"<<endl;

        ifstream ifs;
        ifs.open(file.c_str(),ios::in|ios::binary);
        ENSURE(ifs.is_open(),"Unable to open file");
        fns.resize(gdim[0]*gdim[1]*gdim[2]);
        ifs.read((char*)(void*)fns.data(),sizeof(fn_t)*gdim[0]*gdim[1]*gdim[2]);
        ccc->init(gdim[0],gdim[1],gdim[2]);
        ifs.close();
    }

    dataset_ptr_t ds(new dataset_t(fns,cc));

    ENSURE(pre_simp_thresh <= simp_tresh,"invalid simplification thresholds");

    mscomplex_ptr_t msc(new mscomplex_t());


    if(gdir_conv == ASC)
        for(int i = 0 ; i < fns.size(); ++i)
            fns[i] *= -1.0;

    cout<<"Starting work"<<endl;

    ds->work(msc);

    cout<<"\nMS complex info:"<<endl;
    cout<<msc->info()<<endl;

    if(pre_simp_thresh >= 0)
        msc->simplify_pers(pre_simp_thresh);

    cout << "Collecting geometry " << endl;
    msc->collect_mfolds(ds);

    //  if( arc_geom)
    //    msc->collect_arc_geom(ds);

    if(simp_tresh >= 0)
        msc->simplify_pers(simp_tresh);

    if(gdir_conv == ASC)
        msc->flip_gradient_convention();

    cout << "Saving data " << endl;

    msc->save(mscfile);

    cout<<"==================finish====================================="<<endl;

    return 0;
}
