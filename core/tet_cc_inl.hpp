#ifndef TET_CC_INL_INCLUDED
#define TET_CC_INT_INCLUDED


#include <tet_cc.hpp>

/*****************************************************************************/
namespace tet
{

inline tet_cc_t::cell_t<2>::type mk_cell(int a,int b)
{tet_cc_t::cell_t<2>::type t; t[0]=a;t[1]=b;return t;}

inline tet_cc_t::cell_t<3>::type mk_cell(int a,int b,int c)
{tet_cc_t::cell_t<3>::type t; t[0]=a;t[1]=b;t[2]=c;return t;}

inline tet_cc_t::cell_t<4>::type mk_cell(int a,int b,int c,int d)
{tet_cc_t::cell_t<4>::type t; t[0]=a;t[1]=b;t[2]=c;t[3]=d;return t;}

inline tet_cc_t::cell_t<5>::type mk_cell(int a,int b,int c,int d,int e)
{tet_cc_t::cell_t<5>::type t; t[0]=a;t[1]=b;t[2]=c;t[3]=d;t[4]=e;return t;}

inline tet_cc_t::cell_t<6>::type mk_cell(int a,int b,int c,int d,int e,int f)
{tet_cc_t::cell_t<6>::type t; t[0]=a;t[1]=b;t[2]=c;t[3]=d;t[4]=e;t[5]=f;return t;}

template <size_t N> inline typename tet_cc_t::cell_t<N>::type mk_cell_assign(int a)
{typename tet_cc_t::cell_t<N>::type r;for(int i=0;i<N;++i) r[i]=a; return r;}


/*---------------------------------------------------------------------------*/

inline int tet_cc_t::is_bnd(cellid_t c) const
{
//  ASSERT(is_in_range(c,m_cel_off[0],m_cel_off[4]));

  if(get_dim(c) < 3)
    return beta2(m_cel_drt[c]) == -1;
  else
    return false;
}

/*---------------------------------------------------------------------------*/

inline int tet_cc_t::get_fcts(cellid_t c,cellid_t* f ) const
{
  switch(get_dim(c))
  {
  case 0:
    return 0;
  case 1:
  {ege_t e = get_ege(c); std::copy(e.data(),e.data()+2,f); return 2;}
  case 2:
  {tri_t t = get_tri_ege(c); std::copy(t.data(),t.data()+3,f); return 3;}
  case 3:
  {tet_t t = get_tet_tri(c); std::copy(t.data(),t.data()+4,f); return 4;}
  }
  ASSERT(false);
  return 0;
}

/*---------------------------------------------------------------------------*/

template <> inline tet_cc_t::ege_t tet_cc_t::get_fcts_tpl<2>(cellid_t c) const
{return get_ege(c);}

template <> inline tet_cc_t::tri_t tet_cc_t::get_fcts_tpl<3>(cellid_t c) const
{return get_tri_ege(c);}

template <> inline tet_cc_t::tet_t tet_cc_t::get_fcts_tpl<4>(cellid_t c) const
{return get_tet_tri(c);}

/*---------------------------------------------------------------------------*/

inline int tet_cc_t::get_cofs(cellid_t c,cellid_t* f) const
{
  switch(get_dim(c))
  {
  case 0: return get_vrt_ege(c,f);
  case 1: return get_ege_tri(c,f);
  case 2: return get_tri_tet(c,f);
  case 3: return 0;
  }
  ASSERT(false);
  return 0;
}

/*---------------------------------------------------------------------------*/

inline int tet_cc_t::get_vrts(cellid_t c,cellid_t* f ) const
{
  switch(get_dim(c))
  {
  case 0:
  {f[0] = c; return 1;}
  case 1:
  {ege_t e = get_ege(c); std::copy(e.data(),e.data()+2,f); return 2;}
  case 2:
  {tri_t t = get_tri(c); std::copy(t.data(),t.data()+3,f); return 3;}
  case 3:
  {tet_t t = get_tet(c); std::copy(t.data(),t.data()+4,f); return 4;}
  }
  ASSERT(false);
  return 0;
}

/*---------------------------------------------------------------------------*/

inline int tet_cc_t::get_star(cellid_t c,cellid_t* f ) const
{
  cellid_t cf[DMAX];

  std::set<cellid_t> oset;

  std::set<cellid_t>::iterator beg = oset.insert(c).first;
  std::set<cellid_t>::iterator end = beg;

  for(int i = get_dim(c); i < 3; ++i)
  {
    do
    {
      int ct = get_cofs(*beg,cf);

      for( int j = 0 ; j < ct; ++j)
        oset.insert(cf[j]);

    }
    while (beg++ != end);

    beg = ++end;
    end = --oset.end();
  }

  ASSERT(oset.size()<DMAX);

  br::copy(oset,f);

  return oset.size();
}

/*---------------------------------------------------------------------------*/

inline int tet_cc_t::get_link(cellid_t c,cellid_t* f ) const
{
  cellid_t cfcts[DMAX];
  cellid_t fcts[DMAX];

  std::set<cellid_t> star,link;

  std::set<cellid_t>::iterator beg = star.insert(c).first;
  std::set<cellid_t>::iterator end = beg;

  ASSERT(get_dim(c) == 0);

  for(int i = 0; i < 3; ++i)
  {
    do
    {
      int ct = get_cofs(*beg,cfcts);

      for( int j = 0 ; j < ct; ++j)
      {
        star.insert(cfcts[j]);

        int fcts_ct = get_fcts(cfcts[j],fcts);

        std::copy(fcts,fcts+fcts_ct,std::inserter(link,link.begin()));
      }
    }
    while (beg++ != end);

    beg = ++end;
    end = --star.end();
  }

  cellid_t *fe = br::set_difference(link,star,f);

  ASSERT(f-fe<DMAX);

  return f-fe;
}

/*---------------------------------------------------------------------------*/

//template<> inline utl::ivec<1>::type
//tet_cc_t::get_vrts_tpl<1>(cellid_t c) const {return utl::mk_vec<int>(c);}

template<> inline tet_cc_t::cell_t<2>::type tet_cc_t::get_vrts_tpl<2>(cellid_t c) const
{return get_ege(c);}

template<> inline tet_cc_t::cell_t<3>::type tet_cc_t::get_vrts_tpl<3>(cellid_t c) const
{return get_tri(c);}

template<> inline tet_cc_t::cell_t<4>::type tet_cc_t::get_vrts_tpl<4>(cellid_t c) const
{return get_tet(c);}

}
/*****************************************************************************/



/*****************************************************************************/
namespace tet
{
inline vert_t tet_geom_cc_t::get_vert(cellid_t c) const
{
  ASSERT(is_in_range(c,0,m_verts.size()));
  return m_verts[c];
}
/*---------------------------------------------------------------------------*/
double inline tet_geom_cc_t::get_diagonal_length() const
{
  vert_t lc = utl::mk_vec<double>(m_bounds[0],m_bounds[2],m_bounds[4]);
  vert_t uc = utl::mk_vec<double>(m_bounds[1],m_bounds[3],m_bounds[5]);
  return (uc-lc).norm2();
}
/*---------------------------------------------------------------------------*/

/// \brief template specialzation for tets
template <>vert_t inline tet_geom_cc_t::get_centroid<4>(cellid_t c) const
{
  tet_t e = get_tet(c);
  return (m_verts[e[0]] + m_verts[e[1]]+ m_verts[e[2]] + m_verts[e[3]])/4 ;
}

/// \brief template specialzation for tris
template <>vert_t inline tet_geom_cc_t::get_centroid<3>(cellid_t c) const
{
  tri_t e = get_tri(c);
  return (m_verts[e[0]] + m_verts[e[1]]+ m_verts[e[2]])/3 ;
}

/// \brief template specialzation for edges
template <>vert_t inline tet_geom_cc_t::get_centroid<2>(cellid_t c) const
{
  ege_t e = get_ege(c);
  return (m_verts[e[0]] + m_verts[e[1]])/2 ;
}

/// \brief template specialzation for vertices
template <>vert_t inline tet_geom_cc_t::get_centroid<1>(cellid_t c) const
{ASSERT(is_in_range(c,0,m_verts.size())); return m_verts[c];}

/// \brief implementation for when N is not known
template <>vert_t inline tet_geom_cc_t::get_centroid<0>(cellid_t c) const
{
  switch(get_dim(c))
  {
  case 0:return get_centroid<1>(c);
  case 1:return get_centroid<2>(c);
  case 2:return get_centroid<3>(c);
  case 3:return get_centroid<4>(c);
  }

  ASSERT(false);
  return vert_t();
}

}
/*****************************************************************************/



/*****************************************************************************/
namespace tet
{
template<std::size_t N> typename
ptet_cc_t::pcell_t<N>::type ptet_cc_t::get_pcell(cellid_t c) const
{
  ASSERT(get_dim(c)+1 == N);

  typename cell_t<N>::type           pvrts = get_vrts_tpl<N>(c);
  typename cell_pdomain_t<N>::type pdomain = get_pdomain<N>(c);

  typename pcell_t<N>::type pcell;

  for(int i = 0 ; i < N; ++i)
    pcell[i] = pcellid_t(pvrts[i],pdomain[i]);

  return pcell;
}


inline bool ptet_cc_t::pcellid_t::operator<(const pcellid_t &o) const
{return (cid != o.cid)?(cid < o.cid):did<o.did;}


inline utl::ivec3 ptet_cc_t::pcellid_t::pd_org() const
{
  utl::ivec3 ret;

  ret[0] = ((did>>0)&1)*(((did>>4)&1)?(-1):(1));
  ret[1] = ((did>>1)&1)*(((did>>5)&1)?(-1):(1));
  ret[2] = ((did>>2)&1)*(((did>>6)&1)?(-1):(1));

  return ret;
}

inline void ptet_cc_t::pcellid_t::set_pd_org(utl::ivec3 o)
{
  int ox = o[0];
  int oy = o[1];
  int oz = o[2];

  ASSERT(is_in_range(ox,-1,2));
  ASSERT(is_in_range(oy,-1,2));
  ASSERT(is_in_range(oz,-1,2));

  ox = ((ox != 0)?(1):(0))| ((ox <0)?(0x10):(0));
  oy = ((oy != 0)?(1):(0))| ((oy <0)?(0x10):(0));
  oz = ((oz != 0)?(1):(0))| ((oz <0)?(0x10):(0));

  did = pdomainid_t(ox | (oy << 1) | (oz << 2));

}



}

/*****************************************************************************/



/*****************************************************************************/
namespace tet
{
inline vert_t ptet_geom_cc_t::get_vert(pcellid_t pc) const
{
  ASSERT(is_in_range(pc.cid,0,m_verts.size()));
  return m_verts[pc.cid] + m_dom_sz*pc.pd_org();
}

/*---------------------------------------------------------------------------*/

inline vert_t ptet_geom_cc_t::get_vert(cellid_t c) const
{
  ASSERT(is_in_range(c,0,m_verts.size()));
  return m_verts[c];
}

/*---------------------------------------------------------------------------*/

template <std::size_t N> inline
vert_t ptet_geom_cc_t::get_centroid(typename pcell_t<N>::type pc) const
{
  vert_t v = utl::mk_vec<double,3>(0);

  for(int i = 0 ; i < N; ++i)
    v += get_vert(pc[i]);

  return (v/N);
}

/*---------------------------------------------------------------------------*/

template <std::size_t N> inline
vert_t ptet_geom_cc_t::get_centroid(cellid_t c) const
{return get_centroid<N>(get_pcell<N>(c));}

/// \brief template specialzation for vertices
template <>vert_t inline ptet_geom_cc_t::get_centroid<1>(cellid_t c) const
{ASSERT(is_in_range(c,0,m_verts.size())); return m_verts[c];}

/// \brief implementation for when N is not known
template <>vert_t inline ptet_geom_cc_t::get_centroid<0>(cellid_t c) const
{
  switch(get_dim(c))
  {
  case 0:return get_centroid<1>(c);
  case 1:return get_centroid<2>(c);
  case 2:return get_centroid<3>(c);
  case 3:return get_centroid<4>(c);
  }

  ASSERT(false);
  return vert_t();
}
}
/*****************************************************************************/
#endif
