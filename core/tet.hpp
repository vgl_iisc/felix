#ifndef TET_HPP_INCLUDED
#define TET_HPP_INCLUDED

#include <boost/shared_ptr.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>

#include <fstream>

#include <utl.hpp>
#include <vec.hpp>

namespace br    = boost::range;
namespace badpt = boost::adaptors;

namespace tet
{
typedef int                       cellid_t;
typedef std::vector<cellid_t>     cellid_list_t;

typedef float                        fn_t;
typedef std::vector<fn_t>            fn_list_t;
typedef boost::shared_ptr<fn_list_t> fn_list_ptr_t;

typedef char                           mask_t;
typedef std::vector<mask_t>            mask_list_t;
typedef boost::shared_ptr<mask_list_t> mask_list_ptr_t;

typedef std::vector<int>        int_list_t;
typedef std::vector<char>       char_list_t;
typedef std::vector<char>       bool_list_t;

typedef std::pair<int,int>         int_pair_t;
typedef std::vector<int_pair_t>    int_pair_list_t;

const cellid_t invalid_cellid = -1;
const int    g_max_dim = 3;

const int DMAX = 2048;

enum eGDIR  {DES,ASC,GDIR_CT};

extern const char * const g_cp_type_names[5];
extern const char * const g_gdir_names[2];


class base_cc_t;
class base_cc_geom_t;
class tet_cc_t;
class cube_cc_t;
class cube_mask_cc_t;
class dataset_t;
class mscomplex_t;

typedef boost::shared_ptr<base_cc_t>            base_cc_ptr_t;
typedef boost::shared_ptr<cube_cc_t>            cube_cc_ptr_t;
typedef boost::shared_ptr<cube_mask_cc_t>       cube_mask_cc_ptr_t;
typedef boost::shared_ptr<tet_cc_t>             tet_cc_ptr_t;
typedef boost::shared_ptr<base_cc_geom_t>       base_cc_geom_ptr_t;
typedef boost::shared_ptr<dataset_t>            dataset_ptr_t;
typedef boost::shared_ptr<mscomplex_t>          mscomplex_ptr_t;

typedef boost::shared_ptr<const tet_cc_t>       tet_cc_cptr_t;
typedef boost::shared_ptr<const dataset_t>      dataset_cptr_t;
typedef boost::shared_ptr<const mscomplex_t>    mscomplex_cptr_t;

typedef utl::vec_t<float,3> vert_t;
typedef std::vector<vert_t> vert_list_t;

// stuff relating to periodic triangulations

class tet_geom_cc_t;
class ptet_cc_t;
class ptet_geom_cc_t;

typedef boost::shared_ptr<tet_geom_cc_t>         tet_geom_cc_ptr_t;
typedef boost::shared_ptr<const tet_geom_cc_t>   tet_geom_cc_cptr_t;
typedef boost::shared_ptr<ptet_cc_t>             ptet_cc_ptr_t;
typedef boost::shared_ptr<const ptet_cc_t>       ptet_cc_cptr_t;
typedef boost::shared_ptr<ptet_geom_cc_t>        ptet_geom_cc_ptr_t;
typedef boost::shared_ptr<const ptet_geom_cc_t>  ptet_geom_cc_cptr_t;
}

#endif
