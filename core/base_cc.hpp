#ifndef BASE_CC_HPP_DEFINED
#define BASE_CC_HPP_DEFINED

#include <set>
#include <iterator>
#include <vector>

#include <boost/range/iterator_range.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/array.hpp>

#include <utl.hpp>
#include <vec.hpp>

namespace tet
{

/// \brief A base class for all cell complex classes
///
/// \note All cell complexes provide integer id's for each cell in the complex
///
/// \note The ids are in increasing dimension
///       i.e. id1 < id2 implies dim(id1) <= dim(id2)
///
/// \note The maximum dimension is 3

class base_cc_t
{
public:
  /// \brief cells are identified using integers
  typedef int cellid_t;

  /// \brief list of cellids
  typedef std::vector<cellid_t> cellid_list_t;

  /// \brief iterator type to iterate over cellids
  typedef boost::counting_iterator<cellid_t> iterator;


public:
  /// \brief Get the facets of the given cell
  virtual int get_fcts(cellid_t c,cellid_t* f ) const = 0;

  /// \brief Get the cofacets of the given cell
  virtual int get_cofs(cellid_t c,cellid_t* cf) const = 0;

  /// \brief Get the vertex set of the given cell
  virtual int get_vrts(cellid_t c,cellid_t* v ) const = 0;

  /// \brief Is the given cell on the boundary
  virtual int is_bnd(cellid_t c) const = 0;

  /// \brief Get the cell opposite to the given cell in its cofacet
  virtual cellid_t get_opp_cell(cellid_t c,cellid_t cf) const = 0;




public:
  /// \brief Dimension of the given cell
  int get_dim(cellid_t c) const;

  /// \brief Total number of cells
  int num_cells() const;

  /// \brief Number of cells having dimension d
  int num_dcells(int d) const;

  /// \brief Number of cells having dimension d and lesser
  int num_upto_dcells(int d) const;

  /// \brief Iterator range over all cellids
  boost::iterator_range<iterator> cel_rng() const;

  /// \brief Iterator range over all cellids having dim d
  boost::iterator_range<iterator> cel_rng(int d) const;




public:
  /// \brief string representation of the vertex set of a cell c
  virtual std::string to_string(cellid_t c) const;

  /// \brief test the integrity of the cell complex
  virtual bool test() const;

  /// \brief get basic info
  virtual std::string info() const;

  /// \brief save to stream
  virtual void save(std::ostream &os) const;

  /// \brief load from stream
  virtual void load(std::istream &is);


protected:
  /// \brief init
  void init(int n0, int n1, int n2, int n3);

  /// \brief ctor
  base_cc_t(){init(0,0,0,0);}



private:
  /// \brief id offsets for each dim-cell
  boost::array<cellid_t,5> m_cell_idoffsets;
};
}



namespace tet {
/// \brief An abstract base class for cell complex classes with geometry information.
class base_cc_geom_t:virtual public base_cc_t
{
public:
  /// \brief vertex type
  typedef utl::vec_t<float,3> vert_t;

public:

  /// \brief Get the vertex coordinates of a vertex cell
  virtual vert_t get_vertex(cellid_t c) const = 0;

  /// \brief Get centroid of given cell
  virtual vert_t get_centroid(cellid_t c) const;


public:

  /// \brief Get bounding box .. [xmin,xmax,ymin,ymax,zmin,zmax]
  void   get_bounds(double *b) const;

  /// \brief Get Diagonal length
  double get_diagonal_length() const;

  /// \brief Get bounding box lower corner
  vert_t get_lc() const;

  /// \brief Get bounding box upper corner
  vert_t get_uc() const;


protected:

  /// \brief Init bounding box
  void init_bb(vert_t lc,vert_t uc);

private:

  /// \brief bounding box lower/upper corners
  vert_t m_lc,m_uc;
};

}


/*===========================================================================*/

namespace tet
{

/*---------------------------------------------------------------------------*/

inline int base_cc_t::get_dim(cellid_t c) const
{
  if(m_cell_idoffsets[0] <= c && c < m_cell_idoffsets[1]) return 0;
  if(m_cell_idoffsets[1] <= c && c < m_cell_idoffsets[2]) return 1;
  if(m_cell_idoffsets[2] <= c && c < m_cell_idoffsets[3]) return 2;
  if(m_cell_idoffsets[3] <= c && c < m_cell_idoffsets[4]) return 3;

  throw std::runtime_error("Invalid cellid");
  return -1;
}

/*---------------------------------------------------------------------------*/

inline int base_cc_t::num_cells() const
{return m_cell_idoffsets[4];}

/*---------------------------------------------------------------------------*/

inline int base_cc_t::num_dcells(int d) const
{return m_cell_idoffsets[d+1]-m_cell_idoffsets[d];}

/*---------------------------------------------------------------------------*/

inline int base_cc_t::num_upto_dcells(int d) const
{return m_cell_idoffsets[d+1];}

/*---------------------------------------------------------------------------*/

inline boost::iterator_range<base_cc_t::iterator> base_cc_t::cel_rng() const
{return boost::make_iterator_range(iterator(0),iterator(m_cell_idoffsets[4]));}

/*---------------------------------------------------------------------------*/

inline boost::iterator_range<base_cc_t::iterator> base_cc_t::cel_rng(int d) const
{return boost::make_iterator_range(iterator(m_cell_idoffsets[d]),
                                   iterator(m_cell_idoffsets[d+1]));}

/*---------------------------------------------------------------------------*/

inline std::string base_cc_t::to_string(cellid_t c) const
{
  std::stringstream ss;

  cellid_t vrts[30];

  ss<<"[";

  for(cellid_t *b = vrts , *e = vrts + get_vrts(c,vrts); b!=e ;++b)
    ss << *b <<",";

  ss<< "]";

  return ss.str();
}
/*---------------------------------------------------------------------------*/

inline std::string base_cc_t::info() const
{
  std::stringstream ss;

  ss << "--------------------------" << std::endl;
  ss << "Base cell complex Info    " << std::endl;
  ss << "--------------------------" << std::endl;
  ss << "Num 0-cells      = " << num_dcells(0) << std::endl;
  ss << "Num 1-cells      = " << num_dcells(1) << std::endl;
  ss << "Num 2-cells      = " << num_dcells(2) << std::endl;
  ss << "Num 3-cells      = " << num_dcells(3) << std::endl;
  ss << "0-cell id range  = ["
     << m_cell_idoffsets[0] << ","
     << m_cell_idoffsets[1] << ")"<<std::endl;
  ss << "1-cell id range  = ["
     << m_cell_idoffsets[1] << ","
     << m_cell_idoffsets[2] << ")"<<std::endl;
  ss << "2-cell id range  = ["
     << m_cell_idoffsets[2] << ","
     << m_cell_idoffsets[3] << ")"<<std::endl;
  ss << "3-cell id range  = ["
     << m_cell_idoffsets[3] << ","
     << m_cell_idoffsets[4] << ")"<<std::endl;
  ss << "--------------------------" << std::endl;

  return ss.str();
}

/*---------------------------------------------------------------------------*/

inline bool base_cc_t::test() const
{
  return true;
}

/*---------------------------------------------------------------------------*/

inline void base_cc_t::init(int n0, int n1, int n2, int n3)
{
  m_cell_idoffsets[0] = 0;
  m_cell_idoffsets[1] = n0;
  m_cell_idoffsets[2] = n0 + n1;
  m_cell_idoffsets[3] = n0 + n1 + n2;
  m_cell_idoffsets[4] = n0 + n1 + n2 + n3;
}

/*---------------------------------------------------------------------------*/

inline void base_cc_t::save(std::ostream &os) const
{
  utl::bin_write(os,m_cell_idoffsets[0]);
  utl::bin_write(os,m_cell_idoffsets[1]);
  utl::bin_write(os,m_cell_idoffsets[2]);
  utl::bin_write(os,m_cell_idoffsets[3]);
  utl::bin_write(os,m_cell_idoffsets[4]);
}

/*---------------------------------------------------------------------------*/

inline void base_cc_t::load(std::istream &is)
{
  utl::bin_read(is,m_cell_idoffsets[0]);
  utl::bin_read(is,m_cell_idoffsets[1]);
  utl::bin_read(is,m_cell_idoffsets[2]);
  utl::bin_read(is,m_cell_idoffsets[3]);
  utl::bin_read(is,m_cell_idoffsets[4]);
}

/*---------------------------------------------------------------------------*/

}

/*===========================================================================*/




/*===========================================================================*/
namespace tet{
/*---------------------------------------------------------------------------*/

inline void base_cc_geom_t::get_bounds(double *b) const
{
  b[0] = m_lc[0]; b[2] = m_lc[1]; b[4] = m_lc[2];
  b[1] = m_uc[0]; b[3] = m_uc[1]; b[5] = m_uc[2];
}
/*---------------------------------------------------------------------------*/

inline double base_cc_geom_t::get_diagonal_length() const
{return (get_lc()-get_uc()).norm2();}

/*---------------------------------------------------------------------------*/

inline base_cc_geom_t::vert_t base_cc_geom_t::get_lc() const{return m_lc;}

/*---------------------------------------------------------------------------*/

inline base_cc_geom_t::vert_t base_cc_geom_t::get_uc() const{return m_uc;}

/*---------------------------------------------------------------------------*/

inline void base_cc_geom_t::init_bb
(base_cc_geom_t::vert_t lc,base_cc_geom_t::vert_t uc) {m_lc = lc;m_uc = uc;}
/*---------------------------------------------------------------------------*/
}
/*===========================================================================*/


#endif
