#ifndef TET_CC_HPP_INCLUDED
#define TET_CC_HPP_INCLUDED

#include <set>

#include <boost/iterator/counting_iterator.hpp>

#include <base_cc.hpp>

#include <tet.hpp>

/*****************************************************************************/
namespace tet
{

/// \brief A class to represent triangulations of 3-manifolds.
///
/// The storage size = sizeof(int) * [ntet * (4+4+4+6) + (nv+ne+ntri) + 5]\n
/// The setup complexity is O(ntet + ntri*log(ntri) + ne*log(ne))\n
/// Most queries are O(DMAX) where DMAX is the maximum degree of a vertex.
/// Some queries are bounded by O(DMAX *log(DMAX) ).\n
/// DMAX is a compile time constant.\n
/// All output arrays must be atleast of length DMAX.\n

class tet_cc_t:public virtual base_cc_t
{
public:
  /// \brief cell type
  template <std::size_t N> struct cell_t
  {typedef typename utl::uvec_t<cellid_t,N> type;};

  /// \brief tet cell
  typedef cell_t<4>::type  tet_t;

  /// \brief tri cell
  typedef cell_t<3>::type  tri_t;

  /// \brief edge cell
  typedef cell_t<2>::type  ege_t;

  /// \brief tet list
  typedef std::vector<tet_t>  tet_list_t;




public:
  /// \brief ctor
  tet_cc_t(){}

  /// \brief init from a list of tets
  void init(const tet_list_t &tets);


public:
  int      get_fcts(cellid_t c,cellid_t* f )    const;
  int      get_cofs(cellid_t c,cellid_t* cf)    const;
  int      get_vrts(cellid_t c,cellid_t* v )    const;
  int      is_bnd(cellid_t c)                   const;
  cellid_t get_opp_cell(cellid_t c,cellid_t cf) const;
  void     save(std::ostream &os)               const;
  void     load(std::istream &is);
  bool     test()                               const;


public:
  /// \brief Template version of get_fcts for simplex with N faces
  template <std::size_t N> inline typename
  cell_t<N>::type get_fcts_tpl(cellid_t c) const;

  /// \brief Template version of get_vrts for simplex with N verts
  template <std::size_t N> inline typename
  cell_t<N>::type get_vrts_tpl(cellid_t c) const;

  /// \brief Get the star of the given simplex with id c
  int get_star(cellid_t c,cellid_t* s ) const;

  /// \brief Specialized version of get_star for edges. \n
  /// \param[in]  e id of the edge
  /// \param[out] l ids of the triangle's and tet's incident on the edge \n
  ///               arranged as \code tri1, tet1, tri2, tet2, ... trin,tetn, trin+1 \endcode \n
  ///               trin+1 == tri1 if interior edge else they are distinct. \n
  /// \returns 2*n + 1 (n is no of tets that incident on this edge)
  int get_edge_star(cellid_t e,cellid_t* l ) const;

  /// \brief Get the link of the given simplex with id c
  int get_link(cellid_t c,cellid_t* s ) const;



public:
  /// \brief Get the ids of edges incident on vertex v
  int get_vrt_ege(cellid_t v,cellid_t* e ) const;

  /// \brief Get the ids of triangles incident on edge e
  int get_ege_tri(cellid_t e,cellid_t* t ) const;

  /// \brief Get the ids of tetrahedra incident on triangle t
  int get_tri_tet(cellid_t t,cellid_t* tets ) const;

  /// \brief Get the vertex id's forming a given edge e
  ege_t get_ege(cellid_t e) const;

  /// \brief Get the vertex id's forming a given triangle t
  tri_t get_tri(cellid_t t) const;

  /// \brief Get the vertex id's forming a given tetrahedron t
  tet_t get_tet(cellid_t t) const;

  /// \brief Get the edge id's on the boundary of a given triangle t
  tri_t get_tri_ege(cellid_t t) const;

  /// \brief Get the triangle id's on the boundary of a given tetrahedron t
  tet_t get_tet_tri(cellid_t t) const;


protected:
  /// \privatesection
  tet_list_t                    m_opp_drt;
  tet_list_t                    m_tet_vrt;
  std::vector<cell_t<6>::type>  m_tet_ege;
  tet_list_t                    m_tet_tri;

  cellid_list_t                 m_cel_drt;

  int beta0(int drt) const;
  int beta1(int drt) const;
  int beta2(int drt) const;
};
}

/*****************************************************************************/



/*****************************************************************************/
namespace tet
{
/// \brief A class to represent a triangulation along with geometry.
class tet_geom_cc_t:public virtual tet_cc_t
{
public:
  /// \brief Constructor
  tet_geom_cc_t(const tet_list_t &tets,const vert_list_t &verts);

  /// \brief Construct from binary stream. Assumes internal format written by save.
  tet_geom_cc_t(std::istream &is);

  /// \brief Save to a binary stream. Uses the internal binary representation.
  void save(std::ostream &os) const;

  /// \brief Get bounding box .. [xmin,xmax,ymin,ymax,zmin,zmax]
  inline void get_bounds(double *b) const{std::copy(m_bounds,m_bounds+6,b);}

  /// \brief Get Diagonal length
  inline double get_diagonal_length() const;

  /// \brief Get position of the given vertex
  inline vert_t get_vert(cellid_t pc) const;

//  /// \brief Get the centriod of a periodic cell with vertex set size N
//  template<std::size_t N>
//  inline vert_t get_centroid(typename pcell_t<N>::type pc) const;

//  /// \brief Get a centriod of a cell with vertex set size N
  template<std::size_t N>
  inline vert_t get_centroid(cellid_t c) const;

//  /// \brief Get the N dimensional volume of the given cell.
//  template<eGDIR DIR,std::size_t N>
//  double        get_cell_size(cellid_t c) const;

protected:
  /// \privatesection
  vert_list_t m_verts;
  double      m_bounds[6];

  double      m_min_edge_length;
  double      m_max_edge_length;
};
}
/*****************************************************************************/




/*****************************************************************************/
namespace tet
{
/// \brief A class to represent a periodic triangulation. Builds on tet::tet_cc_t
class ptet_cc_t:public virtual tet_cc_t
{
public:

  typedef u_int8_t pdomainid_t;

  template <std::size_t N> struct cell_pdomain_t
  {typedef typename utl::vec_t<pdomainid_t,N> type;};

  typedef cell_pdomain_t<4>::type    tet_pdomain_t;
  typedef std::vector<tet_pdomain_t> tet_pdomain_list_t;

  struct pcellid_t
  {
    cellid_t     cid;
    pdomainid_t  did;

    pcellid_t():cid(invalid_cellid),did(0){}
    pcellid_t(cellid_t cid,pdomainid_t did):cid(cid),did(did){}

    inline bool operator<(const pcellid_t &o) const;

    inline utl::ivec3 pd_org() const;

    inline void set_pd_org(utl::ivec3 o);

    inline bool is_org_dom() const {return did == 0;}

  };

  template<std::size_t N> struct pcell_t
  {typedef utl::uvec_t<pcellid_t,N> type;};

  typedef pcell_t<2>::type pege_t;
  typedef pcell_t<3>::type ptri_t;
  typedef pcell_t<4>::type ptet_t;

  /// \brief Construct from a list of tets and periodic ids
  ptet_cc_t(const tet_list_t &tets,const tet_pdomain_list_t &ptets);

  /// \brief Construct from binary stream. Assumes internal format written by save.
  ptet_cc_t(std::istream &is);

  /// \brief Save to a binary stream. Uses the internal binary representation.
  void save(std::ostream &os) const;

  /// \brief Get consistent domainids for given periodic cell's vertex set
  ///
  /// \param[in] c cellid of cell
  /// \tparam    N number of vertices in vertex set (dim(c) + 1)
  /// \returns   an N-vec of domain ids
  template<std::size_t N> typename
  cell_pdomain_t<N>::type get_pdomain(cellid_t c) const;

  /// \brief Get vertex ids and consistent domainids for given periodic cell's vertex set
  ///
  /// \param[in] c cellid of cell
  /// \tparam    N number of vertices in vertex set (dim(c) + 1)
  /// \returns   an N-vec of pairs of vertex ids and domain ids
  template<std::size_t N> typename
  pcell_t<N>::type get_pcell(cellid_t c) const;


  /// \brief Given a coface c of f align c such that c and f are "truly" adjacent..
  /// in the periodic sense.
  ///
  /// \param[in] c  cellid of coface
  /// \param[in] f  cellid of face
  /// \tparam    Nc number of vertices in vertex set of c(dim(c) + 1)
    /// \returns   an Nc-vec of pairs of vertex ids and domain ids
  template <std::size_t Nc> typename
  pcell_t<Nc>::type align_cwrtf(cellid_t c,cellid_t f) const;


protected:
  /// \privatesection
  tet_pdomain_list_t m_ptets;

};
}
/*****************************************************************************/



/*****************************************************************************/
namespace tet
{
/// \brief A class to represent a periodic triangulation along with geometry.
class ptet_geom_cc_t:virtual public ptet_cc_t,virtual public base_cc_geom_t
{
public:
  /// \brief Constructor
  ptet_geom_cc_t(const tet_list_t &tets,const tet_pdomain_list_t &ptets,
                 const vert_list_t &verts,const vert_t &dom_sz);

  /// \brief Construct from binary stream. Assumes internal format written by save.
  ptet_geom_cc_t(std::istream &is);

  /// \brief Save to a binary stream. Uses the internal binary representation.
  void save(std::ostream &os) const;

  /// \brief Get position of the given periodic vertex
  inline vert_t get_vert(pcellid_t pc) const;

  /// \brief Get position of the given vertex
  inline vert_t get_vert(cellid_t pc) const;

  /// \brief Get the centriod of a periodic cell with vertex set size N
  template<std::size_t N>
  inline vert_t get_centroid(typename pcell_t<N>::type pc) const;

  /// \brief Get a centriod of a cell with vertex set size N
  template<std::size_t N>
  inline vert_t get_centroid(cellid_t c) const;

  /// \brief Get the N dimensional volume of the given cell.
  template<eGDIR DIR,std::size_t N>
  double        get_cell_size(cellid_t c) const;


public:
  vert_t get_vertex(cellid_t c) const;

protected:
  /// \privatesection
  vert_list_t m_verts;
  vert_t      m_dom_sz;

  double      m_min_edge_length;
  double      m_max_edge_length;
};


void read_ts(std::string f,tet_cc_t::tet_list_t *tets,
             int compno=-1,fn_list_t *fns=NULL,
             vert_list_t *verts=NULL,
             ptet_cc_t::tet_pdomain_list_t *ptets=NULL,vert_t *sz=NULL);


}
/*****************************************************************************/
#include <tet_cc_inl.hpp>

#endif
