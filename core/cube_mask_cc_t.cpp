#include "cube_mask_cc_t.h"

using namespace tet;

using namespace std;

//-=-=-=-=-=-=-=-=-=-=-=-=--=-=- -- is at boundry or not -- -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=/

int cube_mask_cc_t::is_bnd(cube_cc_t::cellid_t c)  const

{

    int dim = get_dim(c);

    if( dim == 3) return false;


    int cts[200];

    int test[200];

    int ctss = cube_mask_cc_t::get_cofs(c, cts);


    if(dim == 0 )

    {

        if(ctss == 6)

        {

            for(int n = 0 ; n < ctss; n++)

            {

                if(is_bnd(cts[n]))
                    return true;

            }

            return false;

        }

        return true;

    }


    else if(dim == 1)

    {

        if(ctss == 4)

        {

            for(int k = 0 ; k < ctss ; k++)

            {

                int tess = cube_mask_cc_t::get_cofs(cts[k],test);

                if( tess != 2 )
                    return true;

            }

            return false;

        }

        return true;

    }


    else

    {

        if(ctss != 2)
            return true;

        return false;

    }


    ASSERT(true);

    return false;

}


//-=-=-==-=-=-=-=-=-=-=-=-=- -- get me actual verticies -- -=-=-=-=-=-=-=-=-=-=-=-=-=-//

int cube_mask_cc_t::get_vrts(cellid_t c,cellid_t* v ) const

{

    int store[9];

    int pos = 0;

    int num = cube_cc_t::get_vrts(to_id_old(c),store);

    for(int i = 0 ; i < num ; i++)

    {

        if(to_id_new(store[i]) >= 0)

        {

            v[pos++] = to_id_new(store[i]);

        }

    }

    return(pos);

}


//-=-=-=-=-=-=-=-=-=-=-=-=- -- get me actual cofacets -- -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

int cube_mask_cc_t::get_cofs(cellid_t c,cellid_t* f )  const

{

    int posi = 0;

    int  cofs[80];

    int t = to_id_old(c);


    int cofs_ct = cube_cc_t::get_cofs( to_id_old(c),cofs);


    for(int i=0; i < cofs_ct ; i++)

    {

        if(to_id_new(cofs[i]) >= 0)

        {

            f[posi++]= to_id_new( cofs[i]);

        }

    }

    return(posi);

}


//-=-==-=-=-=-=-=-=-=-=-=-=--=-=- -- get me actual facets -- -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//

int cube_mask_cc_t::get_fcts(cellid_t c,cellid_t* f )  const

{

    int pos=0;

    int fcts[8];

    int fcts_ct = cube_cc_t::get_fcts( to_id_old(c),fcts);


    for(int i=0; i < fcts_ct; i++)

    {

        if(to_id_new(fcts[i]) >= 0)

        {

            f[pos++]= to_id_new(fcts[i]);

        }

    }

    return(pos);

}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- -- get me to new id -- -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=//

tet::cube_cc_t::cellid_t cube_mask_cc_t::to_id_new(cellid_t id_old)  const

{

    if(old_to_new_ids[id_old]<0)

    {

        return(-100);

    }

    else

    {

        return(old_to_new_ids[id_old]);

    }

}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- -- get me to old id -- =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-//

tet::cube_cc_t::cellid_t cube_mask_cc_t::to_id_old(cellid_t id_new)  const

{

    return(new_to_old_ids[id_new]);

}

//=-=-=-=-=-=-=---------=-=-=-=-= get opposite cells =-=-=-=-=---=-=--=-==-=-=-=-=-=-=-=-=-=/

cellid_t cube_mask_cc_t::get_opp_cell(cellid_t c,cellid_t cf) const
{
    cellid_t l = cube_cc_t::get_opp_cell(cube_mask_cc_t::to_id_old(c),cube_mask_cc_t::to_id_old(cf));
    return cube_mask_cc_t::to_id_new(l);
}


//-=-=-==-=-=-=-=-=-=-=-=-=- -- get me actual vertex -- -=-=-=-=-=-=-=-=-=-=-=-=-=-//

tet::cube_cc_t::vert_t   cube_mask_cc_t::get_vertex(cube_cc_t::cellid_t c)   const
{
    vert_t temp;
    temp = cube_cc_t::get_vertex(to_id_old(c));
    return temp;

}

//-=-=-==-=-=-=-=-=-=-=-=-=- -- get me actual centroid -- -=-=-=-=-=-=-=-=-=-=-=-=-=-//

cube_cc_t::vert_t   cube_mask_cc_t::get_centroid(cube_cc_t::cellid_t c)   const
{
    vert_t tem;
    tem = cube_cc_t::get_centroid(to_id_old(c));
    return tem;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- -- masking of the given data set -- -=-=-=-=-=-=-=-=-=-=---=-=-=-=-=-//


bool cube_mask_cc_t::init(const vector<char> &mask,int x,int y,int z)

{

    //initialization of 3-D grid

    cube_cc_t::init(x,y,z);


    // a set contaning all the invalid cube ids

    std::set<int> invalid_cube_id_set;


    // marking invalid cells

    for(int s =0 ; s<= coords_to_cellid(m_cell_rects[0].uc);s++)

    {

        if (mask[s] < 0)

        {

            int edges[60];

            int faces[40];

            int cubes[20];


            int edge_ct = cube_cc_t::get_cofs(s,edges);

            for(int i = 0 ; i < edge_ct; ++i)

            {

                int face_ct = cube_cc_t::get_cofs(edges[i],faces);

                for(int jq = 0; jq < face_ct; jq++)

                {

                    int cube_ct = cube_cc_t::get_cofs(faces[jq],cubes);

                    for(int k = 0 ; k < cube_ct; ++k)

                        invalid_cube_id_set.insert(cubes[k]);

                }

            }

        }

    }


    //defining size of old_to_new_ids and filling it with default value -2

    old_to_new_ids.resize(coords_to_cellid(m_cell_rects[7].uc)+1,-2);


    int wbeg = coords_to_cellid(m_cell_rects[7].lc);

    int wend = coords_to_cellid(m_cell_rects[7].uc);



    // marking the valid cells

    for( int w = wbeg ; w <= wend; ++w)

    {

        if (invalid_cube_id_set.count(w) == 0)

        {

            int fac[60];

            int edg[40];

            int ver[20];


            old_to_new_ids[w] = 1;

            coords_t crd = cube_cc_t::cellid_to_coords(w);

            int pos = 0;

            for(int d = 0; d< 3; ++d)

                for(int i = 0 ; i < (crd[d]&1);++i)

                    for(int j = -1 ; j <=1; j+=2)

                    {

                        coords_t f_crd = crd;

                        f_crd[d]      += j;

                        fac[pos++]       = cube_cc_t::coords_to_cellid(f_crd);

                    }


            for(int i=0; i< pos ; i++)

            {

                old_to_new_ids[fac[i]] = 1;

                coords_t crd1 = cube_cc_t::cellid_to_coords(fac[i]);

                int edgect = 0;

                for(int d = 0; d< 3; ++d)

                    for(int i = 0 ; i < (crd1[d]&1);++i)

                        for(int j = -1 ; j <=1; j+=2)

                        {

                            coords_t f_crd = crd1;

                            f_crd[d]      += j;

                            edg[edgect++]       = cube_cc_t::coords_to_cellid(f_crd);

                        }


                for(int j = 0 ; j< edgect; j++)

                {

                    old_to_new_ids[edg[j]] = 1;

                    coords_t crd2 = cube_cc_t::cellid_to_coords(edg[j]);

                    int vertct = 0;

                    for(int d = 0; d< 3; ++d)

                        for(int i = 0 ; i < (crd2[d]&1);++i)

                            for(int j = -1 ; j <=1; j+=2)

                            {

                                coords_t f_crd = crd2;

                                f_crd[d]      += j;

                                ver[vertct++]       = cube_cc_t::coords_to_cellid(f_crd);

                            }


                    for(int k=0; k< vertct; k++)

                    {

                        old_to_new_ids[ver[k]] = 1;

                    }

                }

            }

        }

    }


    int idcounter = 0;

    int ccounts[4] = {0,0,0,0};


    //defining old_to_new_ids

    for(int i = 0 ; i < old_to_new_ids.size() ;++i)

    {
        if(old_to_new_ids[i] == 1)

        {
            old_to_new_ids[i] = idcounter++;

            ccounts[base_cc_t::get_dim(i)]++;

            new_to_old_ids.push_back(i);

        }

    }

    //defining init

    base_cc_t::init(ccounts[0],ccounts[1],ccounts[2],ccounts[3]);

    //making a multiset contaning the facet of boundry faces

    multiset<int> boundry_edges;

    boost::iterator_range<iterator> cell_rng = this-> cel_rng(2);

    iterator vbeg = boost::begin(cell_rng);

    iterator vend = boost::end(cell_rng);

    for(; vbeg != vend ; vbeg++)

    {

        int t =  *vbeg;

        if(is_bnd(t))

        {

            int fac[6];

            int facct = get_fcts(*vbeg,fac);

            for(int i = 0 ; i < facct ; i++)

                boundry_edges.insert(fac[i]);

        }

    }


    // checking for the invalid case

    //    for(multiset<int>::iterator t = boundry_edges.begin(); t != boundry_edges.end(); t++)
    //    {
    //        if( boundry_edges.count(*t) == 4)

    //        {
    //            cout<<"failed"<<endl;
    //            return false;

    //        }

    //    }

    return true;

}



void cube_mask_cc_t::read_fns_with_mask(fn_list_t &fns,std::string name)
{
    //reading values to fns
    ifstream ifs;
    ifs.open(name.c_str(),ios::in|ios::binary);
    ENSURE(ifs.is_open(),"Unable to open file");
    int count=0;
    for(int i =0; i<= cube_mask_cc_t::coords_to_cellid(m_cell_rects[0].uc);i++)
    {
        float data;
        ifs.read((char*)&data,4);
        if(i == cube_mask_cc_t::new_to_old_ids[count])
        {
            ENSURE(! isnan(float(data)),"error fns is taking random values")
                    fns.push_back(float(data));
            count++;
        }
    }

    ifs.close();
}


