#ifndef CUBE_MASK_CC_T_H
#define CUBE_MASK_CC_T_H

#include <set>

#include <iostream>

#include <sstream>

#include <fstream>

#include <string>

#include <cube_cc.hpp>
#include <tet.hpp>

namespace tet
{
class cube_mask_cc_t: public cube_cc_t

{

public:

    cube_cc_t::cellid_t to_id_new(cellid_t id_old)  const;

    cube_cc_t::cellid_t to_id_old(cellid_t id_new)  const;

    cellid_t get_opp_cell(cellid_t c,cellid_t cf) const;


    virtual int get_fcts(cellid_t c,cellid_t* f ) const;

    virtual int get_cofs(cellid_t c,cellid_t* f ) const;

    virtual int is_bnd  (cellid_t c)  const;

    virtual int get_vrts(cellid_t c,cellid_t* v ) const;

    virtual vert_t get_vertex(cube_cc_t::cellid_t c)   const;
    virtual vert_t get_centroid(cube_cc_t::cellid_t c)   const;

    bool init(const std::vector<char> &mask,int x, int y, int z);

    void read_fns_with_mask(fn_list_t &fns,std::string name);

    std::vector<cellid_t> new_to_old_ids;

    std::vector<cellid_t> old_to_new_ids;

};


}

#endif // CUBE_MASK_CC_T_H
