#ifndef VEC_HPP_INCLUDED
#define VEC_HPP_INCLUDED

#include <utl.hpp>

#include <cmath>
#include <complex>

namespace utl
{
template<typename T,std::size_t N>
struct vec_t
{
  T m_elems[N];

  vec_t(){}

  template<typename OT>
  vec_t(const vec_t<OT,N>& o)
  {std::copy(o.m_elems,o.m_elems+N,m_elems);}

  template<typename OT>
  inline vec_t& operator=(const vec_t<OT,N> &o)
  {
    std::copy(o.m_elems,o.m_elems+N,m_elems);
    return *this;
  }

  inline T& operator[](int i)
  {
    ASSERT(0 <= i && i <N);
    return m_elems[i];
  }

  inline const T& operator[](int i) const
  {
    ASSERT(0 <= i && i <N);
    return m_elems[i];
  }

  inline T * data(){return m_elems;}

  inline bool operator == (const vec_t<T,N> &r)
  {
    bool ret = true;
    for(int i = 0 ; i < N; ++i)
      ret &= m_elems[i] == r.m_elems[i];
    return ret;
  }

  inline bool operator != (const vec_t<T,N> &r)
  {return !(*this == r);}


  template <std::size_t HN> inline vec_t<T,HN>& head()
  {
    ASSERT(HN > 0);
    vec_t<T,HN> &ret = *(vec_t<T,HN>*)(void*)(this);
    return ret;
  }

  friend std::ostream &operator<< ( std::ostream &os, const vec_t &e )
  {
    os<<"("<<e.m_elems[0];

    for(unsigned int i = 1 ; i < N ; ++i)
      os<<","<<e.m_elems[i];

    os<<")";

    return os;
  }

  friend std::istream &operator>>( std::istream &is, vec_t &e )
  {
    char comma,bracket;

    is>>bracket>>e.m_elems[0];

    for(unsigned int i = 1 ; i < N ; ++i)
      is>>comma>>e.m_elems[i];

    is>>bracket;

    return is;
  }

  template<typename OT>
  inline vec_t & operator+=(const OT &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] += o;

    return *this;
  }

  template<typename OT>
  inline vec_t & operator+=(const vec_t<OT,N> &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] += o[i];

    return *this;
  }

  template<typename OT>
  inline vec_t & operator-=(const OT &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] -= o;

    return *this;
  }

  template<typename OT>
  inline vec_t & operator-=(const vec_t<OT,N> &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] -= o[i];

    return *this;
  }

  template<typename OT>
  inline vec_t & operator*=(const OT &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] *= o;

    return *this;
  }

  template<typename OT>
  inline vec_t & operator*=(const vec_t<OT,N> &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] *= o[i];

    return *this;
  }

  template<typename OT>
  inline vec_t & operator/=(const OT &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] /= o;

    return *this;
  }

  template<typename OT>
  inline vec_t & operator/=(const vec_t<OT,N> &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] /= o[i];

    return *this;
  }

  template<typename OT>
  inline vec_t & operator%=(const OT &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] %= o;

    return *this;
  }

  template<typename OT>
  inline vec_t & operator%=(const vec_t<OT,N> &o)
  {
    for(size_t i = 0 ; i < N;++i )
      (*this)[i] %= o[i];

    return *this;
  }

  // comparison
  template<typename OT>
  inline bool operator < (const vec_t<OT,N> &o) const
  {
    for(size_t i = 0 ; i < N ; ++i)
      if(m_elems[i] != o.m_elems[i])
        return m_elems[i] < o.m_elems[i];

    return false;
  }

  //norm2
  inline double norm2() const
  {
    double r = 0 ;

    for(size_t i = 0 ; i < N; ++i)
      r += m_elems[i]*m_elems[i];

    return std::real(std::sqrt<double>(r));
  }

  //norm2
  inline double norm2_sq() const
  {
    double r = 0 ;

    for(size_t i = 0 ; i < N; ++i)
      r += m_elems[i]*m_elems[i];

    return r;
  }


};

template<typename T>
vec_t<T,4> mk_vec(const T &a,const T &b,const T &c,const T &d)
{vec_t<T,4> r; r[0] =a; r[1] =b; r[2] =c;r[3]=d; return r; }

template<typename T>
vec_t<T,3> mk_vec(const T &a,const T &b,const T &c)
{vec_t<T,3> r; r[0] =a; r[1] =b; r[2] =c; return r; }

template<typename T>
vec_t<T,2> mk_vec(const T &a,const T &b)
{vec_t<T,2> r; r[0] =a; r[1] =b; return r;}

template<typename T>
vec_t<T,1> mk_vec(const T &a)
{vec_t<T,1> r; r[0] =a; return r;}

template<typename T,std::size_t N>
vec_t<T,N> mk_vec(const T &a)
{vec_t<T,N> r; for(int i = 0; i < N; ++i) r[i] = a; return r;}


template <std::size_t N>
struct ivec{typedef vec_t<int,N> type;};

typedef ivec<2>::type ivec2;
typedef ivec<3>::type ivec3;
typedef ivec<4>::type ivec4;

template <std::size_t N>
struct dvec{typedef vec_t<double,N> type;};

typedef dvec<2>::type dvec2;
typedef dvec<3>::type dvec3;
typedef dvec<4>::type dvec4;


typedef vec_t<unsigned int,2> uivec2;
typedef vec_t<unsigned int,3> uivec3;
typedef vec_t<unsigned int,4> uivec4;


template<size_t N,typename iter_t>
inline void ins_sort_static(iter_t it)
{
  for(int i = 0 ; i < N-1; ++i)
    for(int j = i+1; j < N; ++j)
      if(*(it+j) < *(it +i))
        std::swap(*(it+i),*(it+j));
}

template<size_t N,typename iter1_t,typename iter2_t>
inline bool lex_compare_static(iter1_t it1,iter2_t it2)
{
  for(int i = 0 ; i < N; ++i,++it1,++it2)
  {
    if(*it1 < *it2)
      return true;

    if(*it2 < *it1)
      return false;
  }

  return false;
}

// Exactly the same as vec_t ..except it is interpreted to be unique
// i.e  [1,2,3] and [3,1,2] are the same

template<typename T,std::size_t N>
struct uvec_t:public vec_t<T,N>
{
  uvec_t(){}

  template<typename OT>
  uvec_t(const uvec_t<OT,N>& o):vec_t<T,N>(o){}

  // comparison
  template<typename OT>
  inline bool operator < (const uvec_t<OT,N> &o) const
  {
    uvec_t<T,N> a = *this;
    uvec_t<T,N> b = o;

    ins_sort_static<N>(a.m_elems);
    ins_sort_static<N>(b.m_elems);

    return lex_compare_static<N>(a.m_elems,b.m_elems);
  }
};

template<typename T>
uvec_t<T,4> mk_uvec(const T &a,const T &b,const T &c,const T &d)
{uvec_t<T,4> r; r[0] =a; r[1] =b; r[2] =c;r[3]=d; return r; }

template<typename T>
uvec_t<T,3> mk_uvec(const T &a,const T &b,const T &c)
{uvec_t<T,3> r; r[0] =a; r[1] =b; r[2] =c; return r; }

template<typename T>
uvec_t<T,2> mk_uvec(const T &a,const T &b)
{uvec_t<T,2> r; r[0] =a; r[1] =b; return r;}

template<typename T>
uvec_t<T,1> mk_uvec(const T &a)
{uvec_t<T,1> r; r[0] =a; return r;}

template<typename T,std::size_t N>
uvec_t<T,N> mk_uvec(const T &a)
{uvec_t<T,N> r; for(int i = 0; i < N; ++i) r[i] = a; return r;}



/// Dot Product operator for vectors

template <typename T,std::size_t N>
T dot(const vec_t<T,N> &a,const vec_t<T,N> &b)
{
  T ret=0;

  for(int i = 0 ; i < N; ++i)
    ret += a[i]*b[i];

  return ret;
}

/// Cross Product operator for 3 dimensional vectors

template <typename T>
vec_t<T,3> cross(const vec_t<T,3> &a,const vec_t<T,3> &b)
{
  vec_t<T,3> ret;

  ret[0] = a[1]*b[2] - a[2]*b[1];
  ret[1] = a[2]*b[0] - a[0]*b[2];
  ret[2] = a[0]*b[1] - a[1]*b[0];

  return ret;
}


}

// addition
template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator+(const utl::vec_t<T,N> & v, const OT &s)
{
  return utl::vec_t<T,N>(v) += s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator+(const OT &s,const utl::vec_t<T,N> & v)
{
  return utl::vec_t<T,N>(v) += s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator+(const utl::vec_t<T,N> & v1,const utl::vec_t<OT,N> & v2)
{
  return utl::vec_t<T,N>(v1) += v2;
}

// multiplication
template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator*(const utl::vec_t<T,N> & v, const OT &s)
{
  return utl::vec_t<T,N>(v) *= s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator*(const OT &s,const utl::vec_t<T,N> & v)
{
  return utl::vec_t<T,N>(v) *= s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator*(const utl::vec_t<T,N> & v1,const utl::vec_t<OT,N> & v2)
{
  return utl::vec_t<T,N>(v1) *= v2;
}

// subtraction
template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator-(const utl::vec_t<T,N> & v, const OT &s)
{
  return utl::vec_t<T,N>(v) -= s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator-(const utl::vec_t<T,N> & v1,const utl::vec_t<OT,N> & v2)
{
  return utl::vec_t<T,N>(v1) -= v2;
}

// division
template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator/(const utl::vec_t<T,N> & v, const OT &s)
{
  return utl::vec_t<T,N>(v) /= s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator/(const utl::vec_t<T,N> & v1,const utl::vec_t<OT,N> & v2)
{
  return utl::vec_t<T,N>(v1) /= v2;
}

// modulus
template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator%(const utl::vec_t<T,N> & v, const OT &s)
{
  return utl::vec_t<T,N>(v) %= s;
}

template<typename T, std::size_t N,typename OT>
inline const utl::vec_t<T,N> operator%(const utl::vec_t<T,N> & v1,const utl::vec_t<OT,N> & v2)
{
  return utl::vec_t<T,N>(v1) %= v2;
}

#endif
